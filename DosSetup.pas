unit DosSetup;
{.$define NeedAuth}
{.$define test}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, CheckLst, StBase, StSpawn;

type
  TInstForm = class(TForm)
    Panel1: TPanel;
    CanBut: TButton;
    NextBut: TButton;
    BackBut: TButton;
    Label1: TLabel;
    CheckListBox1: TCheckListBox;
    DoRun: TStSpawnApplication;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label3: TLabel;
    procedure GoBack(Sender: TObject);
    procedure GoNext(Sender: TObject);
    procedure DoCancel(Sender: TObject);
    procedure Initialize(Sender: TObject);
    procedure CheckClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InstForm: TInstForm;

implementation
uses
    history,
    util2,
    StStrL,
    StUtils;

{$R *.DFM}

type
    TFileID = packed Record
      Name      : String[12];
      Size      : LongInt;
      Time      : LongInt;
      FileNo    : byte;
      VersNo    : String[8];
    end;

const
     BufferSize = 4 * 1024;
     InstallSize = 500000;
     StartOffset = 58000;
     Instate  : integer = 2;
     acbl_dir : String = '';
     {$ifdef NeedAuth}
     IsACBLscore : boolean = false;
     {$endif}
     SearchLen = 9;
     SearchString : PChar = 'erocsLBCA';
     CombFiles = 3;
     UpdNames : array[1..CombFiles] of String[9]
       = ('DISK1.EXE','DISK2.BIN','DISK3.BIN');
     ACBLvers  : String[8] = '';
     F1Offset  : integer = 0;

var
   CurOffSet    : Longint;
   FileID      : TFileID;
   inlen        : integer;
   Source_File  : String;
   load_path    : String;
   Diskdone         : Array[1..CombFiles] of boolean;


function ICopyFile(const SrcPath, DestPath : string;
         const Offset,IFSize,FTimeStamp: LongInt; const Append: boolean) : integer;
  {-Copy the file specified by SrcPath into DestPath. DestPath must specify
    a complete filename, it may not be the name of a directory without the
    file portion.  This a low level routine, and the input pathnames are not
    checked for validity.}
const
  BufferSize = 4 * 1024;

var
  BytesRead, BytesWritten : LongInt;
  FileDate : LongInt;
  Src, Dest, {Mode, }SaveFAttr : Integer;
  Buffer : Array[1..BufferSize] of byte;
  TotalBytesRead   : longint;
  FSize         : longint;
  BytesToRead   : longint;
  SearchRec     : TSearchRec;

label
     exitpoint;
begin
  Src := 0;
  Dest := 0;
  if SameFile(SrcPath,DestPath,FSize) then begin
    Result := 0;
    exit;
  end;
  Result := 1;
  {try}
    Src := FindFirst(SrcPath,faAnyFile,SearchRec);
    if IFSize > 0 then FSize := IFSize
    else FSize := SearchRec.Size;
    FindClose(SearchRec);
    if Src <> 0 then exit;
    TotalBytesRead := 0;
    {Mode := FileMode and $F0;}
    SaveFAttr := FileGetAttr(SrcPath);
    if SaveFAttr < 0 then exit;
    Src := FileOpen(SrcPath, fmOpenRead);
    if Src < 0 then begin
      Result := 1;                     {unable to open SrcPath}
      goto exitpoint;
    end;
    if OffSet > 0 then FileSeek(Src,OffSet,0);
    Dest := -1;
    if Append then begin
      Dest := FileOpen(DestPath,fmOpenReadWrite);
      if Dest >= 0 then FileSeek(Dest,0,2);
    end;
    if Dest < 0 then Dest := FileCreate(DestPath);
    if Dest < 0 then begin
      Result := 2;                     {unable to open DestPath}
      goto exitpoint;
    end;
    repeat
      if FSize-TotalBytesRead > BufferSize then BytesToRead := BufferSize
      else BytesToRead := FSize-TotalBytesRead;
      BytesRead := FileRead(Src, Buffer, BytesToRead);
      if (BytesRead = -1) then begin
        Result := 3;                   {error reading SrcPath}
        goto exitpoint;
      end;
      BytesWritten := FileWrite(Dest, Buffer, BytesRead);
      if (BytesWritten = -1) or
         (BytesWritten <> BytesRead) then begin
        Result := 4;                   {error reading SrcPath}
        goto exitpoint;
      end;
      Inc(TotalBytesRead,BytesRead);
      Application.ProcessMessages;
      if canceled then goto exitpoint;
    until (BytesRead < BufferSize) or (TotalBytesRead >= FSize);
    if FTimeStamp <> 0 then FileDate := FTimeStamp
    else FileDate := FileGetDate(Src);
    if FileDate = -1 then begin
      Result := 5;                     {error getting SrcPath's Date/Time}
      goto exitpoint;
    end;
    FileSetDate(Dest, FileDate);
    FileSetAttr(DestPath, SaveFAttr);
    Result := 0;
exitpoint:
  {finally}
    if Src > 0 then FileClose(Src);
    if Dest > 0 then begin
      FileClose(Dest);
      if Result <> 0 then SysUtils.DeleteFile(DestPath);
    end;
    if Result <> 0 then canceled := true;
  {end;}
end;

function TestInstallFileName(const FNo: byte; const TryName: String): boolean;
var
   F    : integer;
   FileId : TFileid;
   Search_rec   : TSearchRec;
   found        : boolean;
begin
  found := FindFirst(TryName,faArchive,Search_rec) = 0;
  FindClose(Search_rec);
  if found then begin
    F := FileOpen(TryName,fmOpenRead);
    if F >= 0 then with Fileid do begin
      FileRead(F,FileId,SizeOf(FileId));
      FileClose(F);
      found := (FileNo = Fno) and (Fileid.Size = Search_rec.Size);
    end;
  end;
  Result := found;
end;

procedure SetLabel1(const Name: String);
begin
  if Length(Name) > 0 then InstForm.Label1.Caption := 'Copying '+Name
  else InstForm.Label1.Caption := '';
  Application.ProcessMessages;
end;

procedure ExtractInstall(const TargetDir: string);
var
   F      : integer;
begin
  inlen := 1;
  while inlen > 0 do begin
    F := FileOpen(Source_file,fmOpenRead);
    FileSeek(F,CurOffset,0);
    inlen := FileRead(F,FileID,SizeOf(TFileID));
    FileClose(F);
    if inlen > 0 then begin
      Inc(CurOffSet,SizeOf(TFileID));
      with FileID do begin
        SetLabel1(Name);
        Icopyfile(Source_file,TargetDir+Name,CurOffSet,Size,Time,false);
        SetLabel1('');
      end;
      Inc(CurOffSet,FileID.Size);
    end;
    if canceled then exit;
  end;
end;

procedure ReadFileID(var FileID: TFileID);
var
   F  : integer;
begin
  F := FileOpen(Source_file,fmOpenRead);
  FileSeek(F,CurOffset,0);
  inlen := FileRead(F,FileID,SizeOf(TFileID));
  FileClose(F);
  Inc(CurOffSet,SizeOf(TFileID));
end;

function SetF1Offset: boolean;
var
   Buffer : Array[1..BufferSize] of byte;
   SearchOff    : cardinal;
   F            : integer;
begin
  Source_file := load_path+UpdNames[1];
  Result := false;
  F := FileOpen(Source_file,fmOpenRead);
  if F < 0 then exit;
  CurOffSet := StartOffset;
  FileSeek(F,CurOffSet,0);
  Result := false;
  inlen := 1;
  WHILE NOT Result and (inlen > 0) and (CurOffSet < InstallSize) DO BEGIN
    FileSeek(F,CurOffSet,0);
    inlen := FileRead(F,Buffer,BufferSize);
    IF inlen > 0 THEN begin
      Result := Search(Buffer,inlen,SearchString^,SearchLen,SearchOff);
      {$ifdef showmem}
      ErrBox('Offset:'+Long2Str(CurOffSet)+', inlen:'+Long2str(inlen)
        +', Search:'+Long2Str(SearchOff)+', done:'+Long2Str(Byte(done)),BC,0);
      {$endif}
      if not Result then Inc(CurOffSet,inlen-SearchLen);
    end;
  END;
  FileClose(F);
  if not Result then exit;
  Inc(CurOffSet,SearchOff+SearchLen);
  ReadFileID(FileID);
  ACBLvers := FileID.VersNo;
end;

procedure ExtractInstallFiles(const j: integer; const TargetDir: string);
begin
  case j of
    1: begin
      if not SetF1Offset then exit;
      ReadFileID(FileID);
      with FileID do begin
        SetLabel1(Name);
        Icopyfile(Source_File,TargetDir+Name,0,Size,Time,false);
        SetLabel1('');
      end;
      ExtractInstall(TargetDir);
    end;
    2,3: begin
      Source_File := load_path+UpdNames[j];
      if not FileExists(Source_file) then exit;
      CurOffSet := 0;
      ReadFileID(FileID);
      ExtractInstall(TargetDir);
    end;
  end;
end;

function SetACBLscoreDir: boolean;
var
   xdrive : char;

function TestACBLscore(const drive: char): boolean;
begin
  acbl_dir := drive+':\ACBLSCOR';
  Result := IsDirectory(acbl_dir);
  acbl_dir := acbl_dir+'\';
  Result := Result and FileExists(acbl_dir+'SCORE.OVR');
  Result := Result and FileExists(acbl_dir+'ACBL3.EXE');
end;

begin
  Result := false;
  {$ifdef test}
  for xdrive := 'C' to 'D' do Result := Result or TestACBLscore(xdrive);
  {$else}
  for xdrive := 'C' to 'Z' do Result := Result or TestACBLscore(xdrive);
  {$endif}
  {$ifdef NeedAuth}
  IsACBLscore := Result;
  if Result then Instate := 2
  else begin
    Instate := 0;
    acbl_dir := '';
  end;
  {$else}
  Instate := 2;
  if not Result then acbl_dir := '';
  {$endif}
end;

procedure TestRequiredFiles;
var
   j      : integer;
   FileID : TFileId;
begin
  for j := 2 to CombFiles do begin
    Source_File := load_path+UpdNames[j];
    if not FileExists(Source_file) then begin
      MessageDlg('File '+load_path+UpdNames[j]+' not found.',mtError,[mbOK],0);
      Application.Terminate;
      exit;
    end;
    CurOffSet := 0;
    ReadFileID(FileId);
    with FileID do if ACBLvers <> VersNo then begin
      MessageDlg('File '+Source_file+' is invalid.',mtError,[mbOk],0);
      Application.Terminate;
      exit;
    end;
  end;
end;

procedure SetIState;
const
     InstateOff = 3;

function CopyDiskFiles(const diskno: integer): boolean;

procedure SetButtons;
begin
  with InstForm do begin
    if diskno < Combfiles then Backbut.Show;
    Nextbut.Show;
    Nextbut.SetFocus;
  end;
end;

var
   SearchRec  : TSearchRec;
   FindErr    : integer;
   CurADir    : String;
label
     FindAgain;
begin
  if Diskdone[diskno] then begin
    Instate := diskno+InstateOff;
    SetButtons;
    Result := true;
    exit;
  end;
  Result := false;
  with InstForm do begin
    Backbut.Hide;
    Nextbut.Hide;
  end;
  if not IsDirectory('A:\') then begin
    Beep;
    MessageDlg('Floppy drive A: does not have a diskette.',mtError,[mbOk],0);
    Instate := diskno+InstateOff;
    SetButtons;
    exit;
  end;
  FindErr := DiskSize(1);
  if FindErr < 1400000 then begin
    Beep;
    MessageDlg('Floppy drive A: is the wrong density.'#13
               +'A 3-1/2" 1.4Meg disk is required.',mtError,[mbOk],0);
    Instate := diskno+InstateOff;
    SetButtons;
    exit;
  end;
  FindErr := FindFirst('A:\*.*',faAnyFile,SearchRec);
  FindClose(SearchRec);
  if FindErr = 0 then begin
    Beep;
    if MessageDlg('Floppy drive A: is not empty.  Do you want to'#13
      +'delete ALL files from diskette and continue?',
      mtError,[mbYes,mbNo],0) = mrNo then begin
      Instate := diskno+InstateOff;
      SetButtons;
      exit;
    end;
    CurADir := 'A:\';
    while not canceled and (FindErr = 0) do with InstForm,SearchRec do begin
      if (Attr and faDirectory) <> 0 then CurADir := fixpath(CurADir+Name)
      else begin
        FileSetAttr(CurADir+Name,faArchive);
        Label1.Caption := 'Deleting file: '+CurADir+Name;
        Application.ProcessMessages;
        if not DeleteFile(CurADir+Name) then begin
          MessageDlg('Cannot delete '+CurADir+Name,mtError,[mbOK],0);
          Instate := diskno+InstateOff;
          SetButtons;
          exit;
        end;
        Label1.Caption := '';
      end;
FindAgain:
      FindErr := FindFirst(CurADir+'*.*',faAnyFile,SearchRec);
      while (FindErr = 0) and ((Name = '.') or (Name = '..')) do
        FindErr := FindNext(SearchRec);
      FindClose(SearchRec);
      if (FindErr <> 0) and (Length(CurADir) > 3) then begin
        CurADir := JustPathNameL(CurADir);
        FileSetAttr(CurADir,faDirectory+faArchive);
        Label1.Caption := 'Deleting folder: '+CurADir;
        Application.ProcessMessages;
        if not RemoveDir(CurADir) then begin
          MessageDlg('Cannot delete directory '+CurADir,mtError,[mbOK],0);
          Instate := diskno+InstateOff;
          SetButtons;
          exit;
        end;
        Label1.Caption := '';
        CurADir := fixpath(JustPathNameL(CurADir));
        goto FindAgain;
      end;
    end;
  end;
  if canceled then exit;
  {$ifdef NeedAuth}
  if IsACBLscore then begin
  {$endif}
    ICopyFile(acbl_dir+'DISK','A:\DISK'+Long2StrL(diskno),0,0,0,false);
    ExtractInstallFiles(diskno,'A:\');
  {$ifdef NeedAuth}
  end
  else begin
    case diskno of
      1: begin
        SetLabel1('INSTALL.EXE');
        IcopyFile(load_path+UpdNames[diskno],'A:\INSTALL.EXE',0,0,0,false);
      end;
      2,3: begin
        SetLabel1(UpdNames[diskno]);
        IcopyFile(load_path+UpdNames[diskno],'A:\'+UpdNames[diskno],0,0,0,false);
      end;
    end;
    SetLabel1('');
  end;
  {$endif}
  SetButtons;
  Diskdone[diskno] := true;
  Result := true;
end;

var
   F     : integer;
begin
  if canceled then exit;
  with InstForm do case Instate of
    0: begin
      Label1.Caption := 'An older version of ACBLscore was not found on this computer.'#13#13
                       +'Choose an option from below and click Next to continue.';
      RadioButton1.Caption := 'Create ACBLscore disks to update another computer';
      RadioButton2.Caption := 'I have an Authorization code from ACBL.  This will';
      Label2.Caption := 'allow me to install ACBLscore on this computer or'#13
                       +'create full install disks for any other computer.'#13
                       +#13'To obtain an authorization code email alicia.vangunda@acbl.org'#13
                       +'with your club number, or phone 901-332-5586 ext 1258';
      RadioButton1.Show;
      RadioButton2.Show;
      Label2.Show;
      BackBut.Hide;
    end;
    1: begin
      Label1.Caption := 'Enter your club number and ACBL authorization code below'#13#13
                       +'Then click Next to continue.';
      Label2.Caption := 'Enter Authorization code here --->';
      Label2.Show;
      Label3.Caption := 'Enter Club number here ---->';
      Label3.Show;
      Edit1.Show;
      Edit2.Show;
      BackBut.Show;
      Edit1.SetFocus;
    end;
    2: begin
      {$ifdef NeedAuth}
      if IsACBLscore then begin
      {$endif}
        BackBut.Hide;
        Label1.Caption := 'This procedure will do any of the following:'#13#13
          +'1.  Create diskettes to install ACBLscore '+ACBLvers+#13
          +'    on any other computer.'#13#13
          +'2.  Begin the installation of ACBLscore '+ACBLvers+#13
          +'    on this computer.'#13#13
          +'Click on Next to continue.';
      {$ifdef NeedAuth}
      end
      else begin
        BackBut.Show;
        Label1.Caption := 'This procedure will create diskettes to update'#13
          +'ACBLscore to version '+ACBLvers+' on another computer.'#13#13
          +'Three diskettes are required.'#13#13
          +'Click on Next to continue.';
      end;
      {$endif}
    end;
    3: begin
      BackBut.Show;
      CheckListBox1.Show;
      Label1.Caption := 'Check what you want to do below.'#13#13
        +'If "Create ACBLscore install diskettes" is checked,'#13
        +'3 empty 3-1/2" floppy diskettes are required.';
      CheckClick(InstForm);
    end;
    4: begin
      BackBut.Show;
      Nextbut.Show;
      Nextbut.SetFocus;
      Label1.Caption := '3 empty diskettes are required.'#13#13
        +'Insert empty floppy disk 1 and click on Next to continue.';
      {$ifdef NeedAuth}
      if IsACBLscore then begin
      {$endif}
        if FileExists(acbl_dir+'DISK') then DeleteFile(acbl_dir+'DISK');
        F := FileCreate(acbl_dir+'DISK');
        FileWrite(F,ACBLvers[1],4);
        FileClose(F);
      {$ifdef NeedAuth}
      end;
      {$endif}
      FillChar(Diskdone,CombFiles,0);
    end;
    5,6: begin
      if not CopyDiskFiles(Instate-4) then exit;
      Label1.Caption := 'Insert empty floppy disk '+Long2StrL(Instate-3)
        +' and click on Next to continue.';
    end;
    7: begin
      if not CopyDiskFiles(3) then exit;
      {$ifdef NeedAuth}
      if IsACBLscore then
      {$endif}
        Label1.Caption :=
        'Full installation disks for ACBLscore '+ACBLvers+#13
        +'have been created.  Take the 3 disks to any computer,'#13
        +'insert disk 1 and enter A:\INSTALL at the DOS prompt,'#13
        +'or if using Windows: Click on Start, then Run and'#13
        +'enter A:\INSTALL and click on OK.'
      {$ifdef NeedAuth}
      else Label1.Caption :=
        'Update disks for ACBLscore '+ACBLvers+' have been created.'#13
        +'Take the 3 disks to any computer that has ACBLscore,'#13
        +'insert disk 1 and enter A:\INSTALL at the DOS prompt,'#13
        +'or if using Windows: Click on Start, then Run and'#13
        +'enter A:\INSTALL and click on OK.'
      {$endif};
      if CheckListBox1.State[1] = cbUnchecked then begin
        Nextbut.Caption := '&Finish';
        Canbut.Hide;
      end;
    end;
    8: begin
      Label1.Caption := 'Click on Next to install ACBLscore version '+ACBLvers;
    end;
    9: begin
      Backbut.Hide;
      Nextbut.Hide;
      if Length(acbl_dir) < 1 then begin
        acbl_dir := 'C:\ACBLSCOR';
        if not IsDirectory(acbl_dir) then CreateDir(acbl_dir);
        acbl_dir := acbl_dir+'\';
      end;
      for F := 1 to CombFiles do ExtractInstallFiles(F,acbl_dir);
      with DoRun do begin
        DefaultDir := JustPathNameL(acbl_dir);
        FileName := acbl_dir+'INSTALL.EXE';
        NotifyWhenDone := false;
        RunParameters := '';
        Execute;
      end;
      Application.Terminate;
    end;
  end;
end;

procedure InitCheckListBox;
begin
  with InstForm.CheckListBox1 do begin
    Items.Clear;
    Items.Add('Create ACBLscore install diskettes');
    Items.Add('Install ACBLscore on this computer');
    {$ifdef NeedAuth}
    if IsACBLscore then begin
    {$endif}
      State[0] := cbUnChecked;
      State[1] := cbChecked;
    {$ifdef NeedAuth}
    end
    else begin
      State[0] := cbChecked;
      State[1] := cbUnChecked;
    end;
    {$endif}
    Hide;
  end;
end;

procedure CheckState;
begin
  with InstForm do case Instate of
    0: begin
      RadioButton1.Hide;
      RadioButton2.Hide;
      Label2.Hide;
    end;
    1: begin
      Edit1.Hide;
      Edit2.Hide;
      Label3.Hide;
      Label2.Hide;
    end;
    3: begin
      CheckListBox1.Hide;
      Nextbut.Enabled := true;
    end;
  end;
end;

procedure TInstForm.GoBack(Sender: TObject);
begin
  if Instate > 0 then begin
    CheckState;
    Dec(Instate);
    case Instate of
      1: Dec(Instate);
      {$ifdef NeedAuth}
      3: if not IsACBLscore then Dec(Instate);
      4,5,6: if IsACBLscore then Instate := 3
      else Instate := 2;
      {$else}
      4,5,6: Instate := 3;
      {$endif}
      7,9: Instate := 2;
    end;
    SetIState;
  end;
end;

procedure TInstForm.GoNext(Sender: TObject);
begin
  if Instate < 9 then begin
    {$ifdef NeedAuth}
    if Instate = 1 then begin
      IsACBLscore := GetClubAuthCode(Edit1.Text) = Edit2.Text;
      if not IsACBLscore then begin
        MessageDlg('Invalid authorization code: '+Edit2.Text,mtError,[mbOK],0);
        SetIState;
        exit;
      end;
      InitCheckListBox;
    end;
    {$endif}
    CheckState;
    Inc(Instate);
    case Instate of
      1: if RadioButton1.Checked then Inc(Instate);
      {$ifdef NeedAuth}
      3: if not IsACBLscore then Inc(Instate);
      {$endif}
      4: with CheckListBox1 do if State[0] = cbUnchecked then begin
           if State[1] = cbUnchecked then Instate := 10
           else Instate := 8;
         end;
      8: with CheckListBox1 do if State[1] = cbUnchecked then Application.Terminate;
    end;
    SetIState;
  end
  else Application.Terminate;
end;

procedure TInstForm.DoCancel(Sender: TObject);
begin
  canceled := true;
  Application.Terminate;
end;

procedure TInstForm.Initialize(Sender: TObject);
const
     Sys32folder = 'c:\windows\system32';
     autoexecf = 'autoexec.nt';
     configf = 'config.nt';
begin
  Panel1.Width := Width-20;
  Panel1.Height := Height-80;
  CheckListBox1.Width := Panel1.Width-20;
  Backbut.Top := Height-60;
  Nextbut.Top := Height-60;
  Canbut.Top := Height-60;
  RadioButton1.Hide;
  RadioButton2.Hide;
  Label2.Hide;
  Label3.Hide;
  Edit1.Text := '';
  Edit1.Hide;
  Edit2.Text := '';
  Edit2.Hide;
  load_path := fixpath(JustPathNameL(ParamStr(0)));
  SetF1Offset;
  TestRequiredFiles;
  if Application.Terminated then exit;
  SetACBLscoreDir;
  RadioButton1.Checked := true;
  InitCheckListBox;
  SetIState;
  if IsDirectory(Sys32folder) then begin
    if not FileExists(Sys32folder+'\'+autoexecf) then
      IcopyFile(load_path+autoexecf,Sys32folder+'\'+autoexecf,0,0,0,false);
    if not FileExists(Sys32folder+'\'+configf) then
      IcopyFile(load_path+configf,Sys32folder+'\'+configf,0,0,0,false);
  end;
end;

procedure TInstForm.CheckClick(Sender: TObject);
begin
  with CheckListBox1 do if (State[0] = cbChecked) or (State[1] = cbChecked) then
    Nextbut.Enabled := true
    else Nextbut.Enabled := false;
end;

end.
