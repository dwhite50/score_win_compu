Unit pvio;
{video i/o, general support routines}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TStringProc = Procedure(const St: String);
  TWaitForm = class(TForm)
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


const
     StrBoxField : byte = 0;
     {StrBox field line 0 = last line}

     BoxTitle : String[80] = '';
     {title of message box}

     KHelpTopic : Word = 0;
     {current help topic number}

     EnableHelp : boolean = false;

     {screen location equates}
     TR = 1;          {top right}
     TL = 2;          {top left}
     TC = 3;          {top center}
     MR = 4;          {middle right}
     ML = 5;          {middle left}
     MC = 6;          {middle center}
     BR = 7;          {bottom right}
     BL = 8;          {bottom left}
     BC = 9;          {bottom center}

{const
     BarLine1Loc        = 2;
     BarLIne2Loc        = 4;
     BarLine3Loc        = 5;

{const
     PassField : boolean = false;
     {password field status}

procedure BoxMsg(const Title,Msg: String; const location: byte);
procedure EraseBoxWindow;
procedure setins(const on : boolean);
function getins : boolean;
function getabn : boolean;
procedure setabn(const on : boolean);
function askabn: boolean;
//procedure AnyKeyBox(const Location: shortint; color: byte);
procedure PlsWait;
//function yesnoBox(var esc: boolean; const yes: boolean; const prompt: String;
//         const topic: word{; const Location: shortint; const color: byte}) : boolean;
//procedure ErrBox(const Line1: String; {const Location: Shortint;} const topic: word);
//function ErrEscBox(const Line1: String{; const Location: Shortint}): boolean;
//procedure MsgBox(const Line: String; {const Attribute: byte;
//          const Modal : boolean; const Location: Shortint; const color: byte;}
//          const topic: word);
//function EscBox(const Line1: String{; const Location: Shortint}): boolean;
procedure RingBell(const loud: boolean);
procedure RingClear(const loud: boolean);
function SetFormLeft(const fwidth: integer; const location: byte): integer;
function SetFormTop(const fheight: integer; const location: byte): integer;

implementation

uses
    {Comctrls,}
    YesNoBoxU,
    StStrL,
    history,
    util1,
    csdef;

{$R *.DFM}

type
    PBoxWindow = ^TBoxWindow;
    TBoxWindow = packed record
      Next : PBoxWindow;
      WaitForm : TWaitForm;
    end;

const
     BoxWindowHead : PBoxWindow = nil;
     askesc   : boolean = false;
     insmode    : boolean = true;
     inslock    : boolean = false;
     LastSec  : Word = 0;
     WeekDays : Array[0..6] of String[3] =
              ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');

function SetFormLeft(const fwidth: integer; const location: byte): integer;
var
   scr3 : integer;
   scrmid : integer;
begin
  scr3 := Screen.Width div 3;
  scrmid := scr3 div 2;
  case location of
    TC,MC,BC: scrmid := scr3 + scrmid;
    TR,MR,BR: scrmid := scr3 + scr3 + scrmid;
  end;
  Result := scrmid - (fwidth div 2);
  if Result + fwidth > Screen.Width then Result := Screen.Width - fwidth;
  if Result < 0 then Result := 0;
end;

function SetFormTop(const fheight: integer; const location: byte): integer;
var
   scr3 : integer;
   scrmid : integer;
begin
  scr3 := Screen.Height div 3;
  scrmid := scr3 div 2;
  case location of
    MR,ML,MC: scrmid := scr3 + scrmid;
    BR,BL,BC: scrmid := scr3 + scr3 + scrmid;
  end;
  Result := scrmid - (fheight div 2);
  if Result + fheight > Screen.Height then Result := Screen.Height - fheight;
  if Result < 0 then Result := 0;
end;

procedure SetFormWidth(var WaitForm: TWaitForm; const title,prompt: String;
          const location: byte);
var
   MaxTextWidth        : integer;
   w     : integer;
   th    : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   cw    : integer;
begin
  with WaitForm do begin
    w := 1;
    repeat
      Line := ExtractWordL(w,xLine,#13);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
      end;
      if Length(Line) > 0 then Inc(w);
    until Length(Line) < 1;
    Dec(w);
  end;
end;

begin
  with WaitForm do begin
    Font.Size := ViewFontSize;
    th := Canvas.TextHeight(prompt);
    MaxTextWidth := 0;
    GetTextWidth(title);
    GetTextWidth(prompt);
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    Label1.Width := Width-20;
    Height := w * th + 124;
    Left := SetFormLeft(Width,location);
    Top := SetFormTop(Height,location);
    Caption := title;
    Label1.Caption := prompt;
    Show;
  end;
end;

procedure BoxMsg(const Title,Msg: String; const location: byte);
var
   tempBoxWin : PBoxWindow;
begin
//  if color > 0 then SetBackColor(color);
//  width := Bwidth + 3;
//  if length(BoxTitle) >= width then width := length(BoxTitle) + 1;
//  if width > 78 then width := 78;
//  Location := Abs(BoxLocation);
//  OnEdge := BoxLocation < 0;
  {case Location of
    TR,MR,BR: if OnEdge then XLow := Lo(WindMax) - width + 2
              else XLow := 78 - width;
    TL,ML,BL: if OnEdge then XLow := Lo(WindMin) + 1
              else XLow := 3;
    TC,MC,BC: if OnEdge then XLow := (Lo(WindMax) - Lo(WindMin) - width) div 2
              else XLow := (81 - width) div 2;
  end;
  case Location of
    TR,TL,TC: if OnEdge then YLow := Hi(WindMin) + 1
              else YLow := 2;
    MR,ML,MC: if OnEdge then YLow := (Hi(WindMax) - Hi(WindMin) - Height) div 2
              else YLow := (24 - Height) div 2;
    BR,BL,BC: if OnEdge then YLow := MinWord(Hi(WindMax),22) - Height + 2
              else YLow := 23 - Height;
  end;}
  FreeHintWin;
  New(tempBoxWin);
  tempBoxWin^.Next := BoxWindowHead;
  BoxWindowHead := tempBoxWin;
  with BoxWindowHead^ do begin
    Application.CreateForm(TWaitForm, WaitForm);
    SetFormWidth(WaitForm,Title,Msg,location);
  end;
  Screen.Cursor := crHourGlass;
  Application.ProcessMessages;
end;

procedure EraseBoxWindow;
var
   tempBoxWin : PBoxWindow;
begin
  if not Assigned(BoxWindowHead) then exit;
  tempBoxWin := BoxWindowHead;
  with BoxWindowHead^ do begin
    WaitForm.Close;
    WaitForm.Free;
    BoxWindowHead := Next;
  end;
  Dispose(tempBoxWin);
  Screen.Cursor := crDefault;
end;

procedure RingBell(const loud: boolean);
begin
  MessageBeep(mb_iconasterisk);
end;

procedure RingClear(const loud: boolean);
begin
  MessageBeep(mb_iconasterisk);
end;

{function yesnoBox(var esc: boolean; const yes: boolean; const prompt: String;
         const topic: word) : boolean;
var
   Flags : longint;

begin
  esc := false;
  Flags := MB_YESNOCANCEL;
  if not yes then Inc(Flags,MB_DEFBUTTON2);
  case Application.MessageBox(Pchar(prompt),'Click Yes or No',Flags) of
    IDNO: Result := false;
    IDYES: Result := true;
    IDCANCEL: begin
      esc := true;
      Result := false;
    end;
  end;
end;}

(*function ErrEscBox(const Line1: String{; const Location: Shortint}): boolean;
begin
  Result := Application.MessageBox(Pchar(Line1),'ACBLscore ERROR',
    MB_OKCANCEL) = IDCANCEL;
end;

function EscBox(const Line1: String{; const Location: Shortint}): boolean;
begin
  Result := Application.MessageBox(Pchar(Line1
    +#13'Press Enter (OK) or ESC (Cancel)'),'OK or Cancel',
    MB_OKCANCEL) = IDCANCEL;
end;

(*procedure ErrBox(const Line1: String; {const Location: Shortint;} const topic: word);
begin
  Application.MessageBox(Pchar(Line1),'ACBLscore ERROR',MB_ICONSTOP);
end;*)

(*procedure MsgBox(const Line: String; {const Attribute: byte;
          const Modal : boolean; const Location: Shortint; const color: byte;}
          const topic: word);
begin
  Application.MessageBox(PChar(Line),'Click on OK',MB_OK);
end;

procedure AnyKeyBox{(const Location: shortint; color: byte)};
begin
  Application.MessageBox('Press Enter or Click on OK to continue',
    'Click on OK',MB_OK);
end;*)

procedure setins(const on : boolean);
begin
  insmode := on;
end;

procedure setilk(const on : boolean);
begin
  inslock := on;
end;

function getins : boolean;
begin
  getins := insmode;
end;

procedure setabn(const on : boolean);
begin
  askesc:=on;
end;

function getabn : boolean;
begin
  getabn := askesc;
end;

function askabn: boolean;
var
   esc      : boolean;
begin
  askabn := true;
  if askesc then begin
    askesc := false;
    askabn:=YesNoBox(esc,false,'Abandon Changes',0,MC,nil);
    askesc := true;
  end;
end;

procedure PlsWait;
begin
  BoxMsg('Please Wait','Please Wait...',MC);
end;

end.
