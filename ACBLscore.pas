unit ACBLscore;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure testform(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    scoredef,
    {ListBox,}
    VListBox,
    StStrL,
    pvio,
    util2,
    util1;

{$R *.DFM}

const
   Testpickchoice : Array[1..5] of Pchar =
   (' 1 This is choice number 1',
    '   And this is the second choice',
    '^ The Third choice',
    '+ Choice number four .................4',
    ' ! the fifth choice');

var
   SelArray : Array[1..100] of boolean;

function sendtheitem(const Item:integer):String;
begin
  case Item of
    1..5: Result := Testpickchoice[Item];
    6: Result := Long2StrL(Item)+CharStrL('X',40)+' This is a long item';
    else Result := Long2StrL(Item)+' This is item '+Long2StrL(Item);
  end;
end;

function TestKey(const key: word; const item: integer): integer;
var
   j     : integer;
   test  : boolean;
begin
  case key of
    8: begin
      test := true;
      for j := 1 to 100 do if SelArray[j] then test := false;
      for j := 1 to 100 do SelArray[j] := test;
      Result := 2;
      setabn(true);
    end;
    9: Result := 3;
    32: begin
      SelArray[item] := not SelArray[item];
      Result := -1;
      setabn(true);
    end;
    else result := 0;
  end;
end;

procedure TestSelected(const item: integer; var Selected: boolean);
begin
  Selected := SelArray[item];
end;

procedure TForm1.testform(Sender: TObject);
var
   choice : smallint;
begin
  {choice := PickChoice(5,3,'make a selection',@Testpickchoice,false,0);
  MsgBox('The selected item is '+numcvt(choice,3,false),0);
  choice := LbrPickChoice(2,17,0,'from a library pick table','SSSET.001',false,0);
  MsgBox('The selected item is '+numcvt(choice,3,false),0);}
  choice := VPickChoice(27,2,'from a virtual list box','This is the header',
    false,sendtheitem,nil,nil,0);
  MsgBox('The selected item is '+numcvt(choice,3,false),0);
  {FillChar(selArray,SizeOf(SelArray),true);}
  choice := VPickChoice(8,4,'from a virtual list box','',false,sendtheitem,
    TestSelected,TestKey,0);
  MsgBox('The selected item is '+numcvt(choice,3,false),0);
  Application.Terminate;
end;

end.
