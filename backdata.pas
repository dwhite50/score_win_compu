{$I debugdir.inc}
unit backdata;

interface

uses
    SysUtils;

{$I dbnames.dcl}
{$I dbfiles.dcl}

var
   Escaped      : boolean;
   BackupFree   : Int64;
   BackupSize   : Int64;
   BackupName   : String[8];
   Hsize        : word;
   PickHSize    : word;
   PickChar     : word;
   Name       : String;
   Schar      : Char;
   Typ        : String;
   Key        : word;
   backfile   : TextFile;
   isbackup     : boolean;
   ifunc        : byte;
   len          : byte;
   SubstDr : char;
   ans        : byte;
  year,
  month,
  day,
  dow         : word;
  count       : word;
  wait : boolean;
  Search_Rec  : TSearchRec;
  AnyFiles : boolean;
  key_char    : boolean;
  key_byte    : byte;
  file_name,
  arc_name    : String;
  topic       : word;

const
     PickMaxY = 10;
     backfilename = 'BACKUP.XXX';
     BackSaveFileName = 'BACKUP.LOC';
  BackupLoc : String = 'A:';
  ArcSuffix = '.LZH';
  ArcSuff = '.LZ';
  RestArcSuffix = '.LZ?';
  GameFileCompress = 0.35;
  DataBaseCompress = 0.20;

implementation

end.