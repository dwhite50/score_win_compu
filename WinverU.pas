unit WinverU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure runit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    YesNoBoxU,
    pvio,
    StStrL;

{$R *.DFM}

procedure TForm1.runit(Sender: TObject);
begin
  ErrBox('Windows version: '+Long2StrL(Win32MajorVersion)+'.'
    +Long2StrL(Win32MinorVersion),MC,0);
  Application.Terminate;
end;

end.
