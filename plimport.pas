unit plimport;

interface

uses
    stdctrls,
    comctrls;

{$I delim.dcl}

procedure DelimImport(const impfilename, DestPath: String;
          const tournament: boolean; const ShowProg: boolean;
          var canceled: boolean);

implementation

uses
    Forms,
    SysUtils,
    StStrL,
    StDate,
    StDateSt,
    StUtils,
    StText,
    Progress,
    db33,
    util2,
    history,
    dbsizes,
    pvio,
    dbase;


const
     DefAcbl_pick : Array[1..5] of boolean = (false,false,false,true,false);
     DefAcblt_pick : Array[1..5] of boolean = (true,true,true,true,true);
var
   Field_pick : Array[1..5] of boolean;

function FixCentury(const Ifield: String): String;
begin
  if Copy(Ifield,3,2) = '00' then FixCentury := '00'+Ifield
  else if Ifield[1] < '5' then FixCentury := '20'+Ifield
  else FixCentury := '19'+Ifield;
end;

function GetACBLDate(const Ifield: String; const allowmod: boolean): boolean;
var
   tempstr      : String[8];
   test         : boolean;
begin
  with Playern do begin
    if Empty(Ifield) then tempstr := CharStrL(' ',8)
    else begin
      tempstr := FixCentury(IField);
      tempstr := Copy(tempstr,5,2)+Copy(tempstr,7,2)+Copy(tempstr,1,4);
    end;
    test := Acbl_Date <> tempstr;
    GetACBLDate := test;
    if test and allowmod then Acbl_Date := tempstr;
  end;
end;

function GetPaidThru(const Ifield: String): boolean;
var
   tempstr      : String[6];
   test         : boolean;
begin
  with Playern do begin
    if Empty(Ifield) then tempstr := CharStrL(' ',6)
    else begin
      tempstr := FixCentury(IField);
      tempstr := Copy(tempstr,5,2)+Copy(tempstr,1,4);
    end;
    test := IdxDate(Paid_Thru) < IdxDate(tempstr);
    GetPaidThru := test;
    {$ifdef test}
    if Paid_Thru <> tempstr then
      ErrBox('Pl:'+Player_no+', Old:"'+Paid_Thru+'" New:"'+tempstr+'" Ifield:"'+Ifield+'"',MC,0);
    {$endif}
    if test then Paid_Thru := tempstr;
  end;
end;

procedure get_tot_points(const Ifield: String);
var
   tempstr      : String;
   import_pts   : longint;
begin
  with PLAYERN do begin
    tempstr := Ifield;
    Delete(tempstr,6,1);
    import_pts := Key2Num(Str2Key(tempstr,4));
    if import_pts < Key2Num(Y_T_D) then Tot_points := Num2Key(0,4)
    else Tot_points := Num2Key(import_pts-Key2Num(Y_T_D),4);
  end;
end;

procedure SetImportDate;
var
   Ftime  : longint;
begin
  CloseDBFile(Playern_no);
  Ftime := DateTimeToFileDate(Now);
  PutDBItemSize(Prepend+DBNames[Playern_no,0],dbLastImpDate,Ftime);
end;

procedure DelimImport(const impfilename, DestPath: String;
          const tournament: boolean; const ShowProg: boolean;
          var canceled: boolean);

var
   import_record        : String;

function GetDelimField(const FieldNo: word; const dopad: boolean): String;
var
   TmpField     : String;
   j            : word;
begin
  TmpField := GetCSVField(FieldNo,import_record);
  if FieldNo > 0 then case DelimNum[FieldNo] of
    0: ;
    12,MaxPlField: begin
      j := 0;
      while j < Length(TmpField) do begin
        Inc(j);
        if Pos(TmpField[j],'0123456789 ') < 1 then begin
          Delete(TmpField,j,1);
          Dec(j);
        end;
      end;
      TmpField := Copy(TmpField,1,10);
      if dopad then TmpField := PadL(TmpField,10);
    end;
    1,9,16,24,25:;
    11,19,20,22,31: if dopad then
      TmpField := PadL(TmpField,FieldSize[Playern_no,DelimNum[FieldNo]]);
    else begin
      TmpField := Copy(TmpField,1,FieldSize[Playern_no,DelimNum[FieldNo]]);
      if dopad then
        TmpField := PadL(TmpField,FieldSize[Playern_no,DelimNum[FieldNo]]);
    end;
  end
  else TmpField := '';
  GetDelimField := TmpField;
end;

const
     DateTypeText       : Array[1..3] of Pchar =
        ('1  MM/DD/YY or MM/DD/YYYY or MMDDYY or MMDDYYYY',
         '2  YY/MM/DD or YYYY/MM/DD or YYMMDD or YYYYMMDD',
         '3  DD/MM/YY or DD/MM/YYYY or DDMMYY or DDMMYYYY');
var
   FileDate     : String[8];
   TotWidth     : byte;
   TotDelimFields : word;
   j              : word;
   k              : word;
   islast         : word;
   isfirst        : word;
   isplayer       : word;
   iscity         : word;
   isdate         : boolean;
   datetype       : byte;
   PlayerKey      : String[7];
   NameKey        : String[24];
   firstn         : String[16];
   lastn          : String[16];
   xcity          : String[16];
   is_mod         : boolean;
   ikey           : word;
   FromOffice   : boolean;
   FDatetime    : TStDateTimeRec;
   RankTime     : TDateTime;

function GetDateType(const DateStr: String): byte;
var
   SepPos       : byte;
   tempstr      : String[12];
   dstr         : string[6];
   j            : byte;

function ValidDigits(const NumField: String):boolean;
var
   j    : byte;
begin
  ValidDigits := false;
  for j := 1 to Length(NumField) do if Pos(NumField[j],'0123456789') < 1 then
    exit;
  ValidDigits := true;
end;

function CheckDate(const YearPos: byte): byte;
var
   j    : byte;
   dstr : string[6];
begin
  CheckDate := 0;
  if YearPos = 2 then exit;
  if YearPos = 1 then begin
    CheckDate := 2;
    exit;
  end;
  for j := 1 to 2 do begin
    dstr := ExtractWordL(j,tempstr,'/-');
    if (Length(dstr) = 2) and (dstr > '12') then begin
      case j of
        1: CheckDate := 3;
        2: CheckDate := 1;
      end;
      exit;
    end;
  end;
end;

begin
  tempstr := Strip(DateStr);
  SepPos := Pos('/',tempstr);
  if SepPos < 1 then SepPos := Pos('-',tempstr);
  GetDateType := 0;
  if SepPos < 1 then exit;
  for j := 1 to 3 do begin
    dstr := ExtractWordL(j,tempstr,'/-');
    if not ValidDigits(dstr) then exit;
    if length(dstr) = 4 then break;
  end;
  if Length(dstr) = 4 then begin
    GetDateType := CheckDate(j);
    exit;
  end;
  for j := 1 to 3 do begin
    dstr := ExtractWordL(j,tempstr,'/-');
    if (Length(dstr) = 2) and ((dstr = '00') or (dstr > '31')) then break;
  end;
  if (Length(dstr) = 2) and ((dstr = '00') or (dstr > '31')) then
    GetDateType := CheckDate(j);
end;

procedure UpdatePoints;
var
   j    : word;
begin
  for j := 1 to NumDelimFields do if DelimNum[j] = 20 then begin
    SetPlayerPoints(RRound(Valu(GetDelimField(j,true))*100),ppRecent,false);
    is_mod := true;
  end;
  for j := 1 to NumDelimFields do if DelimNum[j] = 11 then begin
    SetPlayerPoints(RRound(Valu(GetDelimField(j,true))*100),ppMTD,false);
    is_mod := true;
  end;
  for j := 1 to NumDelimFields do if DelimNum[j] = 22 then begin
    SetPlayerPoints(RRound(Valu(GetDelimField(j,true))*100),ppYTD,false);
    is_mod := true;
  end;
  for j := 1 to NumDelimFields do if DelimNum[j] = 31 then begin
    SetPlayerPoints(RRound(Valu(GetDelimField(j,true))*100),ppElig,false);
    is_mod := true;
  end;
  for j := 1 to NumDelimFields do if DelimNum[j] = 19 then begin
    SetPlayerPoints(RRound(Valu(GetDelimField(j,true))*100),ppTotal,false);
    is_mod := true;
  end;
end;

procedure update_record(const all: boolean);

function FixDate(const indate: String): String;
type
    TDateElement = packed Record
      isSlash    : boolean;     {true = includes delimiters}
      DLength    : byte;        {length of field}
      isDate     : boolean;     {true = includes date, false = Y, M only}
      Dorder     : Array[1..3] of byte; {Year,Month,Date sequence}
      DPos       : Array[1..3,1..2] of byte; {offset, length for Y,M,D (no slash)}
    end;

const
     DateFormat : Array[1..7,1..3] of TDateElement =
     {MMYY, YYMM, MMYY}
(((isSlash:false;DLength:4;isDate:false;Dorder:(2,1,0);DPos:((3,2),(1,2),(0,0))),
  (isSlash:false;DLength:4;isDate:false;Dorder:(1,2,0);DPos:((1,2),(3,2),(0,0))),
  (isSlash:false;DLength:4;isDate:false;Dorder:(2,1,0);DPos:((3,2),(1,2),(0,0)))),

  {MM/YY, YY/MM, MM/YY}
 ((isSlash:true;DLength:5;isDate:false;Dorder:(2,1,0);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:5;isDate:false;Dorder:(1,2,0);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:5;isDate:false;Dorder:(2,1,0);DPos:((0,0),(0,0),(0,0)))),

  {MMDDYY, YYMMDD, DDMMYY}
 ((isSlash:false;DLength:6;isDate:true;Dorder:(3,1,2);DPos:((5,2),(1,2),(3,2))),
  (isSlash:false;DLength:6;isDate:true;Dorder:(1,2,3);DPos:((1,2),(3,2),(5,2))),
  (isSlash:false;DLength:6;isDate:true;Dorder:(3,2,1);DPos:((5,2),(3,2),(1,2)))),

  {MM/YYYY, YYYY/MM, MM/YYYY}
 ((isSlash:true;DLength:7;isDate:false;Dorder:(2,1,0);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:7;isDate:false;Dorder:(1,2,0);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:7;isDate:false;Dorder:(2,1,0);DPos:((0,0),(0,0),(0,0)))),

  {MMDDYYYY, YYYYMMDD, DDMMYYYY}
 ((isSlash:false;DLength:8;isDate:true;Dorder:(3,1,2);DPos:((5,4),(1,2),(3,2))),
  (isSlash:false;DLength:8;isDate:true;Dorder:(1,2,3);DPos:((1,4),(5,2),(7,2))),
  (isSlash:false;DLength:8;isDate:true;Dorder:(3,2,1);DPos:((5,4),(3,2),(1,2)))),

  {MM/DD/YY, YY/MM/DD, DD/MM/YY}
 ((isSlash:true;DLength:8;isDate:true;Dorder:(3,1,2);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:8;isDate:true;Dorder:(1,2,3);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:8;isDate:true;Dorder:(3,2,1);DPos:((0,0),(0,0),(0,0)))),

  {MM/DD/YYYY, YYYY/MM/DD, DD/MM/YYYY}
 ((isSlash:true;DLength:10;isDate:true;Dorder:(3,1,2);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:10;isDate:true;Dorder:(1,2,3);DPos:((0,0),(0,0),(0,0))),
  (isSlash:true;DLength:10;isDate:true;Dorder:(3,2,1);DPos:((0,0),(0,0),(0,0)))));

var
   tempstr      : _Str12;
   SepPos       : byte;
   DateLen      : byte;
   FormIndex    : byte;
   FormFound    : boolean;
   issep        : boolean;
   dfields      : byte;
   dstr         : Array[1..3] of String[4];
   j            : byte;

begin
  tempstr := Strip(indate);
  FixDate := CharStrL(' ',8);
  DateLen := Length(tempstr);
  if (DateLen < 4) or (DateLen > 10) or (DateLen = 9) then exit;
  SepPos := Pos('/',tempstr);
  if SepPos < 1 then SepPos := Pos('-',tempstr);
  issep := SepPos > 0;
  FormIndex := 0;
  Formfound := false;
  repeat
    Inc(FormIndex);
    with DateFormat[FormIndex,datetype] do
      FormFound := (DateLen = DLength) and (issep = isSlash);
  until (FormIndex >= 7) or Formfound;
  if not FormFound then exit;
  FillChar(dstr,SizeOf(dstr),0);
  with DateFormat[FormIndex,datetype] do begin
    if isDate then dfields := 3
    else begin
      dfields := 2;
      dstr[3] := '01';
    end;
    if issep then for j := 1 to dfields do begin
      dstr[j] := ExtractWordL(Dorder[j],tempstr,'/-');
      if Empty(dstr[j]) then exit;
    end
    else for j := 1 to dfields do dstr[j] := Copy(tempstr,DPos[j,1],DPos[j,2]);
  end;
  if length(dstr[1]) = 2 then begin
    if dstr[1] < '10' then dstr[1] := '20'+dstr[1]
    else dstr[1] := '19'+dstr[1];
  end;
  FixDate := dstr[2]+dstr[3]+dstr[1];
end;

var
   tempstr      : _Str12;
   mon          : word;
   j            : word;
   other_valid  : boolean;
   NAcblDate    : String[10];
   ytitle       : String[8];
   isnewer      : boolean;
   xcatb        : String[2];
begin
  ytitle := GetCSVField(3,import_record);
  other_valid := Pos('$',ytitle) > 0;
  xcatb := PadL(GetCSVField(5,import_record),2);
  FromOffice := xcatb[1] <> '}';
  if FromOffice then NACBLDate := FileDate
  else begin
    NACBLDate := GetCSVField(14,import_record);
    NACBLDate := FixDate(NACBLDate);
  end;
  if not other_valid then ytitle := ' ';
  with PLAYERN do begin
    if all then PlayernInit(Playern);
    isnewer := (DBSize[Playern_no] = Playern_Size)
      and (IdxDate(NACBLDate) >= IdxDate(ACBL_Update_Date));
    if Player_check(PlayerKey,pnAny) and (all or not Empty(PlayerKey)) then
      player_no := PlayerKey;
    for j := 1 to NumDelimFields do if DelimNum[j] > 0 then case DelimNum[j] of
      1: if all or Field_pick[4] then begin
        tempstr := GetDelimField(j,false);
        tempstr := FixDate(tempstr);
        PutDBField(Playern_no,DelimNum[j],@Playern,tempstr);
        is_mod := true;
      end;
      2: begin
        tempstr := GetDelimField(j,false);
        if not Empty(tempstr) and (acbl_rank <> tempstr) then begin
          is_mod := true;
          PutDBField(Playern_no,DelimNum[j],@Playern,tempstr);
          if (acbl_rank[1] > 'F') and (player_no[1] <= '9') and FromOffice then
            player_no := char(ord(Player_no[1]) + 25) + copy(player_no,2,6);
        end;
      end;
      3: if all or Empty(cat_a) then begin
        tempstr := GetDelimField(j,false);
        if not Empty(tempstr) then begin
          PutDBField(Playern_no,DelimNum[j],@Playern,tempstr);
          is_mod := true;
        end;
      end;
      4: if all or Empty(cat_b) then begin
        tempstr := GetDelimField(j,false);
        if not Empty(tempstr) then begin
          PutDBField(Playern_no,DelimNum[j],@Playern,tempstr);
          is_mod := true;
        end;
      end;
      5: if all or Field_pick[2] or Empty(city) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      6: if all or Field_pick[1] or Empty(first_name) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      7: if all or Empty(Gender) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      8: if all or Field_pick[1] or Empty(last_name) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      12: if all or Field_pick[3] or Empty(Copy(phone,1,10)) then begin
        tempstr := LeftPadL(GetDelimField(j,false),10);
        Move(tempstr[1],Phone[1],10);
        is_mod := true;
      end;
      13:;
      17: if all or Field_pick[2] or Empty(state) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      18: if all or Field_pick[2] or Empty(street2) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      21: if all or Field_pick[5] or Empty(unit_no) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      23: begin
        tempstr := GetDelimField(j,false);
        if (length(tempstr) in [7,8,9]) and (Pos(tempstr[1],'0123456789') > 0)
          and (Pos('-',tempstr) < 1) then Insert('-',tempstr,6);
        if all or Field_pick[2] or Empty(zip) then begin
          zip := PadL(tempstr,10);
          is_mod := true;
        end;
      end;
      24: if (all or Field_pick[4]) and (DBSize[Playern_no] = Playern_Size) then begin
        tempstr := GetDelimField(j,false);
        tempstr := FixDate(tempstr);
        tempstr := Copy(tempstr,1,2)+Copy(tempstr,5,4);
        PutDBField(Playern_no,DelimNum[j],@Playern,tempstr);
        is_mod := true;
      end;
      26: if (DBSize[Playern_no] = Playern_Size)
        and (all or Field_pick[5] or Empty(District_no)) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      27: if (DBSize[Playern_no] = Playern_Size)
        and (all or Field_pick[2] or Empty(country)) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      28: if all or Field_pick[2] or Empty(street1) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
        if not other_valid or (Pos('U',ytitle) < 1) then begin
          if Empty(street1) then mail_code := ' '
          else mail_code := 'M';
        end;
      end;
      29: if (DBSize[Playern_no] = Playern_Size)
        and (all or Empty(email)) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      30: if (DBSize[Playern_no] = Playern_Size)
        and (all or Empty(fee_type)) then begin
        PutDBField(Playern_no,DelimNum[j],@Playern,GetDelimField(j,true));
        is_mod := true;
      end;
      MaxPlField: if all or Field_pick[3] or Empty(Copy(phone,11,10)) then begin
        tempstr := LeftPadL(GetDelimField(j,false),10);
        Move(tempstr[1],Phone[11],10);
        is_mod := true;
      end;
    end;
    if other_valid then begin
      if Pos('S',ytitle) > 0 then begin
        acbl_rank := 'S';
        is_mod := true;
      end;
      if Empty(mail_code) and (Pos('U',ytitle) > 0 ) then begin
        mail_code := 'U';
        is_mod := true;
      end;
    end;
    if tournament then begin
      if FromOffice then begin
        if (Pos('P',ytitle) > 0) and (Pos(fee_type[1],'PX') = 0) then
          fee_type := 'P';
        {else cat_b := ' ';}
        is_mod := true;
      end
      else begin
        if cat_b <> xcatb[2] then is_mod := true;
        cat_b := xcatb[2];
        if cat_b = 'T' then begin
          if Pos(fee_type[1],'PX') = 0 then fee_type := 'P';
          cat_b := ' ';
          is_mod := true;
        end;
      end;
    end;
    if all then local_date := CharStrL(' ',8);
    if isnewer and (all or is_mod) then ACBL_Update_Date := NACBLDate;
  end;
end;

procedure TestSpecial(const j,OffSet: word; const on: boolean);
begin
  case j of
    5: if on then iscity := OffSet else iscity := 0;
    6: if on then isfirst := OffSet else isfirst := 0;
    8: if on then islast := OffSet else islast := 0;
    13: if on then isplayer := OffSet else isplayer := 0;
  end;
end;

procedure SetSpecial(const on: boolean);
var
   j    : word;
begin
  for j := 1 to TotDelimFields do TestSpecial(DelimNum[j],j,on);
end;

var
   isfield      : boolean;
   imp_file   : TextFile;
   TotalRecs  : Longint;
   Tkeytab,
   Tempkey    : keystr;
   savkey       : Array[1..3] of KeyStr;

label
     abort;
begin
  if tournament then Move(DefAcblt_pick,Field_pick,SizeOf(DefAcbl_pick))
  else Move(DefAcbl_pick,Field_pick,SizeOf(DefAcbl_pick));
  assignFile(imp_file,impfilename);
  {$I-} reset(imp_file); {$I+}
  if IOResult <> 0 then begin
    AddToLogFile('File '+impfilename+' not found',true);
    Exit;
  end;
  {$I-}ReadLn(imp_file,import_record);{$I+}
  CheckIOResult(impfilename);
  isfield := false;
  j := 0;
  while not isfield and (j < 10) do begin
    Inc(j);
    isfield := not Empty(GetDelimField(j,false));
  end;
  if not isfield then begin
    Close(imp_file);
    RingBell(false);
    AddToLogFile(impfilename+' is not a valid comma delimited file.',true);
    Exit;
  end;
  Move(DefACBLNum,DelimNum,DefACBLFields);
  NumDelimFields := DefACBLFields;
  TotDelimFields := DefACBLFields;
  SetSpecial(true);
  isdate := true;
  datetype := 1;
  Reset(imp_file);
  FDatetime := FileTimeToStDateTime(GetFTime(impfilename));
  FileDate := StDateToDateString('mmddyyyy',FDateTime.D,true);
  UseLookupDB := tournament;
  OtherPath := true;
  Prepend := DestPath;
  OpenDBFile(Playern_no);
  if ShowProg then ProgressBarInit(impfilename+' --> '+DestPath,'Importing',true);
  Flush_DF := false;
  Flush_IF := false;
  AddToLogFile('Importing '+impfilename+' --> '+DestPath,false);
  TotalRecs := TextFileSize(imp_file);
  while (not eof(imp_file)) do begin
    {$I-}ReadLn(imp_file,import_record);{$I+}
    CheckIOResult(impfilename);
    if Length(import_record) < NumDelimFields * 3 then continue;
    is_mod := false;
    if ShowProg then ProgressBar(TextPos(imp_file),TotalRecs);
    lastn := GetDelimField(islast,true);
    firstn := GetDelimField(isfirst,true);
    NameKey := UpperCase(lastn+Copy(firstn,1,8));
    if NameKey[1] = ' ' then continue;
    if isplayer > 0 then PlayerKey := GetDelimField(isplayer,true)
    else PlayerKey := CharStrL(' ',7);
    ok := false;
    if Player_check(PlayerKey,pnPoundACBL) then begin
      tempkey := player_key(PlayerKey);
      FindKey(IdxKey[Playern_no,2]^,Recno[Playern_no],tempkey);
    end;
    if not ok and (iscity > 0) then with Playern do begin
      xcity := GetDelimField(iscity,true);
      tempkey := NameKey;
      FindKey(IdxKey[Playern_no,1]^,Recno[Playern_no],tempkey);
      if ok then GetARec(Playern_no);
      while ok and ((First_name <> firstn) or (City <> xcity)) do begin
        NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],tempkey);
        ok := ok and CompareKey(tempkey,NameKey);
        if ok then GetARec(Playern_no);
      end;
      ok := ok and (not Player_check(PlayerKey,pnPoundACBL)
        or not Player_check(Player_no,pnPoundACBL));
    end;
    if not ok then begin
      ok := true;
      PlayernInit(Playern);
      update_record(true);
      Add_Record;
      UpdatePoints;
      PutARec(Playern_no);
    end
    else begin
      is_mod := false;
      GetARec(Playern_no);
      SavKey[1] := GetKey(Playern_no, 1);
      SavKey[2] := GetKey(Playern_no, 2);
      update_record(false);
      if Field_Pick[4] then UpdatePoints;
      for ikey := 1 to 2 do begin
        TKeyTab := GetKey(Playern_no, ikey);
        if TKeyTab <> SavKey[ikey] then begin
          AddKey(IdxKey[Playern_no,ikey]^, Recno[Playern_no], TKeyTab);
          DeleteKey(IdxKey[Playern_no,ikey]^, Recno[Playern_no], SavKey[ikey]);
        end;
      end;
      PutARec(Playern_no);
    end;
  end;
abort:
  SetImportDate;
  {$I-} close(imp_file); {$I+}
  if IOResult = 0 then;
  if ShowProg then ProgressBarDone(false);
end;

end.
