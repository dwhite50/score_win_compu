unit FOpen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StBase, StBrowsr, StdCtrls, OvcBase, OvcEF, OvcSF,
  {$ifdef VER120}StShBase{$else}SsBase{$endif};

type
  TOpenSaveForm = class(TForm)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    BrowseFolder: TStBrowser;
    PathEdit: TOvcSimpleField;
    OvcController1: TOvcController;
    PathLabel: TOvcAttachedLabel;
    OKBut: TButton;
    BrowseBut: TButton;
    CanBut: TButton;
    SendToBut: TButton;
    procedure OKClick(Sender: TObject);
    procedure BrowseClick(Sender: TObject);
    procedure CanClick(Sender: TObject);
    procedure SendClick(Sender: TObject);
  private
    { Private declarations }
    canceled : boolean;
    Browse   : boolean;
    sendto   : boolean;
    allowsend : boolean;
  public
    { Public declarations }
  end;

const FolderBrowseLocation : String = '';
      allowfolderchange : boolean = true;

function GetFolder(var FileLocation: String; const otitle: String;
         const BrowseOnly: boolean; IniParm,DefLoc{,FileName}: String):boolean;
function GetFileName(Const FileLoc,FileMask: String; var oFileName: String;
         const otitle,IniParm,DefLoc: String; const dosave,isName: boolean): boolean;
function GetInFileName(Const FileLoc,FileMask: String; var oFileName: String;
         const otitle,IniParm,DefLoc,PickGroup: String): boolean;

implementation
uses
    csdef,
    StRegIni,
    ShellApi,
    VListBox,
    util2,
    pvio,
    StStrL;

{$R *.DFM}

const
     OpenSaveForm: TOpenSaveForm = nil;

{function fixtitle(const xtitle: string): string;
var
   j : integer;
begin
  Result := xtitle;
  for j := 1 to Length(xtitle) do if xtitle[j] = #13 then Result[j] := ' ';
end;}

procedure SetFormWidth(const title,prompt: String);
var
   MaxTextWidth        : integer;
   MaxTextHeight       : integer;
   w     : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   yLine  : String;
   cw    : integer;
   j     : integer;
begin
  with OpenSaveForm do begin
    yline := xline;
    w := 0;
    repeat
      j := Pos(#13,yline);
      if j < 1 then j := Length(yline)+1;
      if j > 1 then Line := Copy(yline,1,j-1)
      else Line := '';
      yline := Copy(yline,j+1,32000);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
        cw := Canvas.TextHeight(Line);
        if cw > MaxTextHeight then MaxTextHeight := cw;
      end;
      Inc(w);
    until Length(yLine) < 1;
  end;
end;

begin
  with OpenSaveForm do begin
    Font.Size := ViewFontSize;
    MaxTextWidth := 0;
    MaxTextHeight := 10;
    GetTextWidth(title);
    GetTextWidth(prompt);
    ClientHeight := w * MaxTextHeight + 85;
    OKBut.Top := Height - 62;
    BrowseBut.Top := OKbut.top;
    BrowseBut.Width := Canvas.TextWidth(' Browse ');
    CanBut.Top := OKBut.Top;
    CanBut.Width := Canvas.TextWidth(' Cancel ');
    SendtoBut.Top := OKbut.Top;
    SendToBut.Visible := allowSend;
    PathEdit.Top := OkBut.Top - 40;
    PathEdit.LabelInfo.OffsetY := (w-1) * -24;
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    PathLabel.Width := Width-20;
    Caption := title;
    PathLabel.Caption := prompt;
  end;
end;

function GetFolder(var FileLocation: String; const otitle: String;
         const BrowseOnly: boolean; IniParm,DefLoc{,FileName}: String):boolean;
var
   ischanged : boolean;
   curdir    : string;
   j         : integer;
   Reg : TStRegIni;
   SendDir : String;
//   otitle  : String;
begin
//  otitle := fixtitle(xtitle);
  if not Assigned(OpenSaveForm) then
    Application.CreateForm(TOpenSaveForm, OpenSaveForm);
  if Length(IniParm) > 0 then FileLocation := GetProfile(IniParm,DefLoc);
  if Pos(':',FileLocation) < 2 then begin
    curdir := GetCurrentDir;
    j := Pos(':',curdir);
    if j > 0 then FileLocation := Copy(curdir,1,j)+FileLocation;
  end;
  with OpenSaveForm do begin
//    browse := Length(BrowseLocation) < 1;
    browse := BrowseOnly;
    allowsend := false;
    if not Browse then begin
      sendto := false;
//      allowsend := (Length(FileName) > 0) and FileExists(FileName);
      allowsend := false;
      ischanged := false;
      SetFormWidth('Enter location',Otitle);
      PathEdit.Text := FileLocation;
      ShowModal;
      {if sendto then begin
        Reg := TStRegIni.Create(RIMachine,false);
        with Reg do begin
          CurSubKey := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Exploret\Shell Folders\SendTo';
          SendDir := ReadString('Directory','');
          Releases;
        end;
        ShellExecute(
      x
      end;}
      if not canceled then begin
        ischanged := FileLocation <> PathEdit.Text;
        FileLocation := PathEdit.Text;
      end;
      Result := not canceled;
    end
    else ischanged := true;
    if browse then begin
      Hide;
      with BrowseFolder do begin
        Caption := Otitle;
        if ischanged or Empty(FolderBrowseLocation) then
          SelectedFolder := FileLocation
        else SelectedFolder := FolderBrowseLocation;
        Result := Execute;
        if Result then FileLocation := SelectedFolder;
      end;
    end;
    Free;
  end;
  FolderBrowseLocation := '';
  if Result and (Length(IniParm) > 0) then SetProfile(IniParm,FileLocation);
  OpenSaveForm := nil;
end;

const
     Flist     : TStringlist = nil;
     nlocs : integer = 0;
     Maxloc = 100;
var
   Locs : Array[1..maxloc] of String;
   Picks : array[1..maxloc] of integer;

function GetPickName(const item: integer): String;
begin
  if Flist[item-1][1] = 'd' then Result := '<'+Copy(Flist[item-1],2,50)+'>'
  else Result := Copy(Flist[item-1],2,50);
end;

function GetInFileName(Const FileLoc,FileMask: String; var oFileName: String;
         const otitle,IniParm,DefLoc,PickGroup: String): boolean;

procedure SaveLoc(const fn: String; const pick: integer);
procedure SaveGroup;
begin
  if Length(Pickgroup) > 0 then SetAppProfile(fn,Copy(Flist[pick-1],2,50),PickGroup);
end;

var
   j : integer;
begin
  for j := 1 to nlocs do if fn = locs[j] then begin
    picks[j] := pick;
    SaveGroup;
    exit;
  end;
  if nlocs < maxloc then begin
    Inc(nlocs);
    j := nlocs;
  end
  else j := 1;
  locs[j] := fn;
  picks[j] := pick;
  SaveGroup;
end;

function Getlocpick(const fn: String): integer;
var
   j : integer;
   profpick : String;
begin
  Result := 1;
  for j := 1 to nlocs do if fn = locs[j] then begin
    Result := picks[j];
    exit;
  end;
  if Length(PickGroup) > 0 then begin
    profpick := GetAppProfile(fn,'',PickGroup);
    if Length(profpick) > 0 then for j := 0 to Flist.Count-1 do
      if Copy(Flist[j],2,50) = profpick then begin
      Result := j+1;
      exit;
    end;
  end;
end;

procedure SearchFolder(const FolderName: String);
var
   SR        : TSearchRec;
   Ser       : integer;
   Fname     : char;
   Xname     : String;
   j         : integer;
   FMask     : String;
begin
  FList.Clear;
  FList.Sorted := true;
  if allowfolderchange then FList.Add('aChange drive or folder');
  Ser := FindFirst(FolderName+'*.*',faDirectory,SR);
  while Ser = 0 do with SR do begin
    Application.ProcessMessages;
    Xname := UpperCase(Name);
    if (xName <> '.') and ((Attr and faDirectory) <> 0) then FList.Add('d'+xName);
    Ser := FindNext(SR);
  end;
  FindClose(SR);
  j := 0;
  repeat
    Inc(j);
    FMask := ExtractWordL(j,FileMask,';');
    if Length(FMask) > 0 then begin
      Ser := FindFirst(FolderName+FMask,faArchive,SR);
      while Ser = 0 do with SR do begin
        Application.ProcessMessages;
        Xname := UpperCase(Name);
        if (Attr and faDirectory) = 0 then FList.Add('f'+xName);
        Ser := FindNext(SR);
      end;
      FindClose(SR);
    end;
  until Length(FMask) < 1;
end;

var
   FileLocation   : String;
   fpick : integer;
   len   : integer;
   isdown : boolean;
   lastloc : String;
//   otitle : string;
label
     NewDrive,
     NextFolder;
begin
//  otitle := fixtitle(xtitle);
  Result := false;
  if Length(IniParm) > 0 then begin
    {if not GetFolder(FileLocation,Otitle,'',IniParm,DefLoc) then exit;
    FileLocation := UpperCase(FixPath(FileLocation));}
    FileLocation := UpperCase(FixPath(GetProfile(IniParm,DefLoc)));
  end
  else FileLocation := UpperCase(FixPath(FileLoc));
  isdown := false;
  if not ValidDir(FileLocation) then begin
    FileLocation := GetCurrentDir;
NewDrive:
    if not GetFolder(FileLocation,Otitle,true,'',DefLoc) then exit;
    FileLocation := UpperCase(FixPath(FileLocation));
  end;
  if not Assigned(FList) then FList := TStringlist.Create;
NextFolder:
  SearchFolder(FileLocation);
  if FList.Count > 0 then begin
    if isdown then for len := 0 to Flist.Count-1 do
      if Flist[len] = lastloc then SaveLoc(FileLocation,len+1);
    isdown := false;
    fpick := vpickchoice(FList.Count,getlocpick(FileLocation),1,Otitle,
      FileLocation,GetPickName,nil,nil,mc,0,false);
    if fpick > 0 then begin
      if Flist[fpick-1][1] = 'a' then begin
        FileLocation := JustPathNameL(FileLocation);
        goto NewDrive;
      end;
      if Flist[fpick-1] = 'd..' then begin
        Len := Length(FileLocation);
        if len > 1 then isdown := true;
        if len > 1 then for fpick := len-1 downto 2 do
          if FileLocation[fpick] = '\' then begin
          lastloc := 'd'+Copy(FileLocation,fpick+1,50);
          lastloc := Copy(lastloc,1,Length(lastloc)-1);
          FileLocation := FixPath(Copy(FileLocation,1,fpick-1));
          goto NextFolder;
        end;
        goto NextFolder;
      end
      else SaveLoc(FileLocation,fpick);
      if Flist[fpick-1][1] = 'd' then begin
        FileLocation := FileLocation+Copy(Flist[fpick-1],2,50)+'\';
        goto NextFolder;
      end;
      oFileName := FileLocation+Copy(Flist[fpick-1],2,50);
      if (Length(IniParm) > 0) then SetProfile(IniParm,JustPathNameL(FileLocation));
      Result := true;
    end;
  end;
  if Assigned(Flist) then Flist.Destroy;
  Flist := nil;
  allowfolderchange := true;
end;

function GetFileName(Const FileLoc,FileMask: String; var oFileName: String;
         const otitle,IniParm,DefLoc: String; const dosave,isName: boolean): boolean;
var
   FileLocation   : String;
//   otitle         : string;
begin
//  otitle := fixtitle(xtitle);
  if not Assigned(OpenSaveForm) then
    Application.CreateForm(TOpenSaveForm, OpenSaveForm);
  if Length(IniParm) > 0 then begin
    {if isName then FileLocation := JustPathNameL(GetProfile(IniParm,DefLoc))
    else }FileLocation := GetProfile(IniParm,DefLoc);
  end
  else begin
    {if isName then FileLocation := JustPathName(FileLoc)
    else }FileLocation := FileLoc;
  end;
  with OpenSaveForm do begin
    allowsend := false;
    if dosave then with SaveDialog do begin
      FileName := oFileName;
      Files.Clear;
      Filter := FileMask;
      FilterIndex := 1;
      InitialDir := FileLocation;
      Options := [ofPathMustExist,ofFileMustExist,ofHideReadOnly,
        ofNoChangeDir,ofEnableSizing,ofOverwritePrompt,ofReadOnly];
      Title := oTitle;
      Result := Execute;
      if Result then oFileName := Filename;
    end
    else with OpenDialog do begin
      FileName := oFileName;
      Files.Clear;
      Filter := FileMask;
      FilterIndex := 1;
      InitialDir := FileLocation;
      Options := [ofFileMustExist,ofPathMustExist,ofHideReadOnly,
        ofNoChangeDir,ofEnableSizing{,ofOldStyleDialog}];
      Title := oTitle;
      Result := Execute;
      if Result then oFileName := Filename;
    end;
    Free;
  end;
  if Result and (Length(IniParm) > 0) then begin
    if isName then SetProfile(IniParm,oFileName)
    else SetProfile(IniParm,JustPathNameL(oFileName));
  end;
  OpenSaveForm := nil;
end;

procedure TOpenSaveForm.OKClick(Sender: TObject);
begin
  browse := false;
  canceled := false;
  Close;
end;

procedure TOpenSaveForm.BrowseClick(Sender: TObject);
begin
  browse := true;
  canceled := false;
  Close;
end;

procedure TOpenSaveForm.CanClick(Sender: TObject);
begin
  canceled := true;
  browse := false;
  Close;
end;

procedure TOpenSaveForm.SendClick(Sender: TObject);
begin
  sendto := true;
  canceled := false;
  Close;
end;

end.
