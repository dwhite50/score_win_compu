Unit formTPDatabase;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, XPStyleActnCtrls,
  Grids, ImgList, LMDCustomComponent, LMDCustomHint, LMDCustomShapeHint,
  LMDMessageHint, LMDWndProcComponent, LMDFormShape, LMDShapeHint, LMDHint,
  LMDTextHint, LMDTextShapeHint, LMDCustomScrollBox, LMDListBox,
  LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel, LMDBaseEdit,
  LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox;

Type
  TfrmTPDataBase = Class(TForm)
    StatusBar1: TStatusBar;
    GBPlayerInfo: TGroupBox;
    Label1: TLabel;
    MELastName: TMaskEdit;
    Label2: TLabel;
    MEFirstName: TMaskEdit;
    Shape1: TShape;
    Label3: TLabel;
    MEPlayerNo: TMaskEdit;
    Shape2: TShape;
    StaticText1: TStaticText;
    GBMenu: TGroupBox;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    Label4: TLabel;
    Label5: TLabel;
    MEStreet1: TMaskEdit;
    MEStreet2: TMaskEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    MECity: TMaskEdit;
    MEState: TMaskEdit;
    MEZip: TMaskEdit;
    MECountry: TMaskEdit;
    Label10: TLabel;
    MEEmail: TMaskEdit;
    Shape3: TShape;
    Label11: TLabel;
    MEPhone1: TMaskEdit;
    MEPhone2: TMaskEdit;
    ActionManager1: TActionManager; // Allows for older version keystoke compatibility
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MELocalActiveDate: TMaskEdit;
    MEAcblActiveDate: TMaskEdit;
    Shape4: TShape;
    Shape5: TShape;
    Label15: TLabel;
    MERank: TMaskEdit;
    Shape6: TShape;
    Bevel1: TBevel;
    Label17: TLabel;
    MECatA: TMaskEdit;
    Label18: TLabel;
    MECatB: TMaskEdit;
    Label16: TLabel;
    Label19: TLabel;
    MEMailCode: TMaskEdit;
    Label20: TLabel;
    MEUnitNo: TMaskEdit;
    Shape7: TShape;
    Shape8: TShape;
    Label21: TLabel;
    MEDistrictNo: TMaskEdit;
    Label22: TLabel;
    MEGender: TMaskEdit;
    Label23: TLabel;
    MEFeeCode: TMaskEdit;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Label24: TLabel;
    MEStartDate: TMaskEdit;
    Shape13: TShape;
    Label25: TLabel;
    MEPaidThru: TMaskEdit;
    Shape14: TShape;
    Label26: TLabel;
    MELastACBLUpdate: TMaskEdit;
    Shape15: TShape;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    MEMPTotal: TMaskEdit;
    MEMPYTD: TMaskEdit;
    MEMPMTD: TMaskEdit;
    MEMPRecent: TMaskEdit;
    MEMPEligibility: TMaskEdit;
    Shape16: TShape;
    Shape17: TShape;
    Shape18: TShape;
    Shape19: TShape;
    Shape20: TShape;
    Shape21: TShape;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    MEGroupCode: TMaskEdit;
    MEGroupNo: TMaskEdit;
    Shape22: TShape;
    Label36: TLabel;
    SGGroups: TStringGrid;
    ButQuit: TButton;
    ButQuitAction: TAction;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Shape23: TShape;
    Button4: TButton;
    Button5: TButton;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    Button6: TButton;
    StaticText7: TStaticText;
    Button7: TButton;
    StaticText8: TStaticText;
    Button8: TButton;
    StaticText9: TStaticText;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    ButDonePlayerInfo: TButton;
    ActionManager2: TActionManager; // Allows for older version keystoke compatibility
    ButPlayerInfoDone: TAction;
    ActionManager3: TActionManager;
    ImageList1: TImageList;
    LMDMessageHintCountry: TLMDMessageHint;
    LMDHeaderListComboBox2: TLMDHeaderListComboBox;
    LMDHeaderListComboBox3: TLMDHeaderListComboBox;
    Procedure FormActivate(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    { pLoadTPFields: Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    Procedure pLoadTPFields;
    Procedure pNextRecord;
    Procedure pPrevRecord;
    { pMakeGBReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
    Procedure pMakeGBReadOnly(bLogical: Boolean);
    Procedure FormCreate(Sender: TObject);
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    { pSetButHints: Set the edit menu buttons hints to their default entries }
    Procedure pSetButHints;
    { pFillVars: Fill the StringList slButtonNames with the button names for the menu.
                 Names must be added in the order of:   ButNext, ButPrev, ButFind, ButTop,
                 ButLast, ButEdit, ButAdd, ButCopy, ButDelete, ButQuit }
    Procedure pFillVars;
    { pFillGroups: Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
    Procedure pFillGroups;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure ButQuitClick(Sender: TObject);
    Procedure F4KeyExecute(Sender: TObject);
    Procedure F3KeyExecute(Sender: TObject);
    Procedure F5KeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    { DEVELOPMENT ONLY!!!!!!!
      pKeyExecuteMsg: Basic procedure for testing to tell which key the user pressed,
                      to make sure they are working as expected }
    Procedure pKeyExecuteMsg(oSender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    { ButAddActionExecute: Add Button OnClick Action }
    Procedure ButAddActionExecute(Sender: TObject);
    { pClearGroups: Clears the entries in the StringGrid SGGroups }
    Procedure pClearGroups;
    { pPlayerInfo: Allow or Deny access to the Player Information fields.
                      If BENABLE is True allow access,
                      If BENABLE is False Deny access  }
    Procedure pPlayerInfo(bEnable: Boolean);
    { ButPlayerInfoDoneExecute: Edit Button OnClick Action
      Done Button is only visible when access is allowed to the Player Information Fields.
      This button is used to trigger saving of any additions or changes to the record or
      reverting the record back to it's premodification state }
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    { ButEditActionExecute: Edit Button OnClick Action }
    Procedure ButEditActionExecute(Sender: TObject);
    { ButDeleteActionExecute: Delete Button OnClick Action }
    Procedure ButDeleteActionExecute(Sender: TObject);
    { ButCopyActionExecute: Copy Button OnClick Action }
    Procedure ButCopyActionExecute(Sender: TObject);
    { MELastNameExit: Validation of Last Name field when exiting it }
    Procedure MELastNameExit(Sender: TObject);
    { pAbortChanges: Abort the edits to the current record }
    Procedure pAbortChanges;
    { pSaveFormInfo: Save the forms Height, Width, Left Side Position and Top Position }
    Procedure pSaveFormInfo;
    { pSetFormBack: Set the forms Height and Width back to what it was }
    Procedure pSetFormBack;
    Function Status_OK(Const Fno: Integer): Boolean;
    Procedure MECountryEnter(Sender: TObject);
    Procedure MECountryExit(Sender: TObject);
    { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
      in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
      with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
      routines in DBASE.PAS, to update the actual PLAYERN.DAT table.}
    Procedure pCheckEditFields;
    { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
      TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
    Function fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
    { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
    Procedure ComboBoxChange(Sender: TObject);
    { COMBOBOXKEYPRESS: TLMDHeaderListComboBox OnKeyPress Routine }
    Procedure ComboBoxKeyPress(Sender: TObject; Var Key: Char);
    { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
    Procedure ComboBoxEnter(Sender: TObject);
    { COMBOBOXEXIT: TLMDHeaderListComboBox OnExit Routine }
    Procedure ComboBoxExit(Sender: TObject);
    { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
      The PLAYERN dataset is used by all record saving, adding and copying routines in DBASE.PAS,
      to update the actual PLAYERN.DAT table. }
    Procedure pCopyRecData;
    procedure LMDHeaderListComboBox3Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    { Integer variables to store the table number }
    // iPlayerNumber, iPlayerGroup: Integer;
    { Integer variable used dynamically to store the tables record count }
    iReccount: Integer;
    { Integer variables to store the forms original: Height, Width, Left Position and Top Position.
      Used to return form back to it's original size and position. }
    iTPFormHeight, iTPFormWidth, iTPFormLeft, iTPFormTop: Integer;
    { iDataMode Integer Variable
      Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    iDataMode: Integer;
  End;

Var
  frmTPDataBase: TfrmTPDataBase;
  { StringList containing the Group Box menu's button names }
  slButtonNames: TStringList;
Const
  {screen location equates}
  TR = 1; {top right}
  TL = 2; {top left}
  TC = 3; {top center}
  MR = 4; {middle right}
  ML = 5; {middle left}
  MC = 6; {middle center}
  BR = 7; {bottom right}
  BL = 8; {bottom left}
  BC = 9; {bottom center}

function duplicate_player(const number: String; Fno,Kno: integer; Func: String) : boolean;

Implementation

Uses DB33, Dbase, Util1, Util2, YesNoBoxU;

{$R *.dfm}

Procedure TfrmTPDataBase.FormActivate(Sender: TObject);
Var
  // Boolean variable to tell whether a table opened successfully.
  bTablesOpened: Boolean;
  sTableName: String;
Begin
  // ----------------------------------------------------------------------------------------------------------
  // String variable to hold the name of the table that did not open successfully.
  sTableName := '';
  // ----------------------------------------------------------------------------------------------------------
  // Create the button name stringlist for working with button settings
  slButtonNames := TStringList.Create;
  // ----------------------------------------------------------------------------------------------------------
  // Fill all this forms static variables
  pFillVars;
  // ----------------------------------------------------------------------------------------------------------
  // iPlayerNumber := dbase.Playern_no;    // Store the table number from dbase.Playern_no
  // iPlayerGroup := dbase.Playergr_no;    // Store the table number from dbase.Playergr_no
  // ----------------------------------------------------------------------------------------------------------
  { OTHERPATH is a boolean variable that is Constructed in Dbase.Pas. It is used to tell the program if it needs
    to get the PATH from the CFG.INI file, if it is False.

      IF NOT OTHERPATH THEN
        BEGIN
          IF USELOOKUPDB OR (FNO = PLCHANGE_NO) THEN
            PREPEND := FIXPATH(CFG.PLPATH)
          ELSE
            PREPEND := FIXPATH(CFG.DBPATH);
        END; }
  OtherPath := True;
  // ----------------------------------------------------------------------------------------------------------
  { Since we are telling the program that we are using a path other that what is stored in the CFG.INI file, we must
    pass the path for the tables in the system wide variable: PREPEND
    FIXPATH checks to make sure there is a forward slash "\" at the end of the path passed.  }
  Prepend := FixPath('C:\ACBLTemp');
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[0].Text := UpperCase(Prepend + 'PLAYERN.DAT');
  // ----------------------------------------------------------------------------------------------------------
  { Begin opening the tables. }
  // Try to open the main Player table (PLAYERN.DAT)
  bTablesOpened := OpenDBFile(Playern_no);
  If bTablesOpened Then Begin // If the main Player table opened (PLAYERN.DAT)
    { Store the number of records in PLAYERN.DAT to iReccount }
    iReccount := UsedRecs(DatF[Playern_no]^);
    { Add the record count to StatusBar1's display }
    StatusBar1.Panels[0].Text := StatusBar1.Panels[0].Text + ' - Contains ' +
      Trim(IntToStr(iReccount)) + ' Records.';
    // Open the Player Group Table (PLAYERGR.DAT)
    bTablesOpened := OpenDBFile(Playergr_no);
    { If the Player Group Table did not open successfully... }
    If Not bTablesOpened Then ;
    If iReccount > 0 Then Begin
      ClearKey(IdxKey[Playern_no, 1]^);
      { Find the first available record and load it's values to the fields on the screen }
      pNextRecord;
      { Make the GroupBox GBPlayerInfo read only, all Maskedits are in this GB, so they by
        default, are read only also }
      pMakeGBReadOnly(True);
    End
    Else Begin
      // Since the record count in PLAYERN.DAT is zero, go directly to add a record mode
    End;
  End
  Else Begin
    // If the table did not open
    MessageDlg('Unable to open file!', mtError, [mbOK], 0);
    Close; // Close the form
  End;
  // Make sure Button Hints are set and available
  pSetButHints;
  // If the Next Button is Enabled, set the initial system focus to it.
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmTPDataBase.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  { Form Cleanup actions }
  { Free the StringList slButtonNames from memory }
  slButtonNames.Free;
  { Close any open tables }
  dBase.CloseFiles;
  //CloseDBFile(iPlayerGroup);
  //CloseDBFile(iPlayerNumber);
End;

Procedure TfrmTPDataBase.pLoadTPFields;
Begin
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  frmTPDataBase.MELastName.Text := Playern.LAST_NAME;
  frmTPDataBase.MEFirstName.Text := Playern.FIRST_NAME;
  frmTPDataBase.MEPlayerNo.Text := Playern.PLAYER_NO;
  frmTPDataBase.MEStreet1.Text := Playern.STREET1;
  frmTPDataBase.MEStreet2.Text := Playern.STREET2;
  frmTPDataBase.MECity.Text := Playern.CITY;
  // STATE   ----------------------------------------------------------------------
  frmTPDataBase.MEState.Text := Playern.STATE;
  frmTPDataBase.LMDHeaderListComboBox2.Text := Playern.STATE;
  // ------------------------------------------------------------------------------
  frmTPDataBase.MEZip.Text := Playern.ZIP;
  // COUNTRY ----------------------------------------------------------------------
  frmTPDataBase.MECountry.Text := Playern.COUNTRY;
  frmTPDataBase.LMDHeaderListComboBox3.Text := Playern.COUNTRY;
  // ------------------------------------------------------------------------------
  frmTPDataBase.MEEmail.Text := Playern.EMAIL;
  frmTPDataBase.MEPhone1.Text := Copy(Playern.PHONE, 1, 10);
  frmTPDataBase.MEPhone2.Text := Copy(Playern.PHONE, 11, 10);
  frmTPDataBase.MELocalActiveDate.Text := Playern.LOCAL_DATE;
  frmTPDataBase.MEAcblActiveDate.Text := Playern.ACBL_DATE;
  frmTPDataBase.MERank.Text := Playern.ACBL_RANK;
  frmTPDataBase.MECatA.Text := Playern.CAT_A;
  frmTPDataBase.MECatB.Text := Playern.CAT_B;
  frmTPDataBase.MEMailCode.Text := Playern.MAIL_CODE;
  frmTPDataBase.MEUnitNo.Text := Playern.UNIT_NO;
  frmTPDataBase.MEDistrictNo.Text := Playern.DISTRICT_NO;
  frmTPDataBase.MEGender.Text := Playern.GENDER;
  frmTPDataBase.MEFeeCode.Text := Playern.FEE_TYPE;
  frmTPDataBase.MEStartDate.Text := Playern.START_DATE;
  frmTPDataBase.MEPaidThru.Text := Playern.PAID_THRU;
  frmTPDataBase.MELastACBLUpdate.Text := Playern.ACBL_UPDATE_DATE;
  // ==============================================================================================
  { Calculate the Players Total Points }
  frmTPDataBase.MEMPTotal.Text := IntToStr(dBase.GetPlayerPoints(PlayerN, ppTotal));
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  frmTPDataBase.MEMPYTD.Text := IntToStr(dBase.GetPlayerPoints(Playern, ppYTD));
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  frmTPDataBase.MEMPMTD.Text := IntToStr(dBase.GetPlayerPoints(Playern, ppMTD));
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  frmTPDataBase.MEMPRecent.Text := IntToStr(dBase.GetPlayerPoints(Playern, ppRecent));
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  frmTPDataBase.MEMPEligibility.Text := IntToStr(dBase.GetPlayerPoints(Playern, ppElig));
  // ==============================================================================================
  { Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
  pFillGroups;
  { Clear the MaskEdit fields for Group Code and Number }
  frmTPDataBase.MEGroupCode.Text := '';
  frmTPDataBase.MEGroupNo.Text := '';
End;

Procedure TfrmTPDataBase.pNextRecord;
Var
  FndKey: KeyStr;
Begin
  // Move the record pointer in the table to the next available record and load the data in the screen fields
  NextKey(IdxKey[Playern_no, 1]^, Recno[Playern_no], FndKey);
  { Read data base file Playern_no.  Record number in Recno[Playern_no]}
  GetARec(Playern_no);
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
End;

Procedure TfrmTPDataBase.pPrevRecord;
Var
  FndKey: KeyStr;
Begin
  // Move the record pointer in the table to the previous record and load the data in the screen fields
  PrevKey(IdxKey[Playern_no, 1]^, Recno[Playern_no], FndKey);
  GetARec(Playern_no);
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
End;

Procedure TfrmTPDataBase.pMakeGBReadOnly(bLogical: Boolean);
Begin
  { pMakeGBReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
  GBPlayerInfo.Enabled := Not bLogical;
End;

Procedure TfrmTPDataBase.FormCreate(Sender: TObject);
Begin
  YesNoBoxU.FormAutoScale(Self, Self.Font.Height, 1024, 768, 120, True);
End;

Procedure TfrmTPDataBase.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmTPDataBase.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus;
End;

Procedure TfrmTPDataBase.ButFindActionExecute(Sender: TObject);
Begin
  // Turn off the Action manager for the form TPDatabase
  ActionManager1.State := asSuspended;
  // -----------------------------------------------------
  // Display the search form
  {frmTPFind := TfrmTPFind.Create(Self); ;
  frmTPFind.ShowModal;
  frmTPFind.Release;}
  // ---------------------------------------------------------------
  // Turn on the Action manager for the Find Button Activation
  //ActionManager3.State := asNormal;
  Filno := Playern_no;
  {Case Filno Of
    Attend_no: If Status_OK(Filno) Then
        Find_Record(GetKey(Playern_no, 3), GetKey(Filno, 1), '', MC);
    Percent_no: If Status_OK(Filno) Then
        Find_Record(GetKey(Playern_no, 3), GetKey(Filno, 1), '', MC);
  Else Find_Record('', '', '', MC);
  End;}
  Find_Record('', '', '', MC);
  //ActionManager3.State := asSuspended;
  // Turn off the Action manager for the Find Button Activation
  // ---------------------------------------------------------------

  // -----------------------------------------------------
  // Turn on the Action manager for the form TPDatabase
  ActionManager1.State := asNormal;
  // Show the form TPDatabase
  //frmTPDataBase.Show;

  // Display the selected record
  GetARec(Playern_no);
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
End;

Procedure TfrmTPDataBase.ButTopActionExecute(Sender: TObject);
Begin
  ClearKey(IdxKey[Playern_no, 1]^);
  pNextRecord; // Find the first available record and load it's values to the screen fields
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmTPDataBase.ButLastActionExecute(Sender: TObject);
Begin
  ClearKey(IdxKey[Playern_no, 1]^);
  pPrevRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmTPDataBase.pSetButHints;
Var
  iForLoop, IControls, iSLIndex: Integer;
  sHint, sButtonName: String;
Begin
  { pSetButHints: Set the edit menu buttons hints to their default entries }
  IControls := GBMenu.ControlCount;
  For iForLoop := 0 To (IControls - 1) Do Begin
    If GBMenu.Controls[iForLoop].ClassType = TButton Then Begin
      (GBMenu.Controls[iForLoop] As TButton).ShowHint := True;
      // Get the button's name
      sButtonName := GBMenu.Controls[iForLoop].Name;
      // Find the position of the button name in the stringlist
      iSLIndex := slButtonNames.IndexOf(sButtonName);
      Case iSLIndex Of
        0: sHint := 'Display the next record...'; // ButNext
        1: sHint := 'Display the previous record...'; // ButPrev
        2: sHint := 'Find a record and display it...'; // ButFind
        3: sHint := 'Display the first record...'; // ButTop
        4: sHint := 'Display the last record...'; // ButLast
        5: sHint := 'Edit the current record...'; // ButEdit
        6: sHint := 'Add a new record...'; // ButAdd
        7: sHint := 'Copy the current record to a new record...'; // ButCopy
        8: sHint := 'Delete the current record...'; // ButDelete
        9: sHint := 'Close this screen...'; // ButQuit
      Else
        sHint := '';
      End;
      // Set the button's hint to the new string
      GBMenu.Controls[iForLoop].Hint := sHint;
    End;
  End;
  {
  ButNext.Hint:='Display the next record...';
  if ButNext.Action <> nil then (ButNext.Action as TAction).Hint:='Display the next record...';
  ButNext.ShowHint:=True;

  ButPrev.Hint:='Display the previous record...';
  ButPrev.ShowHint:=True;

  ButFind.Hint:='Find a record and display it...';
  ButFind.ShowHint:=True;

  ButTop.Hint:='Display the first record...';
  ButTop.ShowHint:=True;

  ButLast.Hint:='Display the last record...';
  ButLast.ShowHint:=True;

  ButEdit.Hint:='Edit the current record...';
  ButEdit.ShowHint:=True;

  ButAdd.Hint:='Add a new record...';
  ButAdd.ShowHint:=True;

  ButCopy.Hint:='Copy the current record to a new record...';
  ButCopy.ShowHint:=True;

  ButDelete.Hint:='Delete the current record...';
  ButDelete.ShowHint:=True;
  }
End;

Procedure TfrmTPDataBase.pFillVars;
Begin
  { Fill the StringList slButtonNames with the button names for the menu.
    Names must be added in the order of:   ButNext, ButPrev, ButFind, ButTop,
    ButLast, ButEdit, ButAdd, ButCopy, ButDelete, ButQuit }
  slButtonNames.Add('ButNext');
  slButtonNames.Add('ButPrev');
  slButtonNames.Add('ButFind');
  slButtonNames.Add('ButTop');
  slButtonNames.Add('ButLast');
  slButtonNames.Add('ButEdit');
  slButtonNames.Add('ButAdd');
  slButtonNames.Add('ButCopy');
  slButtonNames.Add('ButDelete');
  slButtonNames.Add('ButQuit');
End;

Procedure TfrmTPDataBase.pFillGroups;
Var
  ksRecNoKey, ksTempKey: keystr;
  iCol: integer;
Begin
  { Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
  { pClearGroups: Clears the entries in the StringGrid SGGroups }
  pClearGroups;
  // ---------------------------------------------------------------------------
  { Store the current records data to the string grid }
  iCol := 0;
  ksRecNoKey := GetKey(Playern_no, 3);
  ksTempKey := ksRecNoKey;
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  SearchKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  While ok And CompareKey(ksTempKey, ksRecNoKey) Do Begin
    GetARec(Playergr_no); // Store the record
    // First row stores the group
    SGGroups.Cells[iCol, 0] := dbase.Playergr.GROUP;
    // Second row stores the group number
    SGGroups.Cells[iCol, 1] := dbase.Playergr.GROUP_NO;
    // Go to the next record in the table
    NextKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
    // Increment the column count for the next available column in the string grid
    iCol := iCol + 1;
  End;
End;

Procedure TfrmTPDataBase.ButQuitActionExecute(Sender: TObject);
Begin
  Close; // Close the form
End;

Procedure TfrmTPDataBase.ButQuitClick(Sender: TObject);
Begin
  Close; // Close the form
End;

Procedure TfrmTPDataBase.F4KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F3KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F5KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F2KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F7KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.pKeyExecuteMsg(oSender: TObject);
Begin
  { DEVELOPEMENT: Basic procedure for testing to tell which key the user pressed,
                 to make sure they are working as expected }
  MessageDlg('The ' + (oSender As TAction).Caption + ' key was pressed.',
    mtInformation, [mbOK], 0);
End;

Procedure TfrmTPDataBase.F1KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F8KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.F6KeyExecute(Sender: TObject);
Begin
  pKeyExecuteMsg(Sender);
End;

Procedure TfrmTPDataBase.ButAddActionExecute(Sender: TObject);
Begin
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 1;
  // Save the forms Height, Width, Left Side Position and Top Position
  pSaveFormInfo;
  // Turn the Player Info Data Area ON
  pPlayerInfo(True);
  PlayernInit;
  pLoadTPFields;
  // Blank all data entry fields
  {frmTPDataBase.MELastName.Text := '';
  frmTPDataBase.MEFirstName.Text := '';
  frmTPDataBase.MEPlayerNo.Text := '';
  frmTPDataBase.MEStreet1.Text := '';
  frmTPDataBase.MEStreet2.Text := '';
  frmTPDataBase.MECity.Text := '';
  frmTPDataBase.MEState.Text := '';
  frmTPDataBase.MEZip.Text := '';
  frmTPDataBase.OvcComboBox1.Text := '';
  frmTPDataBase.MECountry.Text := '';
  frmTPDataBase.MEEmail.Text := '';
  frmTPDataBase.MEPhone1.Text := '';
  frmTPDataBase.MEPhone2.Text := '';
  frmTPDataBase.MELocalActiveDate.Text := '';
  frmTPDataBase.MEAcblActiveDate.Text := '';
  frmTPDataBase.MERank.Text := '';
  frmTPDataBase.MECatA.Text := '';
  frmTPDataBase.MECatB.Text := '';
  frmTPDataBase.MEMailCode.Text := '';
  frmTPDataBase.MEUnitNo.Text := '';
  frmTPDataBase.MEDistrictNo.Text := '';
  frmTPDataBase.MEGender.Text := '';
  frmTPDataBase.MEFeeCode.Text := '';
  frmTPDataBase.MEStartDate.Text := '';
  frmTPDataBase.MEPaidThru.Text := '';
  frmTPDataBase.MELastACBLUpdate.Text := '';
  // ==============================================================================================
  frmTPDataBase.MEMPTotal.Text := '0';
  // ------------------------------------------------------------------------------------
  frmTPDataBase.MEMPYTD.Text := '0';
  // ------------------------------------------------------------------------------------
  frmTPDataBase.MEMPMTD.Text := '0';
  // ------------------------------------------------------------------------------------
  frmTPDataBase.MEMPRecent.Text := '0';
  // ------------------------------------------------------------------------------------
  frmTPDataBase.MEMPEligibility.Text := '0';}
  // ==============================================================================================
  { pClearGroups: Clears the entries in the StringGrid SGGroups }
  pClearGroups;
  frmTPDataBase.MEGroupCode.Text := '';
  frmTPDataBase.MEGroupNo.Text := '';
  // Put the cursor in the first field
  frmTPDataBase.MELastName.SetFocus;
End;

Procedure TfrmTPDataBase.pClearGroups;
Begin
  { Clear out all previous entries in the String Grid for Groups in preparation
    for any entries from the current player record }
  SGGroups.Rows[0].Clear;
  SGGroups.Rows[1].Clear;
End;

Procedure TfrmTPDataBase.pPlayerInfo(bEnable: Boolean);
Begin
  { Allow or Deny access to the Player Information fields }
  // Set the state of ActionManager1 for the form TPDatabase GBMenu
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  // Set the state of ActionManager2 for the form TPDatabase GBMenu
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBPlayerInfo.Enabled := bEnable;
  // ----------------------------------------------------------------------
  ButDonePlayerInfo.Enabled := bEnable;
  ButDonePlayerInfo.Visible := bEnable;
  // ----------------------------------------------------------------------
  frmTPDataBase.AutoSize := bEnable;
End;

Procedure TfrmTPDataBase.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  dbase.Filno := Playern_no;
  Case iDataMode Of
    0: ; // No data Mode
    1: Begin
        If (MessageDlg('Save this new record?', mtConfirmation, [mbYes, mbNo], 0)
          = mrYes) Then Begin
          pCopyRecData;
          dBase.Add_Record;
        End
        Else pAbortChanges; // Adding a record
      End;
    2: Begin
        If (MessageDlg('Save your changes?', mtConfirmation, [mbYes, mbNo], 0) =
          mrYes) Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          pCheckEditFields;
          dbase.PutARec(Playern_no);
        End
        Else pAbortChanges; // Editing a record
      End;
    3: Begin
        If (MessageDlg('Save this new record?', mtConfirmation, [mbYes, mbNo], 0)
          = mrYes) Then Begin
          pCopyRecData;
          dbase.Add_Record;
        End
        Else pAbortChanges; // Copying a record
      End;
  End;
  // Set the forms Height and Width back to what it was
  pSetFormBack;
  // Turn the Player Info Data Area OFF
  pPlayerInfo(False);
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
End;

Procedure TfrmTPDataBase.ButEditActionExecute(Sender: TObject);
Begin
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  // Save the forms Height, Width, Left Side Position and Top Position
  pSaveFormInfo;
  // Turn the Player Info Data Area ON
  pPlayerInfo(True);
  // Put the cursor in the first field
  frmTPDataBase.MELastName.SetFocus;
End;

Procedure TfrmTPDataBase.ButDeleteActionExecute(Sender: TObject);
Begin
  If MessageDlg('Confirm deletion of this player and all groups, attendance and '
    + 'percent records linked to this player.', mtWarning, [mbYes, mbNo], 0) = mrYes Then Begin
    dbase.Filno := Playern_no;
    dbase.DelARec(Playern_no);
    pNextRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
    If ButNext.Enabled Then ButNext.SetFocus;
  End;
End;

Procedure TfrmTPDataBase.ButCopyActionExecute(Sender: TObject);
Begin
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 3;
  // Save the forms Height, Width, Left Side Position and Top Position
  pSaveFormInfo;
  // Turn the Player Info Data Area ON
  pPlayerInfo(True);
  // Put the cursor in the first field
  frmTPDataBase.MELastName.SetFocus;
End;

Procedure TfrmTPDataBase.MELastNameExit(Sender: TObject);
Begin
  { Validation of Last Name field when exiting it }
  If trim(MELastName.Text) = '' Then Begin
    If MessageDlg('Last Name must NOT be blank! ' + #13 + #10 + '' + #13 + #10 +
      '        Correct this error?', mtError, [mbYes, mbNo], 0) = MRNO Then Begin
      pAbortChanges;
    End
    Else MELastName.SetFocus;
  End;
End;

Procedure TfrmTPDataBase.pSetFormBack;
Begin
  // Set the forms Height and Width back to what it was
  frmTPDataBase.Height := iTPFormHeight;
  frmTPDataBase.Width := iTPFormWidth;
  frmTPDataBase.Left := iTPFormLeft;
  frmTPDataBase.Top := iTPFormTop;
End;

Procedure TfrmTPDataBase.pSaveFormInfo;
Begin
  // Save the forms Height, Width, Left Side Position and Top Position
  iTPFormHeight := frmTPDataBase.Height;
  iTPFormWidth := frmTPDataBase.Width;
  iTPFormLeft := frmTPDataBase.Left;
  iTPFormTop := frmTPDataBase.Top;
End;

Procedure TfrmTPDataBase.pAbortChanges;
Begin
  { Abort the edits to the current record }
  // Set the forms Height and Width back to what it was
  pSetFormBack;
  // Turn the Player Info Data Area OFF
  pPlayerInfo(False);
  //-------------------------------------------------------------------------------------
  If iDataMode = 1 Then Begin // If adding a record
    // Think about returning the previous record to the screen??????????
    // What if the database is empty???????????
    ClearKey(IdxKey[Playern_no, 1]^);
    { Find the first available record and load it's values to the fields on the screen }
    pNextRecord;
  End
  Else
    { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    pLoadTPFields;
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
End;

Function TfrmTPDataBase.Status_OK(Const Fno: Integer): Boolean;
Var
  TOK: Boolean;
Begin
  TOK := True;
  TOK := (UsedRecs(DatF[Fno]^) > 0);
  Status_OK := TOK
End;

Procedure TfrmTPDataBase.MECountryEnter(Sender: TObject);
Begin
  LMDMessageHintCountry.ShowControlMessage('Country Code' + #13 + #10 +
    'Press F1 for a list of Countries', MECountry);
End;

Procedure TfrmTPDataBase.MECountryExit(Sender: TObject);
Begin
  LMDMessageHintCountry.HideMessage;
End;

Procedure TfrmTPDataBase.pCheckEditFields;
Begin
  { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
    in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
    with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
    routines in DBASE.PAS, to update the actual PLAYERN.DAT table.}
  If trim(frmTPDataBase.MELastName.Text) <> Trim(Playern.LAST_NAME) Then
    Playern.LAST_NAME := frmTPDataBase.MELastName.Text;
  If trim(frmTPDataBase.MEFirstName.Text) <> trim(Playern.FIRST_NAME) Then
    Playern.FIRST_NAME := frmTPDataBase.MEFirstName.Text;
  If trim(frmTPDataBase.MEPlayerNo.Text) <> trim(Playern.PLAYER_NO) Then
    Playern.PLAYER_NO := frmTPDataBase.MEPlayerNo.Text;
  If trim(frmTPDataBase.MEStreet1.Text) <> trim(Playern.STREET1) Then
    Playern.STREET1 := frmTPDataBase.MEStreet1.Text;
  If trim(frmTPDataBase.MEStreet2.Text) <> trim(Playern.STREET2) Then
    Playern.STREET2 := frmTPDataBase.MEStreet2.Text;
  If trim(frmTPDataBase.MECity.Text) <> trim(Playern.CITY) Then
    Playern.CITY := frmTPDataBase.MECity.Text;
  // STATE   ----------------------------------------------------------------------
  {If trim(frmTPDataBase.MEState.Text) <> trim(Playern.STATE) Then
    Playern.STATE := frmTPDataBase.MEState.Text;}
  If trim(frmTPDataBase.LMDHeaderListComboBox2.Text) <> trim(Playern.STATE) Then
    Playern.STATE := frmTPDataBase.LMDHeaderListComboBox2.Text;
  // ------------------------------------------------------------------------------
  If trim(frmTPDataBase.MEZip.Text) <> trim(Playern.ZIP) Then
    Playern.ZIP := frmTPDataBase.MEZip.Text;
  // COUNTRY ----------------------------------------------------------------------
  {If trim(frmTPDataBase.MECountry.Text) <> trim(Playern.COUNTRY) Then
    Playern.COUNTRY := frmTPDataBase.MECountry.Text;}
  If trim(frmTPDataBase.LMDHeaderListComboBox3.Text) <> trim(Playern.COUNTRY) Then
    Playern.COUNTRY := frmTPDataBase.LMDHeaderListComboBox3.Text;
  // ------------------------------------------------------------------------------
  If trim(frmTPDataBase.MEEmail.Text) <> trim(Playern.EMAIL) Then
    Playern.EMAIL := frmTPDataBase.MEEmail.Text;
  // PHONE   ----------------------------------------------------------------------
  { Both Phone Numbers are stored in one field, Phone Number 1 first 10 characters,
    Phone Number 2 second 10 characters}
  If ((trim(frmTPDataBase.MEPhone1.Text) <> trim(Copy(Playern.PHONE, 1, 10))) Or
    (trim(frmTPDataBase.MEPhone2.Text) <> trim(Copy(Playern.PHONE, 11, 10)))) Then
    Playern.PHONE := frmTPDataBase.MEPhone1.Text + frmTPDataBase.MEPhone1.Text;
  // ------------------------------------------------------------------------------
  If trim(frmTPDataBase.MELocalActiveDate.Text) <> trim(Playern.LOCAL_DATE) Then
    Playern.LOCAL_DATE := frmTPDataBase.MELocalActiveDate.Text;
  If trim(frmTPDataBase.MEAcblActiveDate.Text) <> trim(Playern.ACBL_DATE) Then
    Playern.ACBL_DATE := frmTPDataBase.MEAcblActiveDate.Text;
  If trim(frmTPDataBase.MERank.Text) <> trim(Playern.ACBL_RANK) Then
    Playern.ACBL_RANK := frmTPDataBase.MERank.Text;
  If trim(frmTPDataBase.MECatA.Text) <> trim(Playern.CAT_A) Then
    Playern.CAT_A := frmTPDataBase.MECatA.Text;
  If trim(frmTPDataBase.MECatB.Text) <> trim(Playern.CAT_B) Then
    Playern.CAT_B := frmTPDataBase.MECatB.Text;
  If trim(frmTPDataBase.MEMailCode.Text) <> trim(Playern.MAIL_CODE) Then
    Playern.MAIL_CODE := frmTPDataBase.MEMailCode.Text;
  If trim(frmTPDataBase.MEUnitNo.Text) <> trim(Playern.UNIT_NO) Then
    Playern.UNIT_NO := frmTPDataBase.MEUnitNo.Text;
  If trim(frmTPDataBase.MEDistrictNo.Text) <> trim(Playern.DISTRICT_NO) Then
    Playern.DISTRICT_NO := frmTPDataBase.MEDistrictNo.Text;
  If trim(frmTPDataBase.MEGender.Text) <> trim(Playern.GENDER) Then
    Playern.GENDER := frmTPDataBase.MEGender.Text;
  If trim(frmTPDataBase.MEFeeCode.Text) <> trim(Playern.FEE_TYPE) Then
    Playern.FEE_TYPE := frmTPDataBase.MEFeeCode.Text;
  If trim(frmTPDataBase.MEStartDate.Text) <> trim(Playern.START_DATE) Then
    Playern.START_DATE := frmTPDataBase.MEStartDate.Text;
  If trim(frmTPDataBase.MEPaidThru.Text) <> trim(Playern.PAID_THRU) Then
    Playern.PAID_THRU := frmTPDataBase.MEPaidThru.Text;
  If trim(frmTPDataBase.MELastACBLUpdate.Text) <> trim(Playern.ACBL_UPDATE_DATE) Then
    Playern.ACBL_UPDATE_DATE := frmTPDataBase.MELastACBLUpdate.Text;
End;

Procedure TfrmTPDataBase.pCopyRecData;
Begin
  { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
    The PLAYERN dataset is used by all record saving, adding and copying routines in DBASE.PAS,
    to update the actual PLAYERN.DAT table. }
  Playern.LAST_NAME := frmTPDataBase.MELastName.Text;
  Playern.FIRST_NAME := frmTPDataBase.MEFirstName.Text;
  Playern.PLAYER_NO := frmTPDataBase.MEPlayerNo.Text;
  Playern.STREET1 := frmTPDataBase.MEStreet1.Text;
  Playern.STREET2 := frmTPDataBase.MEStreet2.Text;
  Playern.CITY := frmTPDataBase.MECity.Text;
  Playern.STATE := frmTPDataBase.LMDHeaderListComboBox2.Text;
  Playern.ZIP := frmTPDataBase.MEZip.Text;
  Playern.COUNTRY := frmTPDataBase.LMDHeaderListComboBox3.Text;
  Playern.EMAIL := frmTPDataBase.MEEmail.Text;
  { Both Phone Numbers are stored in one field, Phone Number 1 first 10 characters,
    Phone Number 2 second 10 characters}
  Playern.PHONE := frmTPDataBase.MEPhone1.Text + frmTPDataBase.MEPhone1.Text;
  Playern.LOCAL_DATE := frmTPDataBase.MELocalActiveDate.Text;
  Playern.ACBL_DATE := frmTPDataBase.MEAcblActiveDate.Text;
  Playern.ACBL_RANK := frmTPDataBase.MERank.Text;
  Playern.CAT_A := frmTPDataBase.MECatA.Text;
  Playern.CAT_B := frmTPDataBase.MECatB.Text;
  Playern.MAIL_CODE := frmTPDataBase.MEMailCode.Text;
  Playern.UNIT_NO := frmTPDataBase.MEUnitNo.Text;
  Playern.DISTRICT_NO := frmTPDataBase.MEDistrictNo.Text;
  Playern.GENDER := frmTPDataBase.MEGender.Text;
  Playern.FEE_TYPE := frmTPDataBase.MEFeeCode.Text;
  Playern.START_DATE := frmTPDataBase.MEStartDate.Text;
  Playern.PAID_THRU := frmTPDataBase.MEPaidThru.Text;
  Playern.ACBL_UPDATE_DATE := frmTPDataBase.MELastACBLUpdate.Text;
End;

Procedure TfrmTPDataBase.ComboBoxChange(Sender: TObject);
Var
  iForLoop, iPos, iTxtLen: Integer;
  sTempStr: String;
Begin
  { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
  If (Sender As TLMDHeaderListComboBox).Focused Then Begin
    With (Sender As TLMDHeaderListComboBox) Do Begin
      sTempStr := Text;
      For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
        iPos := Pos(sTempStr, ListBox.Items[iForLoop]);
        iTxtLen := Length(sTempStr);
        If (iPos = 1) Or (iPos = 2) Then Begin
          ListBox.ItemIndex := iForLoop;
          //SelStart := Length(sTempStr);
          //SelLength := (SelLength - Length(sTempStr));
          Break;
        End;
      End;
    End;
  End;
End;

Procedure TfrmTPDataBase.ComboBoxKeyPress(Sender: TObject; Var Key: Char);
Var
  iTextLength, iSelLength: Integer;
  sText: String;
Begin
  { COMBOBOXKEYPRESS: TLMDHeaderListComboBox OnKeyPress Routine }
  {
    If Key = #8 Then Begin
      iTextLength := Length((Sender As TLMDHeaderListComboBox).Text);
      iSelLength := ((Sender As TLMDHeaderListComboBox).SelLength - 1);
      sText := (Sender As TLMDHeaderListComboBox).Text;
      (Sender As TLMDHeaderListComboBox).Text := Copy(sText, 1, (iTextLength - iSelLength));
      ComboBoxChange(Sender);
      Key := #0;
    End;
  }
End;

Procedure TfrmTPDataBase.ComboBoxEnter(Sender: TObject);
Begin
  { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    DroppedDown := True;
    ListBox.AlternateColors := True;
    Refresh;
    If ListBox.ItemIndex <> -1 Then ListBox.ItemIndex := fFindCBItemIndex(Sender, Text)
    Else ListBox.ItemIndex := 1;
  End;
End;

Function TfrmTPDataBase.fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
Var
  iForLoop: Integer;
  sTempStr: String;
Begin
  { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
    TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
  Result := 1;
  With (cbName As TLMDHeaderListComboBox) Do Begin
    sTempStr := Text;
    For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
      If Pos(sTempStr, ListBox.Items[iForLoop]) = 1 Then Begin
        Result := iForLoop;
        //ListBox.ItemIndex := iForLoop;
        //SelStart := Length(sTempStr);
        //SelLength := (SelLength - Length(sTempStr));
        Break;
      End;
    End;
  End;
End;

Procedure TfrmTPDataBase.ComboBoxExit(Sender: TObject);
Var
  sResult: String;
Begin
  { COMBOBOXEXIT: TLMDHeaderListComboBox OnExit Routine }
  { If the TLMDHEADERLISTCOMBOBOX we are EXITING is NOT BLANK, then store the complete entry for the
    selected item in the variable sResult. The entries for the State ComboBox are:
    CODE;NAME;COUNTRY. }
  If (Sender As tLMDHeaderListComboBox).Text <> '' Then Begin
    sResult := (Sender As tLMDHeaderListComboBox).ListBox.Items.Strings[(Sender As
      tLMDHeaderListComboBox).ListBox.ItemIndex];
    { If exiting the State ComboBox "LMDHeaderListComboBox2", and a state is selected, then get the
      Country Code from the last two characters in sResult. Then change the Country ComboBox entry
      to this Country Code. }
    If (Sender As tLMDHeaderListComboBox).Name = 'LMDHeaderListComboBox2' Then
      LMDHeaderListComboBox3.Text := Copy(sResult, Length(sResult) - 1, 2);
  End;
  { If the TLMDHEADERLISTCOMBOBOX we are EXITING is BLANK, then leave the Country Code ComboBox
    alone. }
  (Sender As tLMDHeaderListComboBox).DroppedDown:=False;
End;

function duplicate_player(const number: String; Fno,Kno: integer; Func: String) : boolean;
var
   tempkey : keystr;
   trecno  : longint;
   Pn      : String[7];
begin
  {
  duplicate_player := false;
  pn := number;
  if not player_check(pn,pnPoundACBL) then exit;
  tempkey := Pad(player_key(number),7);
  findkey(IdxKey[Fno, Kno]^, trecno, tempkey);
  if ok
  and ((trecno <> recno[Playern_no]) or ((func = 'A') or (func = 'C'))) then
    duplicate_player := true;
  ok := true;
  }
end;
procedure TfrmTPDataBase.LMDHeaderListComboBox3Enter(Sender: TObject);
begin
  { TLMDHeaderListComboBox3 OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    ListBox.AlternateColors := True;
  End;
end;

End.

