{$I debugdir.inc}
unit utility;
{$define filecopy}
{$define teamgrid}

interface

procedure CallUtility;

implementation
{.$define test}
uses
  SysUtils,
  StStrS,
  FOpen,
  YesNoBoxU,
  VListBox,
  StrBoxU,
  txtview1,
  formedit,
  CSdef,
  version,
  mtables,
  pvio,
  util1,
  history,
  utilityu,
  util2;

{$I ststrs.dcl}
procedure PleaseWait;
begin
  PlsWait;
end;

const
  MaxIndex = 2000;
  topic    : word = 0;

var
  current_dir : string;
  count       : word;
  key         : word;
  Search_Rec  : TSearchRec;
  key_byte    : byte;
  key_char    : word;
  text_line : string[128];
  text_file : text;
  done : boolean;
  printName      : string;

{$ifdef test}
procedure CheckBrowseStatus(const Status: word);
begin
  if Status = 0 then exit;
  ErrBox('Browse status error = '+Long2Str(Status),MC,0);
end;
{$endif}

const
     MaxPages   = 20;
type
   TPageList    = Array[1..MaxPages,1..2] of String[4];

procedure PrintTheFile(const printname: String; const Pages: word;
          const PagePtr : Pointer);
var
   PageList     : ^TPageList;
   CurrentPList : word;
   Printing     : boolean;
   EndPageW     : word;
   EndPage      : String[4];
   NeedFF       : boolean;
begin
  PleaseWait;
  if Assigned(PagePtr) then PageList := PagePtr;
  CurrentPList := 1;
  NeedFF := false;
  assignFile(text_file,printname);
  {$I-} reset(text_file); {$I+}
  if ioresult = 0 then begin
    done := false;
    Printing := Pages < 1;
    OpenDestination(2,0,80);
    while not eof(text_file) and not done do begin
      {$I-}readln(text_file,text_line);{$I+}
      CheckIOResult(printname);
      for count := 1 to length(text_line) do begin
        case text_line[count] of
          '�' : text_line[count] := '|';
          '�',
          '�',
          '�',
          '�',
          '�' : text_line[count] := '-';
        end;
      end;
      if Printing and (CurrentPList <= Pages) then begin
        if Pos('PAGE '+EndPage,text_line) > 0 then begin
          Printing := false;
          Inc(CurrentPList);
          if CurrentPList > Pages then done := true;
          if NeedFF then PrtLn(#13#12);
          NeedFF := false;
        end;
      end;
      if not Printing and not done then begin
        if Pos('PAGE '+PageList^[CurrentPList,1],text_line) > 0 then begin
          Printing := true;
          Str2WordS(PageList^[CurrentPList,2],EndPageW);
          EndPage := Long2Str(EndPageW+1);
          if text_line[1] = #12 then Delete(text_line,1,1);
        end;
      end;
      if Printing then begin
        if NeedFF and (Pos('(PAGE ',text_line) > 40) then PrtLn(#13#12);
        PrtLn(text_line+#13#10);
        NeedFF := Pos(#12,text_line) < 1;
      end;
    end;
    closeFile(text_file);
    if NeedFF then PrtLn(#13#12);
    Close_Destination;
  end;
  EraseBoxWindow;
end;

procedure PrintStuff;
var
   DosError : integer;
begin
  DosError := FindFirst(printname,faAnyFile,Search_Rec);
  FindClose(Search_Rec);
  if DosError <> 0 then begin
    Search_Rec.Size := 0;
    RingBell(false);
    ErrBox('File '+printname+' could not be found.',MC,0);
    MainHalt;
  end;
  MsgBox('Approximately '+Long2Str(Search_Rec.Size div 2000 + 1)+' pages will be printed.'
    +#13#13'Ready printer and press ENTER to begin printing',MC,0);
  PrintTheFile(printname,0,nil);
  MainHalt;
end;

{$ifdef filecopy}
var
   apath      : ShortString;

const
     EditRec : PEdit = nil;

function CPostEdit(const ERecordP : PEditRecord; const Direction : ShortInt;
         var Ifunc: byte) : integer; Far;
var
   xpath : String;
begin
  CPostEdit := 0;
  with ERecordP^ do case EditSeq of
    1: case ifunc of
      2: begin
        apath := CFG.GFpath;
        CPostEdit := 1;
      end;
      3: begin
        xpath := apath;
        if GetFolder(xpath,'Game file source location',true,'GameBackupLoc','A:\') then
          apath := xpath
        else CPostEdit := 1;
      end;
    end;
    2: if Ifunc = 0 then Ifunc := 13;
  end;
end;

procedure GameFileCopy;
const
     PickMaxY = 10;

label
     path_again,
     hist_again,
     path_againa;

var
   path       : ShortString;
   afile      : ShortString;
   gfile      : ShortString;
   len,
   ifunc      : byte;
   esc        : boolean;
   path_ok    : boolean;
   Count      : word;
   hsize      : word;
   PickHsize  : word;
   YLow       : byte;
   Columns    : byte;
   Name       : String;
   Schar      : Char;
   Typ        : String;
   noroom     : boolean;
   FreeSpace  : Int64;
   FSize      : LongInt;
   Key        : word;
   PickChar   : word;
   HDate      : String;
   Search_Rec : TSearchRec;
   DosError   : integer;
   Histok     : boolean;

procedure get_space;
begin
  FreeSpace := CheckDiskSpace(Path);
  {$ifndef win32}
  FastTextWindow('Free space left on target drive: '+Long2Str(FreeSpace)
    +' Bytes      ',6,2);
  {$endif}
end;

procedure backup(const filename:String);
begin
  gfile:=path+filename;
  afile:=apath+filename;
  pcopyfile(afile,gfile,false,esc,false);
end;

procedure copy_game;
var
   F  : file;
   gtime : LongInt;
   atime : LongInt;
//   Search_Rec : TSearchRec;
begin
  afile:=apath+name+'.AC'+schar;
  gfile:=path+name+'.AC'+schar;
  AssignFile(F,gfile);
  {$I-} Reset(F); {$I+}
  if IOResult = 0 then begin
    gtime := GetFTime(gfile);
    CloseFile(F);
    AssignFile(F,afile);
    {$I-} Reset(F); {$I+}
    if IOResult = 0 then begin
      atime := GetFTime(afile);
      CloseFile(F);
      if atime <= gtime then begin
        RingClear(false);
        esc := YesnoBox(esc,false,'Newer or same version of '
          +name+' ('+schar+')'#13'exists at target location.'#13+
          'Copy anyway',0,MC,nil);
        if not esc then exit;
      end;
    end;
  end;
  get_space;
  DosError := FindFirst(gfile,faAnyFile,Search_Rec);
  FindClose(Search_rec);
  if DosError = 0 then Inc(FreeSpace,Search_Rec.Size);
  DosError := FindFirst(afile,faAnyFile,Search_Rec);
  FindClose(Search_rec);
  if (DosError = 0) and (FreeSpace < Search_Rec.Size) then begin
    ErrBox('Insufficient space on target drive.'#13+name+' ('
      +schar+') NOT copied',MC,0);
    noroom:=true;
    exit;
  end;
  BoxMsg('Copying '+name+' ('+schar+')','',MC);
  backup(name+'.AC'+schar);
  EraseBoxWindow;
  Inc(count);
  MsgBox(Long2Str(count)+' Game file(s) copied   ',MC,0);
end;

begin
  MakeMainWindow('Copy game files');
  {$ifdef office}
  CheckReadOnlyCFG;
  {$endif}
  apath:='A:';
  path := CFG.GFpath;
  count:=0;
  EditInit(EditRec,2,2,'','');
  EditAddPath(EditRec,1,'Location of Game files to copy',apath,40,1,true,
    nil,CPostEdit,1,2,0);
  EditAddLine(EditRec,4,'Press F2 to select current game file path (ESC to Quit)',
    false,0,1,2);
  EditAddLine(EditRec,0,'Press F3 to browse',false,0,1,2);
  EditAddPath(EditRec,2,'Target location of Game files',path,40,1,true,
    nil,CPostEdit,2,2,0);
  EditAddLine(EditRec,5,'',false,0,3,2);
  while true do begin
path_again:
    EditShow(EditRec,Esc,ifunc,[],13);
    if esc then MainHalt;
    apath := fixpath(apath);
    if CheckDrive(esc,apath,false) <> 0 then goto path_again;
    path := fixpath(path);
    if CheckDrive(esc,path,true) <> 0 then goto path_again;
    if path = apath then begin
      ErrBox('Cannot copy to the same location',MC,0);
      goto path_again;
    end;
    Hsize:=HistInit(apath,HsortName,'*.AC?',Histok);
    if hsize = 0 then begin
      HistDone;
      ErrBox('No game files exist at '+apath,MC,0);
      goto path_again;
    end;
    if Hsize > 84 then YLow := 2
    else if Hsize < 57 then YLow := PickMaxY
    else YLow := 23 - ((Hsize + 3) div 4);
    if Hsize > 42 then Columns := 4
    else Columns := (Hsize - 1) div 14 + 1;
    get_space;
hist_again:
    PickHsize:= HistPick('SPACE=tag/untag, F8=tag/untag ALL, F9=accept, ESC=quit',
      true,false,PickChar);
    noroom:=false;
    count:=0;
    if (hsize = 1) and (PickHsize = 1) then begin
      HistGet(HistGetNext, Name, Schar, Typ,Hdate);
      if YesNoBox(esc,true,'One Game File found.'#13'Press ENTER to copy '
        +Name+' ('+schar+'), ESC to quit',0,MC,nil) then begin
        copy_game;
        if not noroom then HistUntag;
      end;
    end
    else while (Pickhsize > 0) and (not noroom) do begin
      HistGet(HistGetNext, Name, Schar, Typ,HDate);
      copy_game;
      Dec(PickHsize);
      if not noroom then HistUntag;
    end;
    get_space;
    if noroom then goto hist_again;
    HistDone;
  end;
end;
{$endif}

var
   IndexLines   : Array[1..MaxIndex] of String;
   IndexSize    : word;
   esc          : boolean;
   FindStr      : String;
   Pages        : word;
   ViewFileName : String;
   ExaminePath  : String;
   PageList     : TPageList;
   IndexName    : String;
   newchoice     : word;
   dopdf         : byte;

procedure GetFindStr;
var
   ifunc        : byte;
   len          : byte;
begin
  ifunc := 0;
  StrBox(esc,ifunc,'','Text to find',FindStr,40,len,1,1,topic,MC,nil);
  if esc then FindStr := '';
end;

function FindTheString(const choice: integer): integer;
var
   j    : word;
   found  : boolean;
begin
  Result := 0;
  if Length(FindStr) < 1 then exit;
  j := Choice+1;
  repeat
    found := Pos(FindStr,StUpCase(IndexLines[j])) > 0;
    if not found then Inc(j);
  until found or (j >= IndexSize);
  if found then Result := j
  else ErrBox(FindStr+' could NOT be found.',MC,0);
end;

function GetFileName(const choice: integer; var dopdf: byte): integer;
var
   j    : word;
   found  : boolean;
   k      : byte;
   pgloc  : byte;
   pagetxt : String[5];
   StartEnd  : byte;
   StartLoc  : byte;
label
     findagain;
begin
  Result := 0;
  j := Choice;
findagain:
  Pages := 0;
  ViewFileName := '';
  pgloc := Pos('pg.',IndexLines[j]);
  dopdf := 0;
  if pgloc < 20 then begin
    pgloc := Pos('http:',IndexLines[j]);
    if pgloc > 0 then dopdf := 2
    else begin
      pgloc := pos('.PDF',IndexLines[j]);
      if pgloc > 10 then dopdf := 1;
    end;
  end;
  found := false;
  if (pgloc > 20) or (dopdf > 0) then begin
    if dopdf < 2 then begin
      StartLoc := pgloc-6;
      While (StartLoc > 2) and
        not (IndexLines[j][StartLoc] in [' ','.']) do Dec(StartLoc);
      ViewFileName := ExtractWordS(1,Copy(IndexLines[j],StartLoc,13),' .');
    end
    else ViewFileName := Trim(Copy(IndexLines[j],pgloc,150));
    found := not empty(ViewFileName);
    if found and (dopdf < 2) then begin
      k := Pos(ViewFileName,Copy(IndexLines[j],StartLoc,80))+Length(ViewFileName)
        +StartLoc-1;
      if IndexLines[j][k] = '.' then ViewFileName := ViewFileName+'.'
        +ExtractWordS(1,Copy(IndexLines[j],k,4),' .');
      found := FileExists(ExaminePath+ViewfileName);
    end;
    if found and (dopdf < 1) then begin
      k := pgloc+3;
      StartEnd := 0;
      repeat
        PageTxt := ExtractWordS(1,Copy(IndexLines[j],k,8),' .-&,');
        if not Empty(PageTxt) then begin
          if StartEnd < 1 then Inc(Pages);
          Inc(StartEnd);
          PageList[Pages,StartEnd] := PageTxt;
          k := Pos(PageTxt,Copy(IndexLines[j],k,8))+k;
          Inc(k,Length(PageTxt));
          if (StartEnd = 1) and ((k > Length(IndexLines[j]))
            or (IndexLines[j][k-1] <> '-')) then begin
            PageList[Pages,2] := PageTxt;
            StartEnd := 0;
          end;
          if StartEnd > 1 then StartEnd := 0;
        end;
      until (Pages >= MaxPages) or Empty(PageTxt)
        or (k > length(IndexLines[j]));
      if (Pages > 0) and (StartEnd = 1) then
        PageList[Pages,2] := PageList[Pages,1];
    end;
    if not found then ViewFileName := '';
  end;
  if not found and (j = Choice) then begin
    Inc(j);
    goto findagain;
  end;
  if Found then Result := j;
end;

function GetIndexChar(const key: word; const item: integer): integer; Far;
begin
  newchoice := item;
  case key of
    2: begin                {F2 = Find}
      GetFindStr;
      if Length(FindStr) > 0 then newchoice := FindTheString(item);
    end;
    3: begin                {F3 = Repeat Find}
      if Length(FindStr) < 1 then GetFindStr;
      if Length(FindStr) > 0 then newchoice := FindTheString(item);
    end;
    7: begin                {F7 = print this file}
      newchoice := GetFileName(item,dopdf);
      if not Empty(ViewfileName) then case dopdf of
        0: PrintTheFile(ExaminePath+ViewfileName,Pages,@PageList);
        1: Runthefile(CFG.LoadPath+'TECH\'+ViewFileName);
        2: Runthefile(ViewFileName);
      end;
    end;
    17: begin               {Shift-F7 = print pick list file}
      PrintTheFile(ExaminePath+IndexName,0,nil);
    end;
    9: begin                {F9 = view this file}
      newchoice := GetFileName(item,dopdf);
      if not Empty(ViewFileName) then case dopdf of
        0: begin
          if Pages > 0 then PositionStr := 'PAGE '+PageList[1,1]
          else PositionStr := '';
          ViewFile('',ExaminePath+ViewfileName,'');
        end;
        1: Runthefile(CFG.LoadPath+'TECH\'+ViewFileName);
        2: Runthefile(ViewFileName);
      end;
    end;
  end;
  if (newchoice > 0) and (newchoice <> item) then Result := 4
  else Result := 0;
end;

function IndexLine(const Item: integer): String; Far;
begin
  IndexLine := IndexLines[Item];
end;

procedure DoTech(const IndexFileName: String);
var
   TextLine     : String[80];
   F            : Text;
   xtitle       : String[78];
   Choice       : integer;
   FirstChoice  : word;
label
     Pick_Again;
begin
  MakeMainWindow('ACBLscore Examine '+IndexFileName);
  topic := 0;
  ExaminePath := JustPathNameS(IndexFileName);
  if (IndexFileName[1] <> '\') and (IndexFileName[2] <> ':') then
    ExaminePath := CFG.LoadPath+ExaminePath;
  ExaminePath := fixpath(ExaminePath);
  IndexName := JustFileNameS(IndexFileName);
//  FillChar(IndexLines,SizeOf(IndexLines),0);
  IndexSize := 0;
  FindStr := '';
  AssignFile(F,ExaminePath+IndexName);
  {$I-} ReSet(F); {$I+}
  if IOResult <> 0 then begin
    RingClear(false);
    ErrBox('File '+IndexFileName+' could not be found.',MC,0);
    MainHalt;
  end;
  while not Eof(F) and (IndexSize < MaxIndex) do begin
    ReadLn(F,TextLine);
    if not Empty(Copy(TextLine,1,5)) then
      TextLine := Trim(Copy(TextLine,1,75))
    else TextLine := TrimTrailS(Copy(TextLine,6,80));
    if not Empty(TextLine) then begin
      Inc(IndexSize);
      IndexLines[IndexSize] := TextLine;
    end;
  end;
  CloseFile(F);
  FirstChoice := 1;
  Choice := 1;
  xtitle := 'F2=Find, F3=Find Next, F7=Print, Shift-F7=Print Index, <ENTER>=View';
Pick_again:
  repeat
    VUseFixedFont := true;
    Choice := VPickChoice(IndexSize,Choice,FirstChoice,xtitle,xtitle,
      IndexLine,nil,GetindexChar,MC,Topic,false);
    if choice < 0 then Choice := newchoice;
  until choice = 0;
  MainHalt;
end;

{$ifdef teamgrid}
function PostBracket(const ERecordP : PEditRecord; const Direction : ShortInt;
         var Ifunc: byte) : integer; Far;
begin
  PostBracket := 0;
  if Ifunc = 0 then Ifunc := 13;
end;

procedure PrintTeamGrid;

procedure DoBracketSheet;
const
     nteams      : smallint = 16;
     matches     : smallint = 4;
     copies      : smallint = 1;
var
   ifunc        : byte;

begin
  copies := 1;
  EditInit(EditRec,8,27,'','');
  EditAddInt(EditRec,1,nteams,5,256,3,'Number of teams',nil,nil,2,2,0);
  EditAddInt(EditRec,2,matches,3,8,2,'Number of rounds',nil,nil,2,2,0);
  EditAddInt(EditRec,3,copies,1,20,2,'Number of copies to print',nil,PostBracket,2,2,0);
  EditAddLine(EditRec,10,'',false,0,1,2);
  EditShow(EditRec,Esc,ifunc,[9],13);
  EditDone(EditRec);
  if esc then MainHalt;
  PrintBracketSheet(nteams,matches,copies,-1,'',false);
end;

procedure DoSwissGrid;
var
   ifunc        : byte;
   CurLineWidth : word;
   CurColWidth  : word;
   ColWidths    : word;
   CaptWidth    : word;
   j            : word;
   n            : word;
   MaxPageSize  : word;

const
     LineSpace  = 8; {lines per inch}
     TeamLines  = 6; {Num lines of vertical bars between dashed lines}
     LineWidth  = 131; {Maximum chars / line}
     MinCaptWidth = 30; {minimum width for team name}
     BreakCaptWidth = 50; {break width for team name}
     MaxCaptWidth = 70; {maximum width for team name}
     MaxColWidth = 15; {maximum column width}
     MinColWidth = 11; {minimum column width}
     nteams      : smallint = 0;
     matches     : smallint = 8;
     breakpages  : smallint = 3;

procedure printbars(const Fill: Char);
var
   k            : word;
begin
  ReportLine(1,pnone,1,'|'+CharStr(Fill,CaptWidth-1));
  for k := 1 to matches do
    ReportLine(0,pnone,1,'|'+CharStr(Fill,CurColWidth-1));
  ReportLine(0,pnone,1,'|');
end;

procedure PrintHeader;
begin
  NewLine(6);
  ReportLine(1,pnone,1,CharStr('_',CurLineWidth));
end;

begin
  EditInit(EditRec,8,17,' Press F9 when done ','');
  EditAddInt(EditRec,1,nteams,1,800,4,'Number of teams                      ',
    nil,nil,2,2,0);
  EditAddInt(EditRec,2,matches,1,12,3,'Number of columns for matches and rank',
    nil,nil,2,2,0);
  if CFG.SheetSize < 1 then begin
    EditAddLine(EditRec,10,'Number of pages to print before',false,0,2,2);
    EditAddInt(EditRec,4,breakpages,0,20,2,
      'page break (0 = continuous)            ',nil,nil,1,2,0);
  end
  else breakpages := 1;
  EditAddLine(EditRec,10,'',false,0,1,2);
  EditShow(EditRec,Esc,ifunc,[9],13);
  EditDone(EditRec);
  if esc then MainHalt;
  if breakpages < 1 then MaxPageSize := 0
  else MaxPageSize := 11 * LineSpace * (breakpages - 1) + LineSpace * 9 - 1;
  CurColWidth := MaxColWidth;
  repeat
    ColWidths := matches * CurColWidth;
    if ColWidths < LineWidth then CaptWidth := LineWidth - ColWidths
    else CaptWidth := 0;
    if CaptWidth < BreakCaptWidth then Dec(CurColWidth);
  until (CaptWidth >= BreakCaptWidth);
  if CurColWidth < MinColWidth then begin
    CurColWidth := MinColWidth;
    repeat
      ColWidths := matches * CurColWidth;
      if ColWidths < LineWidth then CaptWidth := LineWidth - ColWidths
      else CaptWidth := 0;
      if CaptWidth < MinCaptWidth then Dec(CurColWidth);
    until (CaptWidth >= MinCaptWidth);
  end;
  CaptWidth := MinWord(CaptWidth,MaxCaptWidth);
  CurLineWidth := CaptWidth + ColWidths + 1;
  PleaseWait;
  OpenDestination(2,0,132);
  SetLineSpace(LineSpace);
  ReportLine(0,pnorm,0,' ');
  PrintHeader;
  for n := 1 to nteams do begin
    if (MaxPageSize > 0) and (RepLine > MaxPageSize) then begin
      NewPage;
      PrintHeader;
    end;
    for j := 1 to TeamLines do printbars(' ');
    printbars('_');
  end;
  Close_Destination;
  EraseBoxWindow;
end;

const
     GridOpt    : Array[1..2] of Pchar =
                ('1  Swiss Team Grid',
                 '2  Knockout Bracket Sheet');
var
   opt  : word;

begin
  MakeMainWindow('Print Team grid');
//  ShowPrinter;
  Opt := PickChoice(2,1,'item to print ',@GridOpt,MC,false,0);
  case Opt of
    1: DoSwissGrid;
    2: DoBracketSheet;
    else MainHalt;
  end;
  MainHalt;
end;
{$endif}

procedure Browse;
var
   Fname : String;
   Status : word;
   ExitKey : char;
begin
  if ParamCount < 2 then exit;
  Fname := StUpCase(ParamStr(2));
  if not FileExists(Fname) then exit;
  MakeMainWindow('Browse '+Fname);
  ViewFile('',Fname,'');
  MainHalt;
end;

procedure CallUtility;
var
   Params       : String;
begin
  IniName := GetCurrentDir+'\'+DefIniName;
  AppName := 'WUTILITY';
  AllowMinimize := true;
  Key := 0;
  if ParamCount > 0 then begin
    Params := StUpCase(ParamStr(1));
    if Params = '35' then begin
      Key := 35;
      MakeMainWindow('ACBLscore print manual');
      printname := CFG.LoadPath+'MANUAL';
    end
    else if Params = '48' then begin
      Key := 48;
      MakeMainWindow('ACBLscore print new features');
      printname := CFG.LoadPath+'README';
    end
    else if Params = '49' then begin
      Key := 49;
      MakeMainWindow('ACBLscore print new features');
      printname := CFG.LoadPath+'TREADME';
    end;
    if Key > 0 then PrintStuff
//    else if Params = 'PATH' then SetPath
    else if Params = 'COPY' then GameFileCopy
    else if Params = 'TEAMGRID' then PrintTeamGrid
    else if Params = 'BROWSE' then Browse
    else DoTech(Params);
  end;
  MainHalt;
end;

end.