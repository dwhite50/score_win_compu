unit CharUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure TestV(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses
    VListBox,
    util2;

const
     npicks  = 255;
var
   picks     : Array[1..npicks] of boolean;

procedure GetFieldSelect(const Item: integer; var Selected: boolean);
begin
  Selected := Picks[Item];
end;

function GetFieldPick(const Item: integer): String; Far;
begin
  GetFieldPick := numcvt(Item,3,false)+': >'+char(Item)+'<';
end;

function GetFieldChar(const key: word; const item: integer): integer; Far;
var
   j     : word;
   anypicks : Boolean;
begin
  case key of
    8: begin                       {F8 = tag/untag all}
      anypicks := false;
      for j := 1 to npicks do if picks[j] then anypicks := true;
      for j := 1 to npicks do picks[j] := not anypicks;
      Result := 2
    end;
    9: begin                       {F9 = Exit}
      Result := 3;
    end;
    32: begin
      picks[Item] := not picks[Item];
      Result := -1;
    end;
    else Result := 0;
  end;
end;

procedure TForm1.TestV(Sender: TObject);
begin
  FillChar(picks,SizeOf(picks),0);
  VPickChoice(npicks,1,1,'Tag items (F8=all, F9=done)','',false,GetFieldPick,
    GetFieldSelect,GetFieldChar,0);
  Application.Terminate;
end;

end.
