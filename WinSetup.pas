unit WinSetup;
{$define NeedAuth}
{.$define test}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, CheckLst, StBase, StSpawn;

type
  TInstForm = class(TForm)
    Panel1: TPanel;
    CanBut: TButton;
    NextBut: TButton;
    Label1: TLabel;
    DoRun: TStSpawnApplication;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label3: TLabel;
    procedure GoNext(Sender: TObject);
    procedure DoCancel(Sender: TObject);
    procedure Initialize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InstForm: TInstForm;

implementation
uses
    history,
    util2,
    StStrL,
    StUtils;

{$R *.DFM}

const
     Instate  : integer = 1;
     acbl_dir : String = '';
     {$ifdef NeedAuth}
     IsACBLscore : boolean = false;
     {$endif}
     UpdName = 'ACBLinst.exe';
     ACBLvers  : String[8] = '';
     F1Offset  : integer = 0;

var
   Source_File  : String;
   load_path    : String;

procedure SetLabel1(const Name: String);
begin
  if Length(Name) > 0 then InstForm.Label1.Caption := 'Copying '+Name
  else InstForm.Label1.Caption := '';
  Application.ProcessMessages;
end;

function SetACBLscoreDir: boolean;
var
   xdrive : char;

function TestACBLscore(const drive: char): boolean;
begin
  acbl_dir := drive+':\ACBLSCOR';
  Result := IsDirectory(acbl_dir);
  acbl_dir := acbl_dir+'\';
  Result := Result and FileExists(acbl_dir+'SCORE.OVR');
  Result := Result and FileExists(acbl_dir+'ACBL3.EXE');
  Result := Result or FileExists(acbl_dir+'WSCORE.EXE');
end;

begin
  Result := false;
  {$ifdef test}
  for xdrive := 'C' to 'D' do Result := Result or TestACBLscore(xdrive);
  {$else}
  for xdrive := 'C' to 'Z' do Result := Result or TestACBLscore(xdrive);
  {$endif}
  {$ifdef NeedAuth}
  IsACBLscore := Result;
  if Result then Instate := 2
  else begin
    Instate := 1;
    acbl_dir := '';
  end;
  {$else}
  Instate := 2;
  if not Result then acbl_dir := '';
  {$endif}
end;

procedure TestRequiredFiles;
begin
  Source_File := load_path+UpdName;
  if not FileExists(Source_file) then begin
    MessageDlg('File '+load_path+UpdName+' not found.',mtError,[mbOK],0);
    Application.Terminate;
    exit;
  end;
end;

procedure SetIState;
begin
  if canceled then exit;
  with InstForm do case Instate of
    1: begin
      Label1.Caption := 'An older version of ACBLscore was not found on this computer.'#13
                       +'Enter your club number and ACBL authorization code below'#13
                       +'Then click Next to continue.';
      Label2.Caption := 'Enter Authorization code here -->';
      Label2.Show;
      Label3.Caption := 'Enter Club number here ---->';
      Label3.Show;
      Edit1.Show;
      Edit2.Show;
      Edit1.SetFocus;
    end;
    2: begin
      Nextbut.Hide;
      if Length(acbl_dir) < 1 then begin
        acbl_dir := 'C:\ACBLSCOR';
        if not IsDirectory(acbl_dir) then CreateDir(acbl_dir);
        acbl_dir := acbl_dir+'\';
      end;
      with DoRun do begin
        FileName := load_path+UpdName;
        NotifyWhenDone := false;
        RunParameters := '';
        Execute;
      end;
      Application.Terminate;
    end;
  end;
end;

procedure CheckState;
begin
  with InstForm do case Instate of
    1: begin
      Edit1.Hide;
      Edit2.Hide;
      Label3.Hide;
      Label2.Hide;
    end;
  end;
end;

procedure TInstForm.GoNext(Sender: TObject);
begin
  if Instate < 3 then begin
    {$ifdef NeedAuth}
    if Instate = 1 then begin
      IsACBLscore := GetClubAuthCode(Edit1.Text) = Edit2.Text;
      if not IsACBLscore then begin
        MessageDlg('Invalid authorization code: '+Edit2.Text,mtError,[mbOK],0);
        SetIState;
        exit;
      end;
    end;
    {$endif}
    CheckState;
    Inc(Instate);
    SetIState;
  end
  else Application.Terminate;
end;

procedure TInstForm.DoCancel(Sender: TObject);
begin
  canceled := true;
  Application.Terminate;
end;

procedure TInstForm.Initialize(Sender: TObject);
begin
  Panel1.Width := Width-20;
  Panel1.Height := Height-80;
  Nextbut.Top := Height-60;
  Canbut.Top := Height-60;
  Label2.Hide;
  Label3.Hide;
  Edit1.Text := '';
  Edit1.Hide;
  Edit2.Text := '';
  Edit2.Hide;
  load_path := fixpath(JustPathNameL(ParamStr(0)));
  TestRequiredFiles;
  if Application.Terminated then exit;
  SetACBLscoreDir;
  SetIState;
end;

end.
