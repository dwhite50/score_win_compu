unit Txtview1;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, OvcBase, OvcViewr, OvcData, Menus, Printers, ShellAPI;

type
  TViewForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    PrintSetup1: TMenuItem;
    Print1: TMenuItem;
    N2: TMenuItem;
    Open1: TMenuItem;
    Clipboard1: TMenuItem;
    SelectAll1: TMenuItem;
    Copy2: TMenuItem;
    Search1: TMenuItem;
    GotoLine1: TMenuItem;
    N3: TMenuItem;
    FindNext1: TMenuItem;
    Find1: TMenuItem;
    Options1: TMenuItem;
    Font1: TMenuItem;
    N4: TMenuItem;
    TabSize1: TMenuItem;
    N5: TMenuItem;
    TabExpansion1: TMenuItem;
    {Help1: TMenuItem;
    About1: TMenuItem;}
    DefaultController: TOvcController;
    Panel1: TPanel;
    FontDialog1: TFontDialog;
    OpenDialog1: TOpenDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    PrintDialog1: TPrintDialog;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Viewer1: TOvcTextFileViewer;
    Copy1: TMenuItem;
    Saveas1: TMenuItem;
    SaveDialog1: TSaveDialog;
    procedure Font1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure PrintSetup1Click(Sender: TObject);
    procedure Print1Click(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure Clipboard1Click(Sender: TObject);
    procedure File1Click(Sender: TObject);
    procedure CopyClick(Sender: TObject);
    procedure Search1Click(Sender: TObject);
    procedure GotoLine1Click(Sender: TObject);
    procedure Viewer1ShowStatus(Sender: TObject; Line: Longint;
      Col: Integer);
    {procedure About1Click(Sender: TObject);}
    procedure FormCreate(Sender: TObject);
    procedure TabExpansion1Click(Sender: TObject);
    procedure TabSize1Click(Sender: TObject);
    procedure Find1Click(Sender: TObject);
    procedure FindNext1Click(Sender: TObject);
//    procedure WordStarCommands1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure exit2Click(Sender: TObject; var Action: TCloseAction);
    procedure Saveas1Click(Sender: TObject);
  private
    { Private declarations }
    DefSaveName: String;
    MaxLineLen               : integer;
    procedure AppMessage(var Msg: TMsg; var Handled: Boolean);
  public
    { Public declarations }
  end;

function ViewFile(const title,xFname,SaveName : String): boolean;
function ViewerInit: boolean;
function ViewerDone: boolean;
function ViewerAdd(const Line : String; const NewLine: boolean) : boolean;
function ViewerShow(const title,SaveName:String): boolean;

const
     ViewInit    : boolean = false;
     UseACBLPrinter : boolean = false;
     ViewNeedPrint : boolean = false;
     ViewCheckPrintNewPage : boolean = false;
     PositionStr : String = '';

implementation

uses {TxtView2, }TxtView3,
     pvio,
     csdef,
     util1,
     util2,
     selprinter,
     YesNoBoxU,
     StStrL;

{$R *.DFM}

const
     ViewForm: TViewForm = nil;
     ViewDone   : boolean = false;

procedure TViewForm.AppMessage(var Msg: TMsg; var Handled: Boolean);
var
  hDrop : THandle;
  DroppedName : array[0..80] of Char;
begin
  if Msg.Message = WM_DROPFILES then begin
    Handled := True;
    hDrop := Msg.wParam;
    DragQueryFile(hDrop, 0, DroppedName, SizeOf(DroppedName));
    Viewer1.FileName := StrPas(DroppedName);
    Viewer1.IsOpen := True;
    DragFinish(hDrop);
    if IsIconic(Application.Handle) then
      ShowWindow(Application.Handle, sw_ShowNormal)
    else
      BringWindowToTop(Handle);
  end;
end;

procedure TViewForm.Font1Click(Sender: TObject);
begin
  FontDialog1.Font.Assign(Viewer1.FixedFont.Font);                     {!!.10}
  if FontDialog1.Execute then
    if FontDialog1.Font <> nil then begin
      Viewer1.FixedFont.Assign(FontDialog1.Font);
      with Viewer1.FixedFont do begin
//        UseGlobal := true;
        SetAppProfile('ViewFontName',Name,GlobalApp);
//        UseGlobal := true;
        SetAppProfile('ViewFontSize',Long2StrL(Size),GlobalApp);
      end;
    end;
end;

procedure TViewForm.Open1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then begin
    with Viewer1 do begin
      FileName := OpenDialog1.Filename;
      IsOpen := True;
    end;
    ViewForm.Caption := 'Text View - ' + OpenDialog1.Filename;
    Search1Click(Self);
    Clipboard1Click(Self);
  end;
end;

procedure TViewForm.exit2Click(Sender: TObject; var Action: TCloseAction);
begin
  ViewDone := true;
end;

procedure TViewForm.Exit1Click(Sender: TObject);
begin
  if ViewNeedPrint then Print1Click(Sender);
  ViewNeedPrint := false;
  Close;
  ViewDone := true;
end;

procedure TViewForm.PrintSetup1Click(Sender: TObject);
begin
  if UseACBLPrinter then SelectPrinter
  else PrinterSetupDialog1.Execute;
end;

procedure TViewForm.Print1Click(Sender: TObject);
var
  Line: LongInt;
  Buf: array[0..1023] of Char;
  SelRange: TOvcViewerRange;
  PrintText: System.Text;
  Pitch     : TPrintPitch;
  PrintLine : integer;
  TM : TTextMetric;
  defaultchar : Char;
  nppos : integer;
  lenok : boolean;
  lfstr : string;

procedure TestNewPage;
begin
  with CFG do if (SheetSize < 1) or (PrintLine < SheetSize) then exit;
  PrintLine := 1;
  PrtLn(#12);
end;

begin
  if UseACBLPrinter then begin
    PlsWait;
    if Destination > 0 then ViewerInUse := true;
    OpenDestination(2,0,MaxLineLen);
    PrintLine := 1;
    with CFG do begin
      if not Print2File then PrtLn(p_6lpi);
      Pitch := CalcPrintPitch(MaxLineLen);
      if Print2File then PrtLn(#1+Char(Byte(Pitch)-Byte(wp10)+Ord('0')))
      else case Pitch of
        ppica: PrtLn(p_pica);
        pelite: PrtLn(p_elite);
        pcompress: PrtLn(p_cmprs);
      end;
    end;
    with ViewForm.Viewer1 do for Line := 0 to LineCount-1 do begin
      TestNewPage;
      PrtLn(Lines[Line]);
      PrtLn(#13#10);
      Inc(PrintLine);
    end;
    PrtLn(#12);
    Close_Destination;
    if ViewerInUse then Destination := 1;
    ViewerInUse := false;
    EraseBoxWindow;
  end
  else begin
    if Viewer1.GetSelection(SelRange.Start.Line, SelRange.Start.Col,
             SelRange.Stop.Line, SelRange.Stop.Col) then
      PrintDialog1.Options := PrintDialog1.Options + [poSelection]
    else PrintDialog1.Options := PrintDialog1.Options - [poSelection];
    if PrintDialog1.Execute then begin
      AssignPrn(PrintText);
      try
        Rewrite(PrintText);
        Printer.Canvas.Font := Viewer1.FixedFont.Font;
        GetTextMetrics(Printer.Canvas.Handle, TM);
        with TM do begin
          DefaultChar := Char(tmDefaultChar);
        end;
        with Printer,Canvas do repeat
          nppos := TextWidth(CharStrL('M',MaxLineLen));
          lenok := (nppos <= PageWidth) or (Font.Size < 7);
          if not lenok then Font.Size := Font.Size-1;
        until lenok;
        case PrintDialog1.PrintRange of
          prAllPages :
            with ViewForm.Viewer1 do for Line := 0 to LineCount-1 do begin
              if ViewCheckPrintNewPage and (Pos(DefaultChar,Lines[line]) > 0) then begin
                nppos := Pos(DefaultChar,Lines[Line]);
                Write(PrintText,Copy(Lines[Line],1,nppos));
                Write(PrintText,#12);
                lfstr := Copy(Lines[Line],nppos+1,255);
                if Length(lfstr) > 0 then WriteLn(PrintText,lfstr);
              end
              else Writeln(PrintText,Lines[Line]);
            end;
            {for Line := 0 to Pred(Viewer1.LineCount) do begin
              Viewer1.GetPrintableLine(Line, Buf, SizeOf(Buf));
              Writeln(PrintText, Buf);
            end;}
          prSelection :
            for Line := SelRange.Start.Line to SelRange.Stop.Line do begin
              Viewer1.GetPrintableLine(Line, Buf, SizeOf(Buf));
              if Line = SelRange.Start.Line then
                FillChar(Buf, SelRange.Start.Col, #32);
              if Line = SelRange.Stop.Line then
                Buf[SelRange.Stop.Col] := #0;
              Writeln(PrintText, Buf);
            end;
        end;
      finally
        System.Close(PrintText);
        ViewNeedPrint := false;
      end;
    end;
  end;
end;

procedure TViewForm.SelectAll1Click(Sender: TObject);
begin
  Viewer1.SelectAll(False);
end;

procedure TViewForm.Clipboard1Click(Sender: TObject);
var
  SelRange: TOvcViewerRange;
begin
  SelectAll1.Enabled := Viewer1.LineCount > 0;
  Copy1.Enabled := Viewer1.GetSelection(SelRange.Start.Line, SelRange.Start.Col,
          SelRange.Stop.Line, SelRange.Stop.Col);
  Copy2.Enabled := Viewer1.GetSelection(SelRange.Start.Line, SelRange.Start.Col,
          SelRange.Stop.Line, SelRange.Stop.Col);
end;

procedure TViewForm.File1Click(Sender: TObject);
begin
  Print1.Enabled := Viewer1.LineCount > 0;
end;

procedure TViewForm.CopyClick(Sender: TObject);
begin
  Viewer1.CopyToClipboard;
end;

procedure TViewForm.Search1Click(Sender: TObject);
begin
  Find1.Enabled := Viewer1.LineCount > 0;
  FindNext1.Enabled := (Viewer1.LineCount > 0) and (FindDlg.Edit1.Text <> '');
  GoToLine1.Enabled := Viewer1.LineCount > 0;
end;

procedure TViewForm.GotoLine1Click(Sender: TObject);
var
  S : string;
  GoPos : TOvcTextPos;
begin
  S := '';
  if InputQuery('Go to Line Number', 'Enter Line Number (1 to ' +
    IntToStr(Viewer1.LineCount) + ')', S) then begin
    try
      GoPos.Line := Pred(StrToInt(S));
    except
      Exit;
    end;
    GoPos.Col := 0;
    Viewer1.CaretActualPos := GoPos;
  end;
end;

procedure TViewForm.Viewer1ShowStatus(Sender: TObject; Line: Longint;
  Col: Integer);
begin
  Panel2.Caption := IntToStr(Succ(Viewer1.CaretEffectivePos.Line)) +
    ':' + IntToStr(Succ(Viewer1.CaretEffectivePos.Col));
  Panel2.Update;
  Panel3.Caption := 'Total: ' + IntToStr(Viewer1.LineCount);
  Panel4.Caption := 'Top: ' + IntToStr(Succ(Viewer1.TopLine));
  Panel4.Update;
  Panel5.Caption := 'Bytes: ' + IntToStr(Viewer1.FileSize);
  Clipboard1Click(Self);
end;

{procedure TViewForm.About1Click(Sender: TObject);
begin
  ViewAbout.ShowModal;
end;}

procedure TViewForm.FormCreate(Sender: TObject);
begin
  TabExpansion1.Checked := Viewer1.ExpandTabs;
  DragAcceptFiles(Application.Handle, True);
  DragAcceptFiles(Handle, True);
  Application.OnMessage := AppMessage;
end;

procedure TViewForm.TabExpansion1Click(Sender: TObject);
begin
  TabExpansion1.Checked := not(TabExpansion1.Checked);
  Viewer1.ExpandTabs := TabExpansion1.Checked;
end;

procedure TViewForm.TabSize1Click(Sender: TObject);
var
  S : string;
begin
  S := IntToStr(Viewer1.TabSize);
  if InputQuery('Tab Size', 'Enter Tab Size', S) then try
    Viewer1.TabSize := StrToInt(S);
  except
    Exit;
  end;
end;

procedure TViewForm.Find1Click(Sender: TObject);
var
  OptionSet : TSearchOptionSet;
begin
  FindDlg.ShowModal;
  if FindDlg.ModalResult=mrOK then begin
    OptionSet := [];
    if FindDlg.RadioButton2.Checked then
      OptionSet := OptionSet + [soBackward];
    if FindDlg.Checkbox1.Checked then
      OptionSet := OptionSet + [soMatchCase];
    if FindDlg.Checkbox2.Checked then
      OptionSet := OptionSet + [soGlobal];
    if not Viewer1.Search(FindDlg.Edit1.Text, OptionSet) then
      MessageDlg('Search string ''' + FindDlg.Edit1.Text +
        ''' not found', mtInformation, [mbOk], 0);
    Search1Click(Self);
  end;
end;

procedure TViewForm.FindNext1Click(Sender: TObject);
var
  OptionSet : TSearchOptionSet;
begin
  OptionSet := [];
  if FindDlg.RadioButton2.Checked then
    OptionSet := OptionSet + [soBackward];
  if FindDlg.Checkbox1.Checked then
    OptionSet := OptionSet + [soMatchCase];
  if not Viewer1.Search(FindDlg.Edit1.Text, OptionSet) then
    MessageDlg('Search string ''' + FindDlg.Edit1.Text +
      ''' not found', mtInformation, [mbOk], 0);
end;

{procedure TViewForm.WordStarCommands1Click(Sender: TObject);
begin
  with WordStarCommands1 do begin
    Checked := not(Checked);
    if Checked then
      DefaultController.EntryCommands.Table[0].IsActive := True
    else
      DefaultController.EntryCommands.Table[0].IsActive := False;
  end;
end;}

procedure TViewForm.FormDestroy(Sender: TObject);
begin
  DragAcceptFiles(Application.Handle, False);
  DragAcceptFiles(Handle, False);
end;

const
//     ViewFname   = TempDir+'\XVIEWX.TMP';
     ViewFname   = 'XVIEWX.TMP';

var
   VF            : TextFile;

function ViewFile(const title,xFname,SaveName : String): boolean;
var
   j                    : integer;
   FName                : String;
begin
  ViewFile := false;
  ViewDone := false;
  FName := ExpandFileName(xFname);
  if not FileExists(Fname) then exit;
  if not Assigned(ViewForm) then Application.CreateForm(TViewForm,ViewForm);
  if not Assigned(FindDlg) then Application.CreateForm(TFindDlg,FindDlg);
  with ViewForm,Viewer1 do begin
    FileName := Fname;
    IsOpen := True;
    ViewForm.Caption := title;
    DefSaveName := SaveName;
    MaxLineLen := 0;
    for j := 0 to LineCount-1 do if LineLength[j] > MaxLineLen then
      MaxLineLen := LineLength[j];
    with FixedFont do begin
//      UseGlobal := true;
      Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
      Size := ViewFontSize;
    end;
    with ViewForm do begin
      if LineCount > 50 then Height := Screen.Height
      else begin
        Height := FixedFont.Size * LineCount * 2 + 100;
        if Height > Screen.Height then Height := Screen.Height;
      end;
      Width := FixedFont.Size * MaxLineLen + 40;
      if Width > Screen.Width then Width := Screen.Width;
    end;
    Search1Click(ViewForm);
    Clipboard1Click(ViewForm);
    if Length(PositionStr) > 0 then begin
      Search(PositionStr,[]);
      TopLine := CaretActualPos.Line;
    end;
    PositionStr := '';
    ShowModal;
  end;
  while not ViewDone do Application.ProcessMessages;
  ViewForm.Free;
  ViewForm := nil;
  FindDlg.Free;
  FindDlg := nil;
  ViewFile := true;
end;

function ViewerDone: boolean;
begin
  {$I-} CloseFile(VF); {$I+}
  ViewerDone := IOResult = 0;
  ViewInit := false;
end;

function ViewerInit: boolean;
begin
  ViewerDone;
  ViewerInit := true;
  AssignFile(VF,ViewFname);
  ReWrite(VF);
  ViewInit := true;
  ViewCheckPrintNewPage := false;
end;

function ViewerAdd(const Line : String; const NewLine: boolean) : boolean;
begin
  if not ViewInit then ViewerInit;
  ViewerAdd := true;
  if NewLine then WriteLn(VF,Line)
  else Write(VF,Line);
end;

function ViewerShow(const title,SaveName:String): boolean;
begin
  ViewerShow := false;
  if not ViewerDone then exit;
  ViewFile(title,ViewFname,SaveName);
  if FileExists(ViewFname) then DeleteFile(ViewFname);
  Result := true;
end;

procedure TViewForm.Saveas1Click(Sender: TObject);
var
   VDestPath : String;
   VDestName : String;
   esc       : boolean;
   icc       : byte;
   infile    : textfile;
   OldFileMode : byte;
   line        : string;
begin
  if FileExists(Viewer1.FileName) then {with SaveDialog1 do }begin
    VDestPath := GetAppProfile('ViewSaveDir','',GlobalApp);
    VDestName := ForceExtensionL(JustFileNameL(DefSaveName),'TXT');
    if not SetDestFileName(esc,0,VDestPath,VDestName,'File location',
       'File name','',faAppend,true,icc) then exit;
    OldFileMode := FileMode;
    FileMode := 0;
    assignFile(infile,Viewer1.Filename);
    {$I-} Reset(infile);
    FileMode := OldFileMode;
    CheckIOResult(Viewer1.FileName);
    While not Eof(infile) do begin
      ReadLn(infile,line);
      WriteLn(Destfile,line);
    end;{$I+}
    CloseFile(infile);
    CloseFile(Destfile);
    SetAppProfile('ViewSaveDir',VDestPath,GlobalApp);
  end;
end;

end.
