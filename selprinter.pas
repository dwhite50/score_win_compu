{$I defines.dcl}
unit selprinter;

interface

procedure SelectPrinter;
function ShowPrinter: String;

implementation

uses
  CSdef,
  aprint,
  version,
  StStrL,
  VListBox,
  YesNoBoxU,
  util1,
//  mtables,
  util2,
  history,
  pvio;


function ShowPrinter: String;
var
   printer_name: String[20];
begin
  with CFG do if Print2File then Result := WprintName
  else begin
    Move(CFG.p_name[1], printer_name[1], 20);
    printer_name[0] := #20;
    Result := printer_name;
  end;
end;

procedure SelectPrinter;
const
     MaxPrinters = 50;
     PrintFileName = 'PRINTER.DEF';
     MainOptions = 2;
     Printers  : Word = 0;
     PrintChoice : word = 1;
     MainText : Array[1..MainOptions] of String =
              ('1 Print using ACBLscore drivers (dot matrix, etc)',
               '2 Print using Windows drivers (ink jet, laser, etc)');

var
   PrinterP : Array[1..MaxPrinters] of String;
   SelectText : Array[1..MaxPrinters] of String;
   PrintFile : text;
   MainOption : byte;
   CurrentPrinter : word;
   CurrentItem : word;
   Name  : String[20];
   Line  : String;

procedure DisposePrinters;
begin
  Printers := 0;
end;

procedure ReadPrinters;
begin
  CurrentPrinter := 1;
  Printers := 0;
  Reset(PrintFile);
  {$I-}ReadLn(PrintFile,Line);{$I+}
  CheckIOResult(CFG.LoadPath+PrintFileName);
  while not Eof(PrintFile) do begin
    {$I-}ReadLn(PrintFile,Line);{$I+}
    CheckIOResult(CFG.LoadPath+PrintFileName);
    Inc(Printers);
    PrinterP[Printers] := Line;
    SelectText[Printers] := ExtractWordL(1,Line,'''');
    if PadL(SelectText[Printers],20) = CFG.p_name then CurrentPrinter := Printers;
  end;
  Close(PrintFile);
end;

procedure GetElement(var Element : PrintElement; const PLine : String);
var
   NumElements : word;
   Inum : Integer;
   loop : word;
begin
  Inc(CurrentItem);
  if Str2WordL(ExtractWordL(CurrentItem,PLine,''','),NumElements) then;
  Element := '';
  for loop := 1 to NumElements do begin
    Inc(CurrentItem);
    if Str2LongL(ExtractWordL(CurrentItem,PLine,''','),INum) then;
    if Inum < 0 then Inum := 0;
    Element := Element+Char(Inum);
  end;
  if (NumElements > 0) and (Element[1] = #0) then Element := '';
end;

function PickPrinter: word;
var
   PLine : String;
begin
  ReadPrinters;
  PickTStrings := PickLStrings;
  PrintChoice := Pickchoice(Printers,CurrentPrinter,
    'ACBLscore printer driver (ESC to quit) ',@SelectText,MC,false,11);
  Result := PrintChoice;
  if PrintChoice < 1 then exit;
  Pline := PrinterP[PrintChoice];
  Name := PadL(ExtractWordL(1,PLine,''''),20);
  with CFG do begin
    Move(Name[1],p_name,20);
    CurrentItem := 1;
    GetElement(p_norm,PLine);
    GetElement(p_expnd,PLine);
    GetElement(p_pica,PLine);
    GetElement(p_elite,PLine);
    GetElement(p_cmprs,PLine);
    GetElement(p_8lpi,PLine);
    GetElement(p_6lpi,PLine);
    Inc(CurrentItem);
    if Str2Int16L(ExtractWordL(CurrentItem,PLine,''','),pwidth) then;
    Inc(CurrentItem);
    Inc(CurrentItem);
    if Str2Int16L(ExtractWordL(CurrentItem,PLine,''','),SheetSize) then;
    ppcl := (Copy(p_expnd,1,3) = #27'(s');
  end;
end;

procedure SelPrinter;
begin
  if PickPrinter > 0 then begin
    CFG.Print2File := false;
    WriteCFG;
    SelectWindowsPrinter;
  end;
end;

procedure SetWinPrinter;
begin
  with CFG do begin
    if not Print2File then begin
      Print2File := true;
      pwidth := 80;
      SheetSize := 60;
      ppcl := true;
      WriteCFG;
    end;
  end;
  WprintOptions;
end;

begin
  FillChar(PrinterP,SizeOf(PrinterP),0);
  Assign(PrintFile,CFG.LoadPath+PrintFileName);
  {$I-} Reset(PrintFile); {$I+}
  if IOResult <> 0 then begin
    ErrBox('File '+CFG.LoadPath+PrintFileName+' not found',MC,0);
    exit;
  end;
  Close(PrintFile);
  if not CFG.Print2File then
    MainText[2] := '2 Print using Windows drivers (ink jet, laser, etc)'
  else MainText[2] := '2 Windows printer Options';
  with CFG do if Print2File then MainOption := 2
  else MainOption := 1;
  PickTStrings := PickLStrings;
  MainOption := Pickchoice(MainOptions,MainOption,'option (ESC to quit) ',
    @MainText,MC,false,11);
  case MainOption of
    1: SelPrinter;
    2: SetWinPrinter;
  end;
end;

end.
