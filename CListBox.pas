unit CListBox;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcVLB, util1, OvcLB, Ovcbcklb, OvcCkLb, CheckLst;

type
  TCListBoxForm = class(TForm)
    OKButton: TButton;
    CanButton: TButton;
    OvcController1: TOvcController;
    CListBox: TOvcCheckList;
    BoxLabel: TOvcAttachedLabel;
    procedure Acceptit(Sender: TObject);
    procedure Cancelit(Sender: TObject);
    procedure EnableOk(Sender: TObject);
    procedure CStateChange(Sender: TObject; Index: Integer; OldState,
      NewState: TCheckBoxState);
  private
    { Private declarations }
  public
    { Public declarations }
    Choice : integer;
    procedure SetSelect(const Index: integer; const Selected: TCheckBoxState);
  end;

type
    TGetSelect = procedure(const Index: integer; var Selected: TCheckBoxState;
               const InitPhase: boolean);

function CPickchoice(const num_choices,def_choice: word;
         const title,Header: String; const Ptextx : Pointer;
         const GetSelect: TGetSelect; const HelpTopic: Word) : integer;

implementation

uses
    vListBox;

{$R *.DFM}

const
     CListBoxForm: TCListBoxForm = nil;
     CGetSelect : TGetSelect = nil;

procedure TCListBoxForm.Acceptit(Sender: TObject);
begin
  Close;
  Choice := CListBox.ItemIndex+1;
end;

procedure TCListBoxForm.Cancelit(Sender: TObject);
begin
  Close;
  Choice := 0;
end;

procedure TCListBoxForm.EnableOk(Sender: TObject);
begin
  OkButton.Enabled := true;
end;

function CPickchoice(const num_choices,def_choice: word;
         const title,Header: String; const Ptextx : Pointer;
         const GetSelect: TGetSelect; const HelpTopic: Word) : integer;
const
     FontSize = 10;
     MinWidth = 200;
var
   j     : word;
   MaxTextWidth : integer;
   curtextwidth : integer;
   Ptext        : PPCharArray;
   PStext       : PStringArray absolute Ptext;
   CurState     : TCheckBoxState;
   ScrItems     : integer;
begin
  if not Assigned(CListBoxForm) then
    Application.CreateForm(TCListBoxForm, CListBoxForm);
  MaxTextWidth := 0;
  Ptext := Ptextx;
  CGetSelect := GetSelect;
  with CListBoxForm do begin
    Font.Size := FontSize;
    Caption := title;
    CurTextWidth := Canvas.TextWidth(Caption);
    if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
    Choice := def_choice;
    BoxLabel.Caption := Header;
    with CListBox do begin
      ThreeState := false;
      MultiSelect := false;
      CheckStyle := csCheck;
      Font.Size := FontSize;
      Canvas.Font.Size := FontSize;
      for j := 1 to num_choices do begin
        case PickTStrings of
          PickChar: CurTextWidth := Canvas.TextWidth(Ptext[j]);
          PickLStrings: CurTextWidth := Canvas.TextWidth(Pstext[j]);
          PickStrings: CurTextWidth := Canvas.TextWidth(TStrings(Ptextx)[j-1]);
        end;
        if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
      end;
      ScrItems := (Screen.Height-100-ItemHeight*4) div ItemHeight;
      Columns := num_choices div ScrItems + 1;
      ScrItems := (num_choices+Columns-1) div Columns;
      if Columns > 1 then Height := ScrItems {num_choices} * ItemHeight+20
      else Height := num_choices * ItemHeight+4;
      {if Height+ItemHeight*4+100 > Screen.Height then
        Height := Screen.Height-100-ItemHeight*4;}
      Width := (MaxTextWidth+10) * Columns;
      if Width < MinWidth then Width := MinWidth;
      if Width+10 > Screen.Width then Width := Screen.Width-10;
      if Columns > 1 then Columns := Width div (MaxTextWidth+10);
      if PickTStrings = PickStrings then Items := Ptextx;
      for j := 1 to num_choices do begin
        case PickTStrings of
          PickChar: Items.Add(Ptext[j]);
          PickLStrings: Items.Add(PsText[j]);
        end;
//        CurTextWidth := Canvas.TextWidth(Items[j-1])+15;
//        if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
        if Assigned(CGetSelect) then begin
          CGetSelect(j,CurState,true);
          States[j-1] := CurState;
        end
        else States[j-1] := cbUnchecked;
      end;
      ItemIndex := Choice-1;
      OKButton.Enabled := true;
    end;
    Height := CListBox.Height+CListBox.ItemHeight*2+OKButton.Height+30;
    Width := CListBox.Width+10;
    OKButton.Top := Height-55;
    CanButton.Top := OKButton.Top;
    CanButton.Left := Width-CanButton.Width-30;
    ShowModal;
    Result := Choice;
    Free;
  end;
  PickTStrings := PickChar;
  CListBoxForm := nil;
  CGetSelect := nil;
end;

procedure TClistBoxForm.SetSelect(const Index: integer;
          const Selected: TCheckBoxState);
begin
  CListBox.States[Index] := Selected;
end;

procedure TCListBoxForm.CStateChange(Sender: TObject; Index: Integer;
  OldState, NewState: TCheckBoxState);
var
   CurState : TCheckBoxState;
begin
  if Assigned(CGetSelect) then begin
    CurState := NewState;
    CGetSelect(Index+1,CurState,false);
    if CurState <> NewState then CListBox.States[Index] := CurState;
  end;
end;

end.
