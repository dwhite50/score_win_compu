{$I debugdir.inc}
unit backrest;

interface

{procedure restore_files(const GameFiles,DelOld,ShowArc: boolean);}
procedure RestoreDataBase(const dofinance,import: boolean);
procedure RestoreArchive;
procedure RestoreGameFiles;
procedure BackupTournFiles(const BackCommand: char; const askdrive: boolean);
procedure BackupForMemphis;

implementation

uses
    SysUtils,
    YesNoBoxU,
    pvio,
    dbase,
    LHAUnit,
    FOpen,
    StrBoxU,
    StStrL,
    fixpl,
    backdata,
    backutil,
    backback,
    db33,
    version,
    csdef,
    history,
    util2;

procedure restore_files(const GameFiles,DelOld,ShowArc: boolean;
          const filename,TargetDir: String);
var
   j    : word;
   k    : word;
   HDate  : string;
begin
  if GameFiles then begin
    Hsize:=LHAInit(arc_name,HsortName,TargetDir);
    EraseBoxWindow;
    if hsize = 0 then begin
      HistDone;
      ErrBox('No files exist in '+arc_name,MC,0);
      MainHalt;
    end;
    {if hsize > 1 then begin
      if HShowExist then
        MsgBox('Tag game files to restore from '+arc_name+' to'#13
        +TargetDir+' in the list below.'#13+
        '* = File already EXISTS in target location.',intont,false,TC,0,0)
      else MsgBox('Tag game files to restore from'#13+arc_name+' to '
        +TargetDir+#13'in the list below.',intont,false,TC,0,0);
    end;}
    PickHsize:= HistPick('SPACE=tag/untag, F8=tag/untag ALL, F9=accept, ESC=quit',
      true,false,PickChar);
    if hsize > 1 then EraseBoxWindow;
    AssignFile(backfile,backfilename);
    ReWrite(backfile);
    BackupSize := 0;
    isbackup := false;
    if (hsize = 1) and (PickHsize = 1) then begin
      HistGet(HistGetNext, Name, Schar, Typ,HDate);
      if YesNoBox(Escaped,true,'One Game File found.'#13'Click Yes restore '+Name+' ('
        +schar+'), No to quit',0,MC,nil) then begin
        WriteLn(backfile,Name,Typ);
        isbackup := true;
        HistUntag;
      end;
    end
    else while (Pickhsize > 0) do begin
      HistGet(HistGetNext, Name, Schar, Typ,HDate);
      WriteLn(backfile,Name,Typ);
      isbackup := true;
      Dec(PickHsize);
      HistUntag;
    end;
    HistDone;
    CloseFile(backfile);
    if isbackup then begin
      RunLHA('E',arc_name,filename,backfilename,TargetDir,false);
      DeleteFile(backfilename);
    end;
  end
  else begin
    if ShowArc then Escaped := EscBox('Ready to restore from '+arc_name
      +' to '+TargetDir,MC)
    else Escaped := false;
    wait := true;
    if not Escaped then begin
      if DelOld then for j := 1 to MaxFilno do for k := 0 to MaxKeyno do
        if Length(DBNames[j,k]) > 0 then DeleteFile(CFG.DBPath+DBNames[j,k]);
      RunLHA('E',arc_name,filename,backfilename,TargetDir,false);
    end;
  end;
end;

procedure RestoreDataBase(const dofinance,import: boolean);
var
   imptxt       : String[8];
   manyfiles    : boolean;
   DosError     : integer;
{$I officepa.dcl}
begin
  {$ifdef office}
  with CFG do if CompareKey(Upper(DBPath),OfficePath[1])
    or CompareKey(Upper(DBPath),OfficePath[2])
    or CompareKey(Upper(DBPath),Upper(OfficeTournPath)) then begin
    ErrBox('This function cannot be perfomed on the data base in'
      +#13+CFG.DBpath,MC,0);
    exit;
  end;
  {$endif}
  topic := 236;
  if import then imptxt := 'import '
  else imptxt := 'restore ';
  Escaped := false;
  {$ifndef office}
  if dofinance then begin
    if Fileexists(CFG.DBpath+'FTINFO.DAT') then begin
      Escaped := not YesNoBox(Escaped,not CFG.tournament,
        'Financial data exists at '+CFG.DBPath+' and will be overwritten by'#13
        +'restore.  To save the existing data base, use "Backup Financial Data."'#13+
        'Continue with financial data restore',topic,MC,nil);
      if not Escaped then Escaped := not YesNoBox(Escaped,false,
        'Confirm overwrite financial data at '+CFG.DBPath+'.'#13
        +'All information in that data base will be destroyed.'#13+
        'Continue with financial data restore',topic,MC,nil);
    end;
  end
  else if not import and Fileexists(CFG.DBpath+'PLAYERN.DAT') then begin
    Escaped := not YesNoBox(Escaped,not CFG.tournament,
      'A Data Base exists at '+CFG.DBPath+' and will be overwritten by'#13
      +'restore.  To save the existing data base, use "Backup Data Base."'#13+
      'Continue with data base restore',topic,MC,nil);
    if not Escaped then Escaped := not YesNoBox(Escaped,false,
      'Confirm overwrite the data base at '+CFG.DBPath+'.'#13
      +'All information in that data base will be destroyed.'#13+
      'Continue with data base restore',topic,MC,nil);
  end;
  {$endif}
  if not Escaped then begin
    get_drive;
    if not Escaped then begin
      if not Escaped then begin
        DosError := FindFirst(BackupLoc+'D*'+RestArcSuffix,faAnyFile,Search_Rec);
        Anyfiles := DosError = 0;
        file_name := '';
        If AnyFiles then begin
          file_name := BackupLoc+Search_Rec.Name;
          DosError := FindNext(Search_Rec);
        end;
        FindClose(Search_Rec);
        manyfiles := DosError = 0;
        if manyfiles or not Anyfiles then
          key_char := GetFileName(BackupLoc,'D*'+RestArcSuffix,file_name,
          'Select data base archive to '+imptxt,'BackupLocation','A:\',false,false)
        else key_char := true;
        if key_char and (length(file_name) > 0) then begin
          arc_name := file_name;
          file_name := CFG.DBpath;
          if dofinance then
            restore_files(false,false,not manyfiles,'F*.DAT',CFG.DBpath)
          else begin
            if import then begin
              file_name := 'TEMP\';
              {$I-} MkDir('TEMP'); {$I+}
              if IOResult = 0 then;
            end;
            restore_files(false,not import,not manyfiles,'',file_name);
          end;
          If not Escaped then begin
            RepairNoAsk := true;
            RepairDataBase(false,file_name);
//            MainHalt;
          end;
        end;
      end;
    end;
  end;
end;

procedure RestoreArchive;
var
   manyfiles    : boolean;
   DosError     : integer;
begin
  topic := 0;
  get_drive;
  if not Escaped then begin
    DosError := FindFirst(BackupLoc+'AR*'+RestArcSuffix,faAnyFile,Search_Rec);
    Anyfiles := DosError = 0;
    file_name := '';
    If AnyFiles then begin
      file_name := BackupLoc+Search_Rec.Name;
      DosError := FindNext(Search_Rec);
    end;
    FindClose(Search_Rec);
    manyfiles := DosError = 0;
    if manyfiles or not Anyfiles then
      key_char := GetFileName(BackupLoc,'AR*'+RestArcSuffix,file_name,
      'Select archive to restore','BackupLocation','A:\',false,false)
    else key_char := true;
    if key_char and (length(file_name) > 0) then begin
      arc_name := file_name;
      restore_files(false,false,not manyfiles,'',CFG.DBpath);
//      if YesNoBox(Escaped,ifunc,true,'Update data base from restored Archive',
//        topic,MC,0) then runmac(#27#27'DM8'#13);
    end;
  end;
end;

procedure RestoreGameFiles;
var
   ArcMask      : String[12];
   manyfiles    : boolean;
   DosError     : integer;

{$ifdef office}
procedure SetArcMask;
begin
  ArcMask := 'G??????.???';
  ShowExtension := true;
  FindFirst(BackupLoc+ArcMask,AnyFile,Search_Rec);
  if DosError = 0 then exit;
  ArcMask := '???????'+RestArcSuffix;
  ShowExtension := False;
end;
{$endif}

begin
  topic := 237;
  get_drive;
  if not Escaped then begin
    if not Escaped then begin
      {$ifdef office}
      SetArcMask;
      {$else}
      ArcMask := '???????'+RestArcSuffix;
      {$endif}
      DosError := FindFirst(BackupLoc+ArcMask,faAnyFile,Search_Rec);
      Anyfiles := DosError = 0;
      file_name := '';
      If AnyFiles then begin
        file_name := BackupLoc+Search_Rec.Name;
        DosError := FindNext(Search_Rec);
      end;
      FindClose(Search_Rec);
      manyfiles := DosError = 0;
      if manyfiles or not Anyfiles then
        key_char := GetFileName(BackupLoc,ArcMask,file_name,
        'Select Game file archive to restore','BackupLocation','A:\',false,false)
      else key_char := true;
      if key_char and (length(file_name) > 0) then begin
        arc_name := file_name;
        restore_files(true,false,not manyfiles,'',CFG.GFpath);
      end;
    end;
  end;
end;

procedure BackupTournFiles(const BackCommand: char; const askdrive: boolean);
var
   SancKey      : KeyStr;
   FName        : PathStr;
   BackupNumber : byte;
   BackupFileSize : Longint;
   BackupFiles    : word;
   CBackupNumber   : String[1];
   done            : boolean;
   HDate           : String;
   DosError        : integer;
   dbfname  : string;
   Histok          : boolean;

function dobackup: boolean;
var
   backok : boolean;
begin
  backok := RunLHA('a',arc_name+ArcSuff+CBackupNumber,'*.*',backfilename,'',true);
  if backok and (BackCommand = 'm') then begin
    {$I-} Reset(backfile); {$I+}
    if IOResult = 0 then begin
      while not Eof(backfile) do begin
        ReadLn(backfile,FName);
        if Length(FName) > 0 then begin
          DeleteFile(FName);
          {if MakeBMname(FName,dbfname) then DeleteFile(dbfname);}
        end;
      end;
      CloseFile(backfile);
    end;
    EraseBoxWindow;
    MsgBox('Backed up game files have been deleted'#13'from '+CFG.GFpath,MC,0);
  end
  else if BackCommand = 'm' then
    MsgBox('Backed up game files have NOT been deleted'#13'from '+CFG.GFpath,MC,0);
  {$I-} Erase(backfile); {$I+}
  if IOResult = 0 then;
  dobackup := backok;
end;

begin
  topic := 235;
  if askdrive then get_drive;
  if Escaped then exit;
  Hsize:=HistInit(CFG.GFpath,HsortName,'*.AC?',HistOk);
  if hsize = 0 then begin
    HistDone;
    ErrBox('No game files exist at '+CFG.GFpath,MC,0);
    exit;
  end;
  PickHsize:= HistPick('SPACE=tag/untag, F8=tag/untag ALL, F9=accept, ESC=quit',
    true,false,PickChar);
  CBackupNumber := 'H';
  BackupNumber := 1;
  AssignFile(backfile,backfilename);
  ReWrite(backfile);
  BackupFileSize := 0;
  isbackup := false;
  if PickHsize > 0 then begin
    if askdrive then begin
      arc_name := '';
      if CFG.tournament then begin
        if ReadDefaults then arc_name := UpperCase(SancDigits(Strip(Defaults.Sanc)));
        if empty(arc_name) then begin
          OpenDBFile(Tourn_no);
          ClearKey(IdxKey[Filno,1]^);
          NextKey(IdxKey[Filno,1]^,Recno[Filno],SancKey);
          if ok then arc_name := SancDigits(Strip(SancKey));
          CloseDBFile(Tourn_no);
        end;
      end;
      ifunc := 0;
      StrBox(Escaped,ifunc,'','Name of the compressed backup file in '+BackupLoc,
        arc_name,7,len,5,7,topic,MC,nil);
      if Escaped then exit;
      arc_name := BackupLoc+arc_name;
    end
    else arc_name := BackupLoc+PadChL(BackupName,'X',7);
    BackupFree := CheckDiskSpace(BackupLoc);
    BackupFiles := 0;
  end;
  while (Pickhsize > 0) do begin
    HistGet(HistGetNext, Name, Schar, Typ,Hdate);
    DosError := FindFirst(CFG.GFpath+Name+Typ,faAnyFile,Search_Rec);
    FindClose(Search_Rec);
    if DosError = 0 then begin
      if (BackupLoc[2] = ':') then begin
        BackupSize := Round((BackupFileSize+Search_Rec.Size)*GameFileCompress);
        BackupSize := (BackupSize+1000) div 1000 * 1000;
        if BackupSize > BackupFree then begin
          CloseFile(backfile);
          if BackupFiles < 1 then repeat
            Escaped := EscBox('Insufficient space on the backup drive.'#13
              +'Insert a different diskette into drive '+BackupLoc[1]+':'#13+
              'Press ESC to abort backup',MC);
            if not escaped then BackupFree := CheckDiskSpace(BackupLoc);
          until Escaped or (BackupFree >= BackupSize)
          else if (BackupNumber < 2) then
            Escaped := EscBox('More than one backup diskette will be required.'#13
            +'Do NOT change the diskette now.'#13'Press ESC to abort backup',MC);
          if Escaped then Break;
          if (BackupFiles > 0) then CBackupNumber := Long2StrL(BackupNumber);
          if (BackupFiles > 0) and not dobackup then Break;
          if (BackupFiles > 0) then repeat
            RingBell(false);
            done := false;
            Escaped := EscBox('Insert the next empty backup diskette into drive '
              +BackupLoc[1]+':'#13'Press ESC to abort backup',MC);
            if not Escaped and not FileExists(arc_name+ArcSuff+CBackupNumber)
              then begin
              done := CheckTheFloppy(Escaped,BackupLoc);
              if done then begin
                BackupFree := CheckDiskSpace(BackupLoc);
                BackupSize := Round(Search_Rec.Size*GameFileCompress);
                BackupSize := (BackupSize+1000) div 1000 * 1000;
              end;
            end;
          until Escaped or (done and (BackupFree >= BackupSize));
          if Escaped then Break;
          if (BackupFiles > 0) then begin
            Inc(BackupNumber);
            CBackupNumber := Long2StrL(BackupNumber);
          end;
          ReWrite(backfile);
          BackupFileSize := 0;
          BackupFiles := 0;
        end;
      end;
      Inc(BackupFileSize,Search_Rec.Size);
      Inc(BackupFiles);
      WriteLn(backfile,CFG.GFpath,Name,Typ);
      if MakeBMname(CFG.GFpath+Name+Typ,dbfname) then begin
        WriteLn(backfile,dbfname);
        Inc(BackupFiles);
      end;
      isbackup := true;
    end;
    Dec(PickHsize);
    HistUntag;
  end;
  HistDone;
  {$I-} CloseFile(backfile); {$I+}
  if IOResult = 0 then;
  if Escaped then exit;
  if isbackup then begin
    BackupFree := CheckDiskSpace(BackupLoc);
    BackupSize := Round(BackupFileSize*GameFileCompress);
    BackupSize := (BackupSize+1000) div 1000 * 1000;
    if BackupSize > BackupFree then begin
      ErrBox('Insufficient space on the backup drive.'#13
        +'Approximately '+Long2StrL(BackupSize)+' bytes required.'#13+
        Long2StrL(BackupFree)+' bytes available.  Backup not performed.',MC,0);
      Escaped := true;
      exit;
    end;
    dobackup;
  end;
end;

procedure BackupForMemphis;
begin
  BackupLoc := 'A:';
  BackupDataBase(false);
  if not Escaped then BackupTournFiles('a',false);
end;

end.
