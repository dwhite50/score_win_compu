unit Onlinerepu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IpUtils, IpSock, IpHttp, StdCtrls, StBase, StSpawn, OvcBase, OvcEditF,
  OvcEdPop, OvcEdCal;

type
  TOnLine = class(TForm)
    StSpawnApplication1: TStSpawnApplication;
    OKBut: TButton;
    CanBut: TButton;
    Label1: TLabel;
    StartEdit: TOvcDateEdit;
    OvcController1: TOvcController;
    EndEdit: TOvcDateEdit;
    StartEditLabel1: TOvcAttachedLabel;
    EndEditLabel1: TOvcAttachedLabel;
    MPbut: TButton;
    procedure OKclick(Sender: TObject);
    procedure OnCancel(Sender: TObject);
    procedure OnGetDate(Sender: TObject);
    procedure Startit(Sender: TObject);
    procedure MPclick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OnLine: TOnLine;

implementation

uses
    StStrL,
    StRegIni,
    csdef,
    mpdef,
    mtables,
    util2,
    FOpen,
    progress,
    onlinefixmp,
    YesNoboxU;

{$R *.DFM}

const
     ScoreDir   = 'C:\ACBLSCOR\';

var
   docan  : boolean;
   inhttp : boolean;
   htimer : word;
   isactive : boolean;
   TimerId       : LongWord;
   StartDate     : String;
   EndDate       : String;
   SDir          : String;
   Maxtime       : word;
   okclicked     : boolean;

{procedure GetHTTP(const xurl,OutName: String);
begin
  OnLine.Label1.Caption := xurl;
  docan := false;
  with OnLine,IpHttpClient1 do begin
    inhttp := true;
    htimer := 400;
    Maxtime := htimer;
    ProgressBarInit(xurl,OutName,false);
    try
      Download('https://'+xurl,OutName);
    except
    end;
    htimer := 0;
    ProgressBar(Maxtime,Maxtime);
    Application.ProcessMessages;
    sleep(300);
    FreeLink('https://'+xurl);
    inhttp := false;
    ProgressBarDone;
  end;
  DelTempIPFiles;
end;}

procedure GetCacheFolder;
var
   Reg : TStRegIni;
begin
  Reg := TStRegIni.Create(RIMachine,false);
  with Reg do begin
    CurSubKey := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Cache\Paths';
    SDir := ReadString('Directory','')+'\';
    Free;
  end;
end;

procedure GetIE(const xurl,OutName,SerName: String);
var
   de     : integer;
   df     : integer;
   ds     : TSearchRec;
   fs     : TSearchRec;
   txtfound : boolean;
   xName  : string;
   RunHandle : THandle;
begin
  GetCacheFolder;
  De := FindFirst(Sdir+'*.*',faSysFile+faDirectory,Ds);
  while de = 0 do begin
    Application.ProcessMessages;
    txtfound := (Ds.Attr and faDirectory) <> 0;
    if txtfound then repeat
      df := FindFirst(Sdir+Ds.Name+'\'+SerName+'*.*',faArchive,Fs);
      if df = 0 then DeleteFile(Sdir+Ds.Name+'\'+Fs.Name);
      FindClose(Fs);
    until df <> 0;
    De := FindNext(Ds);
  end;
  FindClose(Ds);
  Application.ProcessMessages;
  with OnLine.StSpawnApplication1 do begin
    RunParameters := '';
    FileName := 'https://'+xurl;
    ShowState := ssMinNotActive;
    RunHandle := Execute;
    ShowState := ssNormal;
  end;
  txtfound := false;
  htimer := 150;
  while (htimer > 0) and not txtfound do begin
    De := FindFirst(Sdir+'*.*',faSysFile+faDirectory,Ds);
    while (de = 0) and not txtfound do begin
      Application.ProcessMessages;
      txtfound := (Ds.Attr and faDirectory) <> 0;
      if txtfound then begin
        txtfound := FindFirst(Sdir+Ds.Name+'\'+SerName+'*.*',faArchive,Fs) = 0;
        FindClose(Fs);
        txtfound := txtfound and (Fs.Size > 1500);
      end;
      if txtfound then xName := Sdir+Ds.Name+'\'+Fs.Name
      else De := FindNext(Ds);
    end;
    FindClose(Ds);
    Application.ProcessMessages;
    if not txtfound then Sleep(100);
  end;
  htimer := 0;
  if txtfound then begin
    CopyFile(PChar(xName),PChar(OutName),false);
    DeleteFile(xName);
  end;
  if RunHandle <> 0 then TerminateProcess(RunHandle,0);
  RunHandle := 0;
end;

function HttpTimer(Param: Pointer): integer;
begin
  Result := 0;
  while isactive do begin
    Application.ProcessMessages;
    sleep(1000);
    if htimer > 0 then begin
      while htimer > 0 do begin
        ProgressBar(Maxtime-htimer,Maxtime);
        Application.ProcessMessages;
        sleep(1000);
        Dec(htimer);
      end;
//      if inhttp then OnLine.IpHttpClient1.Cancel;
    end;
  end;
end;

function CheckDates: boolean;
begin
  inhttp := false;
  docan := false;
  htimer := 0;
  isactive := true;
  StartDate := OnLine.StartEdit.DateString('yyyymmdd');
  Result := StartDate > '20020100';
  if Result then with OnLine do begin
    EndEdit.Visible := false;
    StartEdit.Visible := false;
    OKBut.Visible := false;
    MPbut.Visible := false;
  end
  else begin
    ErrBox('Start date must be 01/01/2002 or later',6,0);
    OnLine.StartEdit.PopupOpen;
  end;
end;

procedure TOnLine.OKclick(Sender: TObject);
const
//     EMPrepUrl = 'www.e-bridgemaster.com/admin/ACBLMPs.asp?password=Jim&Month=';
//    ErepUrl = 'www.e-bridgemaster.com/admin/Tournaments.asp?password=Dana&ddmmyy=';
    ErepUrl = 'www.worldwinner.com/cgi/nosession/bridge/acbltourns.pl?password=Dana&ddmmyy=';
var
   isok   : boolean;
   Month  : integer;
   Year   : integer;
   Day    : integer;
   eday   : integer;
   j      : integer;
   oldsess     : word;
   Curlet      : char;
   SaveLocation : String;
   SaveName     : String;
   RF           : Textfile;
   HtmName      : String;

procedure WriteDelim(const Field: String; const comma: boolean);
begin
  Write(RF,Delimiter,Field,Delimiter);
  if comma then Write(RF,',')
  else WriteLn(RF);
end;

function SaveCR: boolean;
const
     tr   = '<TR>';
     trs  = '</TR>';
     td   = '<TD>';
     tds  = '</TD>';
     tdx  = '<TD ';
     tabx = '</TABLE>';
var
   hf     : Textfile;
   htmpage  : String;
   htmline  : String;
   fldline  : String;
   lineok   : boolean;

procedure Gethtmline;
var
   trloc  : integer;
   tabloc : integer;
begin
  trloc := Pos(tr,UpperCase(htmpage));
  tabloc := Pos(tabx,UpperCase(htmpage));
  if (trloc < 1) or ((tabloc > 0) and (tabloc < trloc)) then htmline := ''
  else begin
    htmpage := Copy(htmpage,trloc+4,65000);
    trloc := Pos(trs,UpperCase(htmpage));
    if trloc < 2 then htmline := htmpage
    else htmline := Copy(htmpage,1,trloc-1);
  end;
end;

procedure GetField;
var
   tdloc  : integer;
begin
  tdloc := Pos(td,UpperCase(htmline));
  if tdloc < 1 then tdloc := Pos(tdx,UpperCase(htmline));
  if tdloc < 1 then fldline := ''
  else begin
    htmline := Copy(htmline,tdloc+4,65000);
    tdloc := Pos(tds,UpperCase(htmline));
    if tdloc < 2 then fldline := htmline
    else fldline := Copy(htmline,1,tdloc-1);
    repeat
      tdloc := Pos('>',fldline);
      if tdloc > 0 then fldline := Copy(fldline,tdloc+1,65000);
    until tdloc < 1;
  end;
end;

procedure ParseFields;
var
   j        : integer;
   Year,
   Month,
   Day      : word;
   Gdate    : TDateTime;
   WeekDay  : integer;
   tables   : integer;
   gtype    : char;
   mps      : real;
   Depth    : integer;
   sess     : word;
   xmps     : real;

function GetTotMPs(const first: real; const donew: boolean): real;
var
   k      : integer;
   uncaptop : real;
begin
  Result := first;
  if donew then uncaptop := tables * first * 2 / 30
  else uncaptop := first;
  for k := 2 to Depth do
    Result := Result+GetSecAward(first,uncaptop,k,4,tables,false,rRP);
  if gtype = 'P' then Result := Result * 2;
end;

begin
  for j := 1 to 11 do begin
    GetField;
    case j of
      1: begin {date, time}
        Year := ival(Copy(fldline,1,4));
        Month := ival(copy(fldline,6,2));
        Day := ival(Copy(fldline,9,2));
        Gdate := EncodeDate(Year,Month,Day);
        WeekDay := DayOfWeek(Gdate);
        if WeekDay < 2 then WeekDay := 8;
        WeekDay := (WeekDay-2) * 3;
        if Copy(fldline,12,2) < '12' then sess := WeekDay+1
        else if Copy(fldline,12,2) < '18' then sess := WeekDay+2
        else sess := WeekDay+3;
        fldline := numcvt(month,2,true)+numcvt(day,2,true)+numcvt(year,4,true);
        WriteDelim(fldline,true);
      end;
      2: begin
        WriteDelim(fldline,true);
        WriteDelim(Long2StrL(sess),true);
      end;
      3: begin
        if sess <> oldsess then Curlet := 'A'
        else Inc(CurLet);
        oldsess := sess;
        fldline := Curlet;
        WriteDelim(fldline,true);
      end;
      4: begin
        gtype := fldline[1];
        fldline := gtype;
        WriteDelim(fldline,true);
      end;
      5: WriteDelim(fldline[1],true);
      6: WriteDelim('248286',true);
      10: begin
        tables := Round(Valu((fldline)));
        Inc(tables,tables);
        if gtype = 'I' then Inc(tables,tables);
        Depth := Round(tables * 0.40);
        WriteDelim(fldline,true);
      end;
      11: begin
        mps := valu(fldline);
        xmps := GetTotMPs(1.3,false);
//        xmps := GetTotMPs(1.2,true);
        if mps < xmps then fldline := '12'
        else fldline := '18';
        WriteDelim(fldline,false);
      end;
      {13: fldline := Real2StrL(GetTotMPs(1.2,false),4,2);
      14: fldline := Real2StrL(GetTotMPs(1.5,false),4,2);
      15: fldline := Real2StrL(GetTotMPs(1.2,true),4,2);
      16: fldline := Real2StrL(GetTotMPs(1.5,true),4,2);}
    end;
  end;
end;

begin
  SaveCR := false;
  AssignFile(hf,HtmName);
  {$I-}Reset(hf); {$I+}
  if IOResult <> 0 then exit;
  repeat
    ReadLn(hf,htmpage);
  until (Pos(tr,UpperCase(htmpage)) > 0) or Eof(hf);
  CloseFile(hf);
  DeleteFile(HtmName);
  Gethtmline;
  repeat
    Gethtmline;
    lineok := Length(htmline) > 10;
    if lineok then begin
      ParseFields;
      SaveCR := true;
    end;
  until not lineok;
end;

var
   htries   : byte;
label
     Doexit,
     Htmagain;
begin
  if not CheckDates then exit;
  okclicked := true;
  EndDate := EndEdit.DateString('yyyymmdd');
  if EndDate < StartDate then EndDate := StartDate;
  eday := Ival(Copy(EndDate,7,2));
  Year := Ival(Copy(StartDate,3,2));
  Month := Ival(Copy(StartDate,5,2));
  Day := Ival(Copy(StartDate,7,2));
  SaveName := StartDate+'.CSV';
  if not GetFileName('','*.CSV',SaveName,
    'Select file to save for Club Report input','SaveFileLoc',ScoreDir,true)
    then goto doexit;
  AssignFile(RF,SaveName);
  ReWrite(RF);
  BeginThread(nil,0,HttpTimer,nil,0,TimerId);
  oldsess := 0;
  for j := Day to eday do begin
    htries := 0;
Htmagain:
    HtmName := numcvt(year,2,true)+numcvt(Month,2,true)+numcvt(j,2,true)+'.htm';
//    GetHttp(ErepUrl+numcvt(j,2,true)+'/'+numcvt(Month,2,true)+'/'
//      +numcvt(Year,2,true),HtmName);
    GetIE(ErepUrl+numcvt(Month,2,true)+'/'+numcvt(j,2,true)+'/'
      +numcvt(Year,2,true),HtmName,'acbltourns');
    if docan then Break;
    Inc(htries);
    if not SaveCR and (htries < 2) then goto Htmagain;
  end;
  CloseFile(RF);
doexit:
  isactive := false;
  Application.Terminate;
end;

procedure TOnLine.OnCancel(Sender: TObject);
begin
//  if inhttp then IpHttpClient1.Cancel;
  docan := true;
  {if not okclicked then }ExitProcess(0);
end;

procedure TOnLine.OnGetDate(Sender: TObject);
begin
//  EndEdit.Date := StartEdit.Date;
  EndEdit.Visible := true;
end;

procedure TOnLine.Startit(Sender: TObject);
begin
  AppName := 'GetOnLineRep';
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  okclicked := false;
  StartEdit.Date := Now-1;
//  EndEdit.Visible := false;
  EndEdit.Date := StartEdit.Date;
//  StartEdit.PopupOpen;
end;

procedure TOnLine.MPclick(Sender: TObject);

const
//     tbo   = '<BODY>';
     tbr   = '<BR>';

var
   hf     : Textfile;
   htmpage  : String;
   htmline  : String;
   lineok   : boolean;

procedure Gethtmline;
var
   trloc  : integer;
begin
  trloc := Pos(tbr,UpperCase(htmpage));
  if trloc < 2 then htmline := ''
  else begin
    htmline := Copy(htmpage,1,trloc-1);
    htmpage := Copy(htmpage,trloc+4,650000);
  end;
end;

const
//     EMPrepUrl = 'www.e-bridgemaster.com/admin/ACBLMPs.asp?password=Jim&Month=';
     EMPrepUrl = 'www.worldwinner.com/cgi/nosession/bridge/acblmps.pl?password=Jim&Month=';
var
//   HtmName      : String;
   SaveName     : String;
//   RF           : Textfile;

label
     doexit;
begin
  if not CheckDates then exit;
  SaveName := Copy(StartDate,1,6)+'.TXT';
//  HtmName := Copy(StartDate,1,6)+'.htm';
  BeginThread(nil,0,HttpTimer,nil,0,TimerId);
  GetIE(EMPrepUrl+Copy(StartDate,5,2)+'&Year='+Copy(StartDate,1,4),SaveName,'248286');
  if not docan then begin
    (*AssignFile(RF,SaveName);
    ReWrite(RF);
    AssignFile(hf,HtmName);
    {$I-}Reset(hf); {$I+}
    if IOResult <> 0 then goto doexit;
    repeat
      ReadLn(hf,htmpage);
    until (Pos(tbr,UpperCase(htmpage)) > 0) or Eof(hf);
    CloseFile(hf);
    DeleteFile(HtmName);
    repeat
      Gethtmline;
      lineok := Length(htmline) > 10;
      if lineok then WriteLn(RF,htmline);
    until not lineok;
    CloseFile(RF);*)
    SubmitOnlineMPRep(SaveName);
    DeleteFile(SaveName);
  end;
doexit:
  isactive := false;
  Application.Terminate;
end;

end.
