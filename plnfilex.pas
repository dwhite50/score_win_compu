Unit plnfile;

interface

procedure PlnFileDone;
function PlnFileInit(var esc: boolean; var PlnSize: longint):boolean;
function PlnFileFind(const Player: String): boolean;
function PlnFileNext(var Player: OpenString): boolean;

implementation

Uses
    SysUtils,
    Fopen,
    util2,
    YesNoBoxU,
    history,
    csdef,
    pvio;

type
    TFindRec = packed Record
      PlNum  : Array[0..6] of char;
      crlf   : Array[0..1] of char;
    end;

    TFindVars = packed Record
      FindFile     : file of TFindRec;
      FindFileSize : Longint;
      FindInitArray : Array[1..7] of TFindRec;
      FindInitRecs  : Array[1..7] of longint;
      FindInitSize  : word;
      FindFileName  : String;
      FindFileLocation : String;
      CurSeqRec     : longint;
      CurFindRec    : TFindRec;
      CurFind       : longint;
      LowFind       : longint;
      HighFind      : longint;
    end;
    PFindVars = ^TFindVars;

const FindVars : PFindVars = nil;
     DefFindFileName       = 'PLNUM.PLN';
     InSequence  : boolean = false;

procedure PlnFileDone;
begin
  if Assigned(FindVars) then begin
    {$I-} Close(FindVars^.FindFile); {$I+}
    if IOResult = 0 then;
    Dispose(FindVars);
    FindVars := nil;
  end;
end;

function PlnFileInit(var esc: boolean; var PlnSize: longint):boolean;
var
   {ifunc        : byte;
   len          : byte;
   Search_Rec   : TSearchRec;
   ierr         : integer;}
   n            : word;
   Testnum       : String[7];
   {FindErr       : integer;}
label
     path_again;
begin
  PlnFileDone;
  New(FindVars);
  PlnFileInit := false;
  {ShowSizeDateTime := True;
  FilesUpper := True;}
  with FindVars^ do begin
    if CFG.tournament then FindFileLocation := TournPath
    else FindFileLocation := 'A:\';
    {ShowExtension := True;}
    FindFileName := '';
path_again:
    if not GetFileName(FindFileLocation,'Player number files |*.PLN',
      FindFileName,'Select ACBL number file to use',false) then begin
      PlnFileDone;
      exit;
    end;
    if length(FindFileName) < 1 then goto path_again;
    Assign(FindFile,FindFileName);
    {$I-} ReSet(FindFile); {$I+}
    if IOResult <> 0 then begin
      {RingClear(false);}
      ErrBox('File '+FindFileName+' not found.',0);
      goto path_again;
    end;
    {SavePath(fixpath(JustPathName(FindFileName)),3);}
    FindFileSize := FileSize(FindFile);
    PlnSize := FindFileSize;
    if FindFileSize > 320 then FindInitSize := 7
    else if FindFileSize > 20 then FindInitSize := 3
    else FindInitSize := 1;
    CurFind := FindFileSize div (FindInitSize+1);
    LowFind := 0;
    CurSeqRec := -1;
    InSequence := true;
    for n := 1 to FindInitSize do begin
      Inc(LowFind,CurFind);
      FindInitRecs[n] := LowFind;
      Seek(FindFile,LowFind);
      Read(FindFile,FindInitArray[n]);
      TestNum := FindInitArray[n].PlNum;
      with FindInitArray[n] do if (TestNum <> Player_Key(TestNum))
        or (not Player_check(TestNum,pnACBL)) or ((n > 1)
        and (PlNum <= FindInitArray[n-1].PlNum)) then begin
        InSequence := false;
        {MsgBox(FindFileName+' is not'#13'a valid ACBL number file',intont,
          true,MC,CFG.WarnColor,0);
        PlnFileDone;
        Exit;}
      end;
    end;
  end;
  PlnFileInit := true;
end;

function PlnFileNext(var Player: OpenString): boolean;
begin
  PlnFileNext := false;
  if not Assigned(FindVars) then exit;
  with FindVars^ do if CurSeqRec+1 < FindFileSize then begin
    Inc(CurSeqRec);
    if CurSeqRec < 0 then CurSeqRec := 0;
    Seek(FindFile,CurSeqRec);
    Read(FindFile,CurFindRec);
    Player := Player_key(CurFindRec.PlNum);
    PlnFileNext := true;
  end;
end;

function PlnFileFind(const Player: String): boolean;
var
   FindTarget : String[7];
   j          : word;
begin
  PlnFileFind := false;
  if not InSequence then exit;
  FindTarget := Player_Key(Player);
  if not Player_Check(FindTarget,pnACBL) then exit;
  if not Assigned(FindVars) then exit;
  with FindVars^ do begin
    HighFind := 0;
    for j := 1 to FindInitSize do begin
      if FindTarget <= FindInitArray[j].PlNum then begin
        if j > 1 then LowFind := FindInitRecs[j-1]
        else LowFind := 0;
        HighFind := FindInitRecs[j];
        Break;
      end;
    end;
    if HighFind < 1 then begin
      LowFind := FindInitRecs[FindInitSize];
      HighFind := FindFileSize-1;
    end;
    CurFind := (HighFind+LowFind) div 2;
    Seek(FindFile,CurFind);
    Read(FindFile,CurFindRec);
    with CurFindRec do while (FindTarget <> PlNum)
      and (HighFind > LowFind) do begin
      if LowFind+1 = HighFind then Inc(LowFind)
      else begin
        if FindTarget > PlNum then LowFind := CurFind
        else HighFind := CurFind;
      end;
      CurFind := (HighFind+LowFind) div 2;
      Seek(FindFile,CurFind);
      Read(FindFile,CurFindRec);
    end;
    PlnFileFind := Player_Key(CurFindRec.PlNum) = FindTarget;
  end;
end;

end.