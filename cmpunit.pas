Unit cmpunit;
{.$define test}
{.$define testdupmp}

interface

type
    tft         = Array[1..2] of byte; {position,length}

    TMPRec      = Packed Record
      PlNum     : Array[1..7] of char;
      MPs       : Array[1..5] of char;
      MPtype    : char;
      MPcolor   : char;
    end;

    TMPNonRec   = packed Record
      PlRec     : TMPRec;
      LastN     : Array[1..16] of char;
      FirstN    : Array[1..16] of char;
      City      : Array[1..16] of char;
      state     : Array[1..2] of char;
    end;

    TNMPRec      = Packed Record
      PlNum     : Array[1..7] of char;
      MPs       : Array[1..5] of char;
      MPtype    : char;
      MPcolor   : char;
      Stratum   : char;
      LowRank   : Array[1..3] of char;
      HighRank  : Array[1..3] of char;
      Section   : Array[1..2] of char;
      Direction : char;
      PairNum   : Array[1..3] of char;
      Score     : Array[1..4] of char;
      BdsPlayed : Array[1..3] of char;
    end;

    TNMPNonRec   = packed Record
      PlRec     : TMPRec;
      LastN     : Array[1..16] of char;
      FirstN    : Array[1..16] of char;
      City      : Array[1..16] of char;
      state     : Array[1..2] of char;
      Stratum   : char;
      LowRank   : Array[1..3] of char;
      HighRank  : Array[1..3] of char;
      Section   : Array[1..2] of char;
      Direction : char;
      PairNum   : Array[1..3] of char;
      Score     : Array[1..4] of char;
      BdsPlayed : Array[1..3] of char;
    end;

    TClubMPRec  = packed Record
      RecType   : char;
      sequence  : Array[1..4] of char;
      records   : Array[1..4] of char;
      Erating   : Array[1..2] of char;
      GameDate  : Array[1..8] of char;
      ClubSessNum : Array[1..2] of char;
      ClubNum     : Array[1..6] of char;
      SancNum     : Array[1..10] of char;
      UnitNum     : Array[1..3] of char;
      DistNum     : Array[1..2] of char;
      case char of
        '1': (ClubName  : Array[1..40] of char;
              version   : Array[1..12] of char);
        '2': (sessions  : Array[1..2] of char;
              tables    : Array[1..4] of char;
              strata    : char;
              PTI       : char;
              EventName : Array[1..25] of char;
              seniors   : char;
              MPLimits  : Array[1..3,1..4] of char;
              DetRecs   : Array[1..4] of char;
              StTables  : Array[1..3,1..4] of char;
              GameType  : char;
              Boards    : Array[1..3] of char;
              StLetters : Array[1..3] of char;
              GameRate  : Array[1..5] of char;
              TableRate : Array[1..5] of char;
              TableFees : Array[1..6] of char;
              ExtraRate : Array[1..5] of char;
              ExtraFees : Array[1..6] of char;
              FeeCode   : char);
        '3': (Numentries: Array[1..2] of char;
              PlayerRec : Array[1..14] of TMPrec);
        'X': (Nuxentries: Array[1..2] of char;
              NPlayerRec : Array[1..6] of TNMPrec);
        '4': (Nunentries: Array[1..2] of char;
              PlayNonRec: Array[1..3] of TMPNonRec);
        'Y': (Nuyentries: Array[1..2] of char;
              NPlayNonRec: Array[1..2] of TNMPNonRec);
        '9': (filler    : Array[1..2] of char;
              ChkMPs    : Array[1..5,1..8] of char);
    end;
    PClubMPRec  = ^TClubMPRec;

    TClubFinRec = packed Record
      RecLen    : byte;
      RecType   : char;
      sequence  : Array[1..4] of char;
      ClubNum   : Array[1..6] of char;
      RepYear   : Array[1..4] of char;
      RepMonth  : Array[1..2] of char;
      case char of
        '1': (ClubType  : char;
              UnitNum   : Array[1..3] of char;
              DistNum   : Array[1..2] of char;
              ClubName  : Array[1..40] of char;
              ManagName : Array[1..30] of char;
              ClubStreet: Array[1..26] of char;
              ClubCity  : Array[1..16] of char;
              ClubState : Array[1..2] of char;
              ClubZip   : Array[1..10] of char;
              CountryCode : char;
              version   : Array[1..12] of char);
        '2': (ChargeableGames : Array[1..4] of char;
              GameSancFee     : Array[1..4] of char;
              GameFees        : Array[1..6] of char;
              ChargeableTables: Array[1..5] of char;
              TableSancFee    : Array[1..4] of char;
              TableFees       : Array[1..6] of char;
              LateDays        : Array[1..3] of char;
              LateDayRate     : Array[1..4] of char;
              LateFees        : Array[1..6] of char;
              TotalFees       : Array[1..6] of char;
              UnchargedGames  : Array[1..4] of char;
              TotalGames      : Array[1..4] of char;
              UnchargedTables : Array[1..5] of char;
              TotalTables     : Array[1..5] of char;
              ReportedSess    : Array[1..2] of char;
              PaymentType     : char;
              CreditCardType  : char;
              CreditCardNo    : Array[1..16] of char;
              ExpiryDate      : Array[1..6] of char;
              NameOnCard      : Array[1..25] of char;
              NumSum          : Array[1..2] of char;
              ZipCode         : Array[1..6] of char);
        '3': (SessNum         : Array[1..2] of char;
              GameTime        : Array[1..5] of char;
              GameType        : Array[1..4] of char;
              NumReportedDates: Array[1..3] of char;
              NumReportedSecs : Array[1..3] of char;
              NumUnrepDates   : Array[1..3] of char;
              UnRepDates      : Array[1..4,1..2] of char);
        '4': (DSessNum        : Array[1..2] of char;
              GameDate        : Array[1..8] of char;
              Restriction     : char;
              Rating          : Array[1..2] of char;
              DGameType       : char;
              Chargeable      : char;
              Section         : Array[1..2] of char;
              EventSession    : char;
              Tables          : Array[1..3] of char;
              Strata          : char;
              Handicap        : char;
              MPlimits        : Array[1..3,1..4] of char;
              EventName       : Array[1..25] of char;
              Director        : Array[1..30] of char;
              Comment         : Array[1..36] of char;
              Boards          : Array[1..3] of char;
              FeeCode         : char;
              TableRate       : Array[1..5] of char;
              NTableFees      : Array[1..6] of char;
              ExtraRate       : Array[1..5] of char;
              ExtraFees       : Array[1..6] of char;
              LocalCharity    : Array[1..40] of char;
              ACBLPlayers     : Array[1..3] of char;
              NonPlayers      : Array[1..3] of char);
        '5': (SRating         : Array[1..2] of char;
              SFeeCode        : char;
              SGames          : Array[1..4] of char;
              STables         : Array[1..6] of char;
              SRate           : Array[1..5] of char;
              SFees           : Array[1..7] of char);
    end;
    PClubFinRec = ^TClubFinRec;

    _str40      = String[40];

const
     ColorLetter  : String[4] = 'BRGS';
     ColorOrder   : Array[1..4] of byte = (1,4,2,3);
     ColorROrder  : Array[1..4] of byte = (1,3,4,2);
    {TClubFinRec = Record}
      {RecLen    : tft = (0,1);}
      RecType   : tft = (1,1);
      sequence  : tft = (2,4);
      ClubNum   : tft = (6,6);
      RepYear   : tft = (12,4);
      RepMonth  : tft = (16,2);
      {Rectype 1 definitions follow}
              ClubType  : tft = (18,1);
              UnitNum   : tft = (19,3);
              DistNum   : tft = (22,2);
              ClubName  : tft = (24,40);
              ManagName : tft = (64,30);
              ClubStreet: tft = (94,26);
              ClubCity  : tft = (120,16);
              ClubState : tft = (136,2);
              ClubZip   : tft = (138,10);
              CountryCode : tft = (148,1);
              version   : tft = (149,12);
      {Rectype 2 definitions follow}
              ChargeableGames : tft = (18,4);
              GameSancFee     : tft = (22,4);
              GameFees        : tft = (26,6);
              ChargeableTables: tft = (32,5);
              TableSancFee    : tft = (37,4);
              TableFees       : tft = (41,6);
              LateDays        : tft = (47,3);
              LateDayRate     : tft = (50,4);
              LateFees        : tft = (54,6);
              TotalFees       : tft = (60,6);
              UnchargedGames  : tft = (66,4);
              TotalGames      : tft = (70,4);
              UnchargedTables : tft = (74,5);
              TotalTables     : tft = (79,5);
              ReportedSess    : tft = (84,2);
              PaymentType     : tft = (86,1);
              CreditCardType  : tft = (87,1);
              CreditCardNo    : tft = (88,16);
              ExpiryDate      : tft = (104,6);
              NameOnCard      : tft = (110,25);
              SumRecords      : tft = (135,2);
              ZipCode         : tft = (137,6);
      {Rectype 3 definitions follow}
              SessNum         : tft = (18,2);
              GameTime        : tft = (20,5);
              GameType        : tft = (25,4);
              NumReportedDates: tft = (29,3);
              NumReportedSecs : tft = (32,3);
              NumUnrepDates   : tft = (35,3);
              UnRepDates      : Array[1..5] of tft = (
                              (38,2),(40,2),(42,2),(44,2),(46,2));
      {Rectype 4 definitions follow}
              DSessNum        : tft = (18,2);
              GameDate        : tft = (20,8);
              Restriction     : tft = (28,1);
              Rating          : tft = (29,2);
              DGameType       : tft = (31,1);
              Chargeable      : tft = (32,1);
              Section         : tft = (33,2);
              EventSession    : tft = (35,1);
              Tables          : tft = (36,3);
              Strata          : tft = (39,1);
              Handicap        : tft = (40,1);
              MPlimits        : Array[1..3] of tft = (
                              (41,4),(45,4),(49,4));
              EventName       : tft = (53,25);
              Director        : tft = (78,30);
              Comment         : tft = (108,36);
(*              FBoards         : tft = (144,3);
              FFeeCode        : tft = (147,1);
              FTableRate      : tft = (148,5);
              FTableFees      : tft = (153,6);
              FExtraRate      : tft = (159,5);
              FExtraFees      : tft = (164,6);
              FLocalCharity   : tft = (170,40);
      {Rectype 5 definitions follow}
              SRating         : tft = (18,2);
              SFeeCode        : tft = (20,1);
              SGames          : tft = (21,4);
              STables         : tft = (25,6);
              SRate           : tft = (31,5);
              SFees           : tft = (36,7);*)

     RecLen  = 22;
const
     mpftBad = 0;
     mpftOld = 1;
     mpftNew = 2;
     cftype : String[7] = '';
     TempDir  = 'C:\ACBLTEMP';
     NewRepType : boolean = false;
     ShowCmpErrors : boolean = true;
     RepVersion : integer = 0;

var
   infilename : String;
   infile     : textFile;
   outfile    : textFile;
   outfilename : String;

function GetClubUnitDist(const Club,DefUnitDist: string;
         const isdist: boolean): string;
function TestClubFile(const ClubFileName: String; var AVersion: word): byte;
function ValidField(const PField: Pointer; const len: byte; Value: String):boolean;
function Bad_Eof(var F: textFile): boolean;
function ChrStr(const PArray: Pointer; const len: byte): _Str40;
function ChrNum(const PArray: Pointer; const len: byte): longint;
function ChrReal(const PArray: Pointer; const len,places: byte): Real;
procedure NumChr(const PField: Pointer; const len: byte; const Value: longint);
procedure FixNumField(const PField: Pointer; const len: byte);
procedure closeMPfiles;
function ValidMPFile(var F: textFile; const filename: String;
         var line: OpenString; const allowdupmps: boolean): byte;

implementation

uses
    Windows,
    SysUtils,
    pvio,
    YesNoBoxU,
    StStrL,
    txtview1,
    history,
    util2;


const
     MaxClub  = 3500;
     NumClubs : word = 0;
var
   cclubs : Array[1..MaxClub] of string[6];
   cunits : Array[1..MaxClub] of string[3];
   cDists : Array[1..MaxClub] of string[2];

procedure LoadClubs;
const
     ClubUnitFileName = '\\BRIDGE\SYS\ACBLSCOR\CLUBUNIT.TXT';
var
   F : textfile;
   line : string;
begin
  if NumClubs > 0 then exit;
  Assign(F,ClubUnitFileName);
  {$I-} Reset(F); {$I+}
  if IOResult <> 0 then exit;
  repeat
    ReadLn(F,line);
    if Length(line) > 11 then begin
      Inc(numclubs);
      cclubs[numclubs] := ExtractWordL(1,line,' ');
      cunits[numclubs] := ExtractWordL(2,line,' ');
      cdists[numclubs] := ExtractWordL(3,line,' ');
    end;
  until Eof(F) or (Length(line) < 12) or (NumClubs >= MaxClub);
  CloseFile(F);
end;

function GetClubUnitDist(const Club,DefUnitDist: string;
         const isdist: boolean): string;
var
   j  : word;
begin
  if numclubs < 1 then LoadClubs;
  Result := DefUnitDist;
  if numclubs < 1 then exit;
  for j := 1 to numclubs do begin
    if club = cclubs[j] then begin
      if isdist then begin
        Result := cdists[j];
        if length(Result) < 2 then Result := '0'+Result;
      end
      else Result := cunits[j];
      exit;
    end;
    if club < cclubs[j] then exit;
  end;
end;

function ChrNum(const PArray: Pointer; const len: byte): longint;
var
   TempStr      : String[15];
begin
  ChrNum := 0;
  if len < 16 then begin
    Move(PArray^,TempStr[1],len);
    TempStr[0] := Char(len);
    ChrNum := Ival(TempStr);
  end;
end;

function ChrReal(const PArray: Pointer; const len,places: byte): Real;
var
   x    : real;
   j    : byte;
begin
  x := ChrNum(PArray,len);
  for j := 1 to places do x := x/10;
  ChrReal := x;
end;

function ChrStr(const PArray: Pointer; const len: byte): _Str40;
var
   TempStr      : _Str40;
begin
  ChrStr := '';
  if len < 41 then begin
    Move(PArray^,TempStr[1],len);
    TempStr[0] := Char(len);
    ChrStr := TempStr;
  end;
end;

function TestClubFile(const ClubFileName: String; var AVersion: word): byte;
// 0 = ok; 1 = duplicate mps; 2 = bad; 3 = MP total = 0
{$ifdef testdupmp}
type
    MPCell = Packed Record
      Plnum : String[7];
      MPused : Array[1..4] of boolean;
    end;
const
     MaxMPcells = 1000;
{$endif}
var
   lastseqdiff    : word;
   wseq           : word;
   j              : word;
   Drec           : integer;
   recs     : integer;
   inrec       : String[254];
   ClubMpRec    : PClubMPRec;
   EventRecs    : integer;
   club         : String[6];
   ERec         : integer;
   DetailRecs   : integer;
   Entries      : integer;
   ClubMPHeader   : TClubMPRec;
   ClubMPEvent    : TClubMPRec;
   TotMPS      : Array[1..5] of longint;
   F           : textfile;
{$ifdef testdupmp}
   ncells      : integer;
   MPcells     : Array[1..MaxMPcells] of MPcell;
   badmps      : boolean;

function CheckMP(const PPlNum: Pointer; const color: byte): boolean;
var
   pnum : String[7];
   j     : integer;
begin
  Pnum[0] := char(7);
  move(PPlnum^,pnum[1],7);
  for j := 1 to ncells do with mpcells[j] do if plnum = pnum then begin
    Result := mpused[ColorOrder[color]];
    mpused[ColorOrder[color]] := true;
    {$ifdef test}
    if Result then ErrBox('Duplicate mps: '+pnum+'; color: '+Long2StrL(color),0);
    {$endif}
    exit;
  end;
  Result := false;
  if ncells >= MaxMPcells then exit;
  Inc(ncells);
  with mpcells[ncells] do begin
    plnum := pnum;
    mpused[ColorOrder[color]] := true;
  end;
end;
{$endif}

var
   EvtRecs : word;
   FF      : TextFile;
label
     File_done;
begin
  recs := 0;
  lastseqdiff := 0;
  Result := 2;
  {$ifdef testdupmp}
  badmps := false;
  {$endif}
  ClubMPRec := @inrec[1];
  infilename := ClubFileName;
  FillChar(TotMPS,SizeOf(TotMPs),0);
  Assign(F,ClubFileName);
  {$I-} Reset(F); {$I+}
  if IOResult <> 0 then exit;
  with ClubMPRec^ do try
    EvtRecs := 0;
    {$I-} ReadLn(F,inrec); {$I+}
    CheckIOResult(ClubFileName);
    if canceled then exit;
    EventRecs := ChrNum(@records,4);
    Repeat
      {$I-} ReadLn(F,inrec); {$I+}
      CheckIOResult(ClubFileName);
      if canceled then exit;
      if RecType = '2' then Inc(EvtRecs);
    until (RecType = '9') or Eof(F);
    CloseFile(F);
    if EventRecs <> EvtRecs then begin
      Assign(FF,'TEMPMP.TMP');
      ReWrite(FF);
      ReSet(F);
      ReadLn(F,inrec);
      NumChr(@records,4,EvtRecs);
      WriteLn(FF,inrec);
      ReadLn(F,inrec);
      NumChr(@records,4,EvtRecs);
      WriteLn(FF,inrec);
      Repeat
        ReadLn(F,inrec);
        if RecType = '2' then NumChr(@records,4,EvtRecs);
        WriteLn(FF,inrec);
      until (RecType = '9') or Eof(F);
      CloseFile(FF);
      CloseFile(F);
      DeleteFile(ClubFileName);
      CopyFile('TEMPMP.TMP',PChar(ClubFileName),true);
    end;
    Reset(F);
    {$I-} ReadLn(F,inrec); {$I+}
    CheckIOResult(ClubFileName);
    if canceled then exit;
    inrec := PadL(inrec,96);
    if version[1] > '9' then
      RepVersion := ChrNum(@version[2],1) * 100 + ChrNum(@version[4],2)
    else RepVersion := ChrNum(@version,1) * 100 + ChrNum(@version[3],2);
    NewRepType := RepVersion >= 700;
    AVersion := RepVersion;
    Move(ClubMPRec^,ClubMPHeader,SizeOf(TClubMPRec));
    EventRecs := ChrNum(@records,4);
    club := ChrStr(@ClubNum,6);
    Inc(recs);
    ERec := 0;
    while ERec < EventRecs do begin
      Inc(ERec);
      if Bad_Eof(F) then exit;
      {$I-} ReadLn(F,inrec); {$I+}
      CheckIOResult(ClubFileName);
      if canceled then exit;
      Inc(recs);
      if RecType = '9' then begin
        EventRecs := MaxWord(ERec-1,1);
        goto File_Done;
      end;
      Move(ClubMPRec^,ClubMPEvent,SizeOf(TClubMPRec));
      if not ValidField(@RecType,1,'2') then exit;
      wseq := ChrNum(@sequence,4);
      if wseq > ERec+lastseqdiff then begin
        Dec(EventRecs,wseq-ERec-lastseqdiff);
        lastseqdiff := wseq-ERec;
      end;
      if lastseqdiff > 0 then NumChr(@sequence,4,ERec);
      if not ValidField(@sequence,4,numcvt(ERec,4,true)) then exit;
      if not ValidField(@ClubNum,6,club) then exit;
      if not (seniors in ['Y','N']) then seniors := 'N';
      if not (PTI in ['P','T','I']) then PTI := 'P';
      DetailRecs := ChrNum(@DetRecs,4);
      {$ifdef testdupmp}
      FillChar(MPcells,SizeOf(MPcells),0);
      ncells := 0;
      {$endif}
      for DRec := 1 to DetailRecs do begin
        if Bad_Eof(F) then exit;
        {$I-} ReadLn(F,inrec); {$I+}
        CheckIOResult(ClubFileName);
        if canceled then exit;
        if not (RecType in ['3','4']) then
          if not ValidField(@RecType,1,'3') then exit;
        if not ValidField(@sequence,4,numcvt(DRec,4,true)) then exit;
        if not ValidField(@ClubNum,6,club) then exit;
        Inc(recs);
        Entries := ChrNum(@Numentries,2);
        if NewReptype then case RecType of
          '3': for j := 1 to Entries do with NPlayerRec[j] do begin
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
            {$ifdef testdupmp}
            if CheckMP(@Plnum,Ival(mpcolor)) then badmps := true;
            {$endif}
          end;
          '4': for j := 1 to Entries do with NPlayNonRec[j].PlRec do
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        end
        else case RecType of
          '3': for j := 1 to Entries do with PlayerRec[j] do begin
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
            {$ifdef testdupmp}
            if CheckMP(@Plnum,Ival(mpcolor)) then badmps := true;
            {$endif}
          end;
          '4': for j := 1 to Entries do with PlayNonRec[j].PlRec do
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        end;
      end;
    end;
    if Bad_Eof(F) then exit;
    {$I-} ReadLn(F,inrec); {$I+}
    CheckIOResult(ClubFileName);
    if canceled then exit;
    Inc(recs);
File_done:
    if not ValidField(@RecType,1,'9') then exit;
    if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    {$ifdef testdupmp}
    if badmps then Result := 1
    else
    {$endif}
    Result := 0;
    for j := 1 to 4 do Inc(TotMPS[5],TotMPS[j]);
    for j := 1 to 5 do if TotMPS[j] > ChrNum(@ChkMps[j,1],8) then Result := 2;
    if (Result = 0) and (TotMPS[5] < 1) then Result := 3;
  finally
    {$I-} CloseFile(F); {$I+}
    if IOResult <> 0 then;
  end;
end;

function ValidMPFile(var F: textFile; const filename: String;
         var line: OpenString; const allowdupmps: boolean): byte;
var
   valid        : byte;
   aversion     : word;
begin
  Assign(F,filename);
  Reset(F);
  {$I-} ReadLn(F,line); {$I+}
  CheckIOResult(filename);
  if Length(line) <> RecLen then begin
    {$I-} CloseFile(F); {$I+}
    valid := TestClubFile(filename,Aversion);
    if valid in [0,1,3] then begin
      Result := mpftNew;
      Reset(F);
      {$I-} ReadLn(F,line); {$I+}
    end;
    if valid in [1,2] then begin
      if showCmpErrors and ((valid = 2) or (not allowdupmps and (valid = 1)))
        then ErrBox(filename+' is not a valid Club MP file',MC,0);
      {$I-} CloseFile(F); {$I+}
      if IOResult = 0 then;
      if (valid = 2) or (not allowdupmps and (valid = 1)) then Result := mpftBad;
    end;
  end
  else Result:= mpftOld;
end;

procedure closeMPfiles;
begin
  {$I-} CloseFile(infile); {$I+}
  if IOResult = 0 then;
  {$I-} CloseFile(outfile); {$I+}
  if IOResult = 0 then;
end;

procedure BadErr;
begin
  if not ShowCmpErrors then exit;
//  if ShowCmpErrors then begin
    ErrBox(infilename+' is not a valid Club '+Cftype+' file',MC,0);
    if ViewInit then
      ViewerAdd(infilename+' is not a valid Club '+Cftype+' file',true);
//  end;
  CloseMPFiles;
  canceled := true;
end;

function Bad_Eof(var F: textFile): boolean;
begin
  Bad_Eof := false;
  if not Eof(F) then exit;
  Bad_Eof := true;
  BadErr;
end;

function ValidField(const PField: Pointer; const len: byte; Value: String):boolean;
begin
  ValidField := true;
  if ChrStr(PField,len) = Value then exit;
  ValidField := false;
  BadErr;
end;

procedure NumChr(const PField: Pointer; const len: byte; const Value: longint);
var
   TempStr      : String[15];
begin
  TempStr := numcvt(Value,len,true);
  Move(TempStr[1],PField^,len);
end;

procedure FixNumField(const PField: Pointer; const len: byte);
var
   TempStr      : String[15];
begin
  TempStr := numcvt(ChrNum(PField,len),len,true);
  Move(TempStr[1],PField^,len);
end;

end.