{.$define test}
unit acblwideU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompLHA, viewbox, StBase, StSpawn, {Psock, NMFtp, }StdCtrls, ComCtrls,
  OvcBase, OvcEditF, OvcEdPop, OvcEdCal, OvcEF, OvcPB, OvcPF;

type
  TForm1 = class(TForm)
    CompLHA1: TCompLHA;
    Label1: TLabel;
    StatusBar1: TStatusBar;
    CanBut: TButton;
    StartEdit: TOvcDateEdit;
    OvcController1: TOvcController;
    EndEdit: TOvcDateEdit;
    OvcDateEdit1Label1: TOvcAttachedLabel;
    OvcDateEdit2Label1: TOvcAttachedLabel;
    OKbut: TButton;
    ClubEdit: TOvcPictureField;
    OvcPictureField1Label1: TOvcAttachedLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    procedure Docmpcopy(Sender: TObject);
    procedure Docan(Sender: TObject);
    procedure DoOk(Sender: TObject);
    procedure OnGetDate(Sender: TObject);
    procedure CanClick(Sender: TObject);
    procedure closeit(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
   inpath         : String;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses
    mpdef,
    mtables,
    db33,
    StStrL,
    dbase,
    txtview1,
    pvio,
    {ListBox,}
    VListBox,
    FOpen,
    groups,
    util1,
    util2,
    history,
    YesNoBoxU,
    StrBoxU,
    csdef,
    version,
    cmpunit;

const
     {$ifdef test}
     Archivepath = 'C:\TEMP\';
     {$else}
//     Archivepath = '\\BRIDGE\SYS\DEPT\550\ACBLSCOR\';
     Archivepath = 'E:\DEPT\550\ACBLSCOR\';
     {$endif}
     ScoreDir   = 'C:\ACBLSCOR\';
     LhaEx        = '.LZH';
     DupClubMask  = '????????-?'+LhaEx;
     NewClubMask  = '*'+LhaEx;
     ClubMMask    = 'M??????.';
     ClubFMask    = 'F??????.';
     ClubSavePath = Archivepath+'REPSAVE\';
     RepPath      = 'C:\CLUBREPX';
     DefRFName       = 'CSRANK.CSV';
     DefTabName      = 'TABLES.CSV';
     MaxSpecial   = 47;
     SpecialEvents = MaxSpecial-4;
     MaxRank      = 15;
     MaxDRank     = 10;
     MaxTeamSize  = 6;
     MaxDist      = 200;
     MaxClubs     = 1000;
     MaxVer       = 100;
     ChText       : Array[1..3] of String =
                  ('All Charities (Local + ACBL/Canadian)',
                   'Local Charities only',
                   'ACBL/Canadian Charities only');
     InvText      : Array[1..2] of String =
                  ('Open Games',
                   'Invitational Games');
     SpecialRatings : Array[1..MaxSpecial] of byte =
                    (3,4,7,8,12,17,18,56,57,58,20,21,22,23,24,25,26,59,60,61,27,
                     28,29,30,32,33,34,35,36,37,38{,41},42,43,49,55{,44,45},50,
                     51,11,62,63,64,65,66,16,97,99,98);
     SpecialText : array[1..MaxSpecial] of String =
    {masterpoint ratings text}
                ('Charity Club Championship',
                 'Unit Championship',
                 'Club International Fund',
                 'Club ACBL Membership',
                 'Upgraded Club Championship',
                 'Progressive Sectional',
                 'Unit Charity Championship',
                 'Unit International Fund',
                 'Unit Junior Fund',
                 'Unit Education Foundation',
                 'North American Pairs - Club',
                 'North American Pairs - Unit',
                 'Grand National Teams - Club',
                 'Grand National Teams - Unit',
                 'ACBL wide Charity',
                 'ACBL wide International Fund',
                 'District Charity',
                 'District International Fund',
                 'District Junior Fund',
                 'District Education Foundation',
                 'Canada Wide Olympiad Fund',
                 'WorldWide matchpoint',
                 'ACBL Wide instant matchpoint',
                 'Junior Fund',
                 'ACBL Wide Senior',
                 'COPC - Club',
                 'CNTC - Master / Non-master',
                 'CNTC - Club',
                 'CNTC - Unit',
                 'CWTC',
                 'Canada Rookie / Master',
                 {'North American 49er Pairs',}
                 'Super Club Championship',
                 'Club Appreciation game',
                 'Club Appreciation team game',
                 'Club Education Foundation game',
                 {'ACBL wide Charity: no hands',
                 'ACBL wide Intl Fund: no hands',}
                 'NABC Promotional game',
                 'GNT Fund raiser',
                 'Other Fund raiser',
                 'CBF Simultaneous Pairs',
                 'Club Grass Roots fund game',
                 'Unit Grass Roots fund game',
                 'District Grass Roots fund game',
                 'Alzheimer fund game',
                 'Scan for manually recorded points',
                 'Scan for second (duplicate) reports',
                 'All Special games',
                 'Specify Special games');
     MaxTime    = 6;
     TimeText   : Array[1..MaxTime] of Pchar =
                ('Morning',
                 'Afternoon',
                 'Evening',
                 'Morning+Afternoon',
                 'Afternoon+Evening',
                 'Morning+Afternoon+Evening');
     months     : Array[1..12] of String[3] =
                ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct',
                 'Nov','Dec');

type
   TPairRec        = packed record
     pclub        : String[6];
     pclubex      : String[1];
     pdir         : String[1];
     pscore       : String[4];
     pplayer      : Array[1..MaxTeamSize] of String[7];
     pdist        : String[2];
     punit        : String[3];
     pevdate      : String[8];
     prank        : String[3];
     LastPair     : word;
     LastSect     : String[2];
     LastDir      : byte;
     LastNum      : byte;
   end;
   TOaRanks       = Array[1..2,1..MaxRank] of TPairRec;
   TNRanks        = Array[1..2] of byte;
   TTabGames      = packed record
     TotGames       : Array[1..2] of word;//1=eligible, 2=not eligible
     Members        : Array[1..2] of word;//1=members, 2= non members
     TotTables      : Array[1..2] of real;//1=eligible, 2=not eligible
     StratTab       : Array[1..4] of real;
   end;
   TDistRank      = packed record
     DNumber      : String[2];
     UNumber      : String[3];
     TotTabGames  : TTabGames;
     DNRanks      : TNRanks;
     DRanks       : TOaRanks;
   end;
   TValidClub     = packed record
     Dnum         : String[2];
     Unum         : String[3];
     Cnum         : String[6];
     NumGames     : byte;
     isV7         : boolean;
     Reported     : boolean;
   end;
   TVerElement    = packed record
     cver         : String[6];
     ccount       : word;
   end;

var
   OaRanks        : TDistRank;
   DOaRanks       : Array[1..MaxDist] of TDistRank;
   VersionCounts  : Array[1..Maxver] of TVerElement;
   Versions       : word;
   ClubVersion    : String[6];
   MaxDir         : byte;
   VTotTabGames   : Array[1..2] of TTabGames; //1=old, 2=version 7+
   VClubs         : Array[1..2] of word; //1=old, 2=new
   PClubs         : TStringList;
   NumVClubs      : word;
   NumReported    : word;
   ValidClub      : Array[1..MaxClubs] of TValidClub;
   doStratTab     : boolean;
   doSancCheck    : boolean;
   doRanks        : boolean;
   doInv          : boolean;
   UseFin         : boolean;
   ChType         : byte;
   inrec       : String[254];
   RFName       : String;
   SpecRateText : String;
   HeadPrinted : boolean;
   SpecialRating : byte;
   doManual      : boolean;
   doDups        : boolean;
   escaped     : boolean;
   inname      : String;
   recs        : longint;
   EventRecs   : longint;
   ERec        : longint;
   DetailRecs  : longint;
   intype      : byte;
   ClubMpRec    : PClubMPRec;
   ClubFinRec   : PClubFinRec;
   numsess      : byte;
   club         : String[6];
   Dist         : String[2];
   DZ           : String[2];
   DistName     : String[9];
   DUnit        : String[3];
   ResClub      : String[6];
   ResDist      : String[2];
   ResUnit      : String[3];
   doclub       : boolean;
   dodist       : boolean;
   dounit       : boolean;
   doall        : boolean;
   doMPrep      : boolean;
   canadian     : boolean;
   Unitrank     : boolean;
   CanOnly      : boolean;
   VerCounts    : boolean;
   VTime        : Array[0..2] of boolean;
   ClubMPHeader   : TClubMPRec;
   ClubMPEvent    : TClubMPRec;
   ClubFinHeader  : TClubFinRec;
   ClubFinEvent   : TClubFinRec;
   hrating        : byte;
   FeeCd          : byte;
   pfeecd         : string;
   locchar        : string;
   CSessNum       : byte;
   LastPlayer     : String;
   ThisPlayer     : String;
   ThisStrat      : byte;
   ThisRankl      : word;
   ThisRankh      : word;
   ThisSect       : String[2];
   ThisDir        : byte;
   ThisPair       : word;
   ThisPer        : word;
   ThisBds        : word;
   ThisMPtype     : char;
   date           : String[12];
   EvDate         : String[8];
   Entries        : byte;
   ImpDrive       : char;
   AnyFiles       : boolean;
   CEventNum      : byte;
   PairRec        : TPairRec;
   MLimit         : word;
   MaxMPLimit     : word;
   MinMPLimit     : word;
   ThisMembers    : word;
   ThisNonMembers : word;
   MinTables      : real;
   ThisTables     : Real;
   IsEligible     : boolean;
   StartDate      : String[8];
   EndDate        : String[8];
   gstab           : Array[1..3] of real;
   gttab        : real;
   ClubInd      : word;
   IsSanctioned : boolean;
   AllowInv     : boolean;
   IncludeGames : Array[1..Ratings] of boolean;
   RF             : textfile;

function GetTotGames(const TabGames: TTabGames):word;
begin
  with TabGames do Result := TotGames[1]+TotGames[2];
end;

function GetTotTables(const TabGames: TTabGames):real;
begin
  with TabGames do Result := TotTables[1]+TotTables[2];
//  x
end;

procedure DelTempDirFiles;
var
   Finderr : integer;
   Delok   : boolean;
   Search_Rec  : TSearchRec;
begin
  CreateDir(TempDir);
  repeat
    Finderr := FindFirst(fixpath(TempDir)+'*.*',faArchive,Search_Rec);
    repeat
      Delok := (Finderr <> 0)
        or DeleteFileAttr(fixpath(TempDir)+Search_Rec.Name);
      if not Delok and (Finderr = 0) then Finderr := FindNext(Search_Rec);
    until Delok or (Finderr <> 0);
  until Finderr <> 0;
  FindClose(Search_Rec);
end;

{$I gametype.inc}

procedure closeinfile;
begin
  {$I-} CloseFile(infile); {$I+}
  if IOResult = 0 then;
end;

procedure WriteDelim(const Field: string; const comma,usedelim: boolean);
begin
  if usedelim then Write(RF,Delimiter,Trim(Field),Delimiter)
  else Write(RF,Trim(Field));
  if comma then Write(RF,',')
  else WriteLn(RF);
end;

function RunLHA(const ArcName,OutPath: String): boolean;
begin
  with Form1.CompLHA1 do begin
    FilesToProcess.Clear;
    FilesToProcess.Add('*.*');
    ArchiveName := ArcName;
    TargetPath := Outpath;
    Expand;
  end;
  Result := true;
end;

function GetGameTypeStr(const GameType: char): String;
begin
  case GameType of
    'O': GetGameTypeStr := 'Open';
    'I': GetGameTypeStr := 'Inv ';
    'N': GetGameTypeStr := 'Nov ';
    'B': GetGameTypeStr := 'B+  ';
    else GetGameTypeStr := '    ';
  end;
end;

function GetPairTeamStr(const pt: char): String;
begin
  case pt of
    'P': GetPairTeamStr := 'Pair';
    'T': GetPairTeamStr := 'Team';
    'I': GetPairTeamStr := 'Ind ';
    else GetPairTeamStr := '    ';
  end;
end;

function dorank(const low,high: word; const width: byte): String;
begin
  Result :='';
  if low > 0 then Result:=numcvt(low,0,false);
  if high > low then Result:=Result+'/'+numcvt(high,0,false);
  Result := TrimTrailL(CenterL(Result,Abs(width)));
end;

procedure AddtoOaRank(var DistRank: TDistRank; const Dir:byte);
var
   j      : integer;
   k      : integer;
begin
  with DistRank do begin
    if IVal(PairRec.pscore) > 9000 then exit;
    for j := 1 to DNRanks[Dir] do if PairRec.pscore > DRanks[Dir,j].pscore
      then begin
      if DNRanks[Dir] < MaxRank then Inc(DNRanks[Dir]);
      for k := DNRanks[Dir] downto j+1 do DRanks[Dir,k] := DRanks[Dir,k-1];
      DRanks[Dir,j] := PairRec;
      exit;
    end;
    if DNRanks[Dir] < MaxRank then begin
      Inc(DNRanks[Dir]);
      DRanks[Dir,DNRanks[Dir]] := PairRec;
    end;
  end;
end;

procedure FinishPairRecord;

procedure AddToRank(const Dir: byte);
var
   j      : integer;
begin
  with PairRec do begin
    AddtoOaRank(OaRanks,Dir);
    if unitrank then begin
      for j := 1 to MaxDist do if ((DOaRanks[j].DNumber = pdist)
        and Empty(DOaRanks[j].UNumber) or Empty(DOaRanks[j].Dnumber)) then begin
        DOaRanks[j].Dnumber := pdist;
        DOaRanks[j].Unumber := '   ';
        AddtoOaRank(DOaRanks[j],Dir);
        break;
      end;
      for j := 1 to MaxDist do if ((DOaRanks[j].DNumber = pdist)
        and (DOaRanks[j].UNumber = punit)) or Empty(DOaRanks[j].Dnumber) then begin
        DOaRanks[j].Dnumber := pdist;
        DOaRanks[j].Unumber := punit;
        AddtoOaRank(DOaRanks[j],Dir);
        exit;
      end;
    end
    else for j := 1 to MaxDist do if (DOaRanks[j].DNumber = pdist)
      or Empty(DOaRanks[j].Dnumber) then begin
      DOaRanks[j].Dnumber := pdist;
      DOaRanks[j].Unumber := '   ';
      AddtoOaRank(DOaRanks[j],Dir);
      exit;
    end;
  end;
end;

begin
  with PairRec do begin
    if (MaxDir > 1) and (pdir = 'E') then AddToRank(2)
    else AddToRank(1);
    FillChar(PairRec,SizeOf(PairRec),0);
  end;
end;

procedure FinishLastPlayer;
const
     Dirs : String[4] = 'NESW';
begin
  if not Empty(LastPlayer) then begin
    if NewRepType and (ThisPer > 5000) then begin
      with PairRec do begin
        if (ThisPair <> LastPair) or (ThisDir <> LastDir)
          or (ThisSect <> LastSect) then FinishPairRecord;
        if LastNum < 1 then begin
          LastPair := ThisPair;
          LastDir := ThisDir;
          LastSect := ThisSect;
          pclub := club;
          pclubex := Long2StrL(CEventNum);
          pdir := Dirs[ThisDir];
          pscore := numcvt(ThisPer,4,true);
          pdist := Dist;
          punit := DUnit;
          pevdate := Evdate;
          prank := PadL(dorank(ThisRankl,ThisRankh,3),3);
        end;
        if LastNum < MaxTeamSize then begin
          Inc(LastNum);
          pplayer[LastNum] := Copy(LastPlayer,21,7);
        end;
      end;
    end;
  end;
  LastPlayer := ThisPlayer;
  ThisRankl := 0;
  ThisMPType := '0';
end;

procedure PrintHead;
begin
  if HeadPrinted then exit;
  ReportLine(1,pnone,1,CharStrL('-',60));
  if UseFin then with ClubFinHeader do ReportLine(1,pnone,1,club+DZ+Dist+' U'+DUnit
    +' '+TrimL(ChrStr(@ClubName,40))+' '+TrimL(ClubVersion)+':')
  else with ClubMPHeader do ReportLine(1,pnone,1,club+DZ+Dist+' U'+DUnit
    +' '+TrimL(ChrStr(@ClubName,40))+' '+TrimL(ChrStr(@version,12))+':');
  if doSancCheck and not isSanctioned then ReportLine(0,pnone,1,
    '  not Sanctioned');
  if not NewRepType and doRanks then
    ReportLine(0,pnone,1,'  Ranking not available');
  HeadPrinted := true;
  PClubs.Add(club);
end;

procedure InitClubMPPrint;
begin
  if not UseFin then with ClubMPHeader do begin
    EventRecs := ChrNum(@records,4);
    date := Months[Ival(ChrStr(@GameDate,2))]+' '+Copy(ChrStr(@GameDate,8),5,4);
  end;
  HeadPrinted := false;
  CEventNum := 0;
  if doSancCheck and IsSanctioned then with ValidClub[ClubInd] do begin
    Reported := true;
    Inc(NumReported);
  end;
end;

function TestGameOk: boolean;
var
//   j     : byte;
   rateok : boolean;
begin
  Result := false;
  if doall then begin
    rateok := IncludeGames[hrating];
    {rateok := false;
    for j := 1 to MaxSpecial-3 do
      rateok := rateok or (hrating = SpecialRatings[j]);}
    if not rateok then exit;
  end
  else if hrating <> SpecialRating then exit;
  if not UseFin and domanual and (ChrStr(@ClubMPEvent.EventName,6) <> 'Master') then exit;
  if not doclub then begin
    if not Empty(StartDate) and (IdxDate(EvDate) < StartDate) then exit;
    if not Empty(EndDate) and (IdxDate(EvDate) > EndDate) then exit;
  end;
  if UseFin and (hrating in [3,18,26]) then begin
    if (chtype = 1) and (FeeCd <> 4) then exit;
    if (chtype = 2) and (FeeCd = 4) then exit;
    if (chtype = 1) and not Empty(locchar) and (Pos(locchar,UpperCase(pfeecd)) < 1) then exit;
  end;
  if not VTime[CSessNum] then exit;
  Result := true;
end;

procedure InitEventMPPrint;

var
   Sttabs : Array[1..4] of real;

procedure UpdateTotals(var TotTabGames : TTabGames);
var
   NumStrat    : byte;
   j           : byte;
   Mlim        : word;
   St          : byte;
   ttab        : real;
   stab           : Array[1..3] of real;
begin
  if UseFin then with ClubFinEvent,TotTabGames do begin
    if IsEligible then begin
      Inc(TotGames[1]);
      TotTables[1] := TotTables[1]+ThisTables;
      Members[1] := Members[1]+ThisMembers;
      Members[2] := Members[2]+ThisNonMembers;
    end
    else begin
      Inc(TotGames[2]);
      TotTables[2] := TotTables[2]+ThisTables;
    end;
  end
  else with ClubMPEvent,TotTabGames do begin
    if IsEligible then begin
      Inc(TotGames[1]);
      TotTables[1] := TotTables[1]+ThisTables;
    end
    else begin
      Inc(TotGames[2]);
      TotTables[2] := TotTables[2]+ThisTables;
    end;
    if NewRepType and doStratTab then begin
      NumStrat := ChrNum(@strata,1);
      ttab := 0;
      for j := 1 to NumStrat do begin
        stab[j] := ChrReal(@StTables[j,1],4,1);
        if (GameType = 'T') then stab[j] := stab[j] * numsess;
        ttab := ttab+stab[j];
      end;
      if (NumStrat = 3) and (ttab <> ChrReal(@tables,4,1)) then begin
        stab[1] := ChrReal(@tables,4,1)-stab[2]-stab[3];
        if stab[1] < 0 then stab[1] := 0;
      end;
      FillChar(Sttabs,SizeOf(Sttabs),0);
      for j := 1 to NumStrat do begin
        Mlim := ChrNum(@MPLimits[j],4);
        if (Mlim < 1) or (MLim > 6000) then St := 1
        else if Mlim > 2500 then St := 2
        else if Mlim > 500 then St := 3
        else St := 4;
        Sttabs[St] := Sttabs[St]+stab[j];
      end;
      for j := 1 to 4 do StratTab[j] := StratTab[j]+Sttabs[j];
    end;
  end;
end;

procedure DUpdateTotals;
var
   j      : word;
begin
  if unitrank then begin
    for j := 1 to MaxDist do if ((DOaRanks[j].DNumber = Dist)
      and Empty(DOaRanks[j].UNumber)) or Empty(DOaRanks[j].DNumber) then begin
      DOaRanks[j].DNumber := Dist;
      DOaRanks[j].UNumber := '   ';
      UpDateTotals(DOaRanks[j].TotTabGames);
      break;
    end;
    for j := 1 to MaxDist do if ((DOaRanks[j].DNumber = Dist)
      and (DOaRanks[j].UNumber = DUnit)) or Empty(DOaRanks[j].DNumber) then begin
      DOaRanks[j].DNumber := Dist;
      DOaRanks[j].UNumber := DUnit;
      UpDateTotals(DOaRanks[j].TotTabGames);
      exit;
    end;
  end
  else for j := 1 to MaxDist do if (DOaRanks[j].DNumber = Dist)
    or Empty(DOaRanks[j].DNumber) then begin
    DOaRanks[j].DNumber := Dist;
    DOaRanks[j].UNumber := '   ';
    UpDateTotals(DOaRanks[j].TotTabGames);
    exit;
  end;
end;

var
   j    : word;
   NumStrat : byte;
begin
  if UseFin then with ClubFinEvent do begin
    EvDate := ChrStr(@GameDate[5],4)+ChrStr(@GameDate,4);
    if not TestGameOk then exit;
    NumStrat := ChrNum(@strata,1);
    MLimit := ChrNum(@MPLimits[1],4);
    ThisTables := ChrReal(@tables,3,1);
    ThisMembers := ChrNum(@ACBLPlayers,3);
    ThisNonMembers := ChrNum(@NonPlayers,3);
    numsess := 1;
    IsEligible := true;
    PrintHead;
    Inc(CEventNum);
    UpdateTotals(OaRanks.TotTabGames);
    UpdateTotals(VTotTabGames[2]);
    ReportLine(1,ppica,1,Slash(EvDate)+'; ');
    DUpdateTotals;
    if doall then ReportLine(0,pnone,1,PadL(RatingText[hrating]+pfeecd,32));
    ReportLine(0,pnone,1,Real2StrL(ThisTables,4,1)+' tables; '
      +Trim(GetGameTypeStr(Restriction)));
    ReportLine(0,pnone,1,'/'+GetPairTeamStr(DGameType));
    ReportLine(0,pnone,1,'; MP limits: ');
    for j := 1 to NumStrat do case j of
      1: if ChrNum(@MPLimits[1],4) < 1 then ReportLine(0,pnone,1,'None')
         else ReportLine(0,pnone,1,Long2StrL(ChrNum(@MPLimits[1],4)));
      else ReportLine(0,pnone,1,'/'+Long2StrL(ChrNum(@MPLimits[j],4)));
    end;
    if not doall and not Empty(pfeecd) then ReportLine(0,pnone,1,pfeecd);
    if not doclub and not doall and Not IsEligible then
      ReportLine(0,pnone,1,'; Not eligible');
  end
  else with ClubMPEvent do begin
    DetailRecs := ChrNum(@DetRecs,4);
    EvDate := ChrStr(@GameDate,8);
    if not TestGameOk then exit;
    NumStrat := ChrNum(@strata,1);
    MLimit := ChrNum(@MPLimits[1],4);
    if NewReptype then ThisTables := ChrReal(@tables,4,1)
    else ThisTables := ChrReal(@tables,4,0);
    numsess := ChrNum(@sessions,2);
    if not doRanks then IsEligible := true
    else begin
      if doInv then IsEligible := (PTI = 'I')
      else IsEligible := (AllowInv or (PTI = 'O'));
      IsEligible := IsEligible and ((MLimit = MinMPLimit)
        or ((MinMPLimit > 0) and (MLimit >= MinMPLimit)));
      IsEligible := IsEligible and ((MLimit = MaxMPLimit)
        or ((MaxMPLimit > 0) and (MLimit <= MaxMPLimit)));
      IsEligible := IsEligible and (ThisTables >= MinTables);
      if doSancCheck then begin
        if isSanctioned then Inc(ValidClub[ClubInd].NumGames)
        else IsEligible := false;
      end;
    end;
    PrintHead;
    Inc(CEventNum);
    UpdateTotals(OaRanks.TotTabGames);
    if NewRepType then UpdateTotals(VTotTabGames[2])
    else UpdateTotals(VTotTabGames[1]);
    if domanual then begin
      ReportLine(0,pnone,1,'  '+Copy(EvDate,1,2)+'/'+Copy(EvDate,5,4));
      exit;
    end;
    ReportLine(1,ppica,1,Slash(EvDate)+'; ');
    DUpdateTotals;
    if doall then ReportLine(0,pnone,1,PadL(RatingText[hrating]+pfeecd,32));
    if dostrattab and NewRepType then begin
      NumStrat := ChrNum(@strata,1);
      if NumStrat > 1 then begin
        gttab := 0;
        for j := 1 to NumStrat do begin
          gstab[j] := ChrReal(@StTables[j,1],4,1);
          if (GameType = 'T') then gstab[j] := gstab[j] * numsess;
          gttab := gttab+gstab[j];
        end;
        if (NumStrat = 3) and (gttab <> ChrReal(@tables,4,1)) then begin
          gstab[1] := ChrReal(@tables,4,1)-gstab[2]-gstab[3];
          if gstab[1] < 0 then gstab[1] := 0;
        end;
        for j := 1 to NumStrat do begin
          ReportLine(0,pnone,1,Real2StrL(gstab[j],3,1));
          if j < NumStrat then ReportLine(0,pnone,1,'/');
        end;
        ReportLine(0,pnone,1,' = ');
      end;
    end;
    ReportLine(0,pnone,1,Real2StrL(ThisTables,4,1)+' tables; '
      +Trim(GetGameTypeStr(PTI)));
    if NewRepType then ReportLine(0,pnone,1,'/'+GetPairTeamStr(GameType));
    ReportLine(0,pnone,1,'; MP limits: ');
    for j := 1 to NumStrat do case j of
      1: if ChrNum(@MPLimits[1],4) < 1 then ReportLine(0,pnone,1,'None')
         else ReportLine(0,pnone,1,Long2StrL(ChrNum(@MPLimits[1],4)));
      else ReportLine(0,pnone,1,'/'+Long2StrL(ChrNum(@MPLimits[j],4)));
    end;
    if not doall and not Empty(pfeecd) then ReportLine(0,pnone,1,pfeecd);
    if numsess > 1 then ReportLine(0,pnone,1,'; '+Long2StrL(numsess)+' sessions');
    if not Empty(ChrStr(@SancNum,10)) then
      ReportLine(0,pnone,1,'; Sanction '+ChrStr(@SancNum,10));
    if not doclub and not doall and Not IsEligible then
      ReportLine(0,pnone,1,'; Not eligible');
  end;
end;

procedure PrintMPDetailRec;
var
   j              : word;
   PlayKey        : KeyStr;
begin
  if doMPrep then with ClubMPRec^ do begin
    if RecType = '3' then begin
      Entries := ChrNum(@Numentries,2);
      for j := 1 to Entries do with NPlayerRec[j] do begin
        WriteDelim(ClubNum,true,false);
        WriteDelim(GameDate,true,false);
        WriteDelim(Plnum,true,false);
        WriteDelim(Real2StrL(ChrReal(@MPs,5,2),5,2),false,false);
      end;
    end;
    exit;
  end;
  if not NewRepType or not IsEligible or not (doRanks or domanual) then exit;
  with ClubMPRec^ do begin
    Entries := ChrNum(@Numentries,2);
    for j := 1 to Entries do case RecType of
      '3': with NPlayerRec[j] do begin
        if domanual then begin
          ReportLine(1,pnone,1,ChrStr(@Plnum,7)+' ');
          PlayKey := Player_key(ChrStr(@Plnum,7));
          FindKey(IdxKey[Filno,2]^,Recno[Filno],PlayKey);
          if ok then with Playern do begin
            GetARec(FilNo);
            ReportLine(0,pnone,1,PadL(TrimL(First_Name)+' '+Trim(Last_Name),33));
          end
          else ReportLine(0,pnone,1,PadL('Player not found.',33));
          ReportLine(0,pnone,1,Real2StrL(ChrReal(@MPs,5,2),6,2));
        end
        else if MPType = '2' then begin
          ThisPlayer := CharStrL(' ',20)+ChrStr(@Plnum,7);
          if ThisPlayer <> LastPlayer then FinishLastPlayer;
          ThisStrat := ChrNum(@Stratum,1);
          ThisRankl := ChrNum(@LowRank,3);
          ThisRankh := ChrNum(@HighRank,3);
          ThisSect := ChrStr(@Section,2);
          ThisDir := ChrNum(@Direction,1);
          ThisPair := ChrNum(@PairNum,3);
          ThisPer := ChrNum(@Score,4);
          ThisBds := ChrNum(@BdsPlayed,3);
          if MPtype > ThisMPtype then ThisMPtype := MPtype;
        end;
      end;
    end;
  end;
end;

procedure PrintClubMP;

procedure PrintMasterpoints;

var
   wseq           : word;
   lastseqdiff    : word;
   Drec           : integer;
begin
  lastseqdiff := 0;
  Reset(infile);
  recs := 0;
  {$I-} ReadLn(infile,inrec); {$I+}
  CheckIOResult(infilename);
  if canceled then exit;
  if UseFin then begin
    Move(ClubFinRec^,ClubFinHeader,SizeOf(TClubFinRec));
    InitClubMPPrint;
    While ClubFinRec^.RecType <> '9' do begin
      if Bad_Eof(infile) then exit;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      if ClubFinRec^.RecType = '4' then with ClubFinEvent do begin
        Move(ClubFinRec^,ClubFinEvent,SizeOf(TClubFinRec));
        date := Months[Ival(ChrStr(@GameDate,2))]+' '+Copy(ChrStr(@GameDate,8),5,4);
        hrating := ChrNum(@Rating,2);
        CSessNum := ChrNum(@DSessNum,2) mod 3;
        FeeCd := ChrNum(@FeeCode,1);
        case FeeCd of
          3: pfeecd := ' (C)';
          4: pfeecd := ' '+ChrStr(@LocalCharity,40);
          else pfeecd := '';
        end;
        InitEventMPPrint;
      end;
    end;
  end
  else begin
    Move(ClubMPRec^,ClubMPHeader,SizeOf(TClubMPRec));
    InitClubMPPrint;
    Inc(recs);
    ERec := 0;
    while ERec < EventRecs do begin
      Inc(ERec);
      if Bad_Eof(infile) then exit;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      Move(ClubMPRec^,ClubMPEvent,SizeOf(TClubMPRec));
      with ClubMPEvent do begin
        if not ValidField(@RecType,1,'2') then exit;
        wseq := ChrNum(@sequence,4);
        if wseq > ERec+lastseqdiff then begin
          Dec(EventRecs,wseq-ERec-lastseqdiff);
          lastseqdiff := wseq-ERec;
        end;
        if lastseqdiff > 0 then NumChr(@sequence,4,ERec);
        if not ValidField(@sequence,4,numcvt(ERec,4,true)) then exit;
        if not ValidField(@ClubNum,6,club) then exit;
        hrating := ChrNum(@Erating,2);
        CSessNum := ChrNum(@ClubSessNum,2) mod 3;
        if NewRepType then FeeCd := ChrNum(@FeeCode,1)
        else FeeCd := 0;
        case FeeCd of
          3: pfeecd := ' (C)';
          4: pfeecd := ' (L)';
          else pfeecd := '';
        end;
        InitEventMPPrint;
      end;
      Inc(recs);
      LastPlayer := CharStrL(' ',40);
      for DRec := 1 to DetailRecs do begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
        with ClubMPRec^ do begin
          if not (RecType in ['3','4']) then
            if not ValidField(@RecType,1,'3') then exit;
          if not ValidField(@sequence,4,numcvt(DRec,4,true)) then exit;
          if not ValidField(@ClubNum,6,club) then exit;
          Inc(recs);
          if TestGameOk then PrintMPDetailRec;
        end;
      end;
      if TestGameOk then begin
        FinishLastPlayer;
        FinishPairRecord;
      end;
    end;
    if Bad_Eof(infile) then exit;
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    Inc(recs);
    with ClubMPRec^ do begin
      if not ValidField(@RecType,1,'9') then exit;
      if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    end;
  end;
end;

procedure TestClubFile(const clubfilename: string{; const ftype: char});

procedure AddVer(const vc: integer);
var
   k : integer;
begin
  for k := Versions downto vc do VersionCounts[k+1] := VersionCounts[k];
  Inc(Versions);
  VersionCounts[vc].cver := ClubVersion;
  VersionCounts[vc].ccount := 1;
end;

var
   Mask           : String;
   FindErr        : integer;
   outpath        : String;
   Search_Rec  : TSearchRec;
   ClubFname   : String;
   j           : integer;
   found       : boolean;
begin
  if escaped then exit;
  if doDups then begin
//    if ftype <> 'z' then exit;
    ClubFname := JustNameL(clubfilename);
    if ClubFname[9] <> '-' then exit;
    PClubs.Add(Copy(ClubFname,1,6));
    exit;
  end;
  if UseFin then Mask := ClubFMask+'*'
  else Mask := ClubMMask+'*';
  if doclub then begin
    ClubFname := JustNameL(clubfilename);
//    if ftype = 'z' then begin
    if ResClub <> Copy(ClubFname,1,6) then exit;
    {end
    else if ResClub <> Copy(ClubFname,2,6) then exit;}
  end;
  outPath := fixpath(TempDir);
  {if ftype = 'z' then }RunLHA(clubfilename,outPath);
//  else CopyFile(PChar(clubfilename),PChar(outpath+JustFileNameL(clubfilename)),false);
  FindErr := FindFirst(outPath+Mask,faArchive,Search_Rec);
  FindClose(Search_Rec);
  if FindErr <> 0 then exit;
  infilename := outPath+Search_Rec.Name;
  inname := JustFileNameL(infilename);
  if Empty(inname) then exit;
  if UseFin then begin
    Cftype := 'Fin';
    Assign(infile,infileName);
    Reset(infile);
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    with ClubFinRec^ do begin
      if RecType <> '1' then exit;
      if version[1] = 'W' then ClubVersion := ChrStr(@version,5)
      else ClubVersion := ChrStr(@version,4);
      NewRepType := ClubVersion[1] > '6';
      club := ChrStr(@ClubNum,6);
      DUnit := numcvt(Ival(GetClubUnitDist(club,ChrStr(@UnitNum,3),false)),3,true);
      if canadian or CanOnly then Dist := numcvt(GetCBFZone(DUnit),2,true)
      else Dist := numcvt(Ival(GetClubUnitDist(club,ChrStr(@DistNum,2),true)),2,true);
      if CanOnly and (Ival(Dist) < 1) then exit;
      if dodist and (Dist <> ResDist) then exit;
      if dounit and (DUnit <> ResUnit) then exit;
    end;
  end
  else begin
    Cftype := 'MP';
    intype := ValidMPFile(infile,infileName,inrec,false);
    if (intype < 1) or (intype <> 2) then exit;
    with ClubMPRec^ do begin
      ClubVersion := ChrStr(@version,6);
      NewRepType := ClubVersion[1] > '6';
      club := ChrStr(@ClubNum,6);
      DUnit := numcvt(Ival(GetClubUnitDist(club,ChrStr(@UnitNum,3),false)),3,true);
      if canadian or CanOnly then Dist := numcvt(GetCBFZone(DUnit),2,true)
      else Dist := numcvt(Ival(GetClubUnitDist(club,ChrStr(@DistNum,2),true)),2,true);
      if CanOnly and (Ival(Dist) < 1) then exit;
      IsSanctioned := false;
      if doSancCheck then begin
        ClubInd := 1;
        while (ClubInd < NumVClubs) and (ValidClub[ClubInd].Cnum <> club) do
          Inc(ClubInd);
        with ValidClub[ClubInd] do begin
          IsSanctioned := Cnum = club;
          if IsSanctioned then begin
            isV7 := NewRepType;
            Dist := Dnum;
            DUnit := Unum;
          end;
        end;
      end;
      if dodist and (Dist <> ResDist) then exit;
      if dounit and (DUnit <> ResUnit) then exit;
    end;
  end;
  if NewReptype then Inc(VClubs[2])
  else Inc(VClubs[1]);
  if VerCounts then begin
    found := false;
    for j := 1 to Versions do with VersionCounts[j] do begin
      if cver = ClubVersion then begin
        Inc(ccount);
        found := true;
        Break;
      end
      else if cver > ClubVersion then begin
        AddVer(j);
        found := true;
        Break;
      end;
    end;
    if not found then AddVer(Versions+1);
  end;
  PrintMasterpoints;
end;

procedure SearchFolder(const FolderName: String);
var
   SR        : TSearchRec;
   Ser       : integer;
   Flist     : TStringlist;
   Fname     : char;
   Xname     : String;
   j         : integer;
begin
  Form1.StatusBar1.Simpletext := 'Searching '+FolderName;
  FList := TStringlist.Create;
  FList.Clear;
  FList.Sorted := true;
  Ser := FindFirst(FolderName+'*.*',faArchive+faDirectory,SR);
  while Ser = 0 do with SR do begin
    Application.ProcessMessages;
    if canceled then exit;
    Xname := UpperCase(Name);
    if (xName <> '.') and (xName <> '..') then begin
      if (Attr and faDirectory) <> 0 then FList.Add('d'+xName)
      else if JustExtensionL(xName) = 'LZH' then FList.Add('z'+xName);
//      else if (xName[1] in ['M','m']) then FList.Add('m'+xName);
    end;
    Ser := FindNext(SR);
  end;
  FindClose(SR);
  for j := 0 to Flist.Count-1 do begin
    FName := Flist[j][1];
    XName := Copy(Flist[j],2,50);
    if FName = 'd' then begin
      if doclub and (Length(XName) > 3) and (XName[3] in ['X','x'])
        and (Copy(XName,1,2) <> Copy(ResClub,1,2)) then continue;
      SearchFolder(fixpath(FolderName+XName));
    end
    else begin
      if (j < Flist.Count-2)
        and (Copy(Flist[j],1,9) = Copy(Flist[j+1],1,9)) then continue;
      Form1.StatusBar1.Simpletext := 'Searching '+FolderName+XName;
      TestClubFile(FolderName+XName{,FName});
      CloseinFile;
      DelTempDirFiles;
      Application.ProcessMessages;
      if canceled then begin
        Flist.Destroy;
        exit;
      end;
    end;
  end;
  Flist.Destroy;
end;

const
   LastPick       : String = '';

procedure PrintFinalRanks(const DistRank: TDistRank);
var
   j              : integer;
   k              : integer;
   l              : integer;
   dis            : String;
   PlayKey        : KeyStr;
   CurCol         : integer;
   xDranks        : word;
   line           : String;
   leadtext       : String;

procedure FinishClub;
begin
  if Length(line) > 0 then ReportLine(1,pnone,1,line);
  line := '';
end;

procedure AddClub(const club: String);
begin
  if Length(line) > 65 then FinishClub;
  line := line+club+' ';
end;

begin
  if doDups then begin
    for j := 0 to PClubs.Count-1 do ReportLine(1,pnone,1,PClubs[j]);
    exit;
  end;
  with DistRank do begin
    if doranks then begin
      if canadian or CanOnly then leadtext := 'Canadian Overall leaders for '
      else leadtext := 'Overall leaders for '
    end
    else leadtext := '';
    if Not Empty(Dnumber) then begin
      dis := DistName+DNumber;
      if unitrank and not Empty(UNumber) then dis := dis+', Unit '+UNumber;
      dis := dis+' ';
    end
    else begin
      dis := '';
      if not (domanual or doclub or doDups) then begin
        if (GetTotGames(VTotTabGames[2]) > 0) then
          ReportLine(2,pnone,1,'Version 7+ totals: '
          +Long2StrL(GetTotGames(VTotTabGames[2]))+' games; '
          +Real2StrL(GetTotTables(VTotTabGames[2]),4,1)+' tables');
        if (GetTotGames(VTotTabGames[1]) > 0) then
          ReportLine(1,pnone,1,'Older version totals: '
          +Long2StrL(GetTotGames(VTotTabGames[1]))+' games; '
          +Real2StrL(GetTotTables(VTotTabGames[1]),4,1)+' tables');
        end;
      if VerCounts then begin
        ReportLine(1,pnone,1,'Clubs reports searched by ACBLscore version:');
        for j := 1 to Versions do with VersionCounts[j] do
          ReportLine(1,pnone,1,cver+':'+numcvt(ccount,6,false));
        ReportLine(1,pnone,1,'Total'+numcvt(VClubs[1]+VClubs[2],6,false));
      end
      else ReportLine(1,pnone,1,'Clubs reports searched: version 7+: '
        +Long2StrL(VClubs[2])+'; + older: '+Long2StrL(VClubs[1]));
      if not (domanual or doclub or doDups) then
        ReportLine(0,pnone,1,'; = '+Long2StrL(VClubs[1]+VClubs[2])
        +'; participating clubs: '+Long2StrL(PClubs.Count));
      PClubs.Destroy;
      if doSancCheck then begin
        line := '';
        ReportLine(2,pnone,1,'The following sanctioned clubs have not reported:');
        for j := 1 to NumVClubs do with ValidClub[j] do if not Reported then
          AddClub(Cnum);
        FinishClub;
        ReportLine(2,pnone,1,
          'The following sanctioned clubs (version 7+) reported but did not hold the game');
        for j := 1 to NumVClubs do with ValidClub[j] do
          if (NumGames < 1) and Reported and IsV7 then AddClub(Cnum);
        FinishClub;
        ReportLine(2,pnone,1,
          'The following sanctioned clubs (old version) reported but did not hold the game');
        for j := 1 to NumVClubs do with ValidClub[j] do
          if (NumGames < 1) and Reported and not IsV7 then AddClub(Cnum);
        FinishClub;
        ReportLine(2,pnone,1,
          'The following sanctioed clubs held the game using a version older than 7');
        for j := 1 to NumVClubs do with ValidClub[j] do
          if (NumGames > 0) and not IsV7 then AddClub(Cnum);
        FinishClub;
      end;
    end;
    if Empty(Dnumber) and (dodist or dounit) then exit;
    if not (domanual or doclub or doDups) then for j := 1 to MaxDir do
      if (DNRanks[j] > 0) or (GetTotGames(TotTabGames) > 0) then begin
      if MaxDir > 1 then begin
        if j = 1 then ReportLine(2,pnone,1,dis+'North-South '+leadtext
          +SpecRateText)
        else ReportLine(2,pnone,1,dis+'East-West '+leadtext+SpecRateText);
      end
      else ReportLine(2,pnone,1,dis+leadtext+SpecRateText);
      CurCol := RepColumn;
      ReportLine(0,pnone,1,'; '+Long2StrL(GetTotGames(TotTabGames))+' Games');
      with TotTabGames do if TotGames[2] > 0 then begin
        ReportLine(0,pnone,1,' ('+Long2StrL(TotGames[2])+' not eligible)');
        ReportLine(1,pnone,1,CharStrL(' ',CurCol));
      end
      else ReportLine(0,pnone,1,'; ');
      if dostrattab then begin
        for k := 1 to 4 do begin
          ReportLine(0,pnone,1,Real2StrL(TotTabGames.StratTab[k],3,1));
          if k < 4 then ReportLine(0,pnone,1,'/');
        end;
        ReportLine(0,pnone,1,' = ');
      end;
      ReportLine(0,pnone,1,Real2StrL(GetTotTables(TotTabGames),3,1)+' tables');
      with TotTabGames do if TotTables[2] > 0  then ReportLine(0,pnone,1,' ('
        +Real2StrL(TotTables[2],3,1)+' not eligible)');
      xDranks := MinWord(MaxDRank,DNRanks[j]);
      for k := 1 to xDranks do with DRanks[j,k] do if not Empty(pclub)
        then begin
        ReportLine(1,pnone,1,numcvt(k,2,false)+' '+pclub+' '+Slash(pevdate)+' '
        +pdist+' '+punit+' '+Copy(pscore,1,2)+'.'+Copy(pscore,3,2));
        for l := 1 to MaxTeamSize do if not Empty(pplayer[l]) then begin
          if l > 1 then begin
            ReportLine(0,pnone,1,',');
            ReportLine(1,pnone,1,CharStrL(' ',33));
          end;
          ReportLine(0,pnone,1,' '+pplayer[l]);
          PlayKey := Player_key(pplayer[l]);
          FindKey(IdxKey[Filno,2]^,Recno[Filno],PlayKey);
          if ok then with Playern do begin
            GetARec(FilNo);
            ReportLine(0,pnone,1,' '+TrimL(First_Name)+' '+Trim(Last_Name)
              +', '+TrimL(City)+' '+State);
          end
          else ReportLine(0,pnone,1,' Player not found.');
        end;
      end;
    end;
  end;
end;

var
   j              : integer;
   k              : integer;

procedure SaveFinalRanks(const DistRank: TDistRank);
var
   j              : integer;
   k              : integer;
   xDranks        : word;
begin
  with DistRank do if Empty(UNumber) then for j := 1 to MaxDir do begin
    xDranks := MinWord(MaxDRank,DNRanks[j]);
    for k := 1 to xDranks do with DRanks[j,k] do if not Empty(pclub)
      then begin
      WriteDelim(pclub,true,true);
      WriteDelim(pclubex,true,true);
      WriteDelim(pdir,true,true);
      WriteDelim(pscore,true,true);
      WriteDelim(pplayer[1],true,true);
      WriteDelim(pplayer[2],true,true);
      WriteDelim(pdist,false,true);
    end;
  end;
end;

function Dslash(const D: String): String;
begin
  Result := Copy(d,5,2)+'/'+Copy(d,7,2)+'/'+Copy(d,1,4);
end;

var
   alldone                  : boolean;
   esc                      : boolean;

var
   l   : integer;
begin
  CreateDir(RepPath);
  ChDir(RepPath);
  DelTempDirFiles;
  ShowCmpErrors := false;
  FillChar(PairRec,SizeOf(PairRec),0);
  FillChar(VTotTabGames,SizeOf(VTotTabGames),0);
  FillChar(OaRanks,SizeOf(OaRanks),0);
  FillChar(DOaRanks,SizeOf(DOaRanks),0);
  FillChar(VClubs,SizeOf(VClubs),0);
  PClubs := TStringList.Create;
  PClubs.Clear;
  PClubs.Sorted := true;
  PClubs.Duplicates := dupIgnore;
  OpenDestination(1,0,105);
  PageLength := 65000;
  ClubMPRec := @inrec[1];
  ClubFinRec := @inrec[0];
  if domanual then SpecRateText := 'Manually recorded points'
  else if doDups then SpecRateText := 'Duplicate reports'
  else if doall then SpecRateText := 'Special games'
  else SpecRateText := RatingText[SpecialRating];
  if doclub then SpecRateText := SpecRateText+' - Club '+ResClub;
  if dodist then SpecRateText := SpecRateText+' - '+DistName+ResDist;
  if dounit then SpecRateText := SpecRateText+' - Unit '+ResUnit;
  if doDups then
    ReportLine(1,pnone,1,'ACBLscore second (duplicate) reports ')
  else if canadian or CanOnly then
    ReportLine(1,pnone,1,'ACBLscore Canadian reports for '+SpecRateText)
  else ReportLine(1,pnone,1,'ACBLscore reports for '+SpecRateText);
  if domanual or doDups then ReportLine(0,pnone,1,' for '+Copy(StartDate,5,2)+'/'
    +Copy(StartDate,1,4))
  else begin
    if StartDate = EndDate then ReportLine(0,pnone,1,' for '+dSlash(StartDate))
    else ReportLine(0,pnone,1,' from '+dSlash(StartDate)+' to '+dSlash(EndDate));
  end;
  if SpecialRating in [98] then begin
    ReportLine(1,pnone,1,'The following special games are included:');
    for j := 1 to SpecialEvents do if IncludeGames[SpecialRatings[j]] then
      ReportLine(1,pnone,1,SpecialText[j]);
  end;
  NewLine(1);
  Form1.Caption := 'Special Games - '+SpecRateText;
  Form1.Label1.Caption := 'Scanning for '+SpecRateText+'. Please Wait...';
  alldone := doclub or (Copy(StartDate,1,6) = Copy(EndDate,1,6));
  repeat
    SearchFolder(fixpath(Form1.inpath));
    if not alldone then begin
      j := Ival(Copy(StartDate,5,2))+1;
      StartDate := Copy(StartDate,1,4)+numcvt(j,2,true);
      alldone := StartDate > EndDate;
      if not alldone then
        Form1.InPath := ClubSavePath+Copy(StartDate,1,4)+'\'+Copy(StartDate,5,2);
    end;
  until alldone;
  Form1.Label1.Caption := '';
  ReportLine(1,pnone,1,CharStrL('-',60));
  BoxMsg('Please wait','Getting names....',MC);
  PrintFinalRanks(OaRanks);
  Dist := '  ';
  DUnit := '   ';
  for j := 0 to 99 do begin
    Dist := numcvt(j,2,true);
    if unitrank then begin
      for k := 1 to MaxDist do if (DOaRanks[k].DNumber = Dist)
        and Empty(DOaRanks[k].UNumber) then begin
        PrintFinalRanks(DOaRanks[k]);
        Break;
      end;
      for l := 101 to 600 do begin
        DUnit := numcvt(l,3,true);
        for k := 1 to MaxDist do if (DOaRanks[k].DNumber = Dist)
          and (DOaRanks[k].UNumber = DUnit) then begin
          PrintFinalRanks(DOaRanks[k]);
          Break;
        end;
      end;
    end
    else for k := 1 to MaxDist do if DOaRanks[k].DNumber = Dist then begin
      PrintFinalRanks(DOaRanks[k]);
      Break;
    end;
  end;
  EraseBoxWindow;
  ViewerShow('','Clubrep');
  Close_Destination;
  if doRanks and not doclub then begin
    RFName := DefRFName;
    if not GetFileName('','*.CSV',RFName,
      'Select file to save for A/S-400 input','SaveFileLoc','C:\',true,false) then exit;
    AssignFile(RF,RFName);
    ReWrite(RF);
    for j := 1 to MaxDist do SaveFinalRanks(DOaRanks[j]);
    CloseFile(RF);
  end;
  if doStratTab and not doclub and YesNoBox(esc,false,
    'Save Table counts by stratum and district',0,MC,nil) then begin
    RFName := DefTabName;
    if not GetFileName('','*.CSV',RFName,
      'Select file to save table counts','SaveFileLoc','C:\',true,false) then exit;
    AssignFile(RF,RFName);
    ReWrite(RF);
    for j := 0 to 99 do begin
      Dist := numcvt(j,2,true);
      for l := 1 to MaxDist do if DOaRanks[l].DNumber = Dist then with DOaRanks[l] do begin
        WriteDelim(Dist,true,false);
        for k := 1 to 4 do WriteDelim(Real2StrL(TotTabGames.StratTab[k],3,1),true,false);
        WriteDelim(Real2StrL(GetTotTables(TotTabGames),3,1),false,false);
      end;
    end;
    CloseFile(RF);
  end;
  Application.Terminate;
end;

procedure GetSelect(const Item: integer; var Selected: boolean);
begin
  Selected := IncludeGames[Specialratings[Item]];
end;

function GetPick(const Item: integer): String; Far;
begin
  Result := SpecialText[Item];
end;

function GetChar(const key: word; const item: integer): integer; Far;
var
   j     : word;
   anypicks : Boolean;
begin
  case key of
    8: begin                       {F8 = tag/untag all}
      anypicks := false;
      for j := 1 to SpecialEvents do if IncludeGames[SpecialRatings[j]] then
        anypicks := true;
      for j := 1 to SpecialEvents do IncludeGames[SpecialRatings[j]] := not anypicks;
      Result := 2;
      SetAbn(true);
    end;
    9: begin                       {F9 = Exit}
      Result := 3;
    end;
    32: begin
      IncludeGames[SpecialRatings[Item]] := not IncludeGames[SpecialRatings[Item]];
      Result := -1;
      SetAbn(true);
    end;
    else Result := 0;
  end;
end;

function GetParms: boolean;
var
   RateChoice     : byte;
   hcur           : hicon;
   anypicks       : boolean;
   ifunc          : byte;
   iclen          : byte;
   esc            : boolean;
label
     pick_again;
begin
  with Form1 do begin
    Result := false;
    Caption := 'Special Games';
    escaped := false;
    EndEdit.Visible := false;
    StartEdit.Visible := false;
    OKBut.Visible := false;
    CheckBox1.Visible := false;
    CheckBox2.Visible := false;
    CheckBox3.Visible := false;
    PickTStrings := PickLStrings;
    RateChoice := PickChoice(MaxSpecial,1,'Special game to search',
      @SpecialText,MC,true,0);
    if RateChoice < 1 then exit;
    SpecialRating := SpecialRatings[RateChoice];
    case SpecialRating of
      16: SpecRateText := 'Manually recorded points';
      97: SpecRateText := 'Duplicate reports';
      98,99: SpecRateText := 'Special games';
      else SpecRateText := RatingText[SpecialRating];
    end;
    Caption := Caption+' - '+SpecRateText;
    {if SpecialRating in [29] then MaxDir := 2
    else} MaxDir := 1;
    if SpecialRating in [41] then begin
      MinMPLimit := 1;
      MaxMPLimit := 50;
      MinTables := 3;
    end
    else begin
      MinMPLimit := 0;
      MaxMPLimit := 0;
      MinTables := 5;
    end;
    AllowInv := (MaxMPLimit > 0) or (SpecialRating in [38]);
    doManual := (SpecialRating in [16]);
    doDups := (SpecialRating in [97]);
    doall := (SpecialRating in [98,99]);
    domprep := false;
    doStratTab := (SpecialRating in [20,21,22,23]);
    doSancCheck := (SpecialRating in [24,25,26,28,29,32,44,45]);
    doRanks := (SpecialRating in [24,25,27,28,29,32,38,41,50,51,62]);
    canadian := (SpecialRating in [27,33,34,35,36,37,38,52,62]);
    UseFin := not doStratTab and not doRanks;
    doInv := false;
    if doRanks then doInv := (PickChoice(2,1,'Type of games to rank',@InvText,MC,true,0) = 2);
    if UseFin and (SpecialRating in [3,18,26]) then begin
      ChType := PickChoice(3,1,'Charities to include',@ChText,MC,true,0);
      canceled := ChType < 1;
      if not canceled then Dec(ChType);
      if ChType = 1 then begin
        StrBox(canceled,ifunc,'Local charity','Input part of local charity (blank for all)',
          locchar,32,iclen,2,1,0,MC,nil);
      end;
    end
    else ChType := 0;
    InPath := ClubSavePath;
    hcur := GetCursor;
    if doall then begin
      Fillchar(IncludeGames,SizeOf(IncludeGames),false);
      if SpecialRating in [99] then for RateChoice := 1 to SpecialEvents do
        IncludeGames[Specialratings[RateChoice]] := true
      else begin
        for RateChoice := 1 to SpecialEvents do
          IncludeGames[SpecialRatings[RateChoice]] := Ival(GetProfile('SR'
          +Long2StrL(Specialratings[RateChoice]),'0')) > 0;
pick_again:
        RateChoice := VPickChoice(SpecialEvents,1,1,'Tag Special Games to include','',
          GetPick,GetSelect,GetChar,MC,0,false);
        SetAbn(false);
        canceled := RateChoice < 1;
        if not canceled then begin
          anypicks := false;
          for RateChoice := 1 to SpecialEvents do
            anypicks := anypicks or IncludeGames[SpecialRatings[RateChoice]];
          if not anypicks then begin
            ErrBox('No special events have been tagged',MC,0);
            goto pick_again;
          end;
          for RateChoice := 1 to SpecialEvents do begin
            if IncludeGames[SpecialRatings[RateChoice]] then
              SetProfile('SR'+Long2StrL(SpecialRatings[RateChoice]),'1')
            else SetProfile('SR'+Long2StrL(SpecialRatings[RateChoice]),'0')
          end;
          if YesNoBox(esc,false,'Save masterpoint report for selected special games',
            0,MC,nil) then begin
            RFName := 'MPREP.CSV';
            if GetFileName('','*.CSV',RFName,'Select file for MP report',
              'SaveFileLoc','C:\',true,false) then begin
              AssignFile(RF,RFName);
              ReWrite(RF);
              domprep := true;
              UseFin := false;
            end;
          end;
        end;
      end;
    end;
    OKBut.Visible := true;
    if canadian then
      OvcPictureField1Label1.Caption := 'Club, Unit or Zone (blank for all)'
    else
      OvcPictureField1Label1.Caption := 'Club, Unit or District (blank for all)';
    CheckBox1.Visible := not (SpecialRating in [16,97]);
    CheckBox2.Visible := not (SpecialRating in [16,97]);
    CheckBox3.Visible := not (SpecialRating in [97]);
    StartEdit.Visible := true;
    StartEdit.Date := Now;
    StartEdit.PopupOpen;
    Setcursor(hcur);
    Result := not canceled;
  end;
end;

procedure TForm1.Docmpcopy(Sender: TObject);
begin
  OnActivate := nil;
  FindCFG;
  AppName := 'ACBLwide';
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  ClubEdit.Text := '';
  if not GetParms then Application.Terminate;
end;

procedure TForm1.Docan(Sender: TObject);
begin
  canceled := true;
  if StartEdit.Visible then application.Terminate;
end;

procedure TForm1.DoOk(Sender: TObject);
var
   isok   : boolean;
   f      : textfile;
   line   : String;
   TChoice  : word;
begin
  CanOnly := CheckBox2.Checked;
  if canadian or CanOnly then begin
    DZ := ' Z';
    DistName := 'Zone ';
  end
  else begin
    DZ := ' D';
    DistName := 'District ';
  end;
  ResClub := Trim(ClubEdit.Text);
  if Length(ResClub) = 1 then ResClub := '0'+ResClub;
  if CanOnly and (Length(ResClub) = 6) then begin
    ErrBox('Club # must not be specified for Canadian only',MC,0);
    ClubEdit.SetFocus;
    exit;
  end;
  doclub := (Length(ResClub) > 3);
  if doclub and not valid_club(ResClub,true) then begin
    ErrBox('Club '+ResClub+' is not a valid club number',MC,0);
    ClubEdit.SetFocus;
    exit;
  end;
  dodist := Length(ResClub) = 2;
  dounit := Length(ResClub) = 3;
  if dodist then ResDist := ResClub
  else ResDist := '';
  if dounit then ResUnit := ResClub
  else ResUnit := '';
  if doclub then doSancCheck := false;
  unitrank := CheckBox1.Checked;
  VerCounts := CheckBox3.Checked;
  StartDate := Form1.StartEdit.DateString('yyyymmdd');
  isok := StartDate > '20020100';
  if not isok then begin
    ErrBox('Start date must be 01/01/2002 or later',MC,0);
    StartEdit.PopupOpen;
  end;
  if doclub then begin
    StartDate := Copy(StartDate,1,4);
    EndDate := StartDate;
  end
  else begin
    if domanual or doDups then begin
      EndDate := Copy(StartDate,1,6)+'31';
      StartDate := Copy(StartDate,1,6)+'01';
    end
    else begin
      EndDate := Form1.EndEdit.DateString('yyyymmdd');
      if EndDate < StartDate then EndDate := StartDate;
      if isok and (Copy(StartDate,1,4) <> Copy(EndDate,1,4)) then begin
        isok := false;
        ErrBox('Ending date must be the same year as start date',MC,0);
        EndEdit.PopupOpen;
      end;
    end;
  end;
  if isok then begin
    NumVClubs := 0;
    NumReported := 0;
    Versions := 0;
    if (EndDate = StartDate) and not doManual and not doDups and not doAll
      and not doclub then begin
      TChoice := PickChoice(MaxTime,6,'Time of day for the game',
        @TimeText,MC,true,0);
      if TChoice < 1 then exit;
      if TChoice in [3,5,6] then VTime[0] := true;
      if TChoice in [1,4,6] then VTime[1] := true;
      if TChoice in [2,4,5,6] then VTime[2] := true;
    end
    else FillChar(VTime,Sizeof(VTime),true);
    if doSancCheck then begin
      RFName := GetProfile('ClubFileName','');
      if GetFileName('','*.*',RFName,'Select file of clubs for '+SpecRateText,
        'ClubFileLoc','K:\',false,false) then begin
        AssignFile(F,RFName);
        Reset(F);
        line := 'xxx';
        while not Eof(F) and (Length(line) > 0) do begin
          ReadLn(F,line);
          if Length(line) > 12 then begin
            Inc(NumVClubs);
            with ValidClub[NumVClubs] do begin
              Cnum := Copy(line,8,6);
              Dnum := numcvt(Ival(Copy(line,2,2)),2,true);
              NumGames := 0;
              isV7 := false;
              Reported := false;
              if not Valid_Club(Cnum,true) then begin
                ErrBox('Club '+Cnum+' is not a valid club number',MC,0);
                line := '';
                NumVClubs := 0;
                doSancCheck := false;
              end;
            end;
          end;
        end;
        CloseFile(F);
        SetProfile('ClubFileName',JustFileNameL(RFName));
      end
      else begin
        doSancCheck := false;
        ErrBox('Clubs will not be verified for '+SpecRateText,MC,0);
      end;
    end;
    DBReadOnly := true;
    Flush_DF := true;
    Flush_IF := true;
    UseLookupDB := true;
    OpenDBFile(Playern_no);
    FilNo := Playern_no;
    if doclub then InPath := ClubSavePath+StartDate
    else InPath := ClubSavePath+Copy(StartDate,1,4)+'\'+Copy(StartDate,5,2);
    EndEdit.Visible := false;
    StartEdit.Visible := false;
    ClubEdit.Visible := false;
    OKBut.Visible := false;
    CheckBox1.Visible := false;
    CheckBox2.Visible := false;
    CheckBox3.Visible := false;
    if not canceled then PrintClubMP;
    if doMPrep then CloseFile(RF);
    Application.Terminate;
  end;
end;

procedure TForm1.OnGetDate(Sender: TObject);
begin
  EndEdit.Date := StartEdit.Date;
  EndEdit.Visible := true;
  if doall then ClubEdit.SetFocus
  else OKBut.SetFocus;
end;

procedure TForm1.CanClick(Sender: TObject);
begin
  if not canadian then begin
    if CheckBox2.checked then
     OvcPictureField1Label1.Caption := 'Club, Unit or Zone (blank for all)'
    else OvcPictureField1Label1.Caption := 'Club, Unit or District (blank for all)';
  end;
end;

procedure TForm1.closeit(Sender: TObject; var Action: TCloseAction);
begin
  canceled := true;
  ExitProcess(0);
end;

end.
