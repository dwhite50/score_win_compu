unit onlinefixmp;
{.$define test}

interface

procedure SubmitOnLineMPRep(const RepF: String);

implementation

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    FOpen,
    msort,
    StStrL,
    txtview1,
    YesNoBoxU,
    csdef,
    util1,
    util2;

procedure SubmitOnLineMPRep(const RepF: String);

const
     TempDir    = 'C:\ACBLTEMP\';
     ScoreDir   = 'C:\ACBLSCOR\';
     tempfil    = TempDir+'MPTEMP';
     {$ifdef test}
     Archivepath = 'C:\TEMP\';
     {$else}
     Archivepath = '\\BRIDGE\SYS\DEPT\550\ACBLSCOR\';
     {$endif}
     ClubrepPath = Archivepath+'CLUBREP\';

var
   RepFile       : String;
   repline      : String[24];
   repline2     : String[22];
   inf          : textfile;
   outf         : textfile;
   n            : byte;
   pn           : String[7];
   cn           : String[6];
   my           : String[4];
   tmps         : longint;
   inn          : longint;
   onn          : longint;
   bpn          : longint;
   dpn          : longint;
   mps               : word;
   mpsc              : String[5];

begin
  RepFile := RepF;
  AssignFile(inf,RepFile);
  Reset(Inf);
  AssignFile(outf,tempfil);
  ReWrite(outf);
  inn := 0;
  onn := 0;
  bpn := 0;
  dpn := 0;
  tmps := 0;
  OpenDestination(1,0,80);
  while not eof(inf) do begin
    ReadLn(inf,repline);
    if length(repline) = 24 then Delete(repline,19,2);
    if length(repline) = 22 then begin
      Inc(inn);
      if inn < 2 then begin
        cn := Copy(repline,8,6);
        my := Copy(repline,19,4);
      end;
      repline[1] := UpCase(repline[1]);
      if repline[1] = '0' then repline[1] := '6';
      n := pos(repline[1],'JKLMNOPQR');
      if n > 0 then repline[1] := Char(n+$30);
      pn := Copy(repline,1,7);
      if Player_check(pn,pnACBL) then
        WriteLn(outf,pn,cn,Copy(repline,14,5),my)
      else begin
        Inc(bpn);
        if bpn < 2 then begin
          ReportLine(0,pnone,1,'Report of bad player numbers:');
          NewLine(1);
        end;
        ReportLine(1,pnone,1,Copy(pn,1,7));
      end;
    end;
  end;
  CloseFile(inf);
  CloseFile(outf);
  MrgSort(tempfil,tempfil,'S(1,7,C,A)',24);
  RepFile := ClubRepPath+cn+'.'+Copy(my,2,3);
  AssignFile(inf,tempfil);
  Reset(inf);
  AssignFile(outf,RepFile);
  ReWrite(outf);
  RepLine2 := '';
  while not eof(inf) do begin
    readln(inf,repline);
    if length(repline) = 22 then begin
      if length(repline2) < 22 then repline2 := repline
      else begin
        if Copy(repline,1,7) <> Copy(repline2,1,7) then begin
          Inc(tmps,Ival(Copy(repline2,14,5)));
          Writeln(outf,repline2);
          Inc(onn);
          repline2 := repline;
        end
        else begin
          mps := Ival(Copy(repline2,14,5));
          Inc(mps,Ival(Copy(repline,14,5)));
          mpsc := numcvt(mps,5,true);
          move(mpsc[1],repline2[14],5);
          Inc(dpn);
          if dpn < 2 then begin
            ReportLine(2,pnone,1,'Report of duplicate player numbers:');
            NewLine(1);
          end;
          ReportLine(1,pnone,1,Copy(repline,1,7));
        end;
      end;
    end;
  end;
  if Length(repline2) = 22 then begin
    Inc(tmps,Ival(Copy(repline2,14,5)));
    WriteLn(outf,repline2);
    Inc(onn);
  end;
  CloseFile(inf);
  closeFile(outf);
  ReportLine(2,pnone,1,'File '+RepFile+' is ready.');
  ReportLine(2,pnone,1,'Total Masterpoints: '+Real2StrL(tmps/100,4,2));
  ReportLine(2,pnone,1,'Input records: '+Long2StrL(inn));
  ReportLine(2,pnone,1,'Bad player numbers: '+Long2StrL(bpn));
  ReportLine(2,pnone,1,'Duplicate player numbers: '+Long2StrL(dpn));
  ReportLine(2,pnone,1,'Output records: '+Long2StrL(onn));
  ViewerShow('','');
  ViewerDone;
  if dpn > 15 then MsgBox('File '+RepFile+' is ready.'#13
    +'Total Masterpoints: '+Real2StrL(tmps/100,4,2)+#13
    +'Input records: '+Long2StrL(inn)+#13
    +'Bad player numbers: '+Long2StrL(bpn)+#13
    +'Duplicate player numbers: '+Long2StrL(dpn)+#13
    +'Output records: '+Long2StrL(onn),6,0);
end;
end.
