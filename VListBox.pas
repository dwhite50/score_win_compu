unit VListBox;
interface
{$define help}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcVLB, util1;

type
    TGetItem = function(const Item: integer): String;
    TGetSelect = procedure(const Index: integer; var Selected: boolean);
    TTestKey = function(const key: word; const Index: integer): integer;
    TTabStops = Array of Integer;
    PTabStops = ^TTabStops;

  TVListBoxForm = class(TForm)
    OKButton: TButton;
    CanButton: TButton;
    VListBox: TOvcVirtualListBox;
    OvcController1: TOvcController;
    HelpBut: TButton;
    procedure Acceptit(Sender: TObject);
    procedure Cancelit(Sender: TObject);
    procedure VGetItem(Sender: TObject; Index: Integer;
      var ItemString: String);
    procedure VSelect(Sender: TObject; Index: Integer; var Selected: Boolean);
    procedure VOnSelect(Sender: TObject; Index: Integer;
      Selected: Boolean);
    procedure VTestSelect(Sender: TObject; Command: Word);
    procedure VOnClick(Sender: TObject);
    procedure CharToItem(Sender: TObject; Ch: Char; var Index: Integer);
    procedure VMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure VGetItemColor(Sender: TObject; Index: Integer; var FG,
      BG: TColor);
    procedure DoHelp(Sender: TObject);
    procedure CloseClick(Sender: TObject; var Action: TCloseAction);
    procedure Afterenter(Sender: TObject);
    procedure BoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
   ChoiceMade : boolean;
  public
    { Public declarations }
  end;

const
     VTabStops : PTabStops = nil;

function VPickchoice(const num_choices, def_choice,top_choice: word;
         const title,HeadText : String; const GetItem: TGetItem;
         const GetSelect: TGetSelect; const TestKey: TTestKey;
         const scrloc: byte; const HelpTopic: Word;
         const multitag: boolean) : integer;

function Pickchoice(const num_choices, def_choice: word;
         const title : String; const Ptextx : Pointer; const scrloc: byte;
         const doabn : boolean; const HelpTopic: Word) : smallint;

type
    TPickChoice      = (PickChar,PickStrings,PickLStrings);
                     //Array of char, Strings, Array of string
const
     PickTStrings    : TPickChoice = PickChar;
     VUseFixedFont : boolean = false;

var
   PickChoiceLine    : String;


implementation
uses
    OvcData,
    pvio,
    csdef,
    util2;

{$R *.DFM}

const
     VListBoxForm: TVListBoxForm = nil;
     PickItems : TStringList = nil;
     SrString : String = '';

var
   Choice : integer;
   LastIndex : integer;
   Direction : boolean; {true = up}
   IsButton : boolean;
   GetVItem : TGetItem;
   VGetSelect : TGetSelect;
   VTestKey   : TTestKey;
   {Topic : word;}

   Ptext        : PPCharArray;
   PStext       : PStringArray absolute Ptext;
   PStr         : PString absolute Ptext;
   String_ptr : PString;
   LString_ptr : LongInt absolute String_ptr;

function DefGetItem(const Item: integer): String;
begin
  Result := PickItems[Item-1];
end;

function GetTheItem(const Item: integer): String;
begin
  Result := Copy(GetVItem(Item),1,255);
end;

function Pickchoice(const num_choices, def_choice: word;
         const title : String; const Ptextx : Pointer; const scrloc: byte;
         const doabn : boolean; const HelpTopic: Word) : smallint;
var
   j : integer;
   isp : boolean;
begin
  isp := PickTStrings = PickStrings;
  if isp then PickItems := Ptextx
  else begin
    Ptext := Ptextx;
    PickItems := TStringList.Create;
    PickItems.Clear;
    for j := 1 to num_choices do begin
      case PickTStrings of
        PickChar: PickItems.Add(Copy(Ptext[j],1,255));
        PickLStrings: PickItems.Add(Copy(PsText[j],1,255));
      end;
    end;
  end;
//  Setabn(false);
  Result := VPickchoice(num_choices,def_choice,1,'Select '+title,'',
    nil,nil,nil,scrloc,HelpTopic,false);
  PickTStrings := PickChar;
  if not isp then begin
    PickItems.Destroy;
    PickItems := nil;
  end;
end;

function VPickchoice(const num_choices,def_choice,top_choice: word;
         const title,HeadText : String; const GetItem: TGetItem;
         const GetSelect: TGetSelect; const TestKey: TTestKey;
         const scrloc: byte; const HelpTopic: Word;
         const multitag: boolean) : integer;
const
     MinWidth = 200;
     CheckWidth = 10;
     wslush = 30;
var
   j     : word;
   MaxTextWidth : integer;
   curtextwidth : integer;
   HeaderHeight : integer;
   PCItem       : String;
   lDlgUnits          : Integer;   {used for tab spacing}
   Alpha        : string;
   Tabs         : Array of Integer;
   Itabs        : Array of word;
begin
  FreeHintWin;
  if not Assigned(VListBoxForm) then
    Application.CreateForm(TVListBoxForm, VListBoxForm);
  PickChoiceLine := '';
  MaxTextWidth := MinWidth;
  with VListBoxForm do begin
    ChoiceMade := false;
    if VUseFixedFont then begin
//      UseGlobal := true;
      Font.Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
      VUseFixedFont := false;
    end;
    Font.Size := ViewFontSize;
    VListBox.Font.Name := Font.Name;
    VListBox.Canvas.Font.Name := Font.Name;
    VListBox.Font.Size := Font.Size;
    VListBox.Canvas.Font.Size := Font.Size;
    if Assigned(GetItem) then GetVItem := GetItem
    else GetVItem := DefGetItem;
    VTestKey := TestKey;
    VGetSelect := GetSelect;
    Caption := title;
    CurTextWidth := Canvas.TextWidth(Caption)+wslush;
    if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
    if def_choice <= num_choices then Choice := def_choice
    else Choice := 1;
    if Choice < 1 then Choice := 1;
    with VListBox do begin
      Header := HeadText;
      ShowHeader := Length(Header) > 0;
      MultiSelect := Multitag;
      OkButton.Default := not MultiTag;
      if top_choice > 0 then TopIndex := top_choice-1;
      NumItems := num_choices;
      if Assigned(VGetSelect) then begin
        if Assigned(VTabStops) then SetLength(Tabs,Length(VTabStops^)+1)
        else SetLength(Tabs,1);
        Tabs[0] := CheckWidth;
        if Assigned(VTabStops) then for j := 1 to Length(VTabStops^) do
          Tabs[j] := VTabStops^[j-1] + CheckWidth;
        VTabStops := @Tabs;
      end
      else if Assigned(VTabStops) then begin
        SetLength(Tabs,Length(VTabStops^));
        for j := 0 to Length(VTabStops^)-1 do
          Tabs[j] := VTabStops^[j];
        VTabStops := @Tabs;
      end;
      UseTabStops := Assigned(VTabStops);
      if UseTabStops then begin
        SetTabStops(VTabStops^);
        Alpha := GetOrphStr(30281);
        lDlgUnits := (Canvas.TextWidth(Alpha) div Length(Alpha)) div 4;
        SetLength(Itabs,Length(VTabStops^));
        for j := 0 to Length(VTabStops^)-1 do
          Itabs[j] := VTabStops^[j] * lDlgUnits;
      end;
      for j := 1 to num_choices do begin
        if UseTabStops then begin
          PCItem := GetTheItem(j);
          CurTextWidth := LoWord(GetTabbedTextExtent(Canvas.Handle,PChar(PCItem),
            Length(PCItem),Length(VTabStops^),Itabs[0]))+wslush;
        end;
        if not UseTabStops or (CurTextWidth > 5000) then
          CurTextWidth := Canvas.TextWidth(GetTheItem(j))+wslush;
        if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
      end;
      if ShowHeader then begin
        if UseTabStops then begin
          PCItem := Header;
          CurTextWidth := LoWord(GetTabbedTextExtent(Canvas.Handle,PChar(PCItem),
            Length(PCItem),Length(VTabStops^),Itabs[0]))+wslush;
        end
        else CurTextWidth := Canvas.TextWidth(Header)+wslush;
        if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
      end;
      VTabStops := nil;
      if Choice > 0 then begin
        ItemIndex := Choice-1;
        TopIndex := ItemIndex;
        OKButton.Enabled := true;
      end
      else begin
        ItemIndex := -1;
        OKButton.Enabled := false;
      end;
      LastIndex := ItemIndex;
      Direction := false;
      IsButton := false;
      if ShowHeader then HeaderHeight := RowHeight
      else HeaderHeight := 0;
      Width := MaxTextWidth+10;
      if Width+10 > Screen.Width then begin
        Width := Screen.Width-10;
        ScrollBars := ssBoth;
        Inc(HeaderHeight,RowHeight);
      end;
      Height := num_choices * RowHeight+4+HeaderHeight;
      if Height+130 > Screen.Height then
        Height := (Screen.Height-130) div RowHeight * RowHeight + 4;
    end;
    {$ifdef help}
    HelpFile := DefHelpFile;
    HelpContext := Helptopic;
    {$else}
    HelpContext := 0;
    {$endif}
    HelpBut.Visible := HelpContext > 0;
    ClientHeight := VListBox.Height+OKButton.Height+15;
    Width := VListBox.Width+8;
    Left := SetFormLeft(Width,scrloc);
    Top := SetFormTop(Height,scrloc);
    OKButton.Top := Height-OKButton.Height-35;
    CanButton.Top := OKButton.Top;
    HelpBut.Top :=OKbutton.Top;
    Okbutton.Left := 8;
    if HelpContext > 0 then begin
      HelpBut.Left := Width-HelpBut.Width-14;
      CanButton.Left := (Width-CanButton.Width) div 2;
    end
    else CanButton.Left := Width-CanButton.Width-30;
    if AllowMinimize then Bordericons := [biSystemMenu,biMinimize];
    Application.ProcessMessages;
    ShowModal;
    Result := Choice;
    if Choice > 0 then PickChoiceLine := GetTheItem(Choice);
    Free;
  end;
  SrString := '';
  VListBoxForm := nil;
end;

procedure TVListBoxForm.VGetItem(Sender: TObject; Index: Integer;
  var ItemString: String);
var
   Selected : boolean;
begin
  if Assigned(VGetSelect) then begin
    VGetSelect(Index+1,Selected);
    if Selected then ItemString := '>'#9+GetTheItem(Index+1)
    else ItemString := #9+GetTheItem(Index+1)
  end
  else ItemString := GetTheItem(Index+1);
end;

procedure TVListBoxForm.VSelect(Sender: TObject; Index: Integer;
  var Selected: Boolean);
begin
  if Assigned(VGetSelect) then VGetSelect(Index+1,Selected);
end;

procedure CloseTheForm(const xchoice: integer);
begin
  with VListBoxForm do begin
    ChoiceMade := true;
    Close;
    if xchoice < 0 then Choice := xchoice
    else Choice := VListBox.ItemIndex+1;
  end;
end;

procedure TestTheKey(const Command: word);
var
   key    : word;
   j      : integer;
   k      : integer;
begin
  case Command of
    256: key := 32; {space}
    257: key := 8; {F8}
    258: key := 9; {F9}
    259: key := 68; {D}
    260: key := 82; {R}
    261: key := 88; {X}
    262,263: key := 42; {*}
    264: key := 2; {F2}
    265: key := 3; {F3}
    266: key := 7; {F7}
    269: key := 17; {Shift-F7}
    else key := 0;
  end;
  with VListBoxForm do if Assigned(VTestKey) then with VListBox do begin
    j := VTestKey(key,ItemIndex+1);
    case abs(j) of
      1: DrawItem(ItemIndex);
      2: for k := 0 to NumItems do DrawItem(k);
      3: CloseTheForm(0);
      4: CloseTheForm(-ItemIndex-1);
    end;
    if not IsButton and (j < 0) then begin
      if Direction then begin
//        if ItemIndex = 0 then ItemIndex := NumItems-1
//        else ItemIndex := ItemIndex-1;
        if ItemIndex > 0 then ItemIndex := ItemIndex-1;
      end
      else begin
//        if ItemIndex = NumItems-1 then ItemIndex := 0
//        else ItemIndex := ItemIndex+1;
        if ItemIndex < NumItems-1 then ItemIndex := ItemIndex+1;
      end;
    end;
  end
  else if Key = 9 then CloseTheForm(0);
  IsButton := false;
end;

procedure TVListBoxForm.VOnSelect(Sender: TObject; Index: Integer;
  Selected: Boolean);
begin
  {TestTheKey(256);}
end;

procedure TVListBoxForm.VTestSelect(Sender: TObject; Command: Word);
begin
  TestTheKey(Command);
end;

procedure TVListBoxForm.VOnClick(Sender: TObject);
begin
  OkButton.Enabled := true;
  with VListBox do if LastIndex <> ItemIndex then begin
    if (LastIndex = 0) and (ItemIndex = numitems-1) then Direction := true
    else if (LastIndex = numitems-1) and (ItemIndex = 0) then Direction := false
    else Direction := LastIndex > ItemIndex;
    LastIndex := ItemIndex;
  end;
  if IsButton then TestTheKey(256);
end;

procedure TVListBoxForm.CharToItem(Sender: TObject; Ch: Char;
  var Index: Integer);
var
   NewIndex : integer;
procedure TestCh(const StartIndex,EndIndex: integer);

var
   j,k  : integer;
   ItemString : String;
   Testch     : String;
   TestLen    : word;
begin
  TestLen := Length(SrString);
  for j := StartIndex to EndIndex do begin
    ItemString := GetTheItem(j+1);
    Testch := ' ';
    for k := 1 to Length(ItemString) do
      if (ItemString[k] >= '!') and (ItemString[k] <= '~') then begin
      testch := UpperCase(Copy(ItemString,k,TestLen));
      Break;
    end;
    if (testch > ' ') and (testch = SrString) then begin
      NewIndex := j;
      exit;
    end;
  end;
end;

begin
  with VListBox do begin
    SrString := SrString+UpCase(Ch);
    NewIndex := -1;
    TestCh(Index,numitems-1);
    if NewIndex < 0 then TestCh(0,Index-1);
    if NewIndex < 0 then begin
      SrString := UpCase(Ch);
      NewIndex := -1;
      TestCh(Index+1,numitems-1);
      if NewIndex < 0 then TestCh(0,Index-1);
    end;
    if NewIndex < 0 then SrString := ''
    else Index := NewIndex;
  end;
end;

procedure TVListBoxForm.Acceptit(Sender: TObject);
begin
  ChoiceMade := true;
  TestTheKey(258);
end;

procedure TVListBoxForm.Cancelit(Sender: TObject);
begin
  if not askabn then exit;
  Setabn(false);
  Close;
  Choice := 0;
end;

procedure TVListBoxForm.VMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then IsButton := true;
end;

procedure TVListBoxForm.VGetItemColor(Sender: TObject; Index: Integer;
  var FG, BG: TColor);

procedure SetNormColor;
begin
  with VListBox do begin
    if Index = ItemIndex then begin
      FG := clWindow;
      BG := clWindowText;
    end
    else begin
      FG := clWindowText;
      BG := clWindow;
    end;
  end;
end;

var
   Selected : boolean;

begin
  if Assigned(VGetSelect) then with VListBox do begin
    VGetSelect(Index+1,Selected);
    if Selected then begin
      if Index = ItemIndex then begin
        FG := clRed;
        BG := clWindowText;
      end
      else begin
        FG := clRed;
        BG := clWindow;
      end;
    end
    else SetNormColor;
  end
  else SetNormColor;
end;

procedure TVListBoxForm.DoHelp(Sender: TObject);
begin
  if HelpContext > 0 then begin
    keybd_event(VK_F1,MapVirtualKey(VK_F1,0),0,0);
  end;
end;

procedure TVListBoxForm.CloseClick(Sender: TObject;
  var Action: TCloseAction);
begin
  if not ChoiceMade then begin
    if not askabn then begin
      Action := caNone;
      exit;
    end;
    Setabn(false);
    Close;
    Choice := 0;
  end;
end;

procedure TVListBoxForm.Afterenter(Sender: TObject);
var
   ItemPos : integer;
begin
  with VListBox do begin
    ItemPos := ItemIndex-TopIndex;
    if ItemPos > (Height div RowHeight)-2 then Scroll(0,ItemPos-3);
  end;
end;

procedure TVListBoxForm.BoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Assigned(VTestKey) and (Key = VK_Return) then begin
    Key := 0;
    TestTheKey(256);
  end;
end;

end.
