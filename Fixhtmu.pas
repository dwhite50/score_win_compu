unit Fixhtmu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure Fixit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    Fopen,
    util2,
    StStrL,
    YesNoBoxU,
    csdef;

{$R *.DFM}

const
     ScoreDir   = 'C:\ACBLSCOR\';

procedure TForm1.Fixit(Sender: TObject);
var
   Inpath : string;
   Outpath : string;
   Infilename : string;
   Fname      : string;
   F          : TextFile;
   line       : string;
   isparm     : boolean;
   sr         : TSearchRec;
   ser        : integer;
   sname      : string;
   Flist      : TStringList;
begin
  AppName := 'FixHtm';
  FindCFG;
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  Inpath := '';
  isparm := ParamCount > 0;
  FList := TStringlist.Create;
  FList.Clear;
  FList.Sorted := true;
  if isparm then begin
    sname := ParamStr(1);
    Inpath := fixpath(JustPathNameL(sname));
    ser := FindFirst(sname,faanyfile,sr);
    while ser = 0 do with sr do begin
      FList.Add(name);
      ser := FindNext(sr);
    end;
    FindClose(sr);
  end;
  ser := 0;
  while true do begin
    if isparm then begin
      if ser >= Flist.Count then begin
        Application.Terminate;
        exit;
      end;
      Fname := Flist[ser];
      Infilename := Inpath+Fname;
    end;
    if not isparm then begin
      if not GetFileName(Inpath,'Html files |*.htm',inFileName,
        'Select tournament htm file to convert','HtmLoc','c:\temp',false) then begin
        Application.Terminate;
        exit;
      end;
      Inpath := fixpath(JustPathNameL(Infilename));
      Fname := JustFileNameL(infilename);
    end;
    if ParamCount > 1 then Outpath := fixpath(ParamStr(2))
    else Outpath := Inpath;
    if not FileExists(Infilename) then begin
      Application.Terminate;
      exit;
    end;
    AssignFile(F,infilename);
    ReSet(F);
    ReadLn(F,line);
    if Pos('ACBLscore',line) < 1 then begin
      if isparm then Inc(ser)
      else ErrBox(Fname+' was not produced by ACBLscore.  Not converted',6,0);
      CloseFile(F);
      continue;
    end;
    ReadLn(F,line);
    CloseFile(F);
    if Pos('CSS',line) > 0 then begin
      if not isparm then ErrBox(Fname+' has already been converted',6,0);
    end
    else begin
      if ParamCount > 1 then FixHtmFile(infilename,outpath+Fname,ScoreDir,'')
      else begin
        FixHtmFile(infilename,inpath+'temp.htm',ScoreDir,'');
        DeleteFile(infilename);
        RenameFile(inpath+'temp.htm',infilename);
      end;
      if isparm then Inc(ser)
      else ErrBox(fname+' has been converted',6,0);
    end;
  end;
end;

end.
