unit dfu;

interface

procedure DeepFinesse(const Board: word; const dodf: boolean; const HandSet: String);

implementation
uses
    StStrS,
    dealt,
    handff,
    YesNoboxU,
    Progress;

function mbedEntryPoint(S: Pchar): Pchar; cdecl; external 'df_main.dll';

function CallDF(const S: String; var R: String):boolean;
begin
  R := mbedEntryPoint(Pchar(S));
  Result := Pos('error',R) < 1;
  if not Result then ErrBox(R,6,0);
end;

procedure DeepFinesse(const Board: word; const dodf: boolean; const HandSet: String);
const
     {hand = 'SSA SST SS7 SS5 SS4 SS3 SDA SD3 SD2 SC6 '
    +'SHQ SHJ SH5 ES6 EDK EDQ ED7 ED6 ECQ ECJ EC8 EHT EH9 EH7 EH4 EH2 NSK NSQ '
    +'NS2 NDJ ND5 ND4 NCA NCK NC9 NC7 NC5 NC4 NC2 WSJ WS9 WS8 WDT WD9 WD8 WCT '
    +'WC3 WHA WHK WH8 WH6 WH3 ';}
     {hand = 'SSK SST SS9 SHQ SH8 SH4 SH3 SD7 SD4 SCK SCJ SC9 SC5 '
           +'WSQ WS8 WS4 WS3 WS2 WDK WDQ WDT WD3 WCA WCQ WCT WC4 '
           +'NS7 NS6 NS5 NH7 NDA ND9 ND6 ND5 ND2 NC8 NC6 NC3 NC2 '
           +'ESA ESJ EHA EHK EHJ EHT EH9 EH6 EH5 EH2 EDJ ED8 EC7 ';}
     {hand = 'NSQ NSJ NST NS7 NS5 NS4 NHK NHJ NH9 NH4 NDA NDK ND6 '
           +'WSA WSK WS9 WS8 WS3 WDQ WD8 WD4 WCA WCK WCQ WCT WC3 '
           +'EHT EH7 EH5 EH3 EH2 ED9 ED7 ED3 EC9 EC6 EC5 EC4 EC2 '
           +'SS6 SS2 SHA SHQ SH8 SH6 SDJ SDT SD5 SD2 SCJ SC8 SC7 ';}
    Dirs = 'NWES';
    Suits = 'NSHDC';
var
   hand   : String;
   ret    :String;
   j      : word;
   pr     : word;
   isgood : boolean;
   level  : word;
   dir    : word;
   suit   : word;
   contract : String;

label
     doexit;
begin
  hand := '';
  FillChar(DHCP,SizeOf(DHCP),0);
  for dir := 1 to 4 do for suit := 1 to 4 do
    for j := 1 to length(chand[suit,dir]) do begin
    case chand[suit,dir][j] of
      'A': Inc(DHCP[dir],4);
      'K': Inc(DHCP[dir],3);
      'Q': Inc(DHCP[dir],2);
      'J': Inc(DHCP[dir],1);
    end;
    hand := hand+Dirs[dir]+Suits[suit+1]+chand[suit,dir][j]+' ';
  end;
  if not dodf then exit;
  FillChar(DFana,SizeOf(DFana),0);
  if not CallDF('setlingoxEnglish',ret) then exit;
  ProgressBarInit('Evaluating Board '+Long2StrS(board),
    'Deep Finesse Set '+HandSet+', Board '+Long2StrS(board),false);
  for dir := 1 to 4 do begin
    for suit := 1 to 5 do begin
      pr := (dir-1) * 35 + (suit-1) * 7;
      level := 6;
      repeat
        Inc(level);
        ProgressBar(pr+level-5,140);
        contract := Long2StrS(level)+Suits[suit]+dirs[Dir]+' ';
        if not CallDF('inputDeal'+contract+hand,ret) then goto doexit;
        j := 0;
        repeat
          Inc(j);
          if not CallDF('cardclass0',ret) then goto doexit;
          isgood := Pos('g',ret) > 0;
        until isgood or (Pos('l',ret) < 1) or (j > 99);
      until isgood or (level > 12);
      if isgood then Dec(level);
      if level > 6 then DFana[dir,suit] := level-6;
    end;
  end;
doexit:
  ProgressBarDone(false);
  {contract := '';
  for dir := 1 to 4 do begin
    contract := contract+dirs[dir]+' can make';
    for suit := 1 to 5 do if DFana[dir,suit] > 0 then contract := contract+' '
      +Long2StrS(DFana[dir,suit])+suits[suit]+';';
    contract := contract+#13;
  end;
  ErrBox(contract,0);}
end;

end.
