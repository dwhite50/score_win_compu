unit howellu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  THowelluForm = class(TForm)
    procedure RunHowell(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HowelluForm: THowelluForm;

implementation

uses
    Howell,
    version;

{$R *.DFM}

procedure THowelluForm.RunHowell(Sender: TObject);
begin
  CallHowell;
  DisPoseMainWindow;
  Application.Terminate;
end;

end.
