unit makedst;
{.$define test}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, CompLHA, Psock, NMFtp;

type
  TForm1 = class(TForm)
    CompLHA1: TCompLHA;
    Label2: TLabel;
    NMFTP1: TNMFTP;
    procedure MakeDistrict(Sender: TObject);
    procedure LHAProgress(var PercentageDone: Integer);
    procedure DoCancel(Sender: TObject);
    procedure FTPConnect(Sender: TObject);
    procedure FTPDisconnect(Sender: TObject);
    procedure FTPError(var Handled: Boolean; Trans_Type: TCmdType);
    procedure FTPErr(Sender: TComponent; Errno: Word; Errmsg: String);
    procedure FTPConFail(Sender: TObject);
    {procedure StartDist(Sender: TObject);}
  private
    { Private declarations }
    Connected      : boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses
  csdef,
  dbase,
  util2,
  history,
  pvio,
  StStrL,
  StUtils,
  YesNoBoxU,
  progress,
  plimport,
  version;

{$R *.DFM}
const
//     WebHost = '216.37.76.60';
//     WebHost = '67.18.250.187';
     WebHost = 'www.acbl.org';
     {WebUserID = 'acblscore';
     WebPassword = 'pine8733';}
//     WebDir = 'acblscore';
     WebDir = '';
     WebText     = 'Update includes points received and posted by ';
     WebFilename = 'mpdate.txt';

procedure TForm1.MakeDistrict(Sender: TObject);

const
     inname = 'NABCCSV';
     DateTag    = '<!UPDATEDATE>';
     {$ifdef test}
     Workpath   = 'D:\ACBL\';
     Destpath1  = 'C:\ACBLTEMP\';
     Destpath2  = Destpath1;
     Destpath3  = Destpath1;
     PlDestPath = 'C:\ACBLSCOR\PLAYER\';
     MpDestpath = Destpath1;
     {$else}
     Workpath   = 'F:\MPfiles\';
     Destpath1  = 'F:\secure.acbl.org\4674666\';
     Destpath2  = 'F:\secure.acbl.org\ACBLtd\';
     MpDestpath = 'F:\web2.acbl.org\mpfiles\';
     Destpath3  = 'F:\web2.acbl.org\bb\misc\';
     PlDestPath = '\\Bridge\Sys\ACBLSCOR\PLAYER\';
     {PlDestPath = 'E:\ACBLSCOR\PLAYER\';}
     {$endif}
     RibDestpath = Destpath2+'_nabc\';
     RibSearch  = '*.PLN';
     Lzhext     = '.LZH';
     mp         = 'MP';

var
  infilename : string;
  DistNum : byte;
  Ftime   : TFtime;
  TotInrecs : longint;
  Search_rec   : TSearchRec;
  imp_file : textFile;
  out_file : textFile;
  outrecs,
  recs : LongInt;
  Player_no : string[7];
  tempdist : string[2];
  Import_record : string;
  j             : word;
  Finderr       : integer;

function ReadImport: boolean;
begin
  Result := false;
  {$I-}ReadLn(imp_file, import_record);{$I+}
  CheckIOResult(infilename);
  if length(import_record) > 1 then begin
    inc(recs);
    player_no := GetCSVField(1,import_record);
    if not player_check(player_no,pnACBL) then begin
      AddToLogFile('Invalid player number: '+player_no+' at record '
        +long2strL(recs),false);
      exit;
    end;
    tempdist := GetCSVField(21,import_record);
    Result := true;
  end;
end;

procedure MakeLZH(const Fname,lzhname: string);
begin
  {$ifdef test}
  SetTheFileTime(Fname,FTime);
  ProgressBarInit(Fname+' --> '+lzhname+lzhext,'Compressing',true);
  {$endif}
  AddToLogFile('Compressing '+Fname+' --> '+lzhname+lzhext,false);
  with CompLHA1 do begin
    FilesToProcess.Clear;
    FilesToProcess.Add(Fname);
    ArchiveName := lzhname+lzhext;
    CompressionMethod := colh5;
    Compress;
  end;
  SetTheFileTime(lzhname+lzhext,FTime);
  {$ifdef test}
  ProgressBarDone(false);
  {$endif}
end;

procedure CopyFileWait(const SrcPath, DestPath : string; const UseProg: boolean;
         var canceled: boolean);
const
     MaxTries  = 500;
     TryWait   = 10000;
var
   ThisTry     : integer;
begin
  ThisTry := 0;
  repeat
    Inc(ThisTry);
    canceled := false;
    PCopyFile(SrcPath,DestPath,UseProg,canceled,ThisTry >= MaxTries);
    if canceled then Sleep(TryWait);
  until not canceled or (ThisTry >= MaxTries);
end;

procedure DotheDistrict(const DistNum: word; const DoMP, DoPIF: boolean);
var
  mp_file  : textFile;
  outfilename : String;
  PickDist : String[3];
  accept : boolean;
  datestr  : string;

label
     impdone;
begin
  outrecs := 0;
  PickDist := numcvt(DistNum,2,true);
  outfilename := WorkPath+'D'+PickDist;
  assignFile(imp_file, infilename);
  {$I-} Reset(imp_file); {$I+}
  if IOResult <> 0 then begin
    AddToLogFile('File '+infilename+' not found',true);
    exit;
  end;
  if DoPIF then begin
    assignFile(out_file, outfilename);
    ReWrite(out_file);
  end;
  if DoMP then begin
    assignFile(mp_file, outfilename+mp);
    ReWrite(mp_file);
  end;
  {$ifdef test}
  ProgressBarInit(outfilename,'Creating File',true);
  {$endif}
  recs := 0;
  AddToLogFile('Creating File '+outfilename,false);
  while (not eof(imp_file)) do begin
    if Canceled then goto IMPdone;
    if ReadImport then begin
      accept := (tempdist = PickDist) or (DistNum < 1);
      if accept then begin
        if DoPIF then begin
          {$I-}WriteLn(out_file, import_record);{$I+}
          CheckIOResult(outfilename);
        end;
        if DoMP then begin
          {$I-}
          Datestr := GetCSVField(19,import_record);
          if Length(DateStr) > 8 then DateStr := Copy(DateStr,9,2)
            +Copy(DateStr,1,2)+Copy(DateStr,4,2)
          else DateStr := Copy(DateStr,7,2)+Copy(DateStr,1,2)+Copy(DateStr,4,2);
          WriteLn(mp_file,GetCSVField(1,import_record),DateStr,
            LeftPadChL(GetCSVField(13,import_record),'0',8));
          {$I+}
          CheckIOResult(outfilename+mp);
        end;
        inc(outrecs);
      end;
      {$ifdef test}
      If (recs mod 1000 = 0) then ProgressBar(recs,TotInRecs);
      {$endif}
    end;
    if canceled then exit;
  end;
  {$ifdef test}
  ProgressBarDone(false);
  {$endif}
impdone:
  closeFile(imp_file);
  if DoPIF then begin
    CloseFile(out_file);
    MakeLZH(outfilename,outfilename);
    DeleteFile(outfilename);
    {$ifdef test}
    copyfileWait(outfilename+lzhext,Destpath1+'D'+PickDist+lzhext,true,canceled);
    if canceled then exit;
    copyfileWait(outfilename+lzhext,Destpath2+'D'+PickDist+lzhext,true,canceled);
    {$else}
    CopyFileWait(outfilename+lzhext,Destpath1+'D'+PickDist+lzhext,false,canceled);
    if canceled then exit;
    copyfileWait(outfilename+lzhext,Destpath2+'D'+PickDist+lzhext,false,canceled);
    {$endif}
    if canceled then exit;
    DeleteFile(outfilename+lzhext);
  end;
  if DoMP then begin
    CloseFile(mp_file);
    MakeLZH(outfilename+mp,outfilename+mp);
    DeleteFile(outfilename+mp);
    {$ifdef test}
    copyfileWait(outfilename+mp+lzhext,MPDestpath+'D'+PickDist+mp+lzhext,
      true,canceled);
    {$else}
    copyfileWait(outfilename+mp+lzhext,MPDestpath+'D'+PickDist+mp+lzhext,
      false,canceled);
    {$endif}
    if canceled then exit;
    DeleteFile(outfilename+mp+lzhext);
  end;
end;

procedure ValidFolder(const Path: String);
begin
  if canceled then exit;
  if not IsDirectory(JustPathNameL(Path)) then
    AddToLogFile('Folder: '+Path+' not found.',true);
end;

procedure FixHtmlDates;

{procedure SendWebFTPCommand;
begin
  with Form1 do begin
    while Connected do Application.ProcessMessages;
    AddToLogFile('Connecting via FTP to '+WebHost,false);
    with NMFTP1 do begin
      Host := WebHost;
      Timeout := 60000;
      UserId := WebUserId;
      Password := WebPassword;
      Connect;
    end;
  end;
end;}

var
  TodaysDate : string;

var
   F   : textfile;

begin
  TodaysDate := FormatDateTime('mmmm d, yyyy',Now);
  AssignFile(F,WebFileName);
  ReWrite(F);
  WriteLn(F,WebText,TodaysDate);
  CloseFile(F);
  copyfileWait(WebFileName,MPDestpath+WebFileName,false,canceled);
//  SendWebFtpCommand;
end;

begin
  Connected := false;
  canceled := false;
  background := true;
  ErrFileNm := 'MAKEDIST.LOG';
  DeleteFile(ErrFileNm);
  ValidFolder(Workpath);
  ValidFolder(Destpath1);
  ValidFolder(Destpath2);
  ValidFolder(Destpath3);
  ValidFolder(MPDestpath);
  ValidFolder(PLDestpath);
  if canceled then exit;
  ChDir(JustPathNameL(Workpath));
  infilename := Workpath+inname;
  Application.ProcessMessages;
  {$I-} MkDir(JustPathNameL(Workpath)); {$I+}
  if IOResult = 0 then;
  if FindFirst(infilename,faArchive,Search_rec) = 0 then
    TotInRecs := Search_rec.Size div 172
  else begin
    AddToLogFile('File: '+infilename+' not found.',true);
    exit;
  end;
  SysUtils.FindClose(Search_rec);
  assignFile(imp_file,infilename);
  Reset(imp_file);
  GetFileTime(TTextRec(imp_file).Handle,@FTime[1],@FTime[2],@FTime[3]);
  ReadLn(Imp_file,import_record);
  CloseFile(imp_file);
  CopyTries := 600;
  {$ifdef test}
  For DistNum := 18 to 19 do begin
  {$else}
  For DistNum := 1 to 25 do begin
  {$endif}
    DotheDistrict(DistNum,true,true);
    if canceled then exit;
  end;
  DotheDistrict(99,true,false);
  DotheDistrict(0,true,false);
  MakeLZH(infilename,Workpath+'D99');
  {$ifdef test}
  copyfileWait(Workpath+'D99'+lzhext,Destpath1+'D99'+lzhext,true,canceled);
  if canceled then exit;
  copyfileWait(Workpath+'D99'+lzhext,Destpath2+'D99'+lzhext,true,canceled);
  {$else}
  copyfileWait(Workpath+'D99'+lzhext,Destpath1+'D99'+lzhext,false,canceled);
  if canceled then exit;
  copyfileWait(Workpath+'D99'+lzhext,Destpath2+'D99'+lzhext,false,canceled);
  if canceled then exit;
  copyfileWait(Workpath+'D99'+lzhext,Destpath3+'D99'+lzhext,false,canceled);
  {$endif}
  if canceled then exit;
  DeleteFile(Workpath+'D99'+lzhext);
  DistNum := 0;
  repeat
    Inc(DistNum);
    FindErr := FindFirst(Workpath+RibSearch,faArchive,Search_rec);
    if FindErr = 0 then with Search_rec do begin
      copyfileWait(Workpath+Name,RibDestpath+Name,false,canceled);
      if canceled then exit;
      DeleteFile(Workpath+Name);
    end;
    FindClose(Search_rec);
  until (FindErr <> 0) or (DistNum > 4);
  for j := 0 to 3 do if FileExists(Workpath+DBNames[playern_no,j]) then
    DeleteFile(Workpath+DBNames[playern_no,j]);
  {$ifdef test}
  DelimImport(infilename,WorkPath,true,true,canceled);
  {$else}
  DelimImport(infilename,WorkPath,true,false,canceled);
  {$endif}
  if canceled then exit;
  for j := 0 to 3 do begin
    {$ifdef test}
    copyfileWait(Workpath+DBNames[playern_no,j],PlDestpath+DBNames[playern_no,j],
      true,canceled);
    {$else}
    copyfileWait(Workpath+DBNames[playern_no,j],PlDestpath+DBNames[playern_no,j],
      false,canceled);
    {$endif}
    if canceled then exit;
    DeleteFile(Workpath+DBNames[playern_no,j]);
  end;
  DeleteFile(infilename);
  try
  FixHtmlDates;
  AddToLogFile('Processing complete',false);
  except
    AddToLogFile('Could not update html dates',false);
  end;
  Application.Terminate;
end;

procedure TForm1.LHAProgress(var PercentageDone: Integer);
begin
  {$ifdef test}
  canceled := ProgressBar(PercentageDone,100);
  if canceled then PercentageDone := -1;
  {$endif}
end;

procedure TForm1.DoCancel(Sender: TObject);
begin
  if YesNoBox(canceled,false,'Confirm cancel',0,MC,nil) then begin
    Canceled := true;
    Application.Terminate;
  end
  else canceled := false;
end;

procedure TForm1.FTPConnect(Sender: TObject);
begin
  Connected := true;
  AddtoLogFile('FTP '+WebFileName+' to '+WebHost,false);
  Application.ProcessMessages;
  try
    if not Empty(WebDir) then NMFTP1.ChangeDir(WebDir);
    NMFTP1.Upload(WebFileName,WebFileName);
  except
    AddToLogFile('Unable to FTP '+WebFileName+' to '+WebHost,false);
  end;
  NMFTP1.Disconnect;
end;

procedure TForm1.FTPDisconnect(Sender: TObject);
begin
  Connected := false;
end;

procedure TForm1.FTPError(var Handled: Boolean; Trans_Type: TCmdType);
var
   Errmsg : String;
begin
  Errmsg := '';
  Case Trans_Type of
    cmdChangeDir: Errmsg := 'ChangeDir failure';
    cmdMakeDir: Errmsg := 'MakeDir failure';
    cmdDelete: Errmsg := 'Delete failure';
    cmdRemoveDir: Errmsg := 'RemoveDir failure';
    cmdList: Errmsg := 'List failure';
    cmdRename: Errmsg := 'Rename failure';
    cmdUpRestore: Errmsg := 'UploadRestore failure';
    cmdDownRestore: Errmsg := 'DownloadRestore failure';
    cmdDownload: Errmsg := 'Download failure';
    cmdUpload: Errmsg := 'Upload failure';
    cmdAppend: Errmsg := 'UploadAppend failure';
    cmdReInit: Errmsg := 'ReInit failure';
    cmdAllocate: Errmsg := 'Allocate failure';
    cmdNList: Errmsg := 'NList failure';
    cmdDoCommand: Errmsg := 'DoCommand failure';
  end;
  if Length(errmsg) > 0 then AddToLogfile(errmsg,true);
end;

procedure TForm1.FTPErr(Sender: TComponent; Errno: Word; Errmsg: String);
begin
  if Length(errmsg) > 0 then AddToLogfile(errmsg,true);
end;

procedure TForm1.FTPConFail(Sender: TObject);
begin
  {AddToLogFile('FTP connection to '+WebHost+' failed',true);}
end;

end.

