unit utilityu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure DoUtility(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    point,
    version;

{$R *.DFM}

procedure TForm1.DoUtility(Sender: TObject);
begin
  DoPoint;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
