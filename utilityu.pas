unit utilityu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StBase, StSpawn;

type
  TForm1 = class(TForm)
    StSpawnApplication1: TStSpawnApplication;
    procedure DoUtility(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure runthefile(const fname: string);

const
  Form1: TForm1 = nil;

implementation

uses
    utility,
    version;

{$R *.DFM}

procedure runthefile(const fname: string);
begin
  if not assigned(Form1) then
    Application.CreateForm(TForm1, Form1);
  with Form1,StSpawnApplication1 do begin
    FileName := fname;
    RunParameters := '';
    Execute;
    Free;
  end;
  Form1 := nil;
end;

procedure TForm1.DoUtility(Sender: TObject);
begin
  CallUtility;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
