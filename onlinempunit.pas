unit onlinempunit;
{.$define test}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TOnLineForm = class(TForm)
    procedure DothePoints(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OnLineForm: TOnLineForm;

implementation

uses
    onlinefixmp,
    csdef,
    FOpen;

{$R *.DFM}

procedure TOnLineForm.DothePoints(Sender: TObject);
const
     ScoreDir   = 'C:\ACBLSCOR\';
var
   RepFile      : String;
begin
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  AppName := 'OnLineMps';
  RepFile := '';
  if not GetFileName('','*.*',RepFile,
    'Select On-line Masterpoint file to process','FileLoc','C:\',false) then begin
    Application.Terminate;
    exit;
  end;
  SubmitOnLineMPRep(RepFile);
  Application.Terminate;
end;

end.
