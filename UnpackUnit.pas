{.$define test}
unit UnpackUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompLHA;

type
  TUnpackForm = class(TForm)
    CompLHA1: TCompLHA;
    procedure Unpack(Sender: TObject);
    procedure LHAProgress(var PercentageDone: Integer);
    procedure LHAFileDone(const originalFilePath, newFilePath: String;
      dateTime: Integer; mode: TCompLHAProcessMode);
    procedure LHAFileStart(const originalFilePath: String;
      var newFilePath: String; dateTime: Integer;
      mode: TCompLHAProcessMode);
  private
    { Private declarations }
    LhaTotSize  : integer;  //size in bytes of all files to process
    LhaThisSize : integer;  //size in bytes of file currently in process
    LhaProgDone : integer;  //size in bytes of files already done
  public
    { Public declarations }
  end;

var
  UnpackForm: TUnpackForm;

implementation

uses
  StUtils,
  {$ifdef VER150}
  StSystem,
  {$endif}
  StStrL,
  StStrS,
  db33,
  fixpl,
  CSdef,
  FOpen,
  StdCtrls,
  vListBox,
  CListBox,
  Progress,
  groups,
  version,
  YesNoBoxU,
  pvio,
  util1,
  history,
  util2,
  fdbase,
  fxdbase,
  Dbase,
  xdbase;

{$R *.DFM}

var
   anyok             : byte;
   SancTable         : Array[1..50] of String;
   oktoprocess       : Array[1..50] of boolean;

procedure GetSelect(const Index: integer; var Selected: TCheckBoxState;
               const InitPhase: boolean);
begin
  if InitPhase then begin
    if oktoprocess[Index] then begin
      Selected := cbChecked;
      Inc(anyok);
    end
    else Selected := cbUnChecked;
  end
  else begin
    if oktoprocess[Index] then Dec(anyok);
    oktoprocess[Index] := Selected = cbChecked;
    if oktoprocess[Index] then Inc(anyok);
  end;
end;

procedure TUnpackForm.Unpack(Sender: TObject);
const
     MPftype = '.MPR';
     ArcSuffix = '.LZ?';
     LZHext = '.LZH';
     HtmSuffix = '.HTM';
     PPSuffix = '.PP';
     Repsuffix = '.REP';
     Finsuffix = '.FIN';
     DBLockFileName = 'DBOPEN';
     STACtestFile = 'CLUBDEF.DAT';
     STACtestMinSize = 300;
     GFMask    = '???????';
     DBMask    = 'D???????';
     MinSpaceNeeded = 1024 * 2048;
     TempDir = 'C:\ACBLTEMP\';
     ScoreDir = 'C:\ACBLSCOR';
     MaxDBFileTypes = 2;
     DBFileTypes    : Array[1..MaxDBFileTypes] of String[3] = ('DAT','K?');
     PathChoice   : word = 3;
     {$ifdef test}
     HtmDestLocation = 'C:\TEMP\HTM\';
     PPDestLocation = 'C:\TEMP\PP\';
     TournDBLoc = 'C:\TEMP\TOURNDB\';
     TournArchive = 'C:\TOURNS\';
     ReportPath = TournArchive;
     AS400Loc = TournArchive;
     {$else}
     BridgeSys = '\\BRIDGE\SYS\';
     Dept730 = BridgeSys+'DEPT\730\';
     HtmDestLocation = Dept730+'TournResults\';
     PPDestLocation = Dept730+'DVICKNAI\PP\';
     ReportPath     = Dept730+'TOURNREP\';
     TournArchive = BridgeSys+'TOURNS\';
     TournDBLoc = TournArchive+'DIST\';
     AS400Loc = '\\QACBL\ACBLIFS\';
     {$endif}
     NABCSancPref = 'NABC';
     OtherLocation = TournArchive+'ACBL32A';
     SancLZH      = 'T';

var
   esc        : boolean;
   len,
   ifunc      : byte;
   Search_Rec  : TSearchRec;
   found      : boolean;
//   DBfiles    : word;
   path    : String;
//   DBfileList : Array[1..200] of String[12];
   Sanction   : String[10];
   FileSanc   : String;
   RepDestLocation    : String;
   RepDestValid       : boolean;
   PPDestValid       : boolean;
   HtmDestValid      : boolean;

procedure ReadSancLocs(const TestDest: boolean);

begin
  if TestDest then begin
    RepDestLocation := AS400Loc;
    RepDestValid := IsDirectory(JustPathNameL(RepDestLocation));
    if not RepDestValid then begin
      ErrBox('Location '+RepDestLocation+' is not accessable.',MC,0);
      MainHalt;
      exit;
    end;
    PPDestValid := IsDirectory(JustPathNameL(PPDestLocation));
  end
  else begin
    RepDestValid := false;
    RepDestLocation := '';
  end;
  HtmDestValid := IsDirectory(JustPathNameL(HtmDestLocation));
end;

procedure DelTempDirFiles;
var
   FndErr : integer;
begin
  CreateDir(JustPathNameL(TempDir));
  repeat
    FndErr := FindFirst(TempDir+'*.*',faArchive,Search_Rec);
    FindClose(Search_Rec);
    if FndErr = 0 then if not DeleteFile(TempDir+Search_Rec.Name) then
      FndErr := -1;
  until FndErr <> 0;
end;

procedure RunLHA(const CmdLine,ArcName,TargetDir,Title: String);
var
   SearchRec : TSearchRec;
   FindErr : integer;
begin
  with UnPackForm.CompLHA1 do begin
    CompressionMethod := colh5;
    TargetPath := TargetDir;
    if Pos('.',ArcName) > 0 then ArchiveName := ArcName
    else ArchiveName := ArcName+lzhext;
    LhaTotSize := 0;
    LhaProgDone := 0;
    FilesToProcess.Clear;
    if uppercase(CmdLine) = 'A' then begin
      FindErr := FindFirst(TargetDir+'*.*',faArchive,SearchRec);
      while FindErr = 0 do with FilesToProcess do begin
        Add(TargetDir+SearchRec.Name);
        Inc(LhaTotSize,SearchRec.Size);
        FindErr := FindNext(SearchRec);
      end;
      FindClose(SearchRec);
      ProgressBarInit('Creating '+ArchiveName,'Compressing',false);
      Compress;
    end
    else begin
      Scan;
      for FindErr := 0 to FileList.Count-1 do
        Inc(LhaTotSize,TCompLHAFileInfo(FileInformation[FindErr]).FullSize);
      ProgressBarInit('Extracting from '+ArchiveName,'Expanding',false);
      Expand;
    end;
    ProgressBarDone(false);
  end;
end;

function NumSanc(const Sanction: String): boolean;
var
   j          : byte;
begin
  NumSanc := false;
  if Length(Sanction) < 4 then exit;
  for j := 1 to 4 do if Pos(Sanction[j],'0123456789') < 1 then exit;
  NumSanc := true;
end;

type
    THashTable = Array[1..500,1..2] of String[3];
    PHashTable = ^THashTable;
    TfSancTable = Array[1..200] of String[10];
    PfSancTable = ^TfSancTable;

const
     HashTable : PHashTable = nil;
     HashTableSize : word = 0;
     fSancTable : PfSancTable = nil;
     SancTableSize : word = 0;
     TotalRecs     : Longint = 0;

{$I mpadj.inc}

procedure UnpackTourn(const full: boolean);
var
   DistNo       : String[2];
   DotLoc : byte;
   CurrentDir    : String;
   errcode : boolean;
   GFname  : String;
   DBDelName : String;
   GameDelName : String;
   ArcName : String[12];
   j       : word;
   k       : word;
   SpaceNeeded       : longint;
   SpaceUnused       : longint;
   f                 : file;
   TempKey           : KeyStr;
   FndKey            : KeyStr;
   Sanctions         : word;
   CurSanction       : word;
   Line              : String;
   lenx              : word;
   dorep             : boolean;
   PlayerRecLength   : longint;
   AnyGameFiles      : boolean;
   RRead             : longint;
   Added             : longint;
   Modified     : longint;
   OktoMerge    : boolean;
   dofin             : boolean;
   FindErr      : integer;
   RepPath       : String;

procedure MakeFinReport;

function FM(const num: longint; const width: byte):String;
begin
  FM := numcvt(num,width,true);
end;

function FFM(const rnum: real; const mult: word; const width: byte):String;
begin
  FFM := FM(RRound(rnum*mult),width);
end;

function ZF(const Field: OpenString):String;
var
   len  : byte;
   OutField : String;
   j        : byte;
begin
  len := Length(Field);
  OutField := Field;
  for j := 1 to len do if OutField[j] = ' ' then OutField[j] := '0';
  ZF := OutField;
end;

var
   SancKey      : Keystr;
   FinFileName  : String;
   F            : textfile;
   Seq          : word;
   workrecs     : word;
   tempkey      : keystr;
   invkey       : keystr;
   supp_sess    : real;
   sick_sess    : real;
   Other_sess   : real;
   Train_sess   : real;
   Reg_Hours    : real;
   Ovt_Hours    : real;
   DICReg_Hours    : real;
   DICOvt_Hours    : real;
   isdistno     : boolean;

function NextSeq: _Str4;

begin
  Inc(Seq);
  NextSeq := FM(Seq,4);
end;

label
     abort;
begin
  fXPrepend := Tempdir;
  fOpenDBFile(Ftinfo_no);
  FinFileName := tempdir+Sanction+FinSuffix;
  AssignFile(F,FinFileName);
  SancKey := ExtractWordL(1,SancTable[CurSanction],',');
  FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],SancKey);
  if not ok then goto abort;
  OpenDBFile(Tourn_no);
  FindKey(IdxKey[Tourn_no,1]^,Recno[Tourn_no],SancKey);
  isdistno := ok;
  if ok then GetARec(Tourn_no);
  CloseDBFile(Tourn_no);
  ok := true;
  fGetARec(Ftinfo_no);
  if isdistno then begin
    Ftinfo.District_no := Tourn.District_no;
    fPutARec(Ftinfo_no);
  end;
  ReWrite(F);
  Seq := 0;
  with Ftinfo do begin
    WriteLn(F,'01',Sanction_no,NextSeq,Tourn_type,Tourn_city,tourn_dates,
      tourn_name,tourn_site,Td_no,Dic_Name,District_no,Unit_no,
      FFM(Valu(Dic_Supp),100,5),FFM(Valu(Sanction_fee),100,5),
      FFM(Valu(supply_rate),100,4),FFM(Valu(surcharge),100,5),
      FFM(Valu(Per_Diem_rate),100,5),FFM(Valu(Mileage_rate),1000,4),
      FFM(Valu(Non_Memb_acbl_rate),100,5),Use_EX_rate,
      FFM(Valu(Exchange_rate),100,5),FFM(Valu(Exchange_rate_td),100,5),
      FFM(Valu(Gst),100,4));
    workrecs := 0;
    tempkey := SancKey;
    fOpenDBFile(Fwksheet_no);
    SearchKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Fwksheet do begin
      fGetARec(Fwksheet_no);
      if Event_code <> 'ZZZZ' then Inc(workrecs);
      NextKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    end;
    WriteLn(F,'02',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Entry_total),100,9),FFM(Valu(Non_mem),100,8),
      FFM(Valu(Fill_in),100,8),FFM(Valu(Net),100,9),
      FFM(Valu(total_tables),10,7));
    tempkey := SancKey;
    SearchKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Fwksheet do begin
      fGetARec(Fwksheet_no);
      if Event_code <> 'ZZZZ' then
        WriteLn(F,'03',Sanction_no,NextSeq,IdxDate(Event_date),ZF(Time),
        Event_code,Event_Name,ZF(sessions),FFM(Valu(tables),10,5),ZF(Entries),
        FFM(Valu(Entry_fee),100,5),FFM(Valu(Entry_total),100,7),
        FFM(Valu(non_memb),100,5),FFM(Valu(Fill_in),100,5));
      NextKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    end;
    workrecs := 0;
    fCloseDBFile(Fwksheet_no);
    fOpenDBFile(Finvoice_no);
    tempkey := SancKey;
    SearchKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do begin
      Inc(workrecs);
      NextKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    end;
    WriteLn(F,'04',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Dir_sessions),100,5),FFM(Valu(Director_fees),100,8),
      FFM(Valu(Trans_total_inv),100,7),ZF(Hotel_days_total),
      FFM(Valu(Hotel_total),100,7),FFM(Valu(Per_diem_total_days),10,5),
      FFM(Valu(Per_diem_total),100,8),FFM(Valu(Sanction_tables),10,7),
      FFM(Valu(Sanction_fee_total),100,8),FFM(Valu(Supply_tables),10,7),
      FFM(Valu(Supply_total),100,8),FFM(Valu(Hand_record_total),100,7),
      ZF(Non_members_acbl),FFM(Valu(non_mem_total_acbl),100,8),
      FFM(Valu(Expenses_total),100,9));
    tempkey := SancKey;
    fOpenDBFile(Finvitem_no);
    SearchKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Finvoice do begin
      fGetARec(Finvoice_no);
      InvKey := Fhash;
      supp_sess := 0;
      sick_sess := 0;
      Other_sess := 0;
      Train_Sess := 0;
      Reg_Hours := 0;
      Ovt_Hours := 0;
      DICReg_Hours := 0;
      DICOvt_Hours := 0;
      SearchKey(fIdxKey[Finvitem_no,1]^,fRecno[Finvitem_no],invkey);
      while ok and CompareKey(invkey,Fhash) do with Finvitem do begin
        fGetARec(Finvitem_no);
        if Ival(item_type) = 1 then case Ival(sub_item_type) of
          3: supp_sess := supp_sess+Valu(items);
          4: sick_sess := sick_sess+Valu(items);
          5: Other_sess := Other_sess+Valu(items);
          7: Train_sess := Train_sess+Valu(items);
          8: Reg_Hours := Reg_Hours+Valu(items);
          9: Ovt_Hours := Ovt_Hours+Valu(items);
          10: DICReg_Hours := DICReg_Hours+Valu(items);
          11: DICOvt_Hours := DICOvt_Hours+Valu(items);
        end;
        NextKey(fIdxKey[Finvitem_no,1]^,fRecno[Finvitem_no],invkey);
      end;
      ok := true;
      if salaried = 'Y' then Empl_Stat := 'S      ';
      WriteLn(F,'05',Sanction_no,NextSeq,Td_no,Player_no,Name,Empl_stat[1],
        salaried,td_rank,dates,FFM(Valu(dic_sessions),100,4),
        FFM(Valu(Staff_sessions),100,4),FFM(supp_sess,100,4),
        FFM(sick_sess,100,4),FFM(other_sess,100,4),
        FFM(Valu(Staff_td_total),100,7),FFM(Valu(Trans_amt),100,7),
        FFM(Valu(Trans_amtd),100,7),ZF(Hotel_days),FFM(Valu(Hotel_amt),100,7),
        FFM(Valu(per_diem_days),10,3),FFM(Valu(Advance_amt),100,6),
        FFM(Valu(exchange_total),100,7),FFM(Train_sess,100,4),
        FFM(Reg_hours,100,4),FFM(Ovt_Hours,100,4),
        FFM(DICReg_hours,100,4),FFM(DICOvt_Hours,100,4));
      NextKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    end;
    fCloseDBFile(Finvitem_no);
    fCloseDBFile(Finvoice_no);
    fOpenDBFile(FOfs_no);
    workrecs := 0;
    tempkey := SancKey;
    SearchKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fofs do begin
      fGetARec(Fofs_no);
      if Ival(item_type) in [1,3,4,5,6,7,8,11] then Inc(workrecs);
      NextKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    end;
    if Copy(Sanction,1,2) = '00' then begin
      Part_time_fees := Fstr(Valu(Part_time_fees)+Valu(Extended_fees),9,2);
      Extended_fees := Fstr(0,9,2);
    end;
    WriteLn(F,'06',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Salaried_fees),100,8),FFM(Valu(Full_time_fees),100,8),
      FFM(Valu(Part_time_fees),100,8),FFM(Valu(Sanc_free_tables),10,6),
      FFM(Valu(sanction_tables),10,7),FFM(Valu(Sanction_fee_total),100,8),
      FFM(Valu(hand_record_total),100,7),FFM(Valu(supply_tables),10,7),
      FFM(Valu(supply_total),100,8),FFM(Valu(trans_prepaid),100,7),
      ZF(non_members_acbl),FFM(Valu(non_mem_total_acbl),100,8),
      FFM(Valu(memb_fees),100,6),FFM(Valu(acbl_other),100,7),
      FFM(Valu(acbl_collected),100,9),FFM(Valu(comp_rental),100,5),
      FFM(Valu(trans_dic),100,7),FFM(Valu(acbl_other_paid),100,7),
      FFM(Valu(acbl_paid_out),100,8),FFM(Valu(acbl_due),100,9),
      FFM(Valu(scrip_to_acbl),100,7),FFM(Valu(us_due_acbl),100,9),
      FFM(Valu(us_check),100,9),FFM(Valu(zcanadian_due_acbl),100,9),
      FFM(Valu(can_memb_fees),100,6),FFM(Valu(can_check),100,9),
      FFM(Valu(Extended_fees),100,8));
    tempkey := SancKey;
    SearchKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fofs do begin
      fGetARec(Fofs_no);
      if Ival(item_type) in [1,3,4,5,6,7,8,11] then
        WriteLn(F,'07',Sanction_no,NextSeq,ZF(item_type),description,
        FFM(Valu(items),10,5),FFM(Valu(item_rate),100,7),
        FFM(Valu(amount),100,8));
      NextKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    end;
    fCloseDBFile(FOfs_no);
    fOpenDBFile(FBalance_no);
    workrecs := 0;
    tempkey := SancKey;
    SearchKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fbalance do begin
      fGetARec(Fbalance_no);
      if Ival(item_type) in [1,2,3,4,5] then Inc(workrecs);
      NextKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    end;
    WriteLn(F,'08',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(additions_total),100,8),FFM(Valu(deductions_total),100,8),
      FFM(Valu(exchange_td_total),100,8),FFM(Valu(exchange_acbl_total),100,8),
      FFM(Valu(expenses_total),100,9),FFM(Valu(sponsor_check),100,9),
      FFM(Valu(sponsor_net),100,9),FFM(Valu(checks_total),100,8),
      FFM(Valu(cash_total),100,8),FFM(Valu(other_total),100,8),
      FFM(Valu(money_total),100,9));
    tempkey := SancKey;
    SearchKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fbalance do begin
      fGetARec(Fbalance_no);
      if Ival(item_type) in [1,2,3,4,5] then
        WriteLn(F,'09',Sanction_no,NextSeq,ZF(item_type),description,
        FFM(Valu(items),10,5),FFM(Valu(item_rate),100,7),
        FFM(Valu(amount),100,8));
      NextKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    end;
    fCloseDBFile(FBalance_no);
    WriteLn(F,'99',Sanction_no,NextSeq);
  end;
  CloseFile(F);
  PCopyfile(FinFileName,RepDestLocation+Sanction+FinSuffix,false,esc,false);
abort:
  fCloseFiles;
end;

procedure CheckCat;
begin
  with Playern do begin
    if Pos(cat_a,cat_a_str) = 0 then cat_a_str := cat_a_str+cat_a;
    if Pos(cat_b,cat_b_str) = 0 then cat_b_str := cat_b_str+cat_b;
  end;
end;

procedure GetReportPath(doDB: boolean);
var
   FindErr      : integer;
begin
  if doDB then FindErr := FindFirst(RepPath+DBMask+ArcSuffix,faArchive,Search_Rec)
  else FindErr := FindFirst(RepPath+GFMask+ArcSuffix,faArchive,Search_Rec);
  FindClose(Search_Rec);
  if FindErr <> 0 then begin
    ErrBox('No tournament files found in '+RepPath,MC,0);
    esc := true;
    canceled := true;
    MainHalt;
    exit;
  end;
end;

procedure BackFillDB{(const Sanct: String; const dofill: boolean)};
var
   isplayer     : boolean;
   NameKey      : keystr;
   {MPList       : TStringList;
   TotMPs       : integer;
   mpfile       : TextFile;
   j            : integer;
   mppos        : integer;
   TournTables       : real;}

procedure UpdatePl;
var
   OldKey       : keystr;
begin
  with playern do begin
    Inc(Modified);
    Acbl_Date := xplayern.Acbl_date;
    Acbl_rank := xplayern.Acbl_rank;
    ACBL_Update_Date := xplayern.ACBL_Update_Date;
    Paid_Thru := xplayern.Paid_Thru;
    if Empty(Cat_A) then Cat_a := xplayern.Cat_a;
    if Empty(Cat_b) then Cat_b := xplayern.Cat_b;
    City := xplayern.City;
    State := xplayern.State;
    Street1 := xplayern.Street1;
    Street2 := xplayern.Street2;
    Email := xplayern.Email;
    fee_type := xplayern.fee_type;
    zip := xplayern.zip;
    if not Empty(Street1) then Mail_Code := 'M';
    Country := xplayern.Country;
    if Empty(Gender) then Gender := xplayern.Gender;
    Phone := xplayern.Phone;
    Unit_no := xplayern.Unit_no;
    District_no := xplayern.District_no;
    SetPlayerPoints(GetPlayerPoints(xplayern,ppElig),ppElig,false);
    SetPlayerPoints(GetPlayerPoints(xplayern,ppTotal),ppTotal,false);
    if Player_check(xplayern.Player_no,pnPoundACBL)
      and not Player_check(Player_no,pnPoundACBL) then begin
      OldKey := GetKey(Playern_no,2);
      DeleteKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
      Player_no := xplayern.Player_no;
      OldKey := GetKey(Playern_no,2);
      AddKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
    end;
    if Empty(First_Name) and not Empty(xplayern.First_Name) then begin
      OldKey := GetKey(Playern_no,1);
      DeleteKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
      First_Name := xplayern.First_Name;
      OldKey := GetKey(Playern_no,1);
      AddKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
    end;
    PutARec(Playern_no);
  end;
end;

label
     abort;
begin
//  TournTables := 0;
//  if (Length(Sanct) < 1) and not dofill then exit;
  GroupInit;
  OpenDBFile(Playern_no);
  Filno := Playern_no;
  TotalRecs := UsedRecs(DatF[Filno]^);
  XPrepend := CFG.PLpath;
  {if Length(Sanct) > 0 then begin
    MPList := TStringlist.Create;
    MPList.Clear;
    MPList.Sorted := true;
  end;
  TotMPs := 0;}
  with xplayern do begin
    Paid_Thru := CharStrS(' ',6);
    ACBL_Update_Date := CharStrS(' ',8);
    District_no := '  ';
    Country := '  ';
    Street1 := CharStrS(' ',30);
    email := CharStrS(' ',40);
    xSpare := CharStrS(' ',playern_sp);
    Fee_type := ' ';
  end;
  RRead := 0;
  Modified := 0;
//  if dofill then begin
    ProgressBarInit('Data Base in '+Prepend,'BackFilling',false);
    Flush_DF := false;
    Flush_IF := false;
    xOpenDBFile(Playern_no);
  {end
  else ProgressBarInit('Data Base in '+Prepend,'MP leaders',false);}
  ClearKey(IdxKey[Playern_no,1]^);
  NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  while ok do begin
    GetARec(Playern_no);
    Inc(RRead);
    if (RRead mod 10) = 0 then ProgressBar(RRead,TotalRecs);
//    if dofill then begin
      TempKey := Player_key(Playern.Player_no);
      isplayer := Player_check(Playern.Player_no,pnPoundACBL);
      if isplayer then FindKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],TempKey)
      else ok := false;
      if not ok then with Playern do begin
        Namekey := Uppercase(Last_name+Copy(First_name,1,8));
        TempKey := Namekey;
        SearchKey(xIdxKey[Playern_no,1]^,xRecno[Playern_no],Tempkey);
        while ok and CompareKey(Tempkey,NameKey) do begin
          xGetARec(Playern_no);
          ok := (Uppercase(City) = Uppercase(xPlayern.City))
            and (Uppercase(First_name) = Uppercase(xPlayern.First_name))
            and (Empty(State) or (State = xPlayern.State));
          if ok and isplayer then
            ok := not Player_check(xplayern.Player_no,pnPoundACBL);
          if ok then Break;
          ok := true;
          NextKey(xIdxKey[Playern_no,1]^,xRecno[Playern_no],Tempkey);
        end;
        ok := ok and CompareKey(Tempkey,Namekey);
      end;
      if ok then begin
        xGetARec(Playern_no);
        UpdatePl;
        CheckCat;
      end;
//    end;
    {if Length(Sanct) > 0 then with Playern do if Key2Num(Tourn_pts) > 0 then begin
      MPList.Add(Real2StrL(Key2Num(Tourn_pts)/100,7,2)+' '+Trim(first_name)
        +' '+Trim(last_name)+', '+Trim(City)+' '+State);
      Inc(TotMPs,Key2Num(Tourn_pts));
    end;}
    NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  end;
abort:
  ProgressBarDone(false);
//  if dofill then begin
    GroupWrite;
    XCloseDBFile(Playern_no);
//  end;
  CloseDBFile(Playern_no);
  {if Length(Sanct) > 0 then begin
    fXPrepend := Tempdir;
    if FileExists(fXPrepend+fDBNames[FTinfo_no,0]) then begin
      fOpenDBFile(Ftinfo_no);
      ClearKey(fIdxKey[Ftinfo_no,1]^);
      NextKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],FndKey);
      while ok do begin
        fGetARec(Ftinfo_no);
        TournTables := TournTables+Valu(Ftinfo.Total_Tables);
        NextKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],FndKey);
      end;
      ok := true;
      fCloseDBFile(Ftinfo_no);
    end;
    AssignFile(mpfile,TempDir+Sanct+MPftype);
    ReWrite(mpfile);
    WriteLn(mpfile,'Total masterpoints: ',Real2Strl(TotMPs/100,5,2),
      ' earned by ',Long2StrL(MPList.Count),' players.');
    if TournTables > 1 then WriteLn(mpfile,'Total attendance: ',
      Real2StrL(TournTables,4,0),' tables.');
    WriteLn(mpfile);
    mppos := 0;
    for j := MPList.Count-1 downto 0 do begin
      Inc(mppos);
      WriteLn(mpfile,numcvt(mppos,4,false),MPList[j]);
    end;
    CloseFile(mpfile);
    MPList.Destroy;
  end;}
end;

procedure DoMPRace(const Sanct: String);
var
   MPList       : TStringList;
   TotMPs       : integer;
   mpfile       : TextFile;
   AttendKey    : KeyStr;
   TempKey      : KeyStr;
   HashKey      : KeyStr;
   Sanctx       : String[10];
   PlayerPts    : longint;
   TournTables       : real;
   mppos        : integer;
   j            : integer;
begin
  if Empty(Sanct) then exit;
  TournTables := 0;
  Sanctx := PadS(Sanct,10);
  OpenDBFile(Playern_no);
  OpenDBFile(Attend_no);
  OpenDBFile(TournEv_no);
  Filno := Playern_no;
  TotalRecs := UsedRecs(DatF[Filno]^);
  MPList := TStringlist.Create;
  MPList.Clear;
  MPList.Sorted := true;
  TotMPs := 0;
  RRead := 0;
  ProgressBarInit('Data Base in '+Prepend,'MP leaders',false);
  ClearKey(IdxKey[Playern_no,1]^);
  NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  while ok do begin
    GetARec(Playern_no);
    PlayerPts := 0;
    Inc(RRead);
    if (RRead mod 10) = 0 then ProgressBar(RRead,TotalRecs);
    AttendKey := GetKey(Filno,3);
    TempKey := AttendKey;
    SearchKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
    while ok and CompareKey(TempKey,AttendKey) do with Attend do begin
      GetARec(Attend_no);
      HashKey := Hash;
      FindKey(IdxKey[TournEv_no,3]^,Recno[TournEv_no],HashKey);
      if ok then begin
        GetARec(TournEv_no);
        if (TournEv.SANCTION_NO = Sanctx) and (Valu(MPS) > 0) then
          Inc(PlayerPts,Round(Valu(MPS)*100));
      end;
      ok := true;
      NextKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
    end;
    if PlayerPts > 0 then with Playern do begin
      MPList.Add(Real2StrL(PlayerPts/100,7,2)+' '+Trim(first_name)
        +' '+Trim(last_name)+', '+Trim(City)+' '+State);
      Inc(TotMPs,PlayerPts);
    end;
    ok := true;
    NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  end;
  ProgressBarDone(false);
  CloseDBFile(Playern_no);
  CloseDBFile(Attend_no);
  CloseDBFile(TournEv_no);
  fXPrepend := Tempdir;
  if FileExists(fXPrepend+fDBNames[FTinfo_no,0]) then begin
    fOpenDBFile(Ftinfo_no);
    HashKey := Sanctx;
    FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],HashKey);
    if ok then begin
      fGetARec(Ftinfo_no);
      TournTables := Valu(Ftinfo.Total_Tables);
    end;
    ok := true;
    fCloseDBFile(Ftinfo_no);
  end;
  AssignFile(mpfile,TempDir+SancDigits(Trim(Sanct))+MPftype);
  ReWrite(mpfile);
  WriteLn(mpfile,'Total masterpoints: ',Real2Strl(TotMPs/100,5,2),
    ' earned by ',Long2StrL(MPList.Count),' players.');
  if TournTables > 1 then WriteLn(mpfile,'Total attendance: ',
    Real2StrL(TournTables,4,0),' tables.');
  WriteLn(mpfile);
  mppos := 0;
  for j := MPList.Count-1 downto 0 do begin
    Inc(mppos);
    WriteLn(mpfile,numcvt(mppos,4,false),MPList[j]);
  end;
  CloseFile(mpfile);
  MPList.Destroy;
end;

Procedure Transfile(const Fno: word; const Source,Target,HashLoc,SancLoc: Pointer;
          const Len: word; const BuildHash: boolean);
var
   FndKey       : KeyStr;
   SaveKey      : Array[1..MaxKeyno] of KeyStr;
   Found        : boolean;
   j            : word;
   Sanc         : String[10];
begin
  OpenDBFile(Fno);
  xOpenDBFile(Fno);
  Filno := Fno;
  ClearKey(xIdxKey[Fno,1]^);
  repeat
    NextKey(xIdxKey[Fno,1]^,xRecno[Fno],FndKey);
    if ok then begin
      xGetARec(Fno);
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      if Assigned(SancLoc) then begin
        Found := false;
        Move(SancLoc^,Sanc,SizeOf(Sanc));
        for j := 1 to Sanctions do
          if CompareKey(Sanc,ExtractWordL(1,SancTable[j],',')) then begin
          Found := oktoprocess[j];
          Break;
        end;
        if not Found then continue;
      end;
      if BuildHash then begin
        Inc(HashTableSize);
        HashTable^[HashTableSize,1] := xGetKey(Fno,3);
      end;
      FindKey(IdxKey[Fno,1]^,Recno[Fno],FndKey);
      Found := ok;
      if ok then begin
        GetARec(Fno);
        if BuildHash then HashTable^[HashTableSize,2] := GetKey(Fno,3);
        for j := 1 to MaxKeyno do if KeyLen[Fno,j] > 0 then
          SaveKey[j] := GetKey(Fno,j);
      end;
      Move(Source^,Target^,Len);
      if not Found then begin
        ok := true;
        Add_Record;
        if BuildHash then HashTable^[HashTableSize,2] := GetKey(Fno,3);
        Inc(Added);
      end
      else begin
        if Assigned(HashLoc) then Move(HashTable^[HashTableSize,2],HashLoc^,4);
        for j := 1 to MaxKeyno do if KeyLen[Fno,j] > 0 then begin
          FndKey := GetKey(Fno,j);
          if SaveKey[j] <> FndKey then begin
            DeleteKey(IdxKey[Fno,j]^,Recno[Fno],SaveKey[j]);
            AddKey(IdxKey[Fno,j]^,Recno[Fno],FndKey);
          end;
        end;
        PutARec(Fno);
        Inc(Modified);
      end;
    end;
  until not ok;
  CloseDBFile(Fno);
  xCloseDBFile(Fno);
end;

procedure DeleteRecords(const Fno: word);
var
   j    : word;
   TempKey,
   FndKey  : KeyStr;
begin
  fFilno := Fno;
  for j := 1 to SancTableSize do begin
    FndKey := fSancTable^[j];
    TempKey := FndKey;
    SearchKey(fIdxKey[Fno,1]^,fRecno[Fno],TempKey);
    while ok and CompareKey(FndKey,TempKey) do begin
      fGetARec(Fno);
      fDelARec(Fno);
      TempKey := FndKey;
      SearchKey(fIdxKey[Fno,1]^,fRecno[Fno],TempKey);
    end;
    ok := true;
  end;
end;

Procedure fTransfile(const Fno: word; const Source,Target,HashLoc,SancLoc: Pointer;
          const Len: word; const BuildHash: byte);
          {0 no action, 1= build hash, 2=build sanction table}
var
   FndKey       : KeyStr;
   SaveKey      : Array[1..MaxKeyno] of KeyStr;
   Found        : boolean;
   j            : word;
   Sanc         : String[10];
begin
  fOpenDBFile(Fno);
  fxOpenDBFile(Fno);
  fFilno := Fno;
  if BuildHash <> 2 then DeleteRecords(Fno);
  ClearKey(fxIdxKey[Fno,1]^);
  repeat
    NextKey(fxIdxKey[Fno,1]^,fxRecno[Fno],FndKey);
    if ok then begin
      fxGetARec(Fno);
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      if Assigned(SancLoc) then begin
        Found := false;
        Move(SancLoc^,Sanc,SizeOf(Sanc));
        for j := 1 to Sanctions do
          if CompareKey(Sanc,ExtractWordL(1,SancTable[j],',')) then begin
          Found := oktoprocess[j];
          Break;
        end;
        if not Found then continue;
      end;
      if BuildHash <> 2 then begin
        Found := false;
        for j := 1 to SancTableSize do
          if CompareKey(fSancTable^[j],FndKey) then begin
          Found := true;
          Break;
        end;
        if not Found then continue;
      end;
      case BuildHash of
        1: begin
          Inc(HashTableSize);
          HashTable^[HashTableSize,1] := fxGetKey(Fno,2);
        end;
        2: begin
          Inc(SancTableSize);
          fSancTable^[SancTableSize] := fxGetKey(Fno,1);
        end;
      end;
      Found := false;
      if BuildHash = 2 then begin
        FindKey(fIdxKey[Fno,1]^,fRecno[Fno],FndKey);
        Found := ok;
        if ok then begin
          fGetARec(Fno);
          for j := 1 to fMaxKeyno do if fKeyLen[Fno,j] > 0 then
            SaveKey[j] := fGetKey(Fno,j);
        end;
      end;
      Move(Source^,Target^,Len);
      if not Found then begin
        ok := true;
        fAdd_Record;
        if BuildHash = 1 then HashTable^[HashTableSize,2] := fGetKey(Fno,2);
        Inc(Added);
      end
      else begin
        for j := 1 to fMaxKeyno do if fKeyLen[Fno,j] > 0 then begin
          FndKey := fGetKey(Fno,j);
          if SaveKey[j] <> FndKey then begin
            DeleteKey(fIdxKey[Fno,j]^,fRecno[Fno],SaveKey[j]);
            AddKey(fIdxKey[Fno,j]^,fRecno[Fno],FndKey);
          end;
        end;
        fPutARec(Fno);
        Inc(Modified);
      end;
    end;
  until not ok;
  fCloseDBFile(Fno);
  fxCloseDBFile(Fno);
end;

procedure MergeDistDB;

procedure UpdatePl;
var
   OldKey       : keystr;
begin
  with playern do begin
    Inc(Modified);
    if IdxDate(xplayern.Acbl_Date) > IdxDate(Acbl_Date) then
      Acbl_Date := xplayern.Acbl_date;
    if xplayern.Acbl_rank > Acbl_rank then Acbl_rank := xplayern.Acbl_rank;
    if IdxDate(xplayern.ACBL_Update_Date) > IdxDate(ACBL_Update_Date) then
      ACBL_Update_Date := xplayern.ACBL_Update_Date;
    if IdxDate(xplayern.Paid_Thru) > IdxDate(Paid_Thru) then
      Paid_Thru := xplayern.Paid_Thru;
    if Empty(Cat_A) then Cat_a := xplayern.Cat_a;
    if Empty(Cat_b) then Cat_b := xplayern.Cat_b;
    City := xplayern.City;
    State := xplayern.State;
    Street1 := xplayern.Street1;
    Street2 := xplayern.Street2;
    Email := xplayern.Email;
    Fee_type := xplayern.Fee_type;
    zip := xplayern.zip;
    if not Empty(Street1) then Mail_Code := 'M';
    Country := xplayern.Country;
    Gender := xplayern.Gender;
    if IdxDate(xplayern.Local_date) > IdxDate(Local_date) then
      Local_Date := xplayern.Local_date;
    Phone := xplayern.Phone;
    Unit_no := xplayern.Unit_no;
    District_no := xplayern.District_no;
    SetPlayerPoints(GetPlayerPoints(xplayern,ppElig),ppElig,true);
    SetPlayerPoints(GetPlayerPoints(xplayern,ppTotal),ppTotal,true);
    if Player_check(xplayern.Player_no,pnPoundACBL)
      and not Player_check(Player_no,pnPoundACBL) then begin
      OldKey := GetKey(Playern_no,2);
      DeleteKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
      Player_no := xplayern.Player_no;
      OldKey := GetKey(Playern_no,2);
      AddKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
    end;
    if Empty(First_Name) and not Empty(xplayern.First_Name) then begin
      OldKey := GetKey(Playern_no,1);
      DeleteKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
      First_Name := xplayern.First_Name;
      OldKey := GetKey(Playern_no,1);
      AddKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
    end;
    PutARec(Playern_no);
  end;
end;

var
   LockFile     : file;
   OldFileMode  : byte;
   ic           : word;
   j            : word;
   FndKey       : KeyStr;
   TempKey      : KeyStr;
   NameKey      : KeyStr;
   HashKey      : KeyStr;
   NewPlayer    : boolean;
   HashKey2     : KeyStr;
   Plchanged    : boolean;
   LastDeleted  : KeyStr;
   DelCount     : smallInt;
   isplayer     : boolean;
   tyear        : String;
begin
  path := TournDBloc+DistNo;
  CreateDir(path);
  if not IsDirectory(path) then exit;
  tyear := Copy(Sanction,1,2);
  if (Pos(tyear[1],'0123456789') < 1) or (Pos(tyear[2],'0123456789') < 1) then
    exit;
  path := TournDBloc+DistNo+'\'+tyear;
  CreateDir(path);
  if not IsDirectory(path) then exit;
  path := fixpath(path);
  OldFileMode := FileMode;
  FileMode := $11; {write only, denyall}
  AssignFile(LockFile,path+DBLockFileName);
  {$I-} ReWrite(LockFile); {$I+}
  FileMode := OldFileMode;
  if IOResult <> 0 then exit;
  GroupInit;
  RRead := 0;
  Added := 0;
  Modified := 0;
  CFG.DBpath := path;
  XPrepend := tempdir;
  fXPrepend := XPrepend;
  xOpenDBFile(Playern_no);
  TotalRecs := UsedRecs(xDatF[Playern_no]^);
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[PlayerGr_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[Attend_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[OArank_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[Tourn_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[TournEv_no,0],dbUsedRecs));
  if FileExists(XPrepend+DBNames[ClubDef_no,0]) then
    Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[ClubDef_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[FTinfo_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Ftinfo_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[Fwksheet_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Fwksheet_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[Finvoice_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Finvoice_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[Finvitem_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Finvitem_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[FBalance_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[FBalance_no,0],dbUsedRecs));
  if FileExists(fXPrepend+fDBNames[FOfs_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[FOfs_no,0],dbUsedRecs));
  ProgressBarInit('From '+XPrepend+' to district '+DistNo,'Importing',false);
  if not Assigned(HashTable) then New(HashTable);
  HashTableSize := 0;
  Flush_DF := false;
  Flush_IF := false;
  if FileExists(XPrepend+DBNames[ClubDef_no,0]) then
    TransFile(ClubDef_no,@xClubDef,@ClubDef,nil,nil,SizeOf(ClubDef),false);
  TransFile(Tourn_no,@xTourn,@Tourn,nil,@xTourn.Sanction_no,SizeOf(Tourn),false);
  TransFile(TournEv_no,@xTournEv,@TournEv,@TournEv.Hash,@xTournEv.Sanction_no,
    SizeOf(TournEv),true);
  OpenDBFile(Playern_no);
  xOpenDBFile(Playern_no);
  OpenDBFile(PlayerGr_no);
  xOpenDBFile(PlayerGr_no);
  OpenDBFile(Attend_no);
  xOpenDBFile(Attend_no);
  OpenDBFile(OARank_no);
  xOpenDBFile(OARank_no);
  ClearKey(xIdxKey[Playern_no,2]^);
  NextKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],FndKey);
  while ok do begin
    Inc(RRead);
    ProgressBar(RRead,TotalRecs);
    xGetARec(Playern_no);
    isplayer := Player_check(xplayern.Player_no,pnPoundACBL);
    if isplayer then FindKey(IdxKey[Playern_no,2]^,Recno[Playern_no],FndKey)
    else ok := false;
    if not ok then with xPlayern do begin
      Namekey := Uppercase(Last_name+Copy(First_name,1,8));
      TempKey := Namekey;
      SearchKey(IdxKey[Playern_no,1]^,Recno[Playern_no],Tempkey);
      while ok and CompareKey(Tempkey,NameKey) do begin
        GetARec(Playern_no);
        ok := (Uppercase(City) = Uppercase(Playern.City))
          and (Uppercase(First_name) = Uppercase(Playern.First_name))
          and (Empty(State) or (State = Playern.State));
        if ok and isplayer then
          ok := not Player_check(playern.Player_no,pnPoundACBL);
        if ok then Break;
        ok := true;
        NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],Tempkey);
      end;
      ok := ok and CompareKey(Tempkey,Namekey);
    end;
    if ok then begin
      NewPlayer := false;
      GetARec(Playern_no);
      UpdatePl;
    end
    else begin
      NewPlayer := true;
      Inc(Added);
      ok := true;
      playern := xplayern;
      Filno := Playern_no;
      Add_Record;
      SetPlayerPoints(GetPlayerPoints(xplayern,ppElig),ppElig,false);
      SetPlayerPoints(GetPlayerPoints(xplayern,ppTotal),ppTotal,false);
      PutARec(Playern_no);
    end;
    CheckCat;
    FndKey := xPlayern.Record_no;
    TempKey := FndKey;
    SearchKey(xIdxKey[Playergr_no,1]^,xRecno[Playergr_no],TempKey);
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      xGetARec(PlayerGr_no);
      TempKey := Playern.Record_no+xPlayerGr.Group;
      FindKey(IdxKey[Playergr_no,1]^,Recno[Playergr_no],TempKey);
      if ok then with Playergr do begin
        Inc(Modified);
        GetARec(Playergr_no);
        if empty(Group_no) then Group_no := xplayergr.Group_no;
        PutARec(Playergr_no);
      end
      else begin
        Inc(Added);
        ok := true;
        playergr := xplayergr;
        Filno := Playergr_no;
        Add_Record;
      end;
      GroupAdd(Playergr.Group);
      NextKey(xIdxKey[Playergr_no,1]^,xRecno[Playergr_no],TempKey);
    end;
    plchanged := false;
    TempKey := FndKey;
    SearchKey(xIdxKey[Attend_no,1]^,xRecno[Attend_no],TempKey);
    Filno := Attend_no;
    LastDeleted := '   ';
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      DelCount := 0;
      xGetARec(Attend_no);
      HashKey := '';
      for j := 1 to HashTableSize do if xAttend.Hash = HashTable^[j,1] then
        begin
        HashKey := HashTable^[j,2];
        Break;
      end;
      if Length(HashKey) > 0 then begin
        if LastDeleted <> HashKey then begin
          HashKey2 := Playern.Record_no+HashKey;
          TempKey := HashKey2;
          SearchKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
          while ok and CompareKey(HashKey2,TempKey) do begin
            GetARec(Attend_no);
            MPAdjust(-RRound(Valu(Attend.MPS)*100));
            DelARec(Attend_no);
            Inc(DelCount);
            Inc(Modified);
            plchanged := true;
            TempKey := HashKey2;
            SearchKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
          end;
          LastDeleted := HashKey;
        end;
        Attend := xAttend;
        Attend.Hash := HashKey;
        Dec(DelCount);
        if DelCount < 0 then Inc(Added);
        ok := true;
        Add_Record;
        MPAdjust(RRound(Valu(Attend.MPS)*100));
        plchanged := true;
      end;
      NextKey(xIdxKey[Attend_no,1]^,xRecno[Attend_no],TempKey);
    end;
    TempKey := FndKey;
    SearchKey(xIdxKey[OARank_no,1]^,xRecno[OARank_no],TempKey);
    Filno := OARank_no;
    LastDeleted := '   ';
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      DelCount := 0;
      xGetARec(OARank_no);
      HashKey := '';
      for j := 1 to HashTableSize do if xOARank.Hash = HashTable^[j,1] then
        begin
        HashKey := HashTable^[j,2];
        Break;
      end;
      if Length(HashKey) > 0 then begin
        if LastDeleted <> HashKey then begin
          HashKey2 := Playern.Record_no+HashKey;
          TempKey := HashKey2;
          SearchKey(IdxKey[OARank_no,1]^,Recno[OARank_no],TempKey);
          while ok and CompareKey(HashKey2,TempKey) do begin
            GetARec(OARank_no);
            DelARec(OARank_no);
            Inc(DelCount);
            Inc(Modified);
            TempKey := HashKey2;
            SearchKey(IdxKey[OArank_no,1]^,Recno[OARank_no],TempKey);
          end;
          LastDeleted := HashKey;
        end;
        OARank := xOARank;
        OArank.Hash := HashKey;
        Dec(DelCount);
        if DelCount < 0 then Inc(Added);
        ok := true;
        Add_Record;
      end;
      NextKey(xIdxKey[OARank_no,1]^,xRecno[OARank_no],TempKey);
    end;
    if plchanged then PutARec(Playern_no);
    NextKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],FndKey);
  end;
  CloseFiles;
  xCloseFiles;
  GroupWrite;
  if FileExists(fXPrepend+fDBNames[FTinfo_no,0]) then begin
    if not Assigned(fSancTable) then New(fSancTable);
    HashTableSize := 0;
    SancTableSize := 0;
    fTransFile(FTinfo_no,@xFtinfo,@Ftinfo,nil,@xFTinfo.Sanction_no,
      SizeOf(Ftinfo),2);
    if FileExists(fXPrepend+fDBNames[Fwksheet_no,0]) then
      fTransFile(Fwksheet_no,@xFwksheet,@Fwksheet,nil,@xFwksheet.Sanction_no,
      SizeOf(Fwksheet),0);
    if FileExists(fXPrepend+fDBNames[Finvoice_no,0]) then begin
      fOpenDBFile(Finvitem_no);
      fTransFile(Finvoice_no,@xFinvoice,@Finvoice,@Finvoice.Fhash,
        @xFinvoice.Sanction_no,SizeOf(Finvoice),1);
      fCloseDBFile(Finvitem_no);
    end;
    if FileExists(fXPrepend+fDBNames[FBalance_no,0]) then
      fTransFile(FBalance_no,@xFBalance,@FBalance,nil,@xFBalance.Sanction_no,
      SizeOf(FBalance),0);
    if FileExists(fXPrepend+fDBNames[FOfs_no,0]) then
      fTransFile(FOfs_no,@xFOfs,@FOfs,nil,@xFOfs.Sanction_no,SizeOf(FOfs),0);
    if FileExists(fXPrepend+fDBNames[Finvitem_no,0]) then begin
      fOpenDBFile(Finvitem_no);
      fxOpenDBFile(Finvitem_no);
      fFilno := Finvitem_no;
      fxFilno := fFilno;
      ClearKey(fxIdxKey[fxFilno,1]^);
      repeat
        NextKey(fxIdxKey[fxFilno,1]^,fxRecno[fxFilno],FndKey);
        if ok then begin
          Inc(RRead);
          ProgressBar(RRead,TotalRecs);
          fxGetARec(fxFilno);
          HashKey := '';
          for j := 1 to HashTableSize do
            if xFinvitem.FHash = HashTable^[j,1] then begin
            HashKey := HashTable^[j,2];
            Break;
          end;
          if Length(HashKey) > 0 then begin
            Inc(Added);
            Finvitem := xFinvitem;
            Finvitem.FHash := HashKey;
            ok := true;
            fAdd_Record;
          end;
        end;
      until not ok;
      fCloseDBFile(Finvitem_no);
      fxCloseDBFile(Finvitem_no);
    end;
  end;
  {$I-} CloseFile(LockFile);
  if IOResult = 0 then;
  Erase(LockFile); {$I+}
  if IOResult = 0 then;
  CFG.DBpath := tempdir;
  if Assigned(HashTable) then Dispose(HashTable);
  HashTable := nil;
  ProgressBarDone(false);
end;

procedure SetDistNumber;
begin
  DistNo := '00';
  OktoMerge := false;
  OpenDBFile(Tourn_no);
  Filno := Tourn_no;
  ClearKey(IdxKey[Tourn_no,1]^);
  NextKey(IdxKey[Tourn_no,1]^,Recno[Tourn_no],FndKey);
  if ok and not CompareKey(FndKey,NABCSancPref) then begin
    GetARec(Tourn_no);
    DistNo := Tourn.District_no;
    OktoMerge := ValidDistrict(DistNo) or (DistNo = '00');
  end;
  CloseDBFile(Tourn_no);
end;

const
     CopyText   : Array[1..2] of Pchar =
                ('1 Copy masterpoint and financial files to 400',
                 '2 Copy only masterpoint files to 400');

var
   Tempst        : String;
   FirstSanc     : String;

label
     gf_done,
     gf_again,
     game_again;
begin
  esc := false;
  ReadSancLocs(Full);
  ValidFolder(ReportPath);
  if canceled then exit;
  CFG.tournament := true;
  CFG.DBpath := tempdir;
  RepPath := ReportPath;
  repeat
    DBDelName := '';
    GameDelName := '';
    if Full then begin
      GetReportPath(true);
      if esc then exit;
      errcode := GetFileName(RepPath,
        'Data base archives ('+DBMask+lzhext+')|'+DBMask+lzhext,GFname,
        'Select tournament to process','','',false,false);
      if not errcode then continue;
      GFname := Uppercase(GFname);
      ArcName := JustFileNameL(GFname);
      RepPath := fixpath(JustPathNameL(GFname));
      if ArcName[1] <> 'D' then begin
        GFname := RepPath+'D'+PadChL(JustFileNameL(GFname),'X',7);
        ArcName := JustFileNameL(GFname);
      end;
      DBDelName := GFname;
      DotLoc := Pos('.',ArcName);
      Sanction := copy(ArcName,2,DotLoc-2);
      FileSanc := Sanction;
      if not FileExists(GFname) then begin
        ErrBox('Data base archive '+GFname+' not found.'
          +'Tournament '+Sanction+' not processed.',MC,0);
        continue;
      end;
      tempst := RepPath+Copy(ArcName,2,15);
      if not FileExists(tempst) then begin
        ErrBox('Game file archive '+tempst+' not found.'
          +'Tournament '+Sanction+' not processed.',MC,0);
        continue;
      end;
      DelTempDirFiles;
      RunLHA('e',GFName,tempdir,' Extracting data base files ');
//      if not YesNoBox(esc,true,'Data base extrated',0,MC,nil) then continue;
      CFG.DefDBVersion := 0;
      RepairDataBase(false,'');
//      if not YesNoBox(esc,true,'Data base repaired',0,MC,nil) then continue;
      CheckSerial := false;
    end
    else with CFG do begin
      DeleteFile(DBpath+'LASTGAME');
      DeleteFile(DBpath+'ACBLOPEN');
      DeleteFile(DBpath+'CS.CFG');
    end;
    OpenDBFile(Tourn_no);
//    if not YesNoBox(esc,true,'Data base opened',0,MC,nil) then continue;
    Filno := Tourn_no;
    ClearKey(IdxKey[Filno,1]^);
    NextKey(IdxKey[Filno,1]^,Recno[Filno],TempKey);
    if not ok then begin
      ErrBox('No tournaments found in the data base.',MC,0);
      continue;
    end;
    Sanctions := 0;
    while ok do with Tourn do begin
      Inc(Sanctions);
      GetARec(Filno);
      SancTable[Sanctions] := Sanction_no+', '+Trim(Tourn_City)+' '
        +Trim(Tourn_dates)+' '+Trim(Tourn_Name);
      oktoprocess[Sanctions] := Pos(FileSanc,Sanction_no) > 0;
      NextKey(IdxKey[Filno,1]^,Recno[Filno],TempKey);
    end;
    CloseDBFile(Tourn_no);
    if Full then BackFillDB{(SancDigits(Trim(ExtractWordL(1,SancTable[1],','))))};
//    if not YesNoBox(esc,true,'Data base backfilled',0,MC,nil) then continue;
    if Sanctions < 3 then FillChar(oktoprocess,Sanctions,true);
    Anyok := 0;
    if Sanctions > 1 then begin
      PickTStrings := PickLStrings;
      if CPickchoice(Sanctions,1,
        Long2StrL(Sanctions)+' tournaments found in '+ArcName,
        'Select tournaments to process',@SancTable,GetSelect,0) < 1 then
        Anyok := 0;
    end
    else begin
      oktoprocess[1] := YesNoBox(esc,true,'Ok to process '+SancTable[1],0,MC,nil);
      esc := false;
      if oktoprocess[1] then Anyok := 1;
    end;
    if Anyok < 1 then continue;
    SetDistNumber;
    dorep := false;
    dofin := false;
    if not esc and Full and RepDestValid then begin
      fXPrepend := tempdir;
      dofin := FileExists(fXPrepend+fDBNames[FTinfo_no,0]);
      dofin := dofin and fOpenDBFile(Ftinfo_no)
        and (UsedRecs(fDatF[Ftinfo_no]^) > 0);
      if dofin then begin
        j := PickChoice(2,1,'report copy option',@CopyText,MC,false,0);
        esc := j < 1;
        dofin := j in [1,3];
        fCloseDBFile(FInvitem_no);
      end;
      dorep := true;
    end;
    if esc then continue;
    CurSanction := 0;
    while CurSanction < Sanctions do begin
      Inc(CurSanction);
      if not oktoprocess[CurSanction] then continue;
      Sanction := SancDigits(Trim(ExtractWordL(1,SancTable[CurSanction],',')));
      if Full and dorep then begin
        FindErr := FindFirst(tempdir+Sanction+RepSuffix,faArchive,Search_Rec);
        FindClose(Search_Rec);
        if FindErr <> 0 then begin
          ErrBox('Report file '+Sanction+RepSuffix+' not found.'#13
            +'Tournament '+Sanction+' not processed.',MC,0);
          oktoprocess[CurSanction] := false;
        end;
      end;
      if Full and dofin and oktoprocess[CurSanction] then begin
        TempKey := ExtractWordL(1,SancTable[CurSanction],',');
        FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],TempKey);
        if not ok then begin
          ErrBox('Financial information for '+Sanction+' not found.'#13
            +'Tournament '+Sanction+' not processed.',MC,0);
          oktoprocess[CurSanction] := false;
        end;
      end;
    end;
    fCloseFiles;
    CurSanction := 0;
    FirstSanc := '';
    while CurSanction < Sanctions do begin
      Inc(CurSanction);
      if not oktoprocess[CurSanction] then continue;
      Sanction := SancDigits(Trim(ExtractWordL(1,SancTable[CurSanction],',')));
      if Empty(FirstSanc) then FirstSanc := Sanction;
      if dorep and Full then PCopyfile(tempdir+Sanction+RepSuffix,
        RepDestLocation+Sanction+RepSuffix,false,esc,false);
      if dofin and Full then MakeFinReport;
      if PPDestValid and Full then begin
        FindErr := FindFirst(tempdir+Sanction+PPSuffix,faArchive,Search_Rec);
        FindClose(Search_Rec);
        if FindErr = 0 then PCopyfile(tempdir+Sanction+PPSuffix,
          PPDestLocation+Sanction+PPSuffix,false,esc,false);
      end;
      DoMPRace(ExtractWordL(1,SancTable[CurSanction],','));
      if HtmDestValid then begin
        FindErr := FindFirst(tempdir+Sanction+HtmSuffix,faArchive,Search_Rec);
        FindClose(Search_Rec);
        if (FindErr = 0) and (full
          or YesNoBox(esc,false,'Copy '+Sanction+HtmSuffix,0,MC,nil)) then
          FixHtmFile(tempdir+Sanction+HtmSuffix,
          HtmDestLocation+Sanction+HtmSuffix,ScoreDir,tempdir+Sanction+MPftype);
      end;
    end;
    if not Full then begin
      AnyGameFiles := true;
      goto gf_done;
    end;
    CurSanction := 0;
    AnyGameFiles := false;
gf_again:
    if CurSanction < Sanctions then Inc(CurSanction);
    Sanction := SancDigits(Trim(ExtractWordL(1,SancTable[CurSanction],',')));
    GFname := RepPath+FileSanc+lzhext;
    if not FileExists(GFname) then begin
      ErrBox('Game file archive '+GFname+' not found.'#13
        +'Tournament '+Sanction+' not processed.',MC,0);
      oktoprocess[CurSanction] := false;
    end;
    GameDelName := UpperCase(GFname);
    if oktoprocess[CurSanction] then begin
      RunLHA('e',GFName,tempdir,' Extracting game files ');
      AnyGameFiles := true;
      if (PathChoice in [1,2]) and YesNoBox(esc,GFName[Length(GFName)] <> 'H',
        'Any more game file archives to process for '+Sanction,0,MC,nil) then begin
        if not EscBox('Insert the next diskette into drive.'#13
          +'Click OK to continue.',MC) then goto gf_again;
      end;
    end;
gf_done:
    if AnyGameFiles then esc := false;
    if esc then continue;
    CurSanction := 0;
    while CurSanction < Sanctions do begin
      Inc(CurSanction);
      if not oktoprocess[CurSanction] then continue;
      Sanction := SancDigits(Trim(ExtractWordL(1,SancTable[CurSanction],',')));
//      DBFiles := 0;
      SpaceNeeded := 0;
      FindErr := FindFirst(tempdir+'*.*',faArchive,Search_Rec);
      while FindErr = 0 do with Search_Rec do begin
//        Inc(DBfiles);
//        DBFileList[DBFiles] := Name;
        Inc(SpaceNeeded,Size);
        FindErr := FindNext(Search_Rec);
      end;
      FindClose(Search_Rec);
      RunLHA('a',tempdir+SancLZH+Sanction,tempdir,' Packing files for '+Sanction);
      AssignFile(F,tempdir+SancLZH+Sanction+lzhext);
      {$I-} ReSet(F,1);
      SpaceNeeded := FileSize(F);
      CloseFile(F); {$I+}
      if IOResult = 0 then;
      if CompareKey(Sanction,NABCSancPref) then path := TournArchive+NABCSancPref
      else if NumSanc(Sanction) then begin
        {$I-} MkDir(TournArchive+Copy(Sanction,1,2)); {$I+}
        if IOResult = 0 then;
        path := TournArchive+Copy(Sanction,1,2)+'\'+Copy(Sanction,3,2);
      end
      else path := OtherLocation;
      {$I-} MkDir(path); {$I+}
      if IOResult = 0 then;
      path := FixPath(path);
      FindErr := FindFirst(path+SancLZH+Sanction+lzhext,faArchive,Search_Rec);
      FindClose(Search_Rec);
      if FindErr = 0 then j := 5
      else j := 0;
      if Full then begin
        if j = 5 then begin
          if not YesNoBox(esc,false,
            'Tournament sanction '+Sanction+' has already'#13+
            'been processed.  Process again',0,MC,nil) then begin
            OktoMerge := false;
            continue;
          end;
        end
        else if j <> 0 then begin
          ErrBox('Cannot access the Tournament location.',MC,0);
          exit;
        end;
      end;
      path := fixpath(path);
      PCopyFile(tempdir+SancLZH+Sanction+lzhext,path+SancLZH+Sanction+lzhext,
        true,esc,false);
      {$I-} Erase(F); {$I+}
      if IOResult = 0 then;
    end;
    if OktoMerge then MergeDistDB;
    CloseFiles;
    DelTempDirfiles;
    if Full and (PathChoice in [3]) then begin
      if not Empty(DBDelName) then DeleteFile(DBDelName);
      if not Empty(GameDelName) then DeleteFile(GameDelName);
    end;
  until not Full or not YesNoBox(esc,true,'Process another tournament',0,MC,nil);
end;

begin
  AppName := 'Unpack';
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  if Uppercase(ParamStr(1)) = 'UNPACK' then UnpackTourn(true);
  if Uppercase(ParamStr(1)) = 'REPACK' then UnpackTourn(false);
  Application.Terminate;
end;

procedure TUnpackForm.LHAProgress(var PercentageDone: Integer);
begin
  ProgressBar(LhaProgDone+(PercentageDone*LhaThisSize div 100),LhaTotSize);
end;

procedure TUnpackForm.LHAFileDone(const originalFilePath,
  newFilePath: String; dateTime: Integer; mode: TCompLHAProcessMode);
begin
  Inc(LhaProgDone,LhaThisSize);
end;

procedure TUnpackForm.LHAFileStart(const originalFilePath: String;
  var newFilePath: String; dateTime: Integer; mode: TCompLHAProcessMode);
var
   SearchRec       : TSearchRec;
   FindErr         : integer;
begin
  with CompLHA1 do case mode of
    cmLHACompress: begin
      FindFirst(originalFilePath,faArchive,SearchRec);
      LhaThisSize := SearchRec.Size;
      FindClose(SearchRec);
    end;
    cmLHAExpand: LhaThisSize :=
      TCompLHAFileInfo(FileInformation[FileInformationIndex]).FullSize;
  end;
end;

end.
