unit comquot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure comq(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses
    YesNoBoxU,
    pvio,
    util2;

{$R *.DFM}

procedure TForm1.comq(Sender: TObject);
const
     outextent = '.CDF';
var
   F    : textfile;
   Fo   : textfile;
   inpLine : string;
   outline : string;
   Len  : word;
   p    : word;
   flen : word;
   Infilename : string;
   Outfilename : string;
   Lines       : longint;
   infield     : boolean;
   commafield  : boolean;
begin
  if ParamCount < 1 then begin
    ErrBox('Convert comma separated fields into comma delimited fields'#13
      +'Comma (,) converted to ","'#13'USAGE: COM2QUOT <infilename> [outfilename]'#13
      +'  If outfilename not specified, infilename is used with .CDF extent.',MC,0);
    Application.Terminate;
    exit;
  end;
  infilename := ParamStr(1);
  if ParamCount > 1 then outfilename := ParamStr(2)
  else begin
    p := Pos('.',infilename);
    if p < 1 then outfilename := infilename+outextent
    else outfilename := Copy(infilename,1,p-1)+outextent;
  end;
  if outfilename = infilename then begin
    ErrBox('Input file and output file cannot be the same',MC,0);
    Application.Terminate;
    exit;
  end;
  AssignFile(F,infilename);
  {$I-} Reset(F); {$I+}
  if IOResult <> 0 then begin
    ErrBox('File '+infilename+' not found.',MC,0);
    Application.Terminate;
    exit;
  end;
  AssignFile(Fo,outfilename);
  {$I-} ReWrite(Fo); {$I+}
  if IOResult <> 0 then begin
    ErrBox('Cannot open '+outfilename,MC,0);
    CloseFile(F);
    Application.Terminate;
    exit;
  end;
  lines := 0;
  While not Eof(F) do begin
    ReadLn(F,inpLine);
    if not Empty(inpLine) then begin
      Inc(lines);
      infield := false;
      flen := 0;
      commafield := true;
      outline := '"';
      p := 1;
      while p <= Length(inpLine) do begin
        if infield then begin
          if inpline[p] = '"' then begin
            infield := false;
            if flen < 1 then outline := outline+' ';
            flen := 0;
          end
          else Inc(flen);
          outline := outline+inpline[p];
        end
        else begin
          if Copy(inpline,p,2) = ',"' then begin
            infield := true;
            if commafield then begin
              if flen < 1 then outline := outline+' ';
              outline := outline+'"';
            end;
            flen := 0;
            commafield := false;
            Inc(p);
            outline := outline+',"';
          end
          else if not infield and (inpline[p] = ',') then begin
            if commafield then begin
              if flen < 1 then outline := outline+' ';
              outline := outline+'"';
            end;
            outline := outline+',"';
            commafield := true;
            flen := 0;
          end
          else if inpline[p] = '"' then begin
            if commafield then begin
              outline := outline+'""';
              Inc(flen);
            end
            else begin
              if infield then begin
                if flen < 1 then outline := outline+' ';
                flen := 0;
              end;
              infield := not infield;
            end;
            outline := outline+'"';
          end
          else begin
            outline := outline+inpline[p];
            Inc(flen);
          end;
        end;
        Inc(p);
      end;
      if flen < 1 then outline := outline+' ';
      outline := outline+'"';
      WriteLn(Fo,outline);
    end;
  end;
  CloseFile(Fo);
  CloseFile(F);
  Application.Terminate;
  {WriteLn;
  WriteLn(lines,' lines converted from ',infilename,' to ',outfilename);
  WriteLn;}
end;

end.
