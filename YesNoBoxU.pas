unit YesNoBoxU;
{$define help}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, pvio;

type
  TYesNoBoxForm = class(TForm)
    YesBut: TButton;
    CanButton: TButton;
    NoBut: TButton;
    Label1: TLabel;
    HelpBut: TButton;
    procedure Acceptit(Sender: TObject);
    procedure CancelIt(Sender: TObject);
    procedure NoClick(Sender: TObject);
    procedure Dohelp(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
     YesNoBoxYLoc : word = 50;
     YesNoBoxXLoc : word = 50;
     YUseFixedFont : boolean = false;

procedure FormAutoScale(MForm: TForm; const cFontHeight,cScreenWidth,
          cScreenHeight,cPixelsPerInch: integer; const doscale: boolean);
function EscBox(const Line: String; const xlocation: byte): boolean;
procedure ErrBox(const Line: String; const xlocation: byte; const topic: word);
procedure MsgBox(const Line: String; const xlocation: byte; const topic: word);
function yesnoBox(var esc: boolean; const yes: boolean; const prompt: String;
         const topic: word; const xlocation: byte; const StProc: TStringProc) : boolean;

implementation

uses
    csdef,
    util1,
    util2,
    StStrL;

{$R *.DFM}

type
    TFClass = class(TControl);

{var
   HelpTopic : word;}

procedure FormAutoScale(MForm: TForm; const cFontHeight,cScreenWidth,
          cScreenHeight,cPixelsPerInch: integer; const doscale: boolean);
var
   i : integer;

begin
  if doscale and ((Screen.Width <> cScreenWidth)
    or (Screen.PixelsPerInch <> cPixelsPerInch)) then with MForm do begin
    scaled := true;
    height := height * Screen.Height div cScreenHeight;
    width := width * Screen.Width div cScreenWidth;
    ScaleBy(Screen.width,cScreenWidth);
  end;
  if Screen.PixelsPerInch <> cPixelsPerInch then
    for i := MForm.ControlCount - 1 downto 0 do
    TFClass(MForm.Controls[i]).Font.Height :=
    (MForm.Font.Height div cFontHeight) *
    TFClass(MForm.Controls[i]).Font.Height;
end;

const
  YesNoBoxForm: TYesNoBoxForm = nil;
  Canceled  : boolean = false;
  Yespushed : boolean = false;

procedure SetFormWidth(const title,prompt: String; const topic: word;
          const xlocation: byte);
var
   MaxTextWidth        : integer;
   MaxTextHeight       : integer;
   w     : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   yline : String;
   cw    : integer;
   j     : integer;
begin
  with YesNoBoxForm do begin
    w := 0;
    yline := xline;
    repeat
      j := Pos(#13,yline);
      if j < 1 then j := Length(yline)+1;
      if j > 1 then Line := Copy(yline,1,j-1)
      else Line := '';
      yline := Copy(yline,j+1,32000);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
        cw := Canvas.TextHeight(Line);
        if cw > MaxTextHeight then MaxTextHeight := cw;
      end;
      Inc(w);
    until Length(yLine) < 1;
  end;
end;

begin
  with YesNoBoxForm do begin
    {$ifdef help}
    HelpFile := DefHelpFile;
    HelpContext := topic;
    {$else}
    HelpContext := 0;
    {$endif}
    HelpBut.Visible := HelpContext > 0;
    MaxTextWidth := 0;
    MaxTextHeight := 10;
    GetTextWidth(title);
    GetTextWidth(prompt);
    Width := 277;
    if HelpContext > 0 then Width := Width+HelpBut.Width+14;
    ClientHeight := 110;
    if w < 2 then Label1.Top := 15
    else begin
      Label1.Top := 0;
      ClientHeight := (w-2) * MaxTextHeight{20} + 110;
      YesBut.Top := Height - 60;
      NoBut.Top := YesBut.Top;
      CanButton.Top := YesBut.Top;
      HelpBut.top := Yesbut.Top;
    end;
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    if xlocation < 1 then begin
      Top := (Screen.Height * YesNoBoxYLoc) div 100 - (Height div 2);
      if Top < 0 then Top := 0;
      Left := (Screen.Width * YesNoBoxXLoc) div 100 - (Width div 2);
      if Left < 0 then Left := 0;
    end
    else begin
      Left := SetFormLeft(Width,xlocation);
      Top := SetFormTop(Height,xlocation);
    end;
    Label1.Width := Width-20;
    Caption := title;
    Label1.Caption := prompt;
    YesBut.Left := 8;
    if HelpContext > 0 then begin
      HelpBut.Left := Width-HelpBut.Width-14;
      NoBut.Left := (Width-NoBut.Width) div 3;
      CanButton.Left := ((Width-CanButton.Width) div 3)*2;
    end
    else begin
      CanButton.Left := Width-CanButton.Width-14;
      NoBut.Left := (Width-NoBut.Width) div 2;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure SetYFont;
begin
  with YesNoBoxForm do begin
    if YUseFixedFont then begin
//      UseGlobal := true;
      Font.Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
      YUseFixedFont := false;
    end;
    Font.Size := ViewFontSize;
  end;
end;

function EscBox(const Line: String; const xlocation: byte): boolean;
begin
  FreeHintWin;
  if not Assigned(YesNoBoxForm) then
    Application.CreateForm(TYesNoBoxForm, YesNoBoxForm);
  with YesNoBoxForm do begin
    SetYFont;
    NoBut.Visible := false;
    YesBut.TabOrder := 0;
    YesBut.Default := true;
    YesBut.Caption := '&OK';
    SetFormWidth('Press ENTER (OK) or ESC (Cancel)',Line,0,xlocation);
    ShowModal;
    Result := canceled;
    Free;
  end;
  YesNoBoxForm := nil;
  YesNoBoxYLoc := 50;
  YesNoBoxXLoc := 50;
end;

procedure MsgBox(const Line: String; const xlocation: byte; const topic: word);
begin
  FreeHintWin;
  if not Assigned(YesNoBoxForm) then
    Application.CreateForm(TYesNoBoxForm, YesNoBoxForm);
  with YesNoBoxForm do begin
    SetYFont;
    YesBut.Visible := false;
    CanButton.Visible := false;
    NoBut.TabOrder := 0;
    NoBut.Default := true;
    NoBut.Cancel := true;
    NoBut.Caption := '&OK';
    SetFormWidth('Press ENTER or Click OK',Line,topic,xlocation);
    ShowModal;
    Free;
  end;
  YesNoBoxForm := nil;
  YesNoBoxYLoc := 50;
  YesNoBoxXLoc := 50;
end;

procedure ErrBox(const Line: String; const xlocation: byte; const topic: word);
begin
  RingBell(false);
  MsgBox(Line,xlocation,topic);
end;

function yesnoBox(var esc: boolean; const yes: boolean; const prompt: String;
         const topic: word; const xlocation: byte; const StProc: TStringProc) : boolean;
begin
  Canceled := false;
  FreeHintWin;
  if not Assigned(YesNoBoxForm) then
    Application.CreateForm(TYesNoBoxForm, YesNoBoxForm);
  with YesNoBoxForm do begin
    SetYFont;
    YesBut.Default := yes;
    NoBut.Default := not yes;
    {if yes then begin}
      YesBut.TabOrder := 0;
      NoBut.TabOrder := 1;
    {end
    else begin
      YesBut.TabOrder := 1;
      NoBut.TabOrder := 0;
    end;}
    if yes then SetFormWidth('Press ENTER for Yes',prompt,topic,xlocation)
    else SetFormWidth('Press ENTER for No',prompt,topic,xlocation);
    if not yes then keybd_event(VK_Tab,MapVirtualKey(VK_Tab,0),0,0);
    ShowModal;
    esc := canceled;
    Result := yespushed;
    Free;
  end;
  YesNoBoxForm := nil;
  YesNoBoxYLoc := 50;
  YesNoBoxXLoc := 50;
  if not esc and Assigned(StProc) then begin
    if yespushed then StProc(prompt+' Yes')
    else StProc(prompt+' No');
  end;
end;

procedure TYesNoBoxForm.Acceptit(Sender: TObject);
begin
  Close;
  Yespushed := true;
end;

procedure TYesNoBoxForm.CancelIt(Sender: TObject);
begin
  Close;
  Yespushed := false;
  Canceled := true;
end;

procedure TYesNoBoxForm.NoClick(Sender: TObject);
begin
  Close;
  Yespushed := false;
end;

procedure TYesNoBoxForm.Dohelp(Sender: TObject);
begin
  if HelpContext > 0 then begin
    keybd_event(VK_F1,MapVirtualKey(VK_F1,0),0,0);
  end;
end;

end.
