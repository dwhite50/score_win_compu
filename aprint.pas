unit aprint;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Menus,
  StBase, {$ifdef VER120}StShBase,{$endif} Printera, StStrL, ComCtrls;

type
  TPrintForm = class(TForm)
    FontDialog1: TFontDialog;
    Button1: TButton;
    FontLabel: TLabel;
    PrinterLabel: TLabel;
    Button2: TButton;
    Button4: TButton;
    Button3: TButton;
    CheckBox1: TCheckBox;
    Button5: TButton;
    OrientLabel: TLabel;
    procedure PrnSetup(Sender: TObject);
    procedure PrinterFont(Sender: TObject);
    procedure PrintTestPage(Sender: TObject);
    procedure SetDefaultPrinter(Sender: TObject);
    procedure PrinterClose(Sender: TObject);
    procedure OrientClick(Sender: TObject);
  end;

procedure WprintInit(const LandScape,Rawprint: boolean);
procedure WprintLine(const Line: ShortString);
procedure WprintDone;
procedure WprintOptions;
procedure SelectWindowsPrinter;
function WprintName: String;


implementation
uses
    pvio,
    YesNoBoxU,
    vListBox,
    csdef;

{$R *.DFM}

type
  TByteArray    = Array[0..300] of byte;
  PByte         = ^TByteArray;

const
     PrintForm: TPrintForm = nil;
     PrApp = 'Printer';
//     AppName    = 'Winprnt';
//     ACBLDir    = '\ACBLSCOR';
//     IniName    = 'WINPRNT.INI';
     TestFile   = 'WINPRNT.TST';
//     ACBLid     : HWND = 0;
     isprinting   : boolean = false;
     PagePending  : boolean = false;
     InitOption   : byte = 0;
     prnsetupactive : boolean = false;
     NewFont    : HFont = 0;
     OldFont    : HFont = 0;
     MinCpi     = 9;
     MaxCpi     = 20;
     MinLpi     = 1;
     MaxLpi     = 12;
     DefPitch   = 10;
     DefLpi     = 6;
     Defbold    = false;
     DefFont     : TFont = nil;
     crlf       = #13#10;

var
   LastPitch   : word;
   LastLpi     : word;
   Lastbold    : boolean;
   CurPitch    : word;
   CurLpi      : word;
   Curbold     : boolean;
   isfont      : boolean;
   FontName    : String;
   CurPrinter  : String;

procedure Setfont(const NewLpi,NewCpi: word; const Bold: boolean);
const
     PageWidthIn = 4;
     MinFontHeight = 6;
var
   line        : String;
   PageWidthRes: integer;
   TextLen     : integer;
begin
  LastPitch := NewCpi;
  if LastPitch < MinCpi then LastPitch := MinCpi;
  if LastPitch > MaxCpi then LastPitch := MaxCpi;
  LastLpi := NewLpi;
  if LastLpi < MinLpi then LastLpi := MinLpi;
  if LastLpi > MaxLpi then LastLpi := MaxLpi;
  LastBold := Bold;
  if isprinting then with Printer do begin
    PageWidthRes := GetDeviceCaps(Printer.Handle,LogPixelsX)*PageWidthIn;
    WinPrnFixedHeight := GetDeviceCaps(Printer.Handle,LogPixelsY) div LastLpi;
    line := CharStrL('M',LastPitch*PageWidthIn);
    with Printer.Canvas do begin
      Font.Height := MinFontHeight;
      if Bold then Font.Style := [fsBold]
      else Font.Style := [];
      TextLen := TextWidth(line);
      while TextLen <= PageWidthRes do begin
        Font.Height := Font.Height+1;
        TextLen := TextWidth(line);
      end;
      Font.Height := Font.Height-1;
    end;
  end;
end;

procedure SetDefaultFont;
begin
  Printer.Title := 'ACBLscore';
  isfont := Length(FontName) > 0;
  CurPitch := DefPitch;
  CurLpi := DefLpi;
  Curbold := Defbold;
  SetFont(CurLpi,CurPitch,Curbold);
end;

procedure SetOrientLabel;
begin
  if not Assigned(PrintForm) then exit;
  with PrintForm.OrientLabel do if CFG.LandScape then Caption := 'Orientation: Landscape'
  else Caption := 'Orientation: Portrait';
end;

function SetCurPrinter(const raw: boolean): String;
var
   j      : integer;
   UseDef : String;
begin
//  UseGlobal := true;
  CurPrinter := GetAppProfile('Printer','',GlobalApp);
//  UseGlobal := true;
  UseDef := GetAppProfile('UseDefault','Y',GlobalApp);
//  UseGlobal := true;
  FontName := GetAppProfile('Font','Courier New',GlobalApp);
  if not Assigned(DefFont) then begin
    DefFont := TFont.Create;
    with DefFont do begin
      Name := FontName;
      Charset := Default_Charset;
      Pitch := fpDefault;
      Size := 10;
    end;
    Printer.Canvas.Font := DefFont;
  end;
  if Assigned(PrintForm) then with PrintForm do begin
    Button3.Enabled := UseDef = 'N';
    CheckBox1.Checked := UseDef = 'Y';
    FontLabel.Caption := 'Font: '+FontName;
  end;
  if not raw and ((Length(CurPrinter) < 10) or (UseDef = 'Y')) then
    with Printer do if Printers.Count > 0 then begin
    CurPrinter := Printers[PrinterIndex];
//    UseGlobal := true;
    SetAppProfile('Printer',CurPrinter,GlobalApp);
  end;
  SetOrientLabel;
  Result := '';
  with Printer do if Printers.Count > 0 then
    for j := 0 to Printers.Count-1 do if CurPrinter = Printers[j] then begin
    PrinterIndex := j;
    if Assigned(PrintForm) then
      PrintForm.PrinterLabel.Caption := 'Printer: '+Printers[PrinterIndex];
    Result := Printers[PrinterIndex];
    exit;
  end;
end;

procedure SelectWindowsPrinter;
var
   LastInd : integer;
begin
  with Printer do begin
    SetCurPrinter(true);
    prnsetupactive := true;
    PickTStrings := PickStrings;
    LastInd := pickchoice(Printers.Count,PrinterIndex+1,
      'Windows printer to use',Printers,MC,false,0);
    if LastInd > 0 then begin
//      UseGlobal := true;
      SetAppProfile('Printer',Printers[LastInd-1],GlobalApp);
      SetCurPrinter(true);
    end;
    prnsetupactive := false;
  end;
end;

procedure TPrintForm.PrnSetup(Sender: TObject);
begin
  SelectWindowsPrinter;
end;

procedure TPrintForm.PrinterFont(Sender: TObject);
begin
  prnsetupactive := true;
  SetDefaultFont;
  FontDialog1.Font := Printer.Canvas.Font;
  if FontDialog1.Execute then begin
//    UseGlobal := true;
    SetAppProfile('Font',FontDialog1.Font.Name,GlobalApp);
//    UseGlobal := true;
    FontName := GetAppProfile('Font','Courier New',GlobalApp);
    FontLabel.Caption := 'Font: '+FontName;
    DefFont := FontDialog1.Font;
    Printer.Canvas.Font := DefFont;
  end;
  prnsetupactive := false;
end;

procedure TPrintForm.PrintTestPage(Sender: TObject);
var
   F      : TextFile;
   Line   : ShortString;
begin
  if MessageDlg('Click on OK to send test page to the printer',mtConfirmation,
    [mbOK,mbCancel],0) in [mrOk] then begin
    AssignFile(F,CFG.LoadPath+TestFile);
    {$I-} Reset(F); {$I+}
    if IOResult <> 0 then begin
      ErrBox('File '+CFG.LoadPath+TestFile+' not found.',MC,0);
      exit;
    end;
    WprintInit(false,false);
    while not Eof(F) do begin
      ReadLn(F,Line);
      Line := Line+crlf;
      WprintLine(Line);
    end;
    CloseFile(F);
    WprintDone;
  end;
end;

procedure TPrintForm.SetDefaultPrinter(Sender: TObject);
var
   UseDef : String;
begin
//  UseGlobal := true;
  UseDef := GetAppProfile('UseDefault','Y',GlobalApp);
  if CheckBox1.Checked then begin
    if UseDef = 'N' then begin
//      UseGlobal := true;
      SetAppProfile('UseDefault','Y',GlobalApp);
      SetCurPrinter(false);
      Button3.Enabled := false;
    end;
  end
  else if UseDef = 'Y' then begin
//    UseGlobal := true;
    SetAppProfile('UseDefault','N',GlobalApp);
    Button3.Enabled := true;
  end;
end;

const
     LineLen    = 255;
     Len2Read   = LineLen div 2;

var
   Prn  : textFile;
   FLine       : Array[0..LineLen] of Char;
   PLine       : Pchar;
   BLine       : PByte Absolute Pline;
   j           : word;
   k           : byte;

procedure PrintText;
var
   j    : word;
   Pc     : PChar;
   P      : Pbyte absolute Pc;
   k      : byte;
begin
  j := 0;
  Pc := @Bline^[0];
  while (P^[j] > 0) do begin
    while (P^[j] > 0) and (P^[j] <> 12) do Inc(j);
    k := P^[j];
    P^[j] := 0;
    if Printer.isNewPage then SetFont(LastLpi,LastPitch,LastBold);
    Write(Prn,Pc);
    P^[j] := k;
    if k = 12 then begin
      Write(Prn,#12);
      Pc := @P^[j+1];
      j := 0;
    end;
  end;
end;

procedure WprintInit(const LandScape,Rawprint: boolean);
begin
  if isprinting then exit;
  if Rawprint then InitOption := 3
  else if LandScape then InitOption := 2
  else InitOption := 1;
end;

procedure WprintLine(const Line: ShortString);

procedure RawPrintLine(const Line: string; const checkff: boolean);
type
    TPassThroughData = record
      nlen: word;
      Data: array[0..255] of byte;
    end;
var
   PTBLock : TPassThroughData;
begin
  with PTBlock do begin
    nlen := Length(Line);
    Move(Line[1],Data,nlen);
    if checkff then begin
      PagePending := (CFG.SheetSize > 0) and (Data[nlen-1] = 12);
      if PagePending then Dec(nlen);
    end;
    if nlen > 0 then Escape(Printer.Handle,PASSTHROUGH,nlen+1,@PTBlock,nil);
  end;
end;

var
   ReadLen : integer;

begin
  if InitOption < 1 then exit;
  if not isprinting then begin
    SetCurPrinter(false);
    if Assigned(PrintForm) then with PrintForm do begin
      Button1.Enabled := false;
      Button3.Enabled := false;
    end;
    isprinting := true;
    with Printer do case InitOption of
      1: if not (Orientation in [poPortrait]) then Orientation := poPortrait;
      2: if not (Orientation in [poLandScape]) then Orientation := poLandScape;
    end;
    Printera.isRawPrint := InitOption = 3;
    AssignPrn(Prn);
    ReWrite(Prn);
    SetDefaultFont;
    PagePending := false;
    if (InitOption = 3) then begin
      RawPrintLine(#13,false);
      if CFG.SheetSize > 0 then RawPrintLine(#10,false);
    end;
  end;
  if InitOption > 2 then begin
    if PagePending then RawPrintLine(#12,false);
    PagePending := false;
    RawPrintLine(Line,true);
    exit;
  end;
  ReadLen := Length(Line);
  Move(Line[1],FLine,ReadLen);
  PLine := @Fline;
  Bline^[Readlen] := 0;
  j := 0;
  while BLine^[j] > 0 do begin
    while (BLine^[j] > 0) and not (BLine^[j] in [1,2,3]) do Inc(j);
    if BLine^[j] = 0 then PrintText
    else begin
      k := BLine^[j];
      if j > 0 then begin
        BLine^[j] := 0;
        PrintText;
        Inc(Pline,j);
        j := 0;
      end;
      {if BLine^[1] < 1 then begin
        BlockRead(f,FLine[1],Len2Read,Readlen);
        PLine := @Fline;
        Bline^[Readlen+1] := 0;
      end;}
      if BLine^[1] > 0 then begin
        case k of
          1: begin
            CurPitch := BLine^[1]-Ord('0')+10;
            if CurPitch < MinCpi then CurPitch := MinCpi;
            if CurPitch > MaxCpi then CurPitch := MaxCpi;
          end;
          2: begin
            CurLpi := BLine^[1]-Ord('0');
            if CurLpi < MinLpi then CurLpi := MinLpi;
            if CurLpi > MaxLpi then CurLpi := MaxLpi;
          end;
          3: CurBold := (BLine^[1]-Ord('0') > 4);
        end;
        Pline := @Bline^[2];
      end
      else PLine := @Bline^[1];
      j := 0;
      if not (BLine^[0] in [1,2,3]) and ((CurPitch <> LastPitch)
        or (CurLpi <> LastLpi) or (CurBold <> Lastbold)) then
        SetFont(CurLpi,CurPitch,CurBold);
    end;
  end;
end;

procedure WprintDone;
begin
  if not isprinting then exit;
  CloseFile(Prn);
  isprinting := false;
  InitOption := 0;
  if Assigned(PrintForm) then with PrintForm do begin
    Button1.Enabled := true;
//    UseGlobal := true;
    Button3.Enabled := GetAppProfile('UseDefault','Y',GlobalApp) = 'N';
  end;
end;

procedure WprintOptions;
begin
  if not Assigned(PrintForm) then
    Application.CreateForm(TPrintForm, PrintForm);
  PrintForm.Font.Size := ViewFontSize;
  SetCurPrinter(false);
  with PrintForm do begin
    ShowModal;
    Free;
  end;
  PrintForm := nil;
end;

function WprintName: String;
begin
  Result := SetCurPrinter(false);
  if Length(Result) < 1 then Result := 'No printer'
  else Result := 'Windows '+Result;
end;

procedure TPrintForm.PrinterClose(Sender: TObject);
begin
  Close;
end;

procedure TPrintForm.OrientClick(Sender: TObject);
const
     LandScapeChoiceText     : Array[1..2] of Pchar =
       ('1 Portrait Orientation - Normal 8.5 x 11',
        '2 Landscape Orientation - 11 x 8.5');
begin
  with CFG do begin
    if LandScape then j := 2
    else j := 1;
    j := PickChoice(2,j,'Orientation to use ',@LandScapeChoiceText,MC,false,11);
    if j < 1 then exit;
    LandScape := j = 2;
    if LandScape then pwidth := 105
    else pwidth := 80;
  end;
  WriteCFG;
  SetOrientLabel;
end;

end.

