unit Progress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls;

type
  TProgressForm = class(TForm)
    ProgressBar1: TProgressBar;
    CancelBut: TButton;
    Label1: TLabel;
    PerLabel: TLabel;
    procedure ProgressCancel(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ProgressBar(const Current,Maximum: longint): boolean;
procedure ProgressBarInit(const Line,FormTitle: String; const AllowCan: boolean);
procedure ProgressBarDone(const dowait: boolean);
procedure ProgressBarLabel(const Line: String);

const
     ProgressCanceled: boolean = false;

implementation

uses
    csdef,
    StStrL;

{$R *.DFM}

const
     ProgressForm: TProgressForm = nil;

function ProgressBar(const Current,Maximum: longint): boolean;
var
   CurPer       : byte;
   Curr         : real;
begin
  Result := ProgressCanceled;
  if not Assigned(ProgressForm) then exit;
  if (Maximum < 1) or (Current < 0) then exit;
  if Current > Maximum then Curr := Maximum
  else Curr := Current;
  CurPer := Round((Curr * 100) / Maximum);
  with ProgressForm do begin
    ProgressBar1.Position := CurPer;
    PerLabel.Caption := Long2StrL(CurPer)+'%';
    if CurPer > 99 then CancelBut.Enabled := false;
    if CancelBut.Enabled then Application.ProcessMessages;
  end;
end;

procedure ProgressBarLabel(const Line: String);
begin
  if not Assigned(ProgressForm) then exit;
  ProgressForm.Label1.Caption := Line;
  Application.ProcessMessages;
end;

procedure ProgressBarInit(const Line,FormTitle: String; const AllowCan: boolean);
begin
  if not Assigned(ProgressForm) then
    Application.CreateForm(TProgressForm,ProgressForm);
  with ProgressForm do begin
    Font.Size := ViewFontSize;
    CancelBut.Visible := AllowCan;
    Caption := FormTitle;
    ProgressCanceled := false;
    Label1.Caption := Line;
    {PerLabel.Top := 45;}
    PerLabel.Caption := '0%';
    with ProgressBar1 do begin
      Min := 0;
      Max := 100;
      Position := 0;
      Step := 1;
      {Left := 8;
      Height := 30;
      Width := 500;
      Top := 40;}
      Orientation := pbHorizontal;
      Enabled := true;
    end;
    Show;
  end;
  Screen.Cursor := crHourGlass;
  Application.ProcessMessages;
end;

procedure ProgressBarDone(const dowait: boolean);
begin
  Screen.Cursor := crDefault;
  if Assigned(ProgressForm) then with ProgressForm do begin
    if dowait then begin
      ProgressCanceled := false;
      CancelBut.Caption := '&OK';
      CancelBut.Visible := True;
      CancelBut.Enabled := True;
      CancelBut.SetFocus;
      while not ProgressCanceled do Application.ProcessMessages;
    end;
    ProgressForm.Free;
  end;
  ProgressForm := nil;
end;

procedure TProgressForm.ProgressCancel(Sender: TObject);
begin
  ProgressCanceled := true;
end;

end.
