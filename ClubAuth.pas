unit ClubAuth;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    procedure DoAuthCode(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    YesNoBoxU,
    util2;

{$R *.DFM}

procedure TForm1.DoAuthCode(Sender: TObject);
var
   AuthCode : String;
begin
  AuthCode := GetClubAuthCode(Edit1.Text);
  MsgBox('Authorization code for club '+Edit1.Text+' is '+AuthCode,6,0);
  Application.Terminate;
end;

end.
