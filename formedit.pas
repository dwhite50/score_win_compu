unit formedit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcPB, OvcNF, OvcBase, OvcEF, OvcSF;

const
     {forms editor equates}
     EditInt = 1;
     EditFloat = 2;
     EditText = 3;
     EditBool = 4;
     EditLine = 5;
     EditPick = 6;
     EditOKButton = 7;
     EditLong = 8;
     {EditCancelButton = 8;}
     EditCheckBox = 9;
     EditPath = 10;
     MaxEditItems = 500;
     {maximum number of items in forms editor}
     EditDefCol : byte = 2;
     EditStartSeq : word = 0;
     EditEscFunc  : byte = 0;
     CheckAbn : boolean = false;

type
    {defines a pointer to a string}
//    ValidCharSet = Set of Char;

    PEditRecord = ^EditRecord;
    EditFuncSet = Set of Byte;

    PreEditProc = function(const EditP : PEditRecord;
                const Direction: ShortInt; var Ifunc: byte): integer;
    {definition of user function called before a field is edited}


    PostEditProc = function(const EditP : PEditRecord;
                 const Direction : ShortInt; var Ifunc: Byte) : integer;
    {definition of user function called after a field is edited}


    {edit record definition}
    EditRecord = Packed record
      EditSeq      : word;      {edit sequence number}
      EditPrompt   : TLabel;    {prompt string}
      EditRow      : word;      {screen row}
      EditCol      : word;      {screen column}
      EditPre     : PreEditProc; {pre edit user function}
      EditPost    : PostEditProc;{post edit user function}
      case EditType : Byte of
        EditInt: (IComponent : TOvcNumericField;
                  IDefault   : ^SmallInt; {location of integer field}
                  IEditMin   : SmallInt;  {minimum allowed}
                  IEditMax   : SmallInt;  {maximum allowed}
                  IWidth     : byte);    {field width}
        EditLong: (iLComponent : TOvcNumericField;
                  iLDefault   : ^LongInt; {location of integer field}
                  iLEditMin   : LongInt;  {minimum allowed}
                  iLEditMax   : LongInt;  {maximum allowed}
                  iLWidth     : byte);    {field width}
        EditFloat: (FComponent : TOvcNumericField;
                    FDefault : ^Real48;   {location of float field}
                    FEditMin : Real48;    {minimum allowed}
                    FEditMax : Real48;    {maximum allowed}
                    FWidth   : byte;     {field width}
                    FPlaces  : byte);    {decimal places}
        EditText: (TComponent : TOvcSimpleField;
                   PField    : ^ShortString;  {location of text string field}
                   TLen      : byte;     {maximum length}
                   TType     : SmallInt;  {editing type}
                   TCase     : SmallInt;  {case conversion }
                   TRO       : boolean); {true = read only}
        EditBool: (BComponent : TOvcSimpleField;
                   BDefault  : ^Boolean); {location of boolean field}
        EditLine: (LComponent : TLabel;
                   LCenter   : Boolean;  {display string centered status}
                   LAttr     : byte);    {color attribute}
        EditPick: (PiComponent : TEdit;
                   PiDefault : ^ShortString); {default pick choice}
        EditOKButton: (BuComponent : TButton);
        EditCheckBox: (CComponent : TCheckBox;
                       CDefault : ^Boolean);
//                       CChar    : ^Char;
//                       CFuncSet : ValidCharSet);
        EditPath: (PComponent : TOvcSimpleField;
                   PPath      : ^ShortString; {location of default path}
                   PLen       : byte;    {maximum length}
                   PType      : byte;    {type of history list}
                   pCheck95   : boolean); {true = check for existance}
    end;


type
    PEdit = ^EditPointer;
  TxxEditForm = class(TForm)
    OvcController1: TOvcController;
    OKbut: TButton;
    CanBut: TButton;
    procedure OnFieldEntry(Sender: TObject);
    procedure OnFieldExit(Sender: TObject);
    procedure OnValidate(Sender: TObject; var ErrorCode: Word);
    procedure OnFieldError(Sender: TObject; ErrorCode: word; ErrorMessage: String);
//    procedure UserCommand(Sender: TObject; Command: Word);
    procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure OKClick(Sender: TObject);
    procedure CanClick(Sender: TObject);
    procedure FormActive(Sender: TObject);
    procedure FieldChanged(Sender: TObject);
  private
    { Private declarations }
    FormEditRecord : PEdit;
    CurrentSeq      : word;
//    ifunc         : byte;
    NextSeq     : smallint;
    Direction   : ShortInt;
    isok : boolean;
    iscan : boolean;
    EFuncSet : EditFuncSet;
    EExitFunc : byte;
    FuncCode : word;
    isfirst : boolean;
  public
    { Public declarations }
  end;

    {Global edit window definition}
    EditPointer = Packed record
      EditSize    : Word;       {number of items}
      PEditFooter : String;    {footer}
      XLow,YLow   : word;       {UL screen location in chars}
      XRes,YRes   : word;       {screen to char resolution}
//      hXRes       : word;
      MaxRow      : integer;
      MaxCol      : integer;
      BottomRow   : word;
      EditWindow    : TxxEditForm;{TPwindow definition}
      EditInitDone  : boolean;  {true= initialized}
      FirstDisplay  : boolean;
      EditPtr     : Array[1..MaxEditItems] of PEditRecord;
      {location of edit records}
    end;

function EditInit(var EditP : PEdit; const Row,Col : word;
         const title,footer : String) : Boolean;
procedure EditDone(var EditP : PEdit);
procedure EditAddInt(const EditP : PEdit; const Seq: word;
          var xDefault: SmallInt; const Min,Max : SmallInt; const width : byte;
          const Prompt : String; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
procedure EditAddLong(const EditP : PEdit; const Seq: word;
          var xDefault: LongInt; const Min,Max : LongInt; const width : byte;
          const Prompt : String; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
procedure EditAddLine(const EditP : PEdit; const Seq: word;
          const Prompt : String; const center : boolean;
          const attr,LineSpace,Col : byte);
procedure EditAddCheck(const EditP : PEdit; const Seq: word;
          var xDefault : Boolean; const Prompt : String;
//          const CharSet : ValidCharSet; var ExitChar : Char;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
procedure EditAddBool(const EditP : PEdit; const Seq: word;
          var xDefault : Boolean; const Prompt : String;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
procedure EditAddPick(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Field: ShortString; const len: byte;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte);
procedure EditAddText(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Field : ShortString; const len: byte;
          const Atype,ACase: SmallInt; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
function EditShow(const EditP : PEdit; var esc : boolean; var xfunc : byte;
         const FuncSet : EditFuncSet; const ExitFunc : byte) : boolean;
procedure EditProcess(const EditP : PEdit; const Seq : word);
procedure EditAddFloat(const EditP : PEdit; const Seq: word;
          var xDefault: Real48; const Min,Max : Real48;
          const width,places : byte; const Prompt : String;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col : byte; const Topic : Word);
procedure EditAddPath(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Path: ShortString; const len: byte;
          const Atype : byte; const usedef: boolean;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
procedure EditAddButton(const EditP : PEdit; const Seq: word;
          const ButType: byte; const title: String; const LineSpace,Col:byte;
          const topic: word);
procedure EditSetRow(const EditP : PEdit; const Delta : byte);
procedure EditSetPrompt(const EditP: PEdit; const Seq: word; const Prompt: String);
procedure EditSetReadOnly(const EditP: PEdit; const Seq: word; const RO: boolean);
procedure EditTitle(var EditP: PEdit; const title: String);
procedure EditSetCan(const EditP: PEdit);

const
     eUseFixedFont : boolean = false;
     EditHintText : string = '';
     eFontSize : integer = 1;
     eBackColor : TColor = 0;
     ePromptColor : byte = 0;
     KeyTabLen    : byte = 0;
     EditKeepOpen : boolean = false;
     EditOKShow : boolean = true;
     EditCanShow : boolean = true;
var
     KeyTable : Array [1..10,1..2] of word;

implementation
uses
    StStrL,
    pvio,
    util1,
    csdef;

{$R *.DFM}

const
     hextra = 1;
     wextra = 5;
     Wchar  = '0';
     XresExtra = 2;
     YresExtra = 1;

var
    ifunc         : byte;

procedure EditTitle(var EditP: PEdit; const title: String);
begin
  with EditP^ do if Assigned(EditWindow) then EditWindow.Caption := title;
end;

function EditInit(var EditP : PEdit; const Row,Col : word;
         const title,footer : String) : Boolean;
var
   loop : word;
begin
  Result := false;
  if Assigned(EditP) then EditDone(EditP);
  New(EditP);
  with EditP^ do begin
    EditSize := 0;
    PEditFooter := footer;
    XLow := Col;
    YLow := Row;
    XRes := Screen.Width div 80;
    YRes := Screen.Height div 25;
    MaxRow := -1;
    BottomRow := 0;
    MaxCol := 0;
    Application.CreateForm(TxxEditForm, EditWindow);
    EditInitDone := false;
    for loop := 1 to MaxEditItems do EditPtr[loop] := nil;
    with EditWindow do begin
      if eBackColor > 0 then Color := eBackColor;
      FormEditRecord := EditP;
      if eUseFixedFont then begin
//        UseGlobal := true;
        Font.Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
        eUseFixedFont := false;
      end;
      Font.Size := Screen.Width div 70;
      Top := YLow * YRes;
      Left := XLow * XRes;
      YRes := Canvas.TextHeight(Wchar)+YresExtra;
      XRes := Canvas.TextWidth(Wchar)+XresExtra;
      Caption := title;
    end;
  end;
  eBackColor := 0;
  Result := true;
end;

procedure EditDone(var EditP : PEdit);
var
   loop : word;
begin
  if not Assigned(EditP) then exit;
  FreeHintWin;
  with EditP^ do begin
//    EditWindow.Close;
    EditWindow.Free;
//    EditWindow.Destroy;
    for loop := 1 to MaxEditItems do if Assigned(EditPtr[loop]) then
      with EditPtr[loop]^ do begin
      Dispose(EditPtr[loop]);
    end;
  end;
  Dispose(EditP);
  EditP := nil;
end;

function EditFind(const EditP : PEdit; const Seq,Current : word) : Word;
var
   loop : word;
begin
  with EditP^ do for loop := 1 to EditSize do with EditPtr[loop]^ do
    if Assigned(EditPtr[loop]) and (Seq = EditSeq) then begin
      EditFind := Loop;
      Exit;
    end;
  EditFind := Current + 1;
end;

procedure EditSetReadOnly(const EditP: PEdit; const Seq: word; const RO: boolean);
var
   loop   : word;
begin
  if Seq < 1 then exit;
  loop := EditFind(EditP,Seq,0);
  with EditP^.EditPtr[loop]^ do begin
    if EditType <> EditText then exit;
    TRO := RO;
  end;
end;

procedure EditSetPrompt(const EditP: PEdit; const Seq: word; const Prompt: String);
var
   loop   : word;
begin
  if Seq < 1 then exit;
  loop := EditFind(EditP,Seq,0);
  with EditP^.EditPtr[loop]^ do begin
    if (Seq <> EditSeq) or not Assigned(EditPrompt) then exit;
    EditPrompt.Caption := Prompt;
    EditPrompt.Repaint;
  end;
end;

procedure SetPosition(const EditP: PEdit; const PERecord: PEditRecord;
          const LPrompt: String; Ewidth: word; const fsize,fcolor: byte);
var
   FormRow      : integer;
   FormCol      : integer;
   pw           : integer;
   ph           : integer;
   ew           : integer;
   mc           : integer;
   fontx        : integer;
begin
  with EditP^,PERecord^,EditWindow do begin
    FormRow := EditRow*YRes;
    FormCol := (EditCol-1)*XRes;
    if Assigned(EditPrompt) then with EditPrompt do begin
      Parent := EditWindow;
      Font.Name := EditWindow.Font.Name;
      if (fcolor > 0) and (fcolor < 7) then Font.Color := SecColors[fcolor];
      Fontx := EditWindow.Font.Size*fsize;
      repeat
        Font.Size := fontx;
        Caption := LPrompt;
        pw := Canvas.TextWidth(LPrompt);
        Dec(fontx);
      until (fontx < EditWindow.Font.Size) or (pw < Screen.Width-20);
      ph := Canvas.TextHeight(Wchar);
      Top := FormRow;
      Left := FormCol+wextra;
      Height := ph+hextra;
      Width := pw+wextra;
    end
    else begin
      pw := Canvas.TextWidth(LPrompt);
      ph := Canvas.TextHeight(Wchar);
    end;
    ew := Canvas.TextWidth(CharStrL(Wchar,Ewidth+1));//+hXres;
    mc := FormCol+ew+pw{+ph}+wextra;
    if MaxCol < mc then MaxCol := mc;
    case EditType of
      EditInt: with IComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditLong: with iLComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditFloat: with FComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditText: with TComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditPath: with PComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditBool: with BComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
//        OnError := OnFieldError;
        OnUserValidation := OnValidate;
        OnKeyDown := KeyDown;
        OnChange := FieldChanged;
      end;
      EditPick: with PiComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        ReadOnly := true;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
        OnKeyDown := KeyDown;
      end;
      EditOKButton: with BuComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra;
        OnEnter := OnFieldEntry;
        OnClick := OnFieldExit;
        OnKeyDown := KeyDown;
      end;
      EditCheckBox: with CComponent do begin
        Top := FormRow;
        Left := FormCol+pw+wextra;
        Height := ph+hextra;
        Width := ew+wextra+50;
        OnEnter := OnFieldEntry;
        OnExit := OnFieldExit;
        OnKeyDown := KeyDown;
      end;
    end;
  end;
  eFontSize := 1;
  ePromptColor := 0;
end;

procedure InitEditText(const PERecord : PEditRecord);
var
   atype     :byte;
   doarr         : boolean;
   dofunc        : boolean;
   acase         : byte;
   last_ret      : boolean;
   field         : String;
begin
  with PERecord^,TComponent do begin
    atype := abs(TType);
    doarr := (atype > 9);
    if doarr then Dec(atype,10);
    dofunc := (atype > 9);
    if dofunc then Dec(atype,10);
    acase := Abs(Tcase);
    last_ret := acase > 9;
    if last_ret then Dec(acase,10);
    Datatype := sftString;
    MaxLength := Tlen;
    field := PField^;
    case acase of
      0: PictureMask := 'X';
      1: PictureMask := '!';
      2: PictureMask := 'L';
      3,5: PictureMask := 'x';
      4: PictureMask := '#';
      6: begin
        PictureMask := '!';
        if (Length(field) > 0) and (field[1] = '0') then field[1] := 'O';
      end;
      7: PictureMask := '!';
      else PictureMask := 'X';
    end;
    if TRO then Options := [efoReadOnly]
    else if TType mod 10 = 6 then Options := [efoCaretToEnd,efoTrimBlanks,efoPasswordMode]
    else Options := [efoCaretToEnd,efoTrimBlanks];
    if atype = 2 then SetInitialValue
    else Text := field;
  end;
end;

procedure EditAddInt(const EditP : PEdit; const Seq: word;
          var xDefault: SmallInt; const Min,Max : SmallInt; const width : byte;
          const Prompt : String; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      IComponent := TOvcNumericField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditInt;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      IDefault := @xDefault;
      IEditMin := Min;
      IEditMax := Max;
      IWidth := width;
      with IComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        DataType := nftInteger;
        RangeHi := Long2StrL(IEditMax);
        RangeLo := Long2StrL(IEditMin);
        PictureMask := CharStrL('i',IWidth);
        AsInteger := IDefault^;
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,width,1,ePromptColor);
    end;
  end;
end;

procedure EditAddLong(const EditP : PEdit; const Seq: word;
          var xDefault: LongInt; const Min,Max : LongInt; const width : byte;
          const Prompt : String; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      iLComponent := TOvcNumericField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditLong;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      iLDefault := @xDefault;
      iLEditMin := Min;
      iLEditMax := Max;
      iLWidth := width;
      with iLComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        DataType := nftLongInt;
        RangeHi := Long2StrL(iLEditMax);
        RangeLo := Long2StrL(iLEditMin);
        PictureMask := CharStrL('i',iLWidth);
        AsInteger := iLDefault^;
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,width,1,ePromptColor);
    end;
  end;
end;

procedure EditAddFloat(const EditP : PEdit; const Seq: word;
          var xDefault: Real48; const Min,Max : Real48;
          const width,places : byte; const Prompt : String;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col : byte; const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      FComponent := TOvcNumericField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditFloat;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      FDefault := @xDefault;
      FEditMin := Min;
      FEditMax := Max;
      FWidth := width;
      FPlaces := places;
      with FComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        DataType := nftReal;
        RangeHi := Real2StrL(FEditMax,Fwidth,Fplaces);
        RangeLo := Real2StrL(FEditMin,Fwidth,Fplaces);
        if (FPlaces > 0) and (Fplaces < Fwidth) then
          PictureMask := CharStrL('#',FWidth-Fplaces-1)+'.'+CharStrL('#',Fplaces)
        else PictureMask := CharStrL('#',FWidth);
        AsFloat := FDefault^;
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,width,1,ePromptColor);
    end;
  end;
end;

procedure EditAddButton(const EditP : PEdit; const Seq: word;
          const ButType: byte; const title: String; const LineSpace,Col:byte;
          const topic: word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      BuComponent := TButton.Create(EditWindow);
      EditSeq := Seq;
      EditType := ButType;
      EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := nil;
      EditPost := nil;
      with BuComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        Tag := EditSize;
        Caption := title;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],'',Length(title),1,ePromptColor);
    end;
  end;
end;

procedure EditAddPick(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Field: ShortString; const len: byte;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      PiComponent := TEdit.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditPick;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      PiDefault := @Field;
      with PIComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        Tag := EditSize;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,len,1,ePromptColor);
    end;
  end;
end;

procedure EditAddBool(const EditP : PEdit; const Seq: word;
          var xDefault : Boolean; const Prompt : String;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      BComponent := TOvcSimpleField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditBool;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      BDefault := @xDefault;
      with BComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        Datatype := sftString;
        MaxLength := 1;
        PictureMask := 'Y';
        Options := [efoCaretToEnd,efoTrimBlanks];
        if BDefault^ then Text := 'Y'
        else Text := 'N';
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,1,1,ePromptColor);
    end;
  end;
end;

procedure EditAddCheck(const EditP : PEdit; const Seq: word;
          var xDefault : Boolean; const Prompt : String;
//          const CharSet : ValidCharSet; var ExitChar : Char;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
var
   mc           : integer;
begin
  if EditP = nil then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if EditPtr[EditSize] = nil then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      CComponent := TCheckBox.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditCheckBox;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      CDefault := @xDefault;
      EditPrompt := nil;
//      CChar    := @ExitChar;
//      CFuncSet := CharSet;
      with CComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        Caption := Prompt;
        Checked := CDefault^;
        Tag := EditSize;
        HelpContext := Topic;
        SetPosition(EditP,EditPtr[EditSize],'',1,1,ePromptColor);
        Width := EditWindow.Canvas.TextWidth(Prompt)+EditWindow.Canvas.TextWidth('WW');
      end;
      mc := (EditCol-1)*XRes+CComponent.Width+wextra;
      if MaxCol < mc then MaxCol := mc;
    end;
  end;
end;

procedure EditAddPath(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Path: ShortString; const len: byte;
          const Atype : byte; const usedef: boolean;
          const Pre : PreEditProc; const Post : PostEditProc;
          const LineSpace,Col: byte; const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      PComponent := TOvcSimpleField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditPath;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      PPath := @Path;
      PLen := len;
      PType := Atype;
      pCheck95 := usedef;
      with PComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        Datatype := sftString;
        MaxLength := Plen;
        PictureMask := 'X';
        Options := [efoCaretToEnd,efoTrimBlanks];
        Text := PPath^;
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,len,1,ePromptColor);
    end;
  end;
end;

procedure EditAddText(const EditP : PEdit; const Seq: word;
          const Prompt : String; var Field : ShortString; const len: byte;
          const Atype,ACase: SmallInt; const Pre : PreEditProc;
          const Post : PostEditProc; const LineSpace,Col: byte;
          const Topic : Word);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      TComponent := TOvcSimpleField.Create(EditWindow);
      EditSeq := Seq;
      EditType := EditText;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := Pre;
      EditPost := Post;
      PField := @Field;
      TLen := len;
      TType := Atype;
      TCase := Acase;
      TRO := false;
      while TType < 20 do Inc(TType,10);
      if Atype < 0 then TType := -TType;
      with TComponent do begin
        Parent := EditWindow;
        Font.Name := EditWindow.Font.Name;
        Font.Size := EditWindow.Font.Size;
        InitEditText(EditPtr[EditSize]);
        Tag := EditSize;
        HelpContext := Topic;
      end;
      SetPosition(EditP,EditPtr[EditSize],Prompt,len,1,ePromptColor);
    end;
  end;
end;

procedure EditSetRow(const EditP : PEdit; const Delta : byte);
begin
  if Assigned(EditP) then Dec(EditP^.MaxRow,Delta);
end;

procedure EditAddLine(const EditP : PEdit; const Seq: word;
          const Prompt : String; const center : boolean;
          const attr,LineSpace,Col : byte);
begin
  if not Assigned(EditP) then Exit;
  with EditP^ do begin
    if EditSize >= MaxEditItems then exit;
    Inc(EditSize);
    if not Assigned(EditPtr[EditSize]) then New(EditPtr[EditSize]);
    Inc(MaxRow,LineSpace);
    if BottomRow < MaxRow then BottomRow := MaxRow;
    with EditPtr[EditSize]^ do begin
      EditSeq := Seq;
      EditType := EditLine;
      if Length(Prompt) > 0 then EditPrompt := TLabel.Create(EditWindow)
      else EditPrompt := nil;
      EditRow := MaxRow;
      EditCol := Col;
      EditPre := nil;
      EditPost := nil;
      LCenter := center;
      if center then Inc(EditCol);
    end;
    SetPosition(EditP,EditPtr[EditSize],Prompt,0,eFontSize,attr);
  end;
end;

procedure InitComponent(const PERecord: PEditRecord);
begin
  with PERecord^ do case EditType of
    EditInt: with IComponent do begin
      DataType := nftInteger;
      RangeHi := Long2StrL(IEditMax);
      RangeLo := Long2StrL(IEditMin);
      PictureMask := CharStrL('i',IWidth);
      AsInteger := IDefault^;
      RePaint;
    end;
    EditLong: with iLComponent do begin
      DataType := nftLongInt;
      RangeHi := Long2StrL(iLEditMax);
      RangeLo := Long2StrL(iLEditMin);
      PictureMask := CharStrL('i',iLWidth);
      AsInteger := iLDefault^;
      RePaint;
    end;
    EditFloat: with FComponent do begin
      DataType := nftReal;
      RangeHi := Real2StrL(FEditMax,Fwidth,Fplaces);
      RangeLo := Real2StrL(FEditMin,Fwidth,Fplaces);
      if (FPlaces > 0) and (Fplaces < Fwidth) then
        PictureMask := CharStrL('#',FWidth-Fplaces-1)+'.'+CharStrL('#',Fplaces)
      else PictureMask := CharStrL('#',FWidth);
      AsFloat := FDefault^;
      RePaint;
    end;
    EditText: with TComponent do begin
      InitEditText(PERecord);
      RePaint;
    end;
    EditPick: with PiComponent do begin
      Text := PiDefault^;
      RePaint;
    end;
    EditOKbutton: BuComponent.RePaint;
    EditPath: with PComponent do begin
      Datatype := sftString;
      MaxLength := Plen;
      PictureMask := 'X';
      Options := [efoCaretToEnd,efoTrimBlanks];
      Text := PPath^;
      RePaint;
    end;
    EditBool: with BComponent do begin
      Datatype := sftString;
      MaxLength := 1;
      PictureMask := 'Y';
      Options := [efoCaretToEnd,efoTrimBlanks];
      if BDefault^ then Text := 'Y'
      else Text := 'N';
      RePaint;
    end;
    EditLine: if Assigned(EditPrompt) then EditPrompt.RePaint;
    EditCheckBox: with CComponent do begin
      Checked := CDefault^;
      RePaint;
    end;
  end;
end;

procedure EditSetHint(const EditP: PEdit; const PERecord: PEditRecord);

begin
  with PERecord^ do case EditType of
    EditInt: with IComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditLong: with iLComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditFloat: with FComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditText: with TComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditPath: with PComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditPick: with PiComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditOKButton: with BuComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditBool: with BComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
    EditCheckBox: with CComponent do begin
      Hint := EditHintText;
      ShowHint := Length(EditHintText) > 0;
    end;
  end;
  EditHintText := '';
end;

function EditDispField(const EditP : PEdit; const PERecord : PEditRecord) : Word;
var
   NextField : word;
begin
  EditDispField := 0;
  with EditP^, PERecord^ do begin
    ifunc := 255;
    if Assigned(EditPre) then begin
      NextField := EditPre(PERecord,0,ifunc);
      EditSetHint(EditP,PERecord);
    end
    else NextField := 0;
    if NextField < 1 then begin
      InitComponent(PERecord);
      ifunc := 255;
      if Assigned(EditPost) then NextField := EditPost(PERecord, 0,ifunc)
      else NextField := 0;
    end;
    EditDispField := NextField;
  end;
end;

procedure EditProcess(const EditP : PEdit; const Seq : word);
var
   loop : word;
   oldfunc : byte;
begin
  if not Assigned(EditP) then Exit;
  if EditP^.FirstDisplay then exit;
  with EditP^ do for loop := 1 to EditSize do with EditPtr[loop]^ do
    if Assigned(EditPtr[loop]) and (EditSeq = Seq) and (EditType <> EditLine)
      then begin
      oldfunc := ifunc;
      EditDispField(EditP,EditPtr[loop]);
      ifunc := oldfunc;
      Exit;
    end;
end;

procedure SetEditFocus(const PERecord : PEditRecord);
begin
  with PERecord^ do case Edittype of
    EditInt: IComponent.SetFocus;
    EditLong: iLComponent.SetFocus;
    EditFloat: FComponent.SetFocus;
    EditText: TComponent.SetFocus;
    EditPath: PComponent.SetFocus;
    EditBool: BComponent.SetFocus;
    EditCheckBox: CComponent.SetFocus;
    EditPick: PiComponent.SetFocus;
    EditOKButton: BuComponent.SetFocus;
  end;
end;

procedure TxxEditForm.OnFieldEntry(Sender: TObject);
var
   loop   : word;
begin
  if isok then exit;
  CurrentSeq := TComponent(Sender).Tag;
  with FormEditRecord^,EditPtr[CurrentSeq]^ do if not (EditType in [EditLine]) then begin
//    with TControl(Sender) do if ShowHint then ShowHintWin(1,@Hint,Sender);
    ShowControlHint(Sender);
    ifunc := 0;
    if Assigned(EditPre) then NextSeq := EditPre(EditPtr[CurrentSeq],Direction,ifunc)
    else NextSeq := 0;
    if (NextSeq < 1) {and (ifunc < 2)} then begin
//      if CheckAbn then ifunc := 3
//      else ifunc := 1;
      InitComponent(EditPtr[CurrentSeq]);
      if (EditType = EditPick) and (NextSeq < 1) then NextSeq := -1;
//        keybd_event(VK_TAB,MapVirtualKey(VK_TAB,0),0,0);
    end;
    if NextSeq < 0 then begin
      loop := CurrentSeq;
      repeat
        Inc(loop);
      until (loop > EditSize) or (Assigned(EditPtr[loop])
        and (EditPtr[loop]^.EditType <> EditLine));
      if loop > EditSize then begin
        NextSeq := 0;
        keybd_event(VK_TAB,MapVirtualKey(VK_TAB,0),0,0);
      end
      else NextSeq := EditPtr[loop]^.EditSeq;
    end;
    if NextSeq > 0 then begin
      Loop := EditFind(FormEditRecord,NextSeq,CurrentSeq);
      if loop > 0 then SetEditFocus(EditPtr[loop]);
    end
    else if ifunc = 13 then OkClick(Sender);
  end;
end;

procedure TxxEditForm.OnFieldExit(Sender: TObject);
begin
  with FormEditRecord^,EditPtr[CurrentSeq]^ do begin
    FreeHintWin;
//    EditShowHint(EditPtr[CurrentSeq],false,Sender);
    if (NextSeq = 0) then begin
      case EditType of
        EditInt: IDefault^ := IComponent.AsInteger;
        EditLong: iLDefault^ := iLComponent.AsInteger;
        EditFloat: FDefault^ := FComponent.AsFloat;
        EditText: PField^ := TComponent.Text;
        EditPath: PPath^ := PComponent.Text;
        EditBool: BDefault^ := BComponent.Text = 'Y';
        EditCheckBox: CDefault^ := CComponent.Checked;
        EditOKButton: ifunc := EExitFunc;
      end;
      if Assigned(EditPost) then NextSeq := EditPost(EditPtr[CurrentSeq],Direction,ifunc)
      else NextSeq := 0;
    end;
    if (ifunc in EFuncSet) or (ifunc = EExitFunc) then isok := true;
    if not isok then begin
      if NextSeq > 0 then
        NextSeq := EditFind(FormEditRecord,NextSeq,CurrentSeq);
      if (NextSeq < 1) and (Direction <> 0) then begin
        NextSeq := CurrentSeq;
        repeat
          Inc(NextSeq,Direction);
          if NextSeq > EditSize then NextSeq := 1;
          if NextSeq < 1 then NextSeq := EditSize;
        until (NextSeq = CurrentSeq) or (Assigned(EditPtr[NextSeq])
          and (EditPtr[NextSeq]^.EditType <> EditLine));
      end;
      if NextSeq > 0 then SetEditFocus(EditPtr[NextSeq]);
    end;
  end;
  if isok then Close;
  Direction := 0;
end;

procedure TxxEditForm.OnFieldError(Sender: TObject; ErrorCode: word; ErrorMessage: String);
begin
//xx
end;

procedure TxxEditForm.OnValidate(Sender: TObject; var ErrorCode: Word);
begin
//xxx
end;

procedure TxxEditForm.KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
   j      : word;
begin
  Funccode := 0;
  for j := 1 to KeyTabLen do if KeyTable[j,1] = Key then begin
    key := KeyTable[j,2];
    Break;
  end;
  case key of
    VK_RETURN: begin
      Key := 0;
      keybd_event(VK_TAB,MapVirtualKey(VK_TAB,0),0,0);
    end;
    VK_F2..VK_F11: Funccode := Key - 111;
    VK_LEFT: if Sender is TOvcNumericField then Funccode := 75;
    VK_UP: Funccode := 72;
    VK_RIGHT: if Sender is TOvcNumericField then Funccode := 77;
    VK_DOWN: Funccode := 80;
    VK_PRIOR: Funccode := 73;
    VK_NEXT: Funccode := 81;
    VK_DIVIDE,191,220: with FormEditRecord^.EditPtr[CurrentSeq]^ do
      if (EditType = EditText) and ((Abs(Tcase) mod 10) = 6) then begin
      Key := 0;
      keybd_event(VK_BACK,MapVirtualKey(VK_BACK,0),0,0);
    end;
  end;
  if Funccode > 0 then begin
    Key := 0;
    ifunc := funccode;
    case ifunc of
      72: Direction := -1;
      80: Direction := 1;
    end;
    keybd_event(VK_TAB,MapVirtualKey(VK_TAB,0),0,0);
  end;
end;

procedure TxxEditForm.FieldChanged(Sender: TObject);
begin
  with FormEditRecord^,EditPtr[CurrentSeq]^ do if EditType = EditText then
    if (Abs(Tcase) > 9) then with TComponent do
    if (CurrentPos >= MaxLength) then
    keybd_event(VK_TAB,MapVirtualKey(VK_TAB,0),0,0);
  setabn(CheckAbn);
end;

procedure TxxEditForm.OKClick(Sender: TObject);
begin
  isok := true;
  ifunc := 9;
  Close;
end;

procedure TxxEditForm.CanClick(Sender: TObject);
begin
  if not askabn then exit;
  iscan := true;
  isok := true;
  Close;
end;

procedure EditSetCan(const EditP: PEdit);
begin
  with EditP^.EditWindow do begin
    iscan := true;
    isok := true;
    Close;
  end;
end;

procedure TxxEditForm.FormActive(Sender: TObject);
begin
  if isfirst then exit;
  isfirst := true;
  SetEditFocus(FormEditRecord^.EditPtr[CurrentSeq]);
end;

function EditShow(const EditP : PEdit; var esc : boolean; var xfunc : byte;
         const FuncSet : EditFuncSet; const ExitFunc : byte) : boolean;
var
   loop        : Word;
   itab        : word;
begin
  EditShow := false;
  esc := false;
  if not Assigned(EditP) then Exit;
  Application.HintHidePause := 100000;
  with EditP^,EditWindow do begin
    if EditSize < 1 then Exit;
    if Length(PEditfooter) > 0 then begin
      EditAddLine(EditP,0,PEditfooter,true,0,1,0);
      PEditfooter := '';
    end;
    if not EditInitDone then begin
      HelpFile := DefHelpFile;
      Height := (BottomRow+3) * YRes + 20;
      Width := MaxCol+20;
      OKBut.Top := (BottomRow+1)*YRes;
      OkBut.Left := 20;
      OkBut.Height := Yres+hextra;
      CanBut.Width := Canvas.TextWidth('Cancel')+wextra;
      OkBut.Width := CanBut.Width;
      CanBut.Top := OKBut.top;
      CanBut.Left := Width-Canbut.Width-25;
      CanBut.Height := Yres+hextra;
      EditInitDone := true;
      itab := 0;
      for loop := 1 to EditSize do with EditPtr[loop]^ do
        if EditType <> EditLine then begin
        case EditType of
          EditInt: IComponent.TabOrder := itab;
          EditLong: iLComponent.TabOrder := itab;
          EditFloat: FComponent.TabOrder := itab;
          EditText: TComponent.TabOrder := itab;
          EditPath: PComponent.TabOrder := itab;
          EditBool: BComponent.TabOrder := itab;
          EditCheckBox: CComponent.TabOrder := itab;
          EditPick: PiComponent.TabOrder := itab;
          EditOKButton: BuComponent.TabOrder := itab;
        end;
        Inc(itab);
      end;
      OKBut.Visible := EditOKshow;
      if EditOKShow then begin
        OkBut.TabOrder := itab;
        Inc(itab);
      end;
      CanBut.Visible := EditCanShow;
      if EditCanShow then CanBut.TabOrder := itab;
    end;
    FirstDisplay := true;
    loop := 1;
    NextSeq := 0;
    CurrentSeq := 0;
    repeat
      if Assigned(EditPtr[loop]) then begin
        if (CurrentSeq < 1) and (EditPtr[loop]^.EditType <> EditLine) then
          CurrentSeq := loop;
        NextSeq := EditDispField(EditP,EditPtr[loop]);
      end;
      if NextSeq > 0 then loop := EditFind(EditP, NextSeq,loop)
      else Inc(loop);
    until loop > EditSize;
    FirstDisplay := false;
    if EditStartSeq > 0 then CurrentSeq := EditFind(EditP,EditStartSeq,0)
    else if CurrentSeq < 1 then CurrentSeq := 1;
    EditStartSeq := 0;
    Direction := 0;
    FuncCode := 0;
    isok := false;
    iscan := false;
    EFuncSet := FuncSet;
    EExitFunc := ExitFunc;
    isfirst := false;
    ShowModal;
    esc := iscan;
    xfunc := ifunc{FuncCode};
    if EditKeepOpen then Show;
  end;
  EditKeepOpen := false;
  EditShow := true;
  EditEscFunc := 0;
  CheckAbn := false;
  EditOKShow := true;
  EditCanShow := true;
end;

end.
