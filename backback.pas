{$I debugdir.inc}
unit backback;

interface

procedure BackupDataBase(const finonly: boolean);
procedure BackupArchive;
procedure BackupClubGames(const BackCommand: char);

implementation

uses
    SysUtils,
    Classes,
    StrBoxU,
    YesNoBoxU,
    StStrL,
    vListBox,
    LHAUnit,
    backdata,
    backutil,
    mtables,
    fdbase,
    dbase,
    util1,
    util2,
    version,
    pvio,
    csdef,
    db33;

procedure BackupClubGames(const BackCommand: char);
var
   count  : integer;
begin
  topic := 235;
  get_drive;
  if not Escaped then begin
    file_name := '??????';
    setins(false);
    repeat
      ifunc := 0;
      sInsOff := true;
      StrBox(Escaped,ifunc,'Enter Game files to backup in the box below.',
         'The format is YYMMDD (Year, Month, Day)'#13
        +'Examples:     13????   For all 2013 games'#13
        +'              1302??   For all 2013 February games'#13
        +'              130608   For June 8, 2013 games'#13
        +'              ??????   For all games'#13
        +'Game files to backup (ESC to quit)',
        file_name,6,len,5,1,topic,MC,nil);
      if ((len <> 6) or (Pos(' ',file_name) > 0)) and not Escaped then begin
        RingBell(false);
        ErrBox('Files '+file_name+' is invalid',MC,0);
      end;
    until ((len = 6) and (Pos(' ',file_name) < 1)) or Escaped;
    if not Escaped then begin
      arc_name := file_name;
      for count := 1 to 6 do
        if arc_name[count] = '?' then arc_name[count] := 'X';
      file_name := CFG.GFpath+file_name+'*.AC?';
      arc_name := BackupLoc+arc_name;
      backup_files(GameFileCompress,file_name,BackCommand);
    end;
  end;
end;

procedure BackupArchive;
type
    ArcElement = Packed Record
      ArcName  : String[8];
      BSize    : longint;
      ArcAtt   : boolean;
      ArcClg   : boolean;
      ArcPer   : Boolean;
    end;
    PArcElement = ^ArcElement;
const
     MaxArcs   = 100;
var
   ArcArray     : Array[1..MaxArcs] of PArcElement;
   DotPos       : byte;
   ArcElements  : word;
   ArcPickArray : TStringList;
   done         : boolean;
   j            : word;
   DosError     : integer;
   Arcline      : String;

procedure DelFile(const FName: string);
var
   F    : file;
begin
  Assign(F,FName);
  {$I-} Erase(F); {$I+}
  if IOResult = 0 then;
end;

function TestValid(const Element: word): boolean;

procedure SetValid(var ArcBit: boolean);
begin
  ArcBit := true;
  done := true;
  TestValid := true;
  Inc(ArcArray[Element]^.BSize,Search_Rec.Size);
end;

begin
  TestValid := false;
  with Search_Rec,ArcArray[Element]^ do begin
    if Copy(Name,DotPos+1,3) = 'ATT' then SetValid(ArcAtt);
    if Copy(Name,DotPos+1,3) = 'CLG' then SetValid(ArcClg);
    if Copy(Name,DotPos+1,3) = 'PER' then SetValid(ArcPer);
  end;
end;

label
     ArcExit;
begin
  topic := 0;
  ArcElements := 0;
  FillChar(ArcArray,SizeOf(ArcArray),0);
  ArcPickArray := TStringList.Create;
  ArcPickArray.Clear;
  DosError := FindFirst(CFG.DBPath+'AR*.*',faArchive+faReadOnly,Search_Rec);
  done := DosError <> 0;
  While not done do with Search_Rec do begin
    DotPos := Pos('.',Name);
    done := false;
    if DotPos > 0 then for j := 1 to ArcElements do with ArcArray[j]^ do
      if Copy(Name,1,DotPos-1) = ArcName then if TestValid(j) then Break;
    if not done and (ArcElements < MaxArcs) then begin
      Inc(ArcElements);
      New(ArcArray[ArcElements]);
      FillChar(ArcArray[ArcElements]^,SizeOf(ArcElement),0);
      ArcArray[ArcElements]^.ArcName := Copy(Name,1,DotPos-1);
      if not TestValid(ArcElements) then begin
        Dispose(ArcArray[ArcElements]);
        Dec(ArcElements);
      end;
    end;
    DosError := FindNext(Search_Rec);
    done := DosError <> 0;
  end;
  FindClose(Search_Rec);
  for j := 1 to ArcElements do with ArcArray[j]^ do begin
    Arcline := PadL(ArcName,8);
    if ArcAtt then Arcline := Arcline+'  Attendance';
    if ArcClg then Arcline := Arcline+'  Club Game records';
    if ArcPer then Arcline := Arcline+'  Handicap % records';
    ArcPickArray.Add(ArcLine);
  end;
  if ArcElements < 1 then begin
    ErrBox('No Archives were found.',MC,0);
    goto ArcExit;
  end;
  PickTStrings := PickStrings;
  j := PickChoice(ArcElements,1,'archive to backup',ArcPickArray,MC,false,topic);
  Escaped := j < 1;
  if Escaped then goto Arcexit;
  get_drive;
  if Escaped then goto ArcExit;
  with ArcArray[j]^ do begin
    arc_name := BackupLoc+ArcName;
    AssignFile(backfile,backfilename);
    ReWrite(backfile);
    BackupSize := BSize;
    if ArcAtt then WriteLn(backfile,CFG.DBpath,ArcName,'.ATT');
    if ArcClg then WriteLn(backfile,CFG.DBpath,ArcName,'.CLG');
    if ArcPer then WriteLn(backfile,CFG.DBpath,ArcName,'.PER');
    CloseFile(backfile);
  end;
  BackupFree := CheckDiskSpace(BackupLoc);
  if BackupFree < 0 then MainHalt;
  DosError := FindFirst(arc_name+ArcSuffix,faAnyFile,Search_Rec);
  FindClose(Search_Rec);
  if DosError = 0 then begin
    if not YesNoBox(Escaped,false,'Archive backup '
      +arc_name+Arcsuffix+' already exists.'#13+
      'Confirm overwrite this backup',topic,MC,nil) then
      goto ArcExit;
    Inc(BackupFree,Search_Rec.Size);
  end;
  BackupSize := RRound(BackupSize*DataBaseCompress);
  BackupSize := (BackupSize+1000) div 1000 * 1000;
  if BackupSize > BackupFree then begin
    ErrBox('Insufficient space on the backup drive.'#13
      +'Approximately '+Long2StrL(BackupSize)+' bytes required.'#13+
      Long2StrL(BackupFree)+' bytes available.  Backup not performed.',MC,0);
    MainHalt;
  end;
  EraseBoxWindow;
  if RunLHA('a',arc_name+ArcSuffix,'',backfilename,'',true) then
    with ArcArray[j]^ do begin
    if ArcAtt then DelFile(CFG.DBPath+ArcName+'.ATT');
    if ArcClg then DelFile(CFG.DBPath+ArcName+'.CLG');
    if ArcPer then DelFile(CFG.DBPath+ArcName+'.PER');
  end;
  if not CFG.tournament then ErrBox('The backup file '+arc_name+ArcSuffix+#13
    +'is NOT part of any ACBL report and should'#13+
    'NOT be sent to ACBL.',MC,0);
ArcExit:
  for j := 1 to ArcElements do Dispose(ArcArray[j]);
end;

procedure BackupDataBase(const finonly: boolean);

procedure AddBackFile(const DBName: String);
var
   DosError : integer;
begin
  DosError := FindFirst(CFG.DBpath+DBName,faAnyFile,Search_Rec);
  FindClose(Search_Rec);
  if DosError = 0 then with Search_Rec do begin
    Inc(BackupSize,Size);
    WriteLn(backfile,CFG.DBpath,DBName);
  end;
end;

{$ifdef office}
const
     DistDB     = '730\TOURNDB';
{$endif}

var
   j    : word;
   MemphisRepName       : String[12];
   TournRepName         : PathStr;
   PPRepName            : PathStr;
   SancKey              : KeyStr;
   fSancKey             : KeyStr;
   fFilSelected         : Array[1..fMaxFilno-1] of boolean;
   RepAsked             : boolean;
   isfin                : boolean;
   DosError             : integer;
begin
  topic := 234;
  RepAsked := false;
  isfin := false;
  if not CFG.tournament then Move(ClubFiles,FilSelected,MaxFilno)
  else begin
    Move(TournFiles,FilSelected,MaxFilno);
    if FileExists(CFG.DBpath+DBNames[ClubDef_no,0]) then
      FilSelected[ClubDef_no] := true;
    for j := 1 to fMaxFilno-1 do
      fFilSelected[j] := FileExists(CFG.DBpath+fDBNames[j,0]);
  end;
  get_drive;
  if not Escaped then begin
    getadate(year,month,day);
    if not CFG.tournament then
      arc_name := BackupLoc+'DB'+numcvt(year mod 100,2,true)
      +numcvt(month,2,true)+numcvt(day,2,true)
    else begin
      MemphisRepName := '';
      TournRepName := '';
      PPRepName := '';
      {$ifdef office}
      if finonly then ok := true;
      {$else}
      if finonly or FileExists(CFG.DBpath+fDBNames[Ftinfo_no,0]) then begin
        fOpenDBFile(Ftinfo_no);
        ClearKey(fIdxKey[fFilno,1]^);
        NextKey(fIdxKey[fFilno,1]^,fRecno[fFilno],fSancKey);
        isfin := true;
      end;
      {$endif}
      if not finonly then begin
        OpenDBFile(Tourn_no);
        ClearKey(IdxKey[Filno,1]^);
        NextKey(IdxKey[Filno,1]^,Recno[Filno],SancKey);
      end;
      if ok then begin
        {$ifdef office}
        if not finonly then PickIndex[Filno] := 1;
        with CFG do if (Pos(DistDB,DBpath) > 0) and not ForceDriveA
          then begin
          j := Pos(DistDB,DBpath)+Length(DistDB);
          BackupName := Copy(DBpath,j,8);
          repeat
            j := Pos('\',BackupName);
            if j > 0 then Delete(BackupName,j,1);
          until j < 1;
          if finonly then BackupName := 'F'+BackupName;
          ifunc := 0;
          StrBox(Escaped,ifunc,'Name of the compressed backup file in '
            +BackupLoc,BackupName,7,len,5,7,topic,MC,CFG.InformColor);
          if Escaped then exit;
        end
        else if finonly then begin
          BackupName := 'F';
          ifunc := 0;
          StrBox(Escaped,ifunc,'Name of the compressed backup file in '
            +BackupLoc,BackupName,7,len,5,7,topic,MC,CFG.InformColor);
          if Escaped then exit;
        end
        else begin
        {$else}
          PickIndex[Filno] := 1;
        {$endif}
          if finonly then begin
            if not fFind_Record(' ','',
              ' Select tournament sanction # to use for backup name ',MC,
                1{$ifdef win32},false{$endif}) then begin
              Escaped := true;
              exit;
            end;
          end
          else if not Find_Record(' ','',
            ' Select tournament sanction # to use for backup name ',MC,
              1{$ifdef win32},false{$endif}) then begin
            Escaped := true;
            exit;
          end;
          if finonly then BackupName := SancDigits(Strip(Ftinfo.Sanction_no))
          else BackupName := SancDigits(Strip(Tourn.Sanction_no));
        {$ifdef office}
        end;
        {$endif}
        MemphisRepName := BackupName+'.REP';
        TournRepName := BackupName+'.HTM';
        PPRepName := BackupName+'.PP';
      end
      else begin
        RingClear(false);
        ErrBox('No tournaments found in data base.',MC,0);
        Escaped := true;
        exit;
      end;
      if not finonly then
        if fFilSelected[Ftinfo_no] and isfin then with Ftinfo do begin
        fSancKey := Tourn.Sanction_no;
        FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],fSancKey);
        if ok then begin
          fGetARec(Ftinfo_no);
          if Valu(Money_total) <> Valu(Sponsor_net) then begin
            RingClear(false);
            Escaped := not YesNoBox(Escaped,false,
              'Finacial paperwork for '+Sanction_no+' does not balance.'#13
              +'Sponsor net ='+Sponsor_net+', Money total ='+Money_total+#13+
              'Continue with data base backup',topic,MC,nil);
            if Escaped then begin
              CloseDBFile(Tourn_no);
              fCloseDBFile(Ftinfo_no);
              exit;
            end;
          end;
        end;
        {fCloseDBFile(Ftinfo_no);}
      end;
      if finonly then begin
        if Length(BackupName) > 6 then BackupName := Copy(BackupName,2,6);
        arc_name := BackupLoc+'DF'+PadChL(BackupName,'X',6);
      end
      else arc_name := BackupLoc+'D'+PadChL(BackupName,'X',7);
    end;
    AssignFile(backfile,backfilename);
    ReWrite(backfile);
    BackupFree := CheckDiskSpace(BackupLoc);
    BackupSize := 0;
    if not finonly then for j := 1 to MaxFilno do if FilSelected[j] then
      AddBackFile(DBNames[j,0]);
    if CFG.tournament then begin
      for j := 1 to fMaxFilno-1 do if fFilSelected[j] then
        AddBackFile(fDBNames[j,0]);
      if not finonly then begin
        DosError := FindFirst(CFG.DBpath+'*.SAV',faAnyFile,Search_Rec);
        while DosError = 0 do with Search_Rec do begin
          Inc(BackupSize,Size);
          WriteLn(backfile,CFG.DBpath,Name);
          DosError := FindNext(Search_Rec);
        end;
        FindClose(Search_Rec);
        ClearKey(IdxKey[Filno,1]^);
        NextKey(IdxKey[Filno,1]^,Recno[Filno],SancKey);
      end;
      if not finonly {$ifdef office}and ((Pos('730',CFG.DBpath) < 1)
        or ForceDriveA) {$endif}then while ok do begin
        MemphisRepName := SancDigits(Strip(SancKey))+'.REP';
        DosError := FindFirst(CFG.DBpath+MemphisRepName,faArchive+faReadOnly,Search_Rec);
        FindClose(Search_Rec);
        if DosError = 0 then with Search_Rec do begin
          Inc(BackupSize,Size);
          WriteLn(backfile,CFG.DBpath,MemphisRepName);
        end
        else if not RepAsked then begin
          RingClear(false);
          Escaped := not YesNoBox(Escaped,false,
            'ACBL report '+MemphisRepName+#13
            +'has not been generated.  Continue'#13+
            'with data base backup',topic,MC,nil);
          if Escaped then begin
            CloseDBFile(Tourn_no);
            if isfin then fCloseDBFile(Ftinfo_no);
            {$I-}CloseFile(backfile);
            Erase(backfile);{$I+}
            if IOResult = 0 then;
            exit;
          end
          else RepAsked := true;
        end;
        TournRepName := SancDigits(Strip(SancKey))+'.HTM';
        DosError := FindFirst(CFG.DBpath+TournRepName,faArchive+faReadOnly,Search_Rec);
        FindClose(Search_Rec);
        if DosError = 0 then with Search_Rec do begin
          Inc(BackupSize,Size);
          WriteLn(backfile,CFG.DBpath,TournRepName);
        end;
        PPRepName := SancDigits(Strip(SancKey))+'.PP';
        DosError := FindFirst(CFG.DBpath+PPRepName,faArchive+faReadOnly,Search_Rec);
        FindClose(Search_Rec);
        if DosError = 0 then with Search_Rec do begin
          Inc(BackupSize,Size);
          WriteLn(backfile,CFG.DBpath,PPRepName);
        end;
        NextKey(IdxKey[Filno,1]^,Recno[Filno],SancKey);
      end;
      if not finonly and isfin then begin
        ClearKey(fIdxKey[fFilno,1]^);
        NextKey(fIdxKey[fFilno,1]^,fRecno[fFilno],fSancKey);
        while ok do begin
          SancKey := Trim(fSancKey);
          SearchKey(IdxKey[Filno,1]^,Recno[Filno],SancKey);
          ok := ok and CompareKey(SancKey,fSancKey);
          if not ok then begin
            RingClear(false);
            Escaped := not YesNoBox(Escaped,false,
              'Sanction '+fSancKey+' in Financial Reports'#13
              +'not found in Tournament Setup.'#13
              +'Continue with data base backup',topic,MC,nil);
            if Escaped then begin
              CloseDBFile(Tourn_no);
              if isfin then fCloseDBFile(Ftinfo_no);
              {$I-}CloseFile(backfile);
              Erase(backfile);{$I+}
              if IOResult = 0 then;
              exit;
            end;
          end;
          NextKey(fIdxKey[fFilno,1]^,fRecno[fFilno],fSancKey);
        end;
      end;
      if isfin then fCloseDBFile(Ftinfo_no);
      if not finonly then CloseDBFile(Tourn_no);
    end;
    CloseFile(backfile);
    BackupFree := CheckDiskSpace(BackupLoc);
    BackupSize := RRound(BackupSize*DataBaseCompress);
    BackupSize := (BackupSize+1000) div 1000 * 1000;
    if BackupSize > BackupFree then begin
      ErrBox('Insufficient space on the backup drive.'#13
        +'Approximately '+Long2StrL(BackupSize)+' bytes required.'#13+
        Long2StrL(BackupFree)+' bytes available.  Backup not performed.',MC,0);
      Escaped := true;
      exit;
    end;
    EraseBoxWindow;
    RunLHA('a',arc_name+ArcSuffix,'',backfilename,'',true);
    DeleteFile(backfilename);
    if not CFG.tournament then MsgBox('The backup file '+arc_name+ArcSuffix+#13
      +'is NOT part of any ACBL report and should'#13+
      'NOT be sent to ACBL.',MC,0);
  end;
end;


end.
