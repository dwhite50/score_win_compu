unit backupu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TBackup = class(TForm)
    procedure dobackup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Backup: TBackup;

implementation

uses
    backup,
    version;

{$R *.DFM}

procedure TBackup.dobackup(Sender: TObject);
begin
  CallBackup;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
