unit dftestu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Testdf(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;


implementation
uses
    StStrS,
    pvio,
    Progress,
    YesNoBoxU;

{$R *.DFM}

function mbedEntryPoint(S: Pchar): Pchar; cdecl; external 'df_main.dll';

function CallDF(const S: String; var R: String):boolean;
begin
  R := mbedEntryPoint(Pchar(S));
  Result := Pos('error',R) < 1;
  if not Result then ErrBox(R,0);
end;

procedure TForm1.Testdf(Sender: TObject);
const
     {hand = 'SSA SST SS7 SS5 SS4 SS3 SDA SD3 SD2 SC6 '
    +'SHQ SHJ SH5 ES6 EDK EDQ ED7 ED6 ECQ ECJ EC8 EHT EH9 EH7 EH4 EH2 NSK NSQ '
    +'NS2 NDJ ND5 ND4 NCA NCK NC9 NC7 NC5 NC4 NC2 WSJ WS9 WS8 WDT WD9 WD8 WCT '
    +'WC3 WHA WHK WH8 WH6 WH3 ';}
     hand = 'SSK SST SS9 SHQ SH8 SH4 SH3 SD7 SD4 SCK SCJ SC9 SC5 '
           +'WSQ WS8 WS4 WS3 WS2 WDK WDQ WDT WD3 WCA WCQ WCT WC4 '
           +'NS7 NS6 NS5 NH7 NDA ND9 ND6 ND5 ND2 NC8 NC6 NC3 NC2 '
           +'ESA ESJ EHA EHK EHJ EHT EH9 EH6 EH5 EH2 EDJ ED8 EC7 ';
     {hand = 'NSQ NSJ NST NS7 NS5 NS4 NHK NHJ NH9 NH4 NDA NDK ND6 '
           +'WSA WSK WS9 WS8 WS3 WDQ WD8 WD4 WCA WCK WCQ WCT WC3 '
           +'EHT EH7 EH5 EH3 EH2 ED9 ED7 ED3 EC9 EC6 EC5 EC4 EC2 '
           +'SS6 SS2 SHA SHQ SH8 SH6 SDJ SDT SD5 SD2 SCJ SC8 SC7 ';}
    Dirs = 'NSEW';
    Suits = 'NSHDC';
var
   ret    :String;
   j      : word;
   isgood : boolean;
   level  : word;
   dir    : word;
   suit   : word;
   contract : String;
   makes    : Array[1..4,1..5] of byte;
label
     exitpoint;
begin
  if not CallDF('setlingoxEnglish',ret) then goto exitpoint;
  FillChar(makes,SizeOf(makes),0);
  for dir := 1 to 4 do for suit := 1 to 5 do begin
    level := 6;
    repeat
      Inc(level);
      ProgressBarInit('Evaluating '+Long2StrS(level-6)+Suits[suit]+' by '
        +dirs[Dir],'Deep Finesse',false);
      contract := Long2StrS(level)+Suits[suit]+dirs[Dir]+' ';
      if not CallDF('inputDeal'+contract+hand,ret) then goto exitpoint;
      j := 0;
      repeat
        Inc(j);
        ProgressBar(j,100);
        if not CallDF('cardclass0',ret) then goto exitpoint;
        isgood := Pos('g',ret) > 0;
      until isgood or (Pos('l',ret) < 1) or (j > 99);
      ProgressBarDone;
    until isgood or (level > 12);
    if isgood then Dec(level);
    if level > 6 then makes[dir,suit] := level-6;
  end;
  contract := '';
  for dir := 1 to 4 do begin
    contract := contract+dirs[dir]+' can make';
    for suit := 1 to 5 do if makes[dir,suit] > 0 then contract := contract+' '
      +Long2StrS(makes[dir,suit])+suits[suit]+';';
    contract := contract+#13;
  end;
  ErrBox(contract,0);
exitpoint:
  Application.Terminate;
end;

end.
