unit lhaUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompLHA;

type
  TLHAForm = class(TForm)
    CompLHA1: TCompLHA;
    procedure LHAFileDone(const originalFilePath, newFilePath: String;
      dateTime: Integer; mode: TCompLHAProcessMode);
    procedure LHAFileStart(const originalFilePath: String;
      var newFilePath: String; dateTime: Integer;
      mode: TCompLHAProcessMode);
    procedure LHAProgress(var PercentageDone: Integer);
  private
    { Private declarations }
    LhaTotSize  : integer;  //size in bytes of all files to process
    LhaThisSize : integer;  //size in bytes of file currently in process
    LhaProgDone : integer;  //size in bytes of files already done
  public
    { Public declarations }
  end;

function RunLHA(const CmdLine,ArcName,FileName,ListofFiles,TargetDir: String;
         const Backup: Boolean): boolean;

implementation

uses
    util2,
    StStrL,
    Progress;

{$R *.DFM}

const
     LHAForm: TLHAForm = nil;

function RunLHA(const CmdLine,ArcName,FileName,ListofFiles,TargetDir: String;
         const Backup: Boolean): boolean;
var
   SearchRec : TSearchRec;
   FindErr : integer;
   SourceDir : String;
   F         : TextFile;
begin
  if not Assigned(LHAForm) then Application.CreateForm(TLHAForm, LHAForm);
  with LHAForm,CompLHA1 do begin
    CompressionMethod := colh5;
    TargetPath := TargetDir;
    if Pos('.',ArcName) > 0 then ArchiveName := ArcName
    else ArchiveName := ArcName+'.lzh';
    LhaTotSize := 0;
    LhaProgDone := 0;
    FilesToProcess.Clear;
    if uppercase(CmdLine) = 'A' then begin
      if (Length(ListofFiles) > 0) and FileExists(ListofFiles) then begin
        AssignFile(F,ListofFiles);
        Reset(F);
        while not eof(F) do with FilesToProcess do begin
          ReadLn(F,SourceDir);
          if Length(SourceDir) > 0 then begin
            FindErr := FindFirst(SourceDir,faArchive,SearchRec);
            FindClose(SearchRec);
            if FindErr = 0 then begin
              Add(SourceDir);
              Inc(LhaTotSize,SearchRec.Size);
            end;
          end;
        end;
        CloseFile(F);
      end
      else begin
        SourceDir := fixpath(JustPathNameL(FileName));
        FindErr := FindFirst(FileName,faArchive,SearchRec);
        while FindErr = 0 do with FilesToProcess do begin
          Add(SourceDir+SearchRec.Name);
          Inc(LhaTotSize,SearchRec.Size);
          FindErr := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;
      ProgressBarInit('Creating '+ArchiveName,'Compressing',false);
      Compress;
    end
    else begin
      Scan;
      for FindErr := 0 to FileList.Count-1 do
        Inc(LhaTotSize,TCompLHAFileInfo(FileInformation[FindErr]).FullSize);
      if uppercase(CmdLine) = 'T' then begin
        ProgressBarInit('Verifying '+ArchiveName,'Verifying',false);
        Verify;
      end
      else begin
        if (Length(ListofFiles) > 0) and FileExists(ListofFiles) then begin
          AssignFile(F,ListofFiles);
          Reset(F);
          FilesToProcess.Clear;
          LhaTotSize := 0;
          while not eof(F) do with FilesToProcess do begin
            ReadLn(F,SourceDir);
            if Length(SourceDir) > 0 then begin
              Add(SourceDir);
              Inc(LhaTotSize,TCompLHAFileInfo(FileInformation[Count-1]).FullSize);
            end;
          end;
          CloseFile(F);
        end;
        ProgressBarInit('Extracting from '+ArchiveName,'Expanding',false);
        Expand;
      end;
    end;
    ProgressBarDone(false);
    Free;
  end;
  Result := true;
  LHAForm := nil;
  if Backup then RunLHA := RunLHA('T',ArcName,'','','',false);
end;

procedure TLHAForm.LHAFileDone(const originalFilePath, newFilePath: String;
  dateTime: Integer; mode: TCompLHAProcessMode);
begin
  Inc(LhaProgDone,LhaThisSize);
end;

procedure TLHAForm.LHAFileStart(const originalFilePath: String;
  var newFilePath: String; dateTime: Integer; mode: TCompLHAProcessMode);
var
   SearchRec       : TSearchRec;
begin
  with CompLHA1 do case mode of
    cmLHACompress: begin
      FindFirst(originalFilePath,faArchive,SearchRec);
      LhaThisSize := SearchRec.Size;
      FindClose(SearchRec);
    end;
    cmLHAExpand: LhaThisSize :=
      TCompLHAFileInfo(FileInformation[FileInformationIndex]).FullSize;
  end;
end;

procedure TLHAForm.LHAProgress(var PercentageDone: Integer);
begin
  ProgressBar(LhaProgDone+(PercentageDone*LhaThisSize div 100),LhaTotSize);
end;

end.
