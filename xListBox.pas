unit ListBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcVLB, util1;

type
  TListBoxForm = class(TForm)
    OKButton: TButton;
    CanButton: TButton;
    ListBox1: TListBox;
    HelpBut: TButton;
    procedure Acceptit(Sender: TObject);
    procedure Cancelit(Sender: TObject);
    procedure EnableOk(Sender: TObject);
    procedure DoHelp(Sender: TObject);
    procedure CloseClick(Sender: TObject; var Action: TCloseAction);
    procedure ListEnter(Sender: TObject);
    {procedure ListBoxGetItem(Sender: TObject; Index: Integer;
      var ItemString: String);}
  private
    { Private declarations }
  public
    { Public declarations }
   Choice : integer;
   ChoiceMade : boolean;
  end;


function Pickchoice(const num_choices, def_choice: word;
         const title : String; const Ptextx : Pointer;
         const doabn : boolean; const HelpTopic: Word) : smallint;

type
    TPickChoice      = (PickChar,PickStrings,PickLStrings);
                     //Array of char, Strings, Array of string
const
     PickTStrings    : TPickChoice = PickChar;

var
   PickChoiceLine    : String;

implementation
uses
    csdef,
    util2;

{$R *.DFM}

const
  ListBoxForm: TListBoxForm = nil;

var
   Topic : word;

procedure TListBoxForm.Acceptit(Sender: TObject);
begin
  ChoiceMade := true;
  Close;
  Choice := ListBox1.ItemIndex+1;
end;

procedure TListBoxForm.Cancelit(Sender: TObject);
begin
  Close;
  Choice := 0;
end;

procedure TListBoxForm.EnableOk(Sender: TObject);
begin
  OkButton.Enabled := true;
end;

function Pickchoice(const num_choices, def_choice: word;
         const title : String; const Ptextx : Pointer;
         const doabn : boolean; const HelpTopic: Word) : smallint;
const
     wslush = 30;
var
   j     : word;
   MinWidth : integer;
   MaxTextWidth : integer;
   curtextwidth : integer;
   Ptext        : PPCharArray;
   PStext       : PStringArray absolute Ptext;
   PStr         : PString absolute Ptext;
   String_ptr : PString;
   LString_ptr : LongInt absolute String_ptr;
begin
  if not Assigned(ListBoxForm) then
    Application.CreateForm(TListBoxForm, ListBoxForm);
  PickChoiceLine := '';
  Ptext := Ptextx;
  with ListBoxForm do begin
    ChoiceMade := false;
    MinWidth := OKButton.width+CanButton.Width+20;
    if HelpTopic > 0 then Inc(MinWidth,HelpBut.width+10);
    MaxTextWidth := MinWidth;
    Font.Size := ViewFontSize;
    Caption := 'Select '+title;
    CurTextWidth := Canvas.TextWidth(Caption)+wslush;
    if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
    if def_choice <= num_choices then Choice := def_choice
    else Choice := 0;
    with ListBox1 do begin
      Font.Size := ViewFontSize;
      Canvas.Font.Size := ViewFontSize;
      if PickTStrings = PickStrings then Items := Ptextx;
      for j := 1 to num_choices do begin
        case PickTStrings of
          PickChar: Items.Add(Ptext[j]);
          PickLStrings: Items.Add(PsText[j]);
        end;
        CurTextWidth := Canvas.TextWidth(Items[j-1])+wslush;
        if CurTextWidth > MaxTextWidth then MaxTextWidth := CurTextWidth;
      end;
      if Choice > 0 then begin
        ItemIndex := def_choice-1;
//        TopIndex := ItemIndex;
//        Selected[ItemIndex] := true;
        OKButton.Enabled := true;
      end
      else begin
        ItemIndex := -1;
        OKButton.Enabled := false;
      end;
      Height := num_choices * ItemHeight+4;
      if Height+ItemHeight*4+100 > Screen.Height then
        Height := Screen.Height-100-ItemHeight*4;
      Width := MaxTextWidth+10;
      if Width+10 > Screen.Width then Width := Screen.Width-10;
    end;
    Topic := Helptopic;
    HelpBut.Visible := topic > 0;
    HelpContext := topic;
    Height := ListBox1.Height+ListBox1.ItemHeight*2+OKButton.Height+20;
    Width := ListBox1.Width+10;
    OKButton.Top := ListBox1.Height+15;
    CanButton.Top := OKButton.Top;
    HelpBut.Top := OKbutton.Top;
    Okbutton.Left := 16;
    if topic > 0 then begin
      HelpBut.Left := Width-HelpBut.Width-14;
      CanButton.Left := (Width-CanButton.Width) div 2;
    end
    else CanButton.Left := Width-CanButton.Width-10;
    if AllowMinimize then Bordericons := [biSystemMenu,biMinimize];
    ShowModal;
    Result := Choice;
    if Choice > 0 then PickChoiceLine := ListBox1.Items[Choice-1];
    Free;
  end;
  PickTStrings := PickChar;
  ListBoxForm := nil;
end;

{procedure TListBoxForm.ListBoxGetItem(Sender: TObject; Index: Integer;
  var ItemString: String);
begin
  ItemString := Items[Index+1];
end;}

procedure TListBoxForm.DoHelp(Sender: TObject);
begin
  if Topic > 0 then begin
  end;
end;

procedure TListBoxForm.CloseClick(Sender: TObject;
  var Action: TCloseAction);
begin
  Close;
  if not ChoiceMade then Choice := 0;
end;

procedure TListBoxForm.ListEnter(Sender: TObject);
var
   ItemPos : integer;
   ScrollAmount : integer;
begin
  with ListBox1 do begin
    ItemPos := (ListBox1.ItemIndex-ListBox1.TopIndex)*ListBox1.ItemHeight;
    ScrollAmount := ItemPos-3*ListBox1.ItemHeight;
    if ItemPos > ListBox1.Height then ListBox1.ScrollBy(0,ScrollAmount);
  end;
end;

end.
