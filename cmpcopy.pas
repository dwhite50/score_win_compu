{.$define test}
{.$define pickfolder}
{.$define image}
{.$define repmponly}
unit cmpcopy;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompLHA, viewbox, StBase, StSpawn, Psock, NMFtp, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    CompLHA1: TCompLHA;
    StSpawnApplication1: TStSpawnApplication;
    NMFTP1: TNMFTP;
    Label1: TLabel;
    StatusBar1: TStatusBar;
    procedure Docmpcopy(Sender: TObject);
    procedure TaskComplete(Sender: TObject);
    procedure TaskError(Sender: TObject; Error: Word);
    procedure TaskTimeOut(Sender: TObject);
    procedure FTPerror(var Handled: Boolean; Trans_Type: TCmdType);
    procedure FTPConnect(Sender: TObject);
    procedure FTPDisconnect(Sender: TObject);
    procedure LHAfiledone(const originalFilePath, newFilePath: String;
      dateTime: Integer; mode: TCompLHAProcessMode);
  private
    { Private declarations }
    isTaskComplete : boolean;
    isTaskError    : boolean;
    isTaskTimeOut  : boolean;
    TaskErrorNo    : word;
    Connected      : boolean;
    FTPis400       : boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses
    mpdef,
    mtables,
    db33,
    StStrL,
    StrBoxU,
    dbase,
    txtview1,
    pvio,
    VListBox,
    FOpen,
    util1,
    util2,
    history,
    YesNoBoxU,
    csdef,
    version,
    cmpunit;

const
     {$ifdef test}
     RepFileName = 'XCLUB.PTS';
     OldRepFileName = 'XSCORE';
     FinRepFileName = 'XCLUB.FN';
     Archivepath = 'C:\TEMP\';
     BadPath     = 'C:\TEMP\BAD\';
     BadClubFname  = 'C:\TEMP\MAIL\BADCLUB.TXT';
     BadClubTname  = 'C:\TEMP\MAIL\BADCLUB.TMP';
     CheckFname  = 'C:\TEMP\MAIL\CHECK.TXT';
     CheckTname  = 'C:\TEMP\MAIL\CHECK.TMP';
     LanPlPath   = 'D:\ACBLSCOR\PLAYER\';
     SharedFolder = 'C:\TEMP\';
     {$else}
     RepFileName = 'CLUB.PTS';
     OldRepFileName = 'SCORE';
     FinRepFileName = 'CLUB.FN';
     Archivepath = 'E:\DEPT\550\ACBLSCOR\';
     BadPath = 'E:\DEPT\550\ACBLSCOR\CLUBREP\BAD\';
     BadClubFname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\BADCLUB.TXT';
     BadClubTname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\BADCLUB.TMP';
     CheckFname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\CHECK.TXT';
     CheckTname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\CHECK.TMP';
     AS400Command = 'RCMD sbmjob cmd(CALL CLUBLIB/CLC020) job(clubpoints)';
//     AS400Host = '192.168.20.12';
     AS400Host = '192.0.7.191';
     AS400UserID = 'acblscore';
     AS400Password = 'acblscore';
     LanPlPath   = 'E:\ACBLSCOR\PLAYER\';
     SharedFolder = '\\QACBL\ACBLIFS\';
//     SharedFolder = '\\QACBLB\ACBLIFS\';
     {$endif}
     {$ifdef image}
     ImageHost = 'acbl-pwise1';
     ImageUserID = 'acblscore';
     ImagePassword = 'acblscore';
     ImageDir = 'clubfin';
     {$endif}
     ScoreDir   = 'C:\ACBLSCOR\';
     FinTextName  = 'CLUBFIN.';
     FinTextFileName = FinTextName+'TXT';
     LhaEx        = '.LZH';
//     NewClubMask  = '????????*'+LhaEx;
     NewClubMask  = '????????'+LhaEx;
     DupClubMask  = '????????-?'+LhaEx;
     OldClubMask  = '??????.???';
     outpath = 'C:\CLUBREPX\';
     FileListName = outpath+'400FILES.TXT';
     SharedFolderFileName = SharedFolder+RepFileName;
     OldSharedFolderFileName = SharedFolder+OldRepFileName;
     FinSharedFolderFileName = SharedFolder+FinRepFileName;
     TempFileName = outpath+'CLUB.TMP';
     NonExt = '.TXT';
     CsvExt = '.CSV';
     CFileTypes : Array[0..4] of String[1] = ('','M','F','N','E');
     ClubFileMask  = '??????.';
     EmailLogExt  = '.log';
     SavePath = outpath+'BACK\';
     AppendFileName = outpath+RepFileName;
     FinAppendFileName = outpath+FinRepFileName;
     FinSaveFileName = SavePath+'FCLUB.SAV';
     islhafile : boolean = false;
     ClubrepPath = Archivepath+'CLUBREP\';
     ClubSavePath = Archivepath+'REPSAVE\';
     OldAppendFileName = outpath+OldRepFileName;
     SaveFileName = SavePath+'CLUB.SAV';
     OldSaveFileName = SavePath+'SCORE.SAV';
     NonMemPath = Archivepath+'NONMEMB\';
     VerPath    = Archivepath+'VERSION\';
     EmailPath = ClubrepPath+'MAIL\';
     ProcessYears = 1;
     ProcessMonths = ProcessYears * 12 + 1;

     MaxLocPick = 6;
     inLocT : Array[1..MaxLocPick] of PChar =
             ('A: Diskette drive',
              'B: Diskette drive',
              'Good report files',
              'Other',
              'Bad Email report files',
              'Second (Duplicate) report files'
              );

     inLocP : Array[1..MaxLocPick] of String = ('A:\','B:\',
            ClubrepPath,'G:\',BadPath,ClubrepPath);
     inLocMask : Array[1..MaxLocPick] of String[5] =
             ('*.*',
              '*.*',
              '*.*',
              '*.*',
              '*.LZH',
              '????????-?.LZH'
              );

     MPloc   = 14;
     MPlen   = 5;

var
   inrec       : String[254];
   ChangeMPS   : Array[1..5] of boolean;
   TotMPS      : Array[1..5] of longint;
   inPath      : String;
   OldTotMPS   : longint;
   escaped     : boolean;
   InLoc       : byte;
   quarintine  : boolean;
   Search_Rec  : TSearchRec;
   inpathsave     : String;
   inname      : String;
   outrec      : String[254];
   recs        : longint;
   EventRecs   : longint;
   ERec        : longint;
   DetailRecs  : longint;
   intype      : byte;
   outtype     : byte;
   ClubMpRec    : PClubMPRec;
   club         : String[6];
   ClubMPHeader   : TClubMPRec;
   ClubMPEvent    : TClubMPRec;
   Lhaname      : String;
   ClubFinRec   : PClubFinRec;
   ClubFinMast1 : TClubFinRec;
   ClubFinMast2 : TClubFinRec;
   ClubFinSess  : TClubFinRec;
   ClubFinDet   : TClubFinRec;
   ClubSaveRec  : TClubFinRec;
   CFinDetArray : Array[1..100] of PClubFinRec;
   hrating        : byte;
   LastPlayer     : String;
   ThisPlayer     : String;
   RankStr        : String;
   LastMPs        : Array[1..4] of longint;
   ThisStrat      : byte;
   ThisRankl      : word;
   ThisRankh      : word;
   ThisSect       : String;
   ThisDir        : byte;
   ThisPair       : word;
   ThisPer        : real;
   ThisBds        : word;
   ThisMPtype     : char;
   MaxHighColor   : byte;
   EvDate         : String[8];
   Entries        : byte;
   ImpDrive       : char;
   AnyFiles       : boolean;
   mprec             : packed Record
     mplayer         : String[7];
     mclub           : String[6];
     mpts            : String[5];
     mmonth          : String[4];
   end;

const
     months     : Array[1..12] of String[3] =
                ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct',
                 'Nov','Dec');
     Pcolors     : Array[1..5] of String[7] =
                 (' Black',' Red',' Gold',' Silver',' Total');

var
   date              : String[12];
   lskip             : byte;
   NClubNo           : String[6];
   NClubName         : String[40];
   NClubDate         : String[7];
   {bad49    : boolean;
   check49  : boolean;}
   gtables  : word;
   Foa      : real;
   LowR,HighR : word;
   TMP        : real;
   Itmp       : integer;

procedure DelTempDirFiles;
var
   Finderr : integer;
   Delok   : boolean;
begin
  CreateDir(TempDir);
  repeat
    Finderr := FindFirst(fixpath(TempDir)+'*.*',faArchive,Search_Rec);
    repeat
      Delok := (Finderr <> 0)
        or DeleteFileAttr(fixpath(TempDir)+Search_Rec.Name);
      if not Delok and (Finderr = 0) then Finderr := FindNext(Search_Rec);
    until Delok or (Finderr <> 0);
    FindClose(Search_Rec);
  until Finderr <> 0;
end;

procedure MakeOutpathDirs;
begin
  CreateDir(Copy(outpath,1,Length(outpath)-1));
  CreateDir(Copy(Savepath,1,Length(Savepath)-1));
end;

procedure SetPageNo;
begin
  if Destination = 2 then begin
    if PageNo>0 then ppage
    else Inc(PageNo);
    ReportLine(0,pnone,1,#13);
  end
  else ppage;
end;

procedure MPEndPage; Far;
{var
   club : String;}
begin
  SetPageNo;
  ReportLine(0,ppica,1,CharStrL(' ',20)+'CLUB MASTERPOINT REPORT'
    +CharStrL(' ',20)+'Page '+Long2StrL(pageno));
  if intype = mpftNew then with ClubMPHeader do begin
//    club := ChrStr(@ClubNum,6);
    ReportLine(lskip,pnone,1,CharStrL(' ',20)+'Processed by ACBLscore '
      +ChrStr(@version,12));
    ReportLine(lskip,pnone,1,'Club Name: '+ChrStr(@ClubName,40)
      +' Month: '+date);
    ReportLine(lskip,pnone,1,'Club Number: '+club
      +'    District: '+GetClubUnitDist(club,ChrStr(@DistNum,2),true)
      +'    Unit: '+GetClubUnitDist(club,ChrStr(@UnitNum,3),false));
  end
  else begin
    ReportLine(lskip,pnone,1,CharStrL(' ',20)+'Internet Club');
    ReportLine(lskip,pnone,1,'Club Number: '+club
      +'    District: '+GetClubUnitDist(club,ChrStr(@DistNum,2),true)
      +'    Unit: '+GetClubUnitDist(club,ChrStr(@UnitNum,3),false)
      +'    Month: '+date);
  end;
  if (PageNo < 2) or (ChrNum(@ClubMPEvent.Erating,2) = 16) then
    ReportLine(lskip,pelite,1,'NAME'+CharStrL(' ',16)+'PLAYER    POINTS');
  if PageNo > 1 then NewLine(1);
end;

procedure FEndPage; Far;
var
   club : String;
begin
  SetPageNo;
  with ClubFinMast1 do begin
    club := ChrStr(@ClubNum,6);
    if Destination = 3 then begin
      ReportLine(1,pnone,1,'=Club '+ChrStr(@ClubNum,6)+'==Month '
        +ChrStr(@RepYear,4)+ChrStr(@RepMonth,2)+CharStrL('=',54));
      NewLine(2);
    end;
    if CountryCode = '2' then PrintTo(20,' ')
    else PrintTo(30,' ');
    ReportLine(0,pnone,1,'MONTHLY CLUB REPORT');
    if CountryCode = '2' then ReportLine(0,pnone,1,' - CANADIAN CLUB');
    PrintTo(70,' ');
    ReportLine(0,pnone,1,'Page '+Long2StrL(pageno));
    NewLine(lskip);
    PrintTo(30,' ');
    ReportLine(0,pnone,1,'Processed by ACBLscore '+ChrStr(@version,12));
    ReportLine(lskip,pnone,1,'Name of club: '+ChrStr(@ClubName,40));
    PrintTo(64,' ');
    ReportLine(0,pnone,1,'Club # '+club);
    ReportLine(lskip,pnone,1,'Manager: '+ChrStr(@ManagName,30)
      +' Dist: '+GetClubUnitDist(club,ChrStr(@DistNum,2),true)
      +' Unit: '+GetClubUnitDist(club,ChrStr(@UnitNum,3),false));
    PrintTo(64,' ');
    ReportLine(0,pnone,1,'Month: '+Months[ChrNum(@RepMonth,2)]+' '
      +ChrStr(@RepYear,4));
    ReportLine(lskip,pnone,1,'Address: '+ChrStr(@ClubStreet,26)
      +' '+ChrStr(@ClubCity,16)+' '+ChrStr(@ClubState,2)+' '
      +ChrStr(@ClubZip,10));
  end;
  if PageNo > 1 then NewLine(1);
end;

{$I gametype.inc}

procedure TForm1.Docmpcopy(Sender: TObject);
function RunLHA(const ArcName,OutPath: String): boolean;
begin
  with CompLHA1 do try
    FilesToProcess.Clear;
    FilesToProcess.Add('*.*');
    ArchiveName := ArcName;
    TargetPath := Outpath;
    Expand;
    Result := true;
  except
    Result := false;
  end;
end;

var
   needff       : boolean;

function GetGameTypeStr(const GameType: char): String;
begin
  case GameType of
    'O': GetGameTypeStr := 'Open';
    'I': GetGameTypeStr := 'Inv ';
    'N': GetGameTypeStr := 'Nov ';
    'B': GetGameTypeStr := 'B+  ';
    else GetGameTypeStr := '    ';
  end;
end;

function GetPairTeamStr(const pt: char): String;
begin
  case pt of
    'P': GetPairTeamStr := 'Pair';
    'T': GetPairTeamStr := 'Team';
    'I': GetPairTeamStr := 'Ind ';
    else GetPairTeamStr := '    ';
  end;
end;

procedure PrintFinance{(const cover: boolean)};

var
   SessRecs     : word;
   SumRecs      : word;
   Sess         : word;
   SectRecs     : word;
   Sect         : word;
   sum_done     : boolean;
   sess_tables  : real;
   Num_ACBL     : word;
   Num_non      : word;

procedure InitClubFinPrint;
begin
  with ClubFinMast2 do begin
    SessRecs := ChrNum(@ReportedSess,2)+20;
    if NewRepType then SumRecs := ChrNum(@NumSum,2)
    else SumRecs := 0;
    club := ChrStr(@ClubNum,6);
    date := Months[Ival(ChrStr(@RepMonth,2))]+' '+ChrStr(@RepYear,4);
  end;
  if (Destination = 3) and needff then ReportLine(1,pnone,1,#12);
  PageNo := 0;
  newpage;
end;

procedure print_sum;
var
   games        : word;
   xgames       : word;
   xtables      : real;
   total_tables,
   xtotal_tables : real;
   Cnum          : string;
   j             : word;
   LastRating    : integer;
   LastFeeCode   : integer;
   CurRating     : byte;
   CurFeeCode    : byte;
   Rate          : real;
   RegFees       : real;
   Fees          : real;
   included      : boolean;

procedure ShowRegFees;
begin
  if RegFees = 0 then exit;
  ReportLine(1,pnone,1,CharStrL(' ',36)+CharStrL('-',35));
  ReportLine(1,pnone,1,PadL('Regular club fees total',48)+'$'
    +Real2StrL(RegFees,7,2)+' ----> $'+Real2StrL(RegFees,7,2));
  RegFees := 0;
end;

begin
  if sum_done then exit;
  if {cover or }NewReptype then NewLine(1)
  else begin
    if RepLine > PageLength-14 then NewPage
    else case Destination of
      1,3: NewLine(2);
      2: NewLine(3);
    end;
    if Destination = 2 then while RepLine < PageLength-16 do NewLine(1);
  end;
  {if not cover then }with ClubFinMast2 do begin
    xgames := ChrNum(@TotalGames,4);
    xtotal_tables := ChrReal(@TotalTables,5,1);
    ReportLine(1,pnone,1,'Total all games '+Long2StrL(xgames)
      +'   Tables all games'+Real2StrL(xtotal_tables,7,1));
  end;
  if NewRepType then with ClubFinRec^ do begin
    LastRating := -1;
    LastFeeCode := -1;
    included := false;
    RegFees := 0;
    for j := 1 to SumRecs do begin
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      Inc(recs);
      if not ValidField(@RecType,1,'5') then exit;
      if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
      CurRating := ChrNum(@SRating,2);
      CurFeeCode := ChrNum(@SFeeCode,1);
      games := ChrNum(@SGames,4);
      xTables := ChrReal(@STables,6,1);
      Rate := ChrReal(@SRate,5,2);
      Fees := ChrReal(@SFees,7,2);
      if (CurRating <> LastRating) or (CurFeeCode <> LastFeeCode) then begin
        if LastRating < 0 then begin
          NewLine(1);
          if (CurRating < 1) or (CurFeeCode in [0,1,5]) then begin
            ReportLine(1,ppica,1,'Summary of fees included with this report:');
            included := true;
          end
          else ReportLine(1,ppica,1,
            'Summary of special game fees not included with this report:');
        end;
        if not included and (CurRating < 1) then begin
          if LastRating > 0 then begin
            ReportLine(1,pnone,1,CharStrL('-',71));
            NewLine(1);
          end;
          ReportLine(1,ppica,1,'Summary of fees included with this report:');
          included := true;
        end;
        if (LastRating = 0) and (CurRating > 0) then begin
          ShowRegFees;
          ReportLine(1,pnone,1,CharStrL(' ',48)+CharStrL('-',23));
        end;
        if not included and (CurFeeCode <> LastFeeCode)
          and (CurFeeCode in [2,3,4]) then begin
          case CurFeeCode of
            2: ReportLine(2,pnone,1,'Separate cheque(s) in Canadian funds to ACBL:');
            3: ReportLine(2,pnone,1,'Forwarded to Canadian Bridge Federation:');
            4: ReportLine(2,pnone,1,'Local Charity:');
          end;
        end;
        LastFeeCode := CurFeeCode;
        LastRating := CurRating;
      end;
      if CurRating > 0 then begin
        if CurFeeCode < 5 then ReportLine(1,pnone,1,
          PadL(StrPas(RatingText[CurRating])+' Tables',45)+Real2StrL(xtables,7,1)
          +' @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2))
        else ReportLine(1,pnone,1,PadL(StrPas(RatingText[CurRating])
          +' Sanction fee Tables',45)+Real2StrL(xtables,7,1)
          +' @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2))
      end
      else begin
        case CurFeeCode of
          1: ReportLine(1,pnone,1,PadL('Total Games',30)+numcvt(games,5,false)
            +'   @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2));
          2: ReportLine(1,pnone,1,PadL('Regular Tables',30)+Real2StrL(xtables,7,1)
            +' @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2));
          3: ReportLine(1,pnone,1,PadL('Short game Tables',30)+Real2StrL(xtables,7,1)
            +' @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2));
          4: ReportLine(1,pnone,1,PadL('Late Fees',30)+numcvt(games,5,false)
            +'   @ $'+Real2StrL(Rate,5,2)+'= $'+Real2StrL(Fees,7,2));
        end;
        RegFees := RegFees+Fees;
      end;
    end;
    ShowRegFees;
    ReportLine(1,pnone,1,CharStrL(' ',60)+'-----------');
    ReportLine(1,pnone,1,PadL('Total Remittance',60)+'US $'
      +Real2StrL(ChrReal(@TotalFees,6,2),7,2));
    ReportLine(1,pnone,1,CharStrL(' ',60)+'-----------');
  end
  else with ClubFinMast2 do begin
    games := ChrNum(@ChargeableGames,4);
    ReportLine(1,pnone,1,'Total Games'+numcvt(games,7,false)
      +'  @  $'+Real2StrL(ChrReal(@GameSancFee,4,2),4,2)
      +'  $'+Real2StrL(ChrReal(@GameFees,6,2),7,2));
    if Destination in [1,2] then NewLine(1);
    total_tables := ChrReal(@ChargeableTables,5,1);
    ReportLine(1,pnone,1,'Total Tables'+Real2StrL(total_tables,7,1)+' @  $'
      +Real2StrL(ChrReal(@TableSancFee,4,2),4,2)+'  $'
      +Real2StrL(ChrReal(@TableFees,6,2),7,2));
    if Destination in [1,2] then NewLine(1);
    if ChrNum(@LateDays,3) > 0 then begin
      ReportLine(1,pnone,1,'Late Fees   '
        +numcvt(ChrNum(@LateDays,3),6,false)+'  @  $'
        +Real2StrL(ChrReal(@LateDayRate,4,2),4,2)+'  $'
        +Real2StrL(ChrReal(@LateFees,6,2),7,2));
      if Destination in [1,2] then NewLine(1);
    end;
    ReportLine(1,pnone,1,'Total Remittance           US $'
      +Real2StrL(ChrReal(@TotalFees,6,2),7,2));
    if Destination in [1,2] then NewLine(1);
  end;
  with ClubFinMast2 do if PaymentType = '1' then begin
    DeCodeNumber(ChrStr(@CreditCardNo,16),Cnum);
    ReportLine(1,pnone,1,'Card type: '+StrPas(CCNames[Ival(CreditCardType)]));
    if Destination = 2 then ReportLine(0,pnone,1,'   Card #: '+Cnum);
    if Destination in [1,2] then NewLine(1);
    ReportLine(1,pnone,1,'Card expires: '+ChrStr(@ExpiryDate[5],2)+'/'
      +ChrStr(@ExpiryDate,4)+'   Name on card: '+ChrStr(@NameOnCard,25));
    if Not Empty(ChrStr(@ZipCode,6)) then
      ReportLine(0,pnone,1,' Zip/Postal code: '+ChrStr(@ZipCode,6));
  end
  else begin
    ReportLine(1,pnone,1,'Payment type:  Check');
  end;
  quarintine := (inloc in [5]) and (Pos('CLUBREP\BAD',UpperCase(inpathsave)) > 0);
//  if cover and (Destination in [1,2]) then NewLine(1);
  sum_done:=true;
end;

procedure check_tables;
var
   itables      : real;
   NumUnrep     : word;
   j            : word;
begin
  with ClubFinSess do begin
    itables:= Sess_tables;
    if itables > 0 then begin
      if NewRepType then begin
        ReportLine(1,pnone,1,'Tot'+Real2StrL(itables,6,1));
        if RepVersion > 713 then ReportLine(0,pnone,1,numcvt(Num_ACBL,4,false)
          +numcvt(Num_non,5,false));
      end
      else ReportLine(1,pnone,1,'Total'+Real2StrL(itables,6,1));
      sess_tables := 0;
      NumUnrep := ChrNum(@NumUnrepDates,3);
      if NumUnrep > 0 then begin
        ReportLine(0,pnone,1,'  Unreported game dates: '
          +Months[ChrNum(@RepMonth,2)]);
        for j := 1 to NumUnrep do
          ReportLine(0,pnone,1,' '+Long2strL(ChrNum(@UnRepDates[j,1],2)));
      end;
    end;
  end;
end;

procedure PrintHeader;
var
   lines_needed      : byte;
   clubsess          : byte;
begin
  Num_ACBL := 0;
  Num_non := 0;
  with ClubFinSess do begin
    clubsess := ChrNum(@SessNum,2);
    Lines_needed := ChrNum(@NumReportedSecs,3)+10;
    check_tables;
    if not sum_done and (pageno = 1)
      and (RepLine > PageLength-lines_needed-15) then begin
      print_sum;
      NewPage;
    end;
    if RepLine > PageLength-lines_needed then NewPage;
    NewLine(1);
    ReportLine(1,pnone,1,'Session:'+numcvt(clubsess,2,false)+'  Time: '
      +ChrStr(@GameTime,5)+'  Sanctioned Game(s):');
    if Pos('Y',ChrStr(@GameType,4)) > 0 then begin
      if GameType[1] = 'Y' then ReportLine(0,pnone,1,' Open');
      if GameType[2] = 'Y' then ReportLine(0,pnone,1,' Invitational');
      if GameType[3] = 'Y' then ReportLine(0,pnone,1,' Newcomer');
      if GameType[4] = 'Y' then ReportLine(0,pnone,1,' Bridge+');
    end
    else ReportLine(0,pnone,1,' NONE');
    if NewRepType then begin
      if RepVersion > 713 then begin
        ReportLine(1,pelite,1,
'---- ---- --------- --- ---- ---- ----- ------ ------------- ----- ------- -----------');
        ReportLine(1,pnone,1,
'Date|    | Players |   | Fee|Reg.|Spec.| Spec.| Masterpoint |Type/|Special|');
        ReportLine(1,pnone,1,months[ChrNum(@RepMonth,2)]
  +' |Tab.|Memb| NM |Bds|Code|Rate|Rate | Fees |   Limits    | Sec | Event |Director');
        ReportLine(1,pnone,1,
'---- ---- ---- ---- --- ---- ---- ----- ------ ------------- ----- ------- -----------');
      end
      else begin
        ReportLine(1,pelite,1,
'---- ---- --- ---- ---- ----- ------ ------------- ----- ------- -----------');
        ReportLine(1,pnone,1,
'Date|    |   | Fee|Reg.|Spec.| Spec.| Masterpoint |Type/|Special|');
        ReportLine(1,pnone,1,months[ChrNum(@RepMonth,2)]
  +' |Tab.|Bds|Code|Rate|Rate | Fees |   Limits    | Sec | Event |Director');
        ReportLine(1,pnone,1,
'---- ---- --- ---- ---- ----- ------ ------------- ----- ------- -----------');
      end;
    end
    else begin
      ReportLine(1,pnone,1,
'---- ------ ----- ------------- ----- --- ------- -------- -----------------');
      ReportLine(1,pnone,1,
'Date|      |     | Masterpoint |     |   |Special|Strat / |');
      ReportLine(1,pnone,1,months[ChrNum(@RepMonth,2)]
  +' |Tables|Game |   Limits    |Type |Sec| Event |Handicap|Director');
      ReportLine(1,pnone,1,
'---- ------ ----- ------------- ----- --- ------- -------- -----------------');
    end;
  end;
end;

procedure PrintDetail;
var
   j            : word;
   MpStr        : String[50];
   ThisMonth    : boolean;
   gdate        : string[8];
   grating       : byte;
   isreg         : boolean;
   isext         : boolean;
   nplayers      : word;

procedure AddMPStr(const Strat_mp: String; const doslash: boolean);
var
   MPLimit : word;
begin
  MPLimit := Ival(Strat_mp);
  if doslash then begin
    if MPLimit < 1 then exit;
    MPStr := MPStr+'/'+Long2StrL(MPLimit);
    if MPLimit mod 10 = 1 then MPStr := MPStr+'+';
  end
  else begin
    if MPLimit < 1 then MPStr := MPStr+'None'
    else begin
      MPStr := MPStr+Long2StrL(MPLimit);
      if MPLimit mod 10 = 1 then MPStr := MPStr+'+';
    end;
  end;
end;

begin
  with ClubFinDet do begin
    gdate := ChrStr(@GameDate,8);
    ThisMonth := copy(gdate,5,2) = ChrStr(@RepMonth,2);
    if ThisMonth then ReportLine(1,pnone,1,' '+Copy(gdate,7,2)+'  ')
    else ReportLine(1,pnone,1,copy(gdate,5,2)+'/'+Copy(gdate,7,2));
    grating := ChrNum(@Rating,2);
    if grating = 0 then ReportLine(0,pnone,1,CharStrL(' ',19)+ChrStr(@comment,36))
    else begin
      Sess_tables := Sess_tables + ChrReal(@tables,3,1);
      if NewReptype then begin
        ReportLine(0,pnone,1,Real2StrL(ChrReal(@tables,3,1),4,1));
        if RepVersion > 713 then begin
          nplayers := ChrNum(@ACBLPlayers,3);
          if nplayers > 0 then ReportLine(0,pnone,1,numcvt(nplayers,4,false))
          else ReportLine(0,pnone,1,CharStrL(' ',4));
          Inc(Num_ACBL,nplayers);
          nplayers := ChrNum(@NonPlayers,3);
          if nplayers > 0 then ReportLine(0,pnone,1,numcvt(nplayers,5,false)+' ')
          else ReportLine(0,pnone,1,CharStrL(' ',6));
          Inc(Num_Non,nplayers);
        end;
        if ChrNum(@Boards,3) > 0 then ReportLine(0,pnone,1,
          numcvt(ChrNum(@Boards,3),4,false))
        else ReportLine(0,pnone,1,'    ');
        isreg := ChrNum(@NTablefees,6) > 0;
        isext := ChrNum(@Extrafees,6) > 0;
        if isreg or isext then ReportLine(0,pnone,1,'   '+Feecode)
        else ReportLine(0,pnone,1,'    ');
        if isreg then ReportLine(0,pnone,1,Real2StrL(ChrReal(@Tablerate,5,2),6,2))
        else ReportLine(0,pnone,1,CharStrL(' ',6));
        if isext then begin
          ReportLine(0,pnone,1,Real2StrL(ChrReal(@ExtraRate,5,2),6,2));
          if feecode = '1' then ReportLine(0,pnone,1,' '
            +Real2StrL(ChrReal(@Extrafees,6,2),6,2)+' ')
          else ReportLine(0,pnone,1,'('+Real2StrL(ChrReal(@Extrafees,6,2),6,2)+')')
        end
        else ReportLine(0,pnone,1,CharStrL(' ',14));
        if grating > 0 then begin
          MPStr := '';
          for j := 1 to 3 do AddMPStr(ChrStr(@MPlimits[j,1],4),j > 1);
          ReportLine(0,pnone,1,PadL(MpStr,14));
        end
        else ReportLine(0,pnone,1,CharStrL(' ',14));
        ReportLine(0,pnone,1,Restriction+'/'+DGametype+'/'+ChrStr(@section,2)+' '
          +PadL(game_types[grating],7)+Trim(ChrStr(@director,18)));
        if Feecode = '4' then ReportLine(1,pnone,1,CharStrL(' ',6)
          +'Local charity for above game: '+Trim(ChrStr(@LocalCharity,40)));
      end
      else begin
        if Chargeable = 'Y' then
          ReportLine(0,pnone,1,' '+Real2StrL(ChrReal(@tables,3,1),5,1)+' ')
        else ReportLine(0,pnone,1,'('+Real2StrL(ChrReal(@tables,3,1),5,1)+')');
        if grating > 0 then begin
          if TestRating(grating,rmClubRegFees,Ival(ChrStr(@RepYear,4)))
            or (grating in [3,7]) then
          case Restriction of
            'O': MpStr := 'Open ';
            'I': MPStr := 'Inv  ';
            'N': MPStr := 'Nov  ';
            'B': MPStr := 'B+   ';
            else MPStr := '     ';
          end
          else MPStr := '     ';
          for j := 1 to 3 do AddMPStr(ChrStr(@MPlimits[j,1],4),j > 1);
          ReportLine(0,pnone,1,' '+PadL(MpStr,18));
        end
        else ReportLine(0,pnone,1,CharStrL(' ',19));
        case DGameType of
          'P': MPStr := 'Pair';
          'T': MPStr := 'Team';
          'I': MPStr := 'Ind ';
          else MPStr := '    ';
        end;
        ReportLine(0,pnone,1,' '+MPStr+'   '+ChrStr(@section,2)+' '
          +PadL(game_types[grating],7)+'    ');
        if handicap = 'Y' then ReportLine(0,pnone,1,'H')
        else if Ival(strata) > 1 then ReportLine(0,pnone,1,'S')
        else ReportLine(0,pnone,1,' ');
        ReportLine(0,pnone,1,'     '+Trim(ChrStr(@director,18)));
      end;
    end;
  end;
end;

label
     at_end,
     Next_SessRec;
begin
  Cftype := 'Finance';
  AssignFile(infile,infilename);
  Reset(infile);
  recs := 0;
  ClubFinRec := @inrec;
  FillChar(inrec,SizeOf(inrec),' ');
  {$I-} ReadLn(infile,inrec); {$I+}
  CheckIOResult(infilename);
  if canceled then exit;
  Inc(recs);
  ClubFinMast1 := ClubFinRec^;
  sum_done := false;
  Sess_tables := 0;
  with ClubFinRec^ do begin
    club := ChrStr(@ClubNum,6);
    if not ValidField(@RecType,1,'1') then exit;
    if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    if version[1] > '9' then
      RepVersion := ChrNum(@version[2],1) * 100 + ChrNum(@version[4],2)
    else RepVersion := ChrNum(@version,1) * 100 + ChrNum(@version[3],2);
    NewRepType := RepVersion >= 700;
    FillChar(inrec,SizeOf(inrec),' ');
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    Inc(recs);
    ClubFinMast2 := ClubFinRec^;
    if ChrNum(@UnchargedGames,4) > 1000 then begin
      Move(TotalGames[2],ClubFinMast2.TotalGames,66);
      FillChar(ClubFinMast2.UnchargedGames,4,'0');
    end;
    if not ValidField(@RecType,1,'2') then exit;
    if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    InitClubFinPrint;
    if (Destination in [1,3]) {or cover }then print_sum;
    if canceled then exit;
//    if not cover then begin
      Sess := 0;
      while Sess < SessRecs do begin
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
        Inc(recs);
Next_SessRec:
        Case RecType of
          '9': goto at_end;
        end;
        Inc(Sess);
        if club <> ChrStr(@ClubNum,6) then begin
          ClubFinSess := ClubFinRec^;
          move(club[1],ClubNum,6);
          move(ClubFinSess.ClubNum,RepYear,50);
        end;
        ClubFinSess := ClubFinRec^;
        if not ValidField(@RecType,1,'3') then exit;
        if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
        SectRecs := ChrNum(@NumReportedSecs,3);
        PrintHeader;
        Sect := 0;
        while Sect < SectRecs do begin
          {$I-} ReadLn(infile,inrec); {$I+}
          CheckIOResult(infilename);
          if canceled then exit;
          Inc(recs);
          case RecType of
            '3': goto Next_SessRec;
            '9': goto at_end;
          end;
          Inc(Sect);
          ClubFinDet := ClubFinRec^;
          if not ValidField(@RecType,1,'4') then exit;
          if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
          PrintDetail;
        end;
at_end:
        check_tables;
        if RecType = '9' then Sess := SessRecs;
      end;
//    end;
  end;
  if not sum_done then print_sum;
  if Destination = 3 then NewLine(1);
  CloseMPFiles;
  if Destination = 1 then ViewerShow('',infilename);
end;

procedure SetPageProc(const Non: byte);
begin
  case Non of
    1,3: PageProc := MPEndPage;
    2: PageProc := FEndPage;
  end;
  case Destination of
    1,3: begin
      lskip:=2;
      PageLength := 65000;
    end;
    2: begin
      lskip:=2;
      SetLineSpace(6);
      if PageLines > 0 then PageLength := PageLines-1
      else PageLength:=58;
      ReportLine(0,ppica,1,'');
    end;
  end;
end;

procedure FinishLastPlayer;
const
     Dirs : String[4] = 'NESW';
function dorank(const low,high: word; const width: byte): String;
begin
  Result :='';
  if low > 0 then Result:=numcvt(low,0,false);
  if high > low then Result:=Result+'/'+numcvt(high,0,false);
  Result := TrimTrailL(CenterL(Result,Abs(width)));
end;
var
   j    : byte;
   HighMPColor  : byte;
begin
  if not Empty(LastPlayer) then begin
    HighMPColor := 0;
    for j := 1 to 4 do if LastMPs[j] > 0 then HighMPColor := j;
    MaxHighColor := MaxWord(HighMPColor,MaxHighColor);
    if ThisRankl > 0 then HighMPColor := 4;
    if HighMPColor > 0 then begin
      ReportLine(1,pelite,1,LastPlayer);
      for j := 1 to HighMPColor do if LastMPs[j] > 0 then
        ReportLine(0,pnone,1,Real2StrL(LastMPs[j]/100,7,2)+ColorLetter[j])
        else ReportLine(0,pnone,1,CharStrL(' ',8));
      if NewRepType and (ThisRankl > 0) then begin
        RankStr := '';
        if ThisPair > 0 then begin
          RankStr := ' '+Trim(ThisSect)+'.'+Long2StrL(ThisPair);
          if ThisDir > 0 then RankStr := RankStr+'.'+Dirs[ThisDir];
        end;
        ReportLine(0,pnone,1,PadL(RankStr,8));
        RankStr := dorank(ThisRankl,ThisRankh,5)+'-';
        if ThisMPtype = '2' then RankStr := RankStr+'O'
        else RankStr := RankStr+'S';
        with ClubMPEvent do if strata > '1' then
          RankStr := RankStr+StLetters[ThisStrat];
        ReportLine(0,pnone,1,PadL(RankStr,10));
        ReportLine(0,pnone,1,Real2StrL(ThisPer,5,2)+' '+numcvt(ThisBds,3,false));
      end;
    end;
  end;
  LastPlayer := ThisPlayer;
  ThisRankl := 0;
  ThisMPType := '0';
  FillChar(LastMPs,SizeOf(LastMPs),0);
end;

procedure InitClubMPPrint;
begin
  if intype = mpftNew then with ClubMPHeader do begin
    EventRecs := ChrNum(@records,4);
    club := ChrStr(@ClubNum,6);
    date := Months[Ival(ChrStr(@GameDate,2))]+' '+Copy(ChrStr(@GameDate,8),5,4);
    if version[1] > '9' then
      RepVersion := ChrNum(@version[2],1) * 100 + ChrNum(@version[4],2)
    else RepVersion := ChrNum(@version,1) * 100 + ChrNum(@version[3],2);
    NewRepType := RepVersion >= 700;
  end
  else begin
    club := Copy(inrec,8,6);
    date := Months[Ival(Copy(inrec,21,2))]+' 20'+Copy(inrec,19,2);
    RepVersion := 400;
    NewRepType := false;
  end;
  PageNo := 0;
  newpage;
end;

procedure InitEventMPPrint;
var
   j    : word;
   NumStrat : byte;
   stab     : Array[1..3] of real;
   ttab     : real;
begin
  with ClubMPEvent do begin
    DetailRecs := ChrNum(@DetRecs,4);
    if PageLength-RepLine < 9 then newpage;
    EvDate := ChrStr(@GameDate,8);
    if hrating = 16 then
      ReportLine(lskip,ppica,1,Copy(EvDate,1,2)+'/'+Copy(EvDate,5,4))
    else ReportLine(lskip,ppica,1,Slash(EvDate));
    ReportLine(0,pnone,1,' '+Trim(ChrStr(@EventName,25))+' '
      +StrPas(RatingText[hrating]));
    if hrating <> 16 then begin
      NumStrat := ChrNum(@strata,1);
      ReportLine(1,pnone,1,'Sessions: '+Long2StrL(ChrNum(@sessions,2)));
      ReportLine(0,pnone,1,'  Club Session Number: '
        +Long2StrL(ChrNum(@ClubSessNum,2)));
      if NumStrat > 1 then ReportLine(0,pnone,1,'  Number of Strata: '+strata);
      if NewRepType and (ChrNum(@Boards,3) > 0) then
        ReportLine(0,pnone,1,'  Boards: '+Long2StrL(ChrNum(@Boards,3)));
      NewLine(1);
      if NewReptype and (NumStrat> 1) then begin
        ReportLine(0,pnone,1,'Tables by stratum:  ');
        ttab := 0;
        for j := 1 to NumStrat do begin
          stab[j] := ChrReal(@StTables[j,1],4,1);
          ttab := ttab+stab[j];
        end;
        if (NumStrat = 3) and (ttab <> ChrReal(@tables,4,1)) then begin
          stab[1] := ChrReal(@tables,4,1)-stab[2]-stab[3];
          if stab[1] < 0 then stab[1] := 0;
        end;
        for j := 1 to NumStrat do begin
          if j > 1 then ReportLine(0,pnone,1,'/');
          ReportLine(0,pnone,1,Real2StrL(stab[j],4,1));
        end;
        ReportLine(0,pnone,1,'  ');
      end;
      if NewRepType then
        ReportLine(0,pnone,1,'Tables: '+Real2StrL(ChrReal(@tables,4,1),4,1))
      else ReportLine(0,pnone,1,'Tables: '+Long2StrL(ChrNum(@tables,4)));
      if NewRepType then begin
        ReportLine(0,pnone,1,'  Type: '+Trim(GetGameTypeStr(PTI)
          +'/'+GetPairTeamStr(GameType)));
        ReportLine(1,pnone,1,'Masterpoint limits: ');
      end
      else ReportLine(0,pnone,1,'  Masterpoint limits: ');
      for j := 1 to NumStrat do case j of
        1: if ChrNum(@MPLimits[1],4) < 1 then ReportLine(0,pnone,1,'None')
           else ReportLine(0,pnone,1,Long2StrL(ChrNum(@MPLimits[1],4)));
        else ReportLine(0,pnone,1,'/'+Long2StrL(ChrNum(@MPLimits[j],4)));
      end;
      if NewRepType and (NumStrat > 1) then begin
        ReportLine(0,pnone,1,'  Strata: ');
        for j := 1 to NumStrat do begin
          if j > 1 then ReportLine(0,pnone,1,'/');
          ReportLine(0,pnone,1,StLetters[j]);
        end;
      end;
      if not Empty(ChrStr(@SancNum,10)) then
        ReportLine(0,pnone,1,'  Sanction '+ChrStr(@SancNum,10));
      ReportLine(2,pelite,1,'NAME'+CharStrL(' ',16)+'PLAYER    BLACK');
      ReportLine(0,pnone,1,'     RED    GOLD  SILVER POSITION RANK    SCORE BDS');
    end
    else if Copy(ChrStr(@EventName,25),1,6) = 'Master' then
      ReportLine(1,pnone,1,
      'Manually recorded points for the following players:');
    NewLine(1);
  end;
end;

procedure PrintMPDetailRec;
var
   PlayKey        : KeyStr;
   j              : word;
   k              : word;
   tempstr        : String[5];
begin
  with ClubMPRec^ do begin
    Entries := ChrNum(@Numentries,2);
    if NewRepType then for j := 1 to Entries do case RecType of
      '3': with NPlayerRec[j] do begin
        PlayKey := Player_key(ChrStr(@PlNum,7));
        FindKey(IdxKey[Filno,2]^,Recno[Filno],PlayKey);
        if ok then with Playern do begin
          GetARec(FilNo);
          ThisPlayer := PadL(Copy(TrimL(First_Name)+' '+Trim(Last_Name),1,20),20)
            +Player_no;
        end
        else ThisPlayer := PadL('Player not found.',20)+ChrStr(@Plnum,7);
        if ThisPlayer <> LastPlayer then FinishLastPlayer;
        {if not bad49 and (hrating = 41) and not check49
          and (MPType = '2') and (ChrNum(@LowRank,3) > 0) then begin
          Foa := OAaward(Bfactor(gtables),1,1,0.45,1,1);
          LowR := ChrNum(@LowRank,3);
          HighR := ChrNum(@HighRank,3);
          if HighR < LowR then HighR := LowR;
          TMP := 0;
          for k := LowR to HighR do TMP := TMP+GetOAaward(Foa,k,1);
          TMP := TMP / (HighR-LowR+1);
          check49 := true;
          bad49 := ChrReal(@MPs,5,2) < (TMP * 0.75);
        end;
        if bad49 and check49 then begin
          iTMP := RRound(ChrReal(@MPs,5,0) * 2.222222);
          ChangeMPs[ColorOrder[Ival(MPcolor)]] := true;
          tempstr := numcvt(itmp,5,true);
          Move(tempstr[1],MPs,5);
        end;}
        Inc(LastMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        ThisStrat := ChrNum(@Stratum,1);
        ThisRankl := ChrNum(@LowRank,3);
        ThisRankh := ChrNum(@HighRank,3);
        ThisSect := ChrStr(@Section,2);
        ThisDir := ChrNum(@Direction,1);
        ThisPair := ChrNum(@PairNum,3);
        ThisPer := ChrReal(@Score,4,2);
        ThisBds := ChrNum(@BdsPlayed,3);
        if MPtype > ThisMPtype then ThisMPtype := MPtype;
      end;
      '4': with NPlayNonRec[j],PlRec do begin
        ThisPlayer := PadL(Copy(TrimL(ChrStr(@FirstN,16))+' '
          +TrimL(ChrStr(@LastN,16)),1,20),20)+ChrStr(@PlNum,7);
        if ThisPlayer <> LastPlayer then FinishLastPlayer;
        Inc(LastMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        ThisStrat := ChrNum(@Stratum,1);
        ThisRankl := ChrNum(@LowRank,3);
        ThisRankh := ChrNum(@HighRank,3);
        ThisSect := ChrStr(@Section,2);
        ThisDir := ChrNum(@Direction,1);
        ThisPair := ChrNum(@PairNum,3);
        ThisPer := ChrReal(@Score,4,2);
        ThisBds := ChrNum(@BdsPlayed,3);
      end;
    end
    else for j := 1 to Entries do case RecType of
      '3': with PlayerRec[j] do begin
        PlayKey := Player_key(ChrStr(@PlNum,7));
        FindKey(IdxKey[Filno,2]^,Recno[Filno],PlayKey);
        if ok then with Playern do begin
          GetARec(FilNo);
          ThisPlayer := PadL(Copy(TrimL(First_Name)+' '+Trim(Last_Name),1,20),20)
            +Player_no;
        end
        else ThisPlayer := PadL('Player not found.',20)+ChrStr(@Plnum,7);
        if ThisPlayer <> LastPlayer then FinishLastPlayer;
        Inc(LastMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
      end;
      '4': with PlayNonRec[j],PlRec do begin
        ThisPlayer := PadL(Copy(TrimL(ChrStr(@FirstN,16))+' '
          +TrimL(ChrStr(@LastN,16)),1,20),20)+ChrStr(@PlNum,7);
        if ThisPlayer <> LastPlayer then FinishLastPlayer;
        Inc(LastMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
      end;
    end;
  end;
end;

procedure CopyClubMP;
var
   VerName      : String;
   VerFile      : textFile;
   Linfilename  : String;
   iscruiseclub : boolean;
   Autoprocess  : boolean;
   ClubCount    : word;

function ProcessClubFile(const EvRecs: integer; var EventRecs: LongInt): boolean;
var
   EvRecords    : String[4];
   k            : word;
   GPath        : String;
   tempstr      : String[8];
   badmps       : boolean;
   xver         : String[12];
   lastseqdiff    : word;
   wseq           : word;
   j              : word;
   Drec           : integer;
   stabc          : String[4];
   stab     : Array[1..3] of real;
   ttab     : real;
label
     File_done;
begin
  recs := 0;
  lastseqdiff := 0;
  ProcessClubFile := false;
  if EvRecs >= 0 then Reset(infile);
  with ClubMPRec^ do begin
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    inrec := PadL(inrec,96);
    if version[1] > '9' then
      RepVersion := ChrNum(@version[2],1) * 100 + ChrNum(@version[4],2)
    else RepVersion := ChrNum(@version,1) * 100 + ChrNum(@version[3],2);
    NewRepType := RepVersion >= 700;
    Move(ClubMPRec^,ClubMPHeader,SizeOf(TClubMPRec));
    iscruiseclub := ClubSessNum[1] in ['C','L','P'];
    xver := Trim(ChrStr(@version,12));
    k := Pos('.',xver);
    if k > 0 then Delete(xver,k,1);
    VerName := VerPath+xver+Copy(inname,8,4);
    if EvRecs < 0 then EventRecs := ChrNum(@records,4)
    else EventRecs := EvRecs;
    EvRecords := numcvt(EventRecs,4,true);
    club := ChrStr(@ClubNum,6);
    Inc(recs);
    if EvRecs >= 0 then begin
      Move(EvRecords[1],records,4);
      FixNumField(@UnitNum,3);
      FixNumField(@DistNum,2);
      WriteLn(outfile,inrec);
    end;
    ERec := 0;
    while ERec < EventRecs do begin
      Inc(ERec);
      if Bad_Eof(infile) then exit;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      Inc(recs);
      if (EvRecs < 0) and (RecType = '9') then begin
        EventRecs := MaxWord(ERec-1,1);
        goto File_Done;
      end;
      Move(ClubMPRec^,ClubMPEvent,SizeOf(TClubMPRec));
      if not ValidField(@RecType,1,'2') then exit;
      if EvRecs < 0 then begin
        wseq := ChrNum(@sequence,4);
        if wseq > ERec+lastseqdiff then begin
          Dec(EventRecs,wseq-ERec-lastseqdiff);
          lastseqdiff := wseq-ERec;
        end;
        if lastseqdiff > 0 then NumChr(@sequence,4,ERec);
      end
      else NumChr(@sequence,4,ERec);
      if not ValidField(@sequence,4,numcvt(ERec,4,true)) then exit;
      if not ValidField(@ClubNum,6,club) then exit;
      if not (seniors in ['Y','N']) then seniors := 'N';
      if not (PTI in ['P','T','I']) then PTI := 'P';
      if NewReptype and (strata = '3') then begin
        ttab := 0;
        for j := 1 to 3 do begin
          stab[j] := ChrReal(@StTables[j,1],4,1);
          ttab := ttab+stab[j];
        end;
        if ttab <> ChrReal(@tables,4,1) then begin
          stab[1] := ChrReal(@tables,4,1)-stab[2]-stab[3];
          if stab[1] < 0 then stab[1] := 0;
          stabc := Numcvt(RRound(stab[1] * 10),4,true);
          Move(stabc[1],StTables,4);
        end;
      end;
      DetailRecs := ChrNum(@DetRecs,4);
      hrating := ChrNum(@Erating,2);
      gtables := RRound(ChrReal(@tables,4,1)+0.4);
      {bad49 := false;
      check49 := false;}
      if EvRecs >= 0 then begin
        Move(EvRecords[1],records,4);
        FixNumField(@UnitNum,3);
        FixNumField(@DistNum,2);
        WriteLn(outfile,inrec);
      end;
      for DRec := 1 to DetailRecs do begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
        if not (RecType in ['3','4']) then
          if not ValidField(@RecType,1,'3') then exit;
        if not ValidField(@sequence,4,numcvt(DRec,4,true)) then exit;
        if not ValidField(@ClubNum,6,club) then exit;
        Inc(recs);
        Entries := ChrNum(@Numentries,2);
        if NewReptype then case RecType of
          '3': for j := 1 to Entries do with NPlayerRec[j] do begin
            tempstr := ChrStr(@PlNum,7);
            if not Player_Check(tempstr,pnACBL) then begin
              ViewerAdd('Invalid Player number: '+ChrStr(@PlNum,7)+'.'#13
                +'File '+infilename+' has NOT been copied.',true);
              exit;
            end;
            if EvRecs >= 0 then begin
              Move(tempstr[1],PlNum,7);
              {if not bad49 and (hrating = 41) and not check49
                and (MPType = '2') and (ChrNum(@LowRank,3) > 0) then begin
                Foa := OAaward(Bfactor(gtables),1,1,0.45,1,1);
                LowR := ChrNum(@LowRank,3);
                HighR := ChrNum(@HighRank,3);
                if HighR < LowR then HighR := LowR;
                TMP := 0;
                for k := LowR to HighR do TMP := TMP+GetOAaward(Foa,k,1);
                TMP := TMP / (HighR-LowR+1);
                check49 := true;
                bad49 := ChrReal(@MPs,5,2) < (TMP * 0.75);
              end;
              if bad49 and check49 then begin
                iTMP := RRound(ChrReal(@MPs,5,0) * 2.222222);
                ChangeMPs[ColorOrder[Ival(MPcolor)]] := true;
                tempstr := numcvt(itmp,5,true);
                Move(tempstr[1],MPs,5);
              end;}
              Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
            end;
          end;
          '4': if EvRecs >= 0 then for j := 1 to Entries do
            with NPlayNonRec[j].PlRec do
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        end
        else case RecType of
          '3': for j := 1 to Entries do with PlayerRec[j] do begin
            tempstr := ChrStr(@PlNum,7);
            if not Player_Check(tempstr,pnACBL) then begin
              ViewerAdd('Invalid Player number: '+ChrStr(@PlNum,7)+'.'#13
                +'File '+infilename+' has NOT been copied.',true);
              exit;
            end;
            if EvRecs >= 0 then begin
              Move(tempstr[1],PlNum,7);
              Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
            end;
          end;
          '4': if EvRecs >= 0 then for j := 1 to Entries do
            with PlayNonRec[j].PlRec do
            Inc(TotMPs[ColorOrder[Ival(MPcolor)]],ChrNum(@MPs,5));
        end;
        if EvRecs >= 0 then begin
          FixNumField(@UnitNum,3);
          FixNumField(@DistNum,2);
          WriteLn(outfile,inrec);
        end;
      end;
    end;
    if Bad_Eof(infile) then exit;
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    Inc(recs);
File_done:
    if not ValidField(@RecType,1,'9') then exit;
    if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    ProcessClubFile := true;
    if EvRecs >= 0 then begin
      AssignFile(Verfile,VerName);
      {$I-} Append(Verfile);
      if IOResult <> 0 then ReWrite(Verfile); {$I+}
      if IOResult = 0 then begin
        WriteLn(Verfile,Copy(inname,2,6));
        CloseFile(Verfile);
      end;
      for j := 1 to 4 do Inc(TotMPS[5],TotMPS[j]);
      badmps := false;
      for j := 1 to 5 do if TotMPS[j] <> ChrNum(@ChkMps[j,1],8) then begin
        if j < 5 then ChangeMPs[5] := ChangeMPs[5] or ChangeMPs[j];
        if ChangeMPS[j] or (TotMPS[j] < ChrNum(@ChkMps[j,1],8)) then begin
          tempstr := numcvt(TotMPS[j],8,true);
          Move(tempstr[1],ChkMps[j,1],8);
        end
        else begin
          ViewerAdd('Actual'+Pcolors[j]+' pts ('+Real2StrL(TotMPS[j]/100,4,2)
            +') is not equal to hash total ('
            +Real2StrL(ChrNum(@ChkMPs[j,1],8)/100,4,2)+')',true);
          ViewerAdd('File '+infilename+' has NOT been copied.',true);
          badmps := true;
          ProcessClubFile := false;
          CloseMPFiles;
          exit;
        end;
      end;
      WriteLn(outfile,inrec);
      if TotMPS[5] < 1 then ViewerAdd('File '+infilename+' has zero masterpoints.'
        +'  Please check financial report also.',true);
      CloseMPFiles;
    end;
  end;
end;

function CopyFinFile(const inLoc: byte): boolean;
var
   SumRecs      : word;
   SessRecs     : word;
   isess        : word;
   idet         : word;
   DetRecs      : word;
   tmpstr       : String[10];
   Abnormalend  : boolean;
   Reprecs      : word;
begin
  Cftype := 'Finance';
  Recs := 0;
  CopyFinFile := false;
  Abnormalend := false;
  with ClubFinRec^ do begin
    Reprecs := 0;
    Repeat
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      if RecType = '3' then Inc(Reprecs);
    until (RecType = '9') or Eof(infile);
    CloseFile(infile);
    ReSet(infile);
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    if version[1] > '9' then
      RepVersion := ChrNum(@version[2],1) * 100 + ChrNum(@version[4],2)
    else RepVersion := ChrNum(@version,1) * 100 + ChrNum(@version[3],2);
    NewRepType := RepVersion >= 700;
    ClubFinMast1 := ClubFinRec^;
    Inc(recs);
    if not ValidField(@RecType,1,'1') then exit;
    if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
    club := ChrStr(@ClubNum,6);
    FixNumField(@UnitNum,3);
    FixNumField(@DistNum,2);
    if ClubType = 'W' then ClubType := 'O';
    WriteLn(outfile,inrec);
    if Bad_Eof(infile) then exit;
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    ClubFinMast2 := ClubFinRec^;
    if ChrNum(@UnchargedGames,4) > 1000 then begin
      Move(TotalGames[2],ClubFinMast2.TotalGames,66);
      FillChar(ClubFinMast2.UnchargedGames,4,'0');
      ClubFinRec^ := ClubFinMast2;
    end;
    Inc(recs);
    if not ValidField(@RecType,1,'2') then exit;
    if not ValidField(@ClubNum,6,club) then exit;
    if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
    SessRecs := ChrNum(@ReportedSess,2);
    if SessRecs <> Reprecs then begin
      SessRecs := Reprecs;
      NumChr(@ReportedSess,2,SessRecs);
    end;
    if NewRepType then SumRecs := ChrNum(@NumSum,2)
    else SumRecs := 0;
    WriteLn(outfile,inrec);
    if NewRepType then for isess := 1 to SumRecs do begin
      if not Abnormalend then begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
      end;
      ClubFinSess := ClubFinRec^;
      Inc(recs);
      if not ValidField(@RecType,1,'5') then exit;
      if not ValidField(@ClubNum,6,club) then exit;
      if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
      WriteLn(outfile,inrec);
    end;
    for isess := 1 to SessRecs do begin
      if not Abnormalend then begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
      end;
      if club <> ChrStr(@ClubNum,6) then begin
        ClubFinSess := ClubFinRec^;
        move(club[1],ClubNum,6);
        move(ClubFinSess.ClubNum,RepYear,50);
      end;
      ClubFinSess := ClubFinRec^;
      Inc(recs);
      if not ValidField(@RecType,1,'3') then exit;
      if not ValidField(@ClubNum,6,club) then exit;
      if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
      DetRecs := ChrNum(@NumReportedSecs,3);
      Abnormalend := false;
      for idet := 1 to DetRecs do begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
        if RecType <> '4' then begin
          Abnormalend := true;
          Break;
        end;
        if not Assigned(CFinDetArray[idet]) then New(CFinDetArray[idet]);
        CFinDetArray[idet]^ := ClubFinRec^;
      end;
      if Abnormalend then begin
        ClubSaveRec := ClubFinRec^;
        tmpstr := numcvt(idet-1,3,true);
        Move(tmpstr[1],ClubFinSess.NumReportedSecs,3);
      end;
      ClubFinRec^ := ClubFinSess;
      DetRecs := ChrNum(@NumReportedSecs,3);
      WriteLn(outfile,inrec);
      for idet := 1 to DetRecs do begin
        ClubFinRec^ := CFinDetArray[idet]^;
        Dispose(CFinDetArray[idet]);
        CFinDetArray[idet] := nil;
        Inc(recs);
        if not ValidField(@RecType,1,'4') then exit;
        if not ValidField(@ClubNum,6,club) then exit;
        if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
        WriteLn(outfile,inrec);
      end;
      if Abnormalend then ClubFinRec^ := ClubSaveRec;
    end;
    if not Abnormalend then begin
      if Bad_Eof(infile) then exit;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
    end;
    Inc(recs);
    if not ValidField(@RecType,1,'9') then exit;
    if not ValidField(@ClubNum,6,club) then exit;
    if not ValidField(@sequence,4,Numcvt(recs,4,true)) then exit;
    WriteLn(outfile,inrec);
  end;
  CloseMPFiles;
  CopyFinFile := true;
  OpenDestination(3,0,105);
  DestFileName := outpath+FinTextFileName;
  AssignFile(DestFile,DestFileName);
  {$I-} Append(DestFile); {$I+}
  if IOResult <> 0 then begin
    ReWrite(DestFile);
    needff := false;
  end
  else needff := true;
  SetPageProc(2);
  PrintFinance;
  if canceled then exit;
  Close_Destination;
end;

procedure CheckNameFile(const Nontype,Nonpath,Nonex: string);
var
   NPath        : String;
   NonMemName   : String;
   inf          : textFile;
   outf         : textFile;
   Line         : String;
   Clubnumdate  : String;
   FindErr      : integer;
begin
  clubnumdate := Copy(inname,2,10);
  NPath := inPath+Nontype+clubnumdate;
  FindErr := FindFirst(NPath,faArchive,Search_Rec);
  FindClose(Search_Rec);
  if FindErr <> 0 then exit;
  AssignFile(inf,NPath);
  {$I-} Reset(inf); {$I+}
  CheckIOResult(NPath);
  if canceled then exit;
  {$I-} ReadLn(inf,Line); {$I+}
  CheckIOResult(NPath);
  if canceled then exit;
  if Nontype = 'A' then begin
    NonMemName := NonPath+'20'+GetCSVField(2,Line)+Nonex;
    Reset(inf);
  end
  else NonMemName := NonPath+Copy(Line,4,4)+Copy(Line,1,2)+Nonex;
  AssignFile(outf,NonMemName);
  {$I-} Append(outf);
  if IOResult <> 0 then ReWrite(outf); {$I+}
  CheckIOResult(NonMemName);
  if canceled then exit;
  repeat
    {$I-} ReadLn(inf,Line); {$I+}
    CheckIOResult(NPath);
    if canceled then exit;
    {$I-} if Nontype = 'A' then WriteLn(outf,Line)
    else WriteLn(outf,clubnumdate,' ',Trim(Line)); {$I+}
    CheckIOResult(NonMemName);
    if canceled then exit;
  until Eof(inf);
  CloseFile(inf);
  CloseFile(outf);
end;

procedure RenameTempFile;
begin
  if not FileExists(TempFileName) then exit;
  AssignFile(infile,TempFileName);
  if FileExists(outfilename) then begin
    AssignFile(outfile,outfilename);
    Append(outfile);
    Reset(infile);
    while not Eof(infile) do begin
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(TempFileName);
      if canceled then exit;
      WriteLn(outfile,inrec);
    end;
    CloseFile(outfile);
    CloseFile(infile);
  end
  else Rename(infile,outfilename);
  DeleteFileAttr(TempFileName);
end;

procedure OpenRepFiles;
begin
  AssignFile(outfile,TempFileName);
  ReWrite(outfile);
  Reset(infile);
end;

procedure AddFileList;
var
   F      : TextFile;
begin
  AssignFile(F,FileListName);
  if FileExists(FileListName) then Append(F)
  else ReWrite(F);
  WriteLn(F,inname);
  CloseFile(F);
end;

procedure DelMPFile;
begin
  DeleteFileAttr(outpath+'M'+Copy(inname,2,12));
end;

procedure SaveClubReportFile(const sourcename,targetpath,targetname: String);

function FileSameSize(const file1,file2: String): boolean;
var
   fss   : integer;
   fst   : integer;
   Oldfmode : byte;
   F     : file of byte;
begin
  Result := true;
  Oldfmode := FileMode;
  FileMode := 0;
  AssignFile(f,file1);
  {$I-} ReSet(f);
  If IOResult = 0 then begin
    fss := FileSize(f);
    CloseFile(f);
    AssignFile(f,file2);
    ReSet(f);
    if IOResult = 0 then begin
      fst := FileSize(f);
      CloseFile(f);
    end
    else fst := 0;
    FileMode := Oldfmode;
    if Abs(fst-fss) < 6 then exit;
  end;{$I+}
  Result := false;
  FileMode := Oldfmode;
end;

var
   tname : String;
   suf   : String;
   nname : String;
   xs    : word;
begin
  tname := Copy(JustNameL(targetname),1,8)+'.'+JustExtensionL(targetname);
  if FileExists(targetpath+tname) then begin
    if FileSameSize(sourcename,targetpath+tname) then exit;
    xs := Pos('.',tname);
    if xs > 0 then begin
      nname := Copy(tname,1,xs-1)+'-';
      suf := Copy(tname,xs,255);
      xs := 2;
      while FileExists(targetpath+nname+Long2StrL(xs)+suf) do begin
        if FileSameSize(sourcename,targetpath+nname+Long2StrL(xs)+suf) then exit;
        Inc(xs);
      end;
      tname := nname+Long2StrL(xs)+suf;
    end;
  end;
  CopyFileAttr(PChar(sourcename),Pchar(targetpath+tname),faReadOnly);
end;

var
   Lastpick       : String;
   SYear          : String;
   CPref          : string;
   SMonth         : String[2];
   FindErr        : integer;
   ClubNumber     : String;
   found          : boolean;
   j              : integer;
   SName          : String;
label
     SaveTheFile,
     DoFinance,
     path_again1,
     path_again;
begin
  Form1.Caption := 'Copy club report files';
  MakeOutpathDirs;
  if CheckDrive(Escaped,OutPath,false) <> 0 then exit;
  escaped := false;
  LastPick := '';
  FillChar(CFinDetArray,SizeOf(CFinDetArray),0);
  ClubCount := 0;
  AutoProcess := false;
path_again1:
  if ClubCount > 0 then begin
    ViewerAdd(Long2StrL(ClubCount)+' clubs were processed',true);
    ViewerShow('','');
    if Autoprocess then exit;
    StatusBar1.SimpleText := '';
  end;
  Autoprocess := false;
  ClubCount := 0;
  LastPick := '';
path_again:
  if islhafile then DelTempDirFiles;
  Close_Destination;
  MaxHighColor := 0;
  iscruiseclub := false;
  if escaped then exit;
  if Autoprocess then inLoc := 3
  else if Empty(LastPick) then begin
    InLoc := PickChoice(4,inloc,
      'club report location (ESC to quit) ',@inLocT,MC,false,0);
    if InLoc < 1 then exit;
    Autoprocess := InLoc = 3;
    if (Inloc = 4)
      and not GetFolder(inLocP[Inloc],'Select Club report location',true,
        'OtherCopyLoc','G:\') then exit;
  end;
  inPath := fixpath(inLocP[inLoc]);
  if CheckDrive(Escaped,inPath,false) <> 0 then goto path_again1;
  FindErr := FindFirst(inPath+InLocMask[inLoc],faArchive,Search_Rec);
  found := (FindErr = 0) and Autoprocess;
  if (FindErr = 0) and not Autoprocess then begin
    if not Empty(LastPick) then while (FindErr = 0)
      and (LastPick <> Search_Rec.Name) do FindErr := FindNext(Search_Rec);
    if not Empty(LastPick) and (FindErr = 0) then FindErr := FindNext(Search_Rec);
    while (FindErr = 0) and not found do begin
      SName := UpperCase(Search_rec.Name);
      j := Pos('.',SName);
      found := (SName[1] in ['M','1','2','9']) and (Pos(SName[2],'0123456789') > 0);
      if found then begin
        found := j < 1;
        if not found and (Length(SName) > j) then
          found := (Pos(SName[j+1],'0123456789') > 0) or (Pos(LhaEx,SName) > 0);
      end;
      if not found then FindErr := FindNext(Search_Rec);
    end;
  end;
  FindClose(Search_Rec);
  if found then begin
    SName := UpperCase(Search_rec.Name);
    infilename := inPath+SName;
    LastPick := Search_rec.Name;
    if SName[1] in ['M'] then ClubNumber := Copy(SName,2,6)+'-'+SName[9]+'-'
      +Copy(SName,10,2)
    else begin
      ClubNumber := Copy(SName,1,6)+'-';
      if Pos('LZH',UpperCase(SName)) > 0 then ClubNumber := ClubNumber
        +SName[7]+'-'+SName[8]
      else ClubNumber := ClubNumber+SName[8]+'-'+Copy(SName,9,2);
      StatusBar1.SimpleText := 'Processing '+ClubNumber;
      Application.ProcessMessages;
    end;
    if not ViewInit then
      ViewerAdd('The following clubs have been processed:',true);
    ViewerAdd(ClubNumber,true);
  end
  else begin
    if Empty(LastPick) or (ClubCount < 1) then begin
      if Viewinit then ViewerShow('','')
      else ErrBox('No File(s) found at '+inPath,MC,0);
    end;
    goto Path_again1;
  end;
  inname := UpperCase(JustFileNameL(infilename));
  if Empty(inname) then goto path_again1;
  islhafile := false;
  ClubFinRec := @inrec;
  if Pos(LhaEx,inname) > 0 then begin
    Lhaname := inname;
    Linfilename := infilename;
    DelTempDirFiles;
    canceled := not RunLHA(Linfilename,fixpath(TempDir));
    if canceled then begin
      ViewerAdd(infilename+' is corrupt.  This report NOT processed.',true);
      goto SaveTheFile;
    end;
    inPath := fixpath(TempDir);
    FindErr := FindFirst(inPath+CfileTypes[1]+ClubFileMask+'*',faArchive,Search_Rec);
    if FindErr <> 0 then begin
      FindClose(Search_Rec);
      FindErr := FindFirst(inPath+'1?????.???',faArchive,Search_Rec);
    end;
    if FindErr <> 0 then begin
      FindClose(Search_Rec);
      FindErr := FindFirst(inPath+'2?????.???',faArchive,Search_Rec);
    end;
    if FindErr <> 0 then begin
      FindClose(Search_Rec);
      FindErr := FindFirst(inPath+'9?????.???',faArchive,Search_Rec);
    end;
    islhafile := true;
    if FindErr <> 0 then begin
      FindClose(Search_Rec);
      inname := CfileTypes[0]+ClubFileMask+'???';
      iscruiseclub := false;
      goto DoFinance;
    end;
    inname := Search_Rec.name;
    infileName := inPath+inname;
  end;
  Cftype := 'MP';
  intype := ValidMPFile(infile,infileName,inrec,false);
  if intype = mpftBad then goto SaveTheFile;
  outfilename := outpath+inname;
  AssignFile(outfile,outfilename);
  if FileExists(outfilename) then begin
    if intype = mpftNew then begin
      CloseMPFiles;
      ViewerAdd(infilename+' has already been copied.'#13
        +'File '+infilename+' not copied.',true);
      goto SaveTheFile;
    end;
    outtype := ValidMPFile(outfile,outfileName,outrec,false);
    if outtype = mpftBad then begin
      CloseMPFiles;
      goto SaveTheFile;
    end;
    if (outtype <> intype) then begin
      CloseMPFiles;
      ViewerAdd(infilename+' and '+outfilename+#13'are not the same type.'#13
        +'File '+infilename+' not copied.',true);
      goto SaveTheFile;
    end;
    if inrec = outrec then begin
      CloseMPFiles;
      ViewerAdd(infilename+' and '+outfilename+#13'contain the same data.'#13
        +'File '+infilename+' not copied.',true);
      goto SaveTheFile;
    end;
    if not YesNoBox(escaped,true,
      inname+' already exists in '+outpath+'.'#13
      +'Confirm append to existing file',0,MC,nil) then begin
      CloseMPFiles;
      goto SaveTheFile;
    end;
  end
  else outtype := intype;
  OpenRepFiles;
  FillChar(TotMPS,SizeOf(TotMPs),0);
  FillChar(ChangeMPS,SizeOf(ChangeMPs),0);
  OldTotMPS := 0;
  recs := 0;
  ClubMPRec := @inrec[1];
  case intype of
    mpftOld: begin
      while not Eof(infile) do begin
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then goto SaveTheFile;
        inrec := trim(inrec);
        if not Empty(inrec) then begin
          if Length(inrec) <> RecLen then begin
            ViewerAdd(infilename+' is not a valid Club MP file',true);
            CloseMPFiles;
            goto path_again;
          end;
          Inc(OldTotMPS,Ival(Copy(inrec,MPLoc,MPLen)));
          Inc(recs);
          WriteLn(outfile,inrec);
        end;
      end;
      CloseMPFiles;
      AddFileList;
      VerName := VerPath+'OLD'+Copy(inname,7,4);
      AssignFile(Verfile,VerName);
      {$I-} Append(Verfile);
      if IOResult <> 0 then ReWrite(Verfile); {$I+}
      if IOResult = 0 then begin
        WriteLn(Verfile,Copy(inname,1,6));
        CloseFile(Verfile);
      end;
    end;
    mpftNew: begin
      if not ProcessClubFile(-1,EventRecs) then goto SaveTheFile;
      if canceled then goto SaveTheFile;
      if EventRecs >= 0 then if not ProcessClubFile(EventRecs,EventRecs)
        then goto SaveTheFile;
      if EventRecs < 0 then DeleteFileAttr(outfilename)
      else AddFileList;
    end;
  end;
  RenameTempFile;
  if canceled then goto SaveTheFile;
DoFinance:
  if islhafile then begin
    FindErr := FindFirst(inPath+'F'+Copy(inname,2,10),faArchive,Search_Rec);
    if FindErr <> 0 then begin
      FindClose(Search_Rec);
      FindErr := FindFirst(inPath+'F'+Copy(inname,1,10),faArchive,Search_Rec);
    end;
    if FindErr <> 0 then begin
      if not iscruiseclub then
        ViewerAdd('No Financial report files found in '+Lhaname,true);
    end
    else iscruiseclub := false;
    if FindErr = 0 then begin
      inname := Search_Rec.name;
      infileName := inPath+inname;
      AssignFile(infile,infilename);
      outfilename := outpath+inname;
      if FileExists(outfilename) then begin
        CloseMPFiles;
        ViewerAdd(infilename+' has already been copied.'#13
          +'File '+infilename+' not copied.',true);
        DelMPFile;
        goto SaveTheFile;
      end;
      OpenRepFiles;
      if not CopyFinFile(InLoc) then begin
        if canceled then goto SaveTheFile;
        DeleteFileAttr(outfilename);
        DelMPFile;
        goto SaveTheFile;
      end;
      if canceled then goto SaveTheFile;
      AddFileList;
    end;
    RenameTempFile;
    if canceled then goto SaveTheFile;
  end;
  if intype = mpftNew then begin
    CheckNameFile('A',NonMemPath,CsvExt);
    if canceled then goto SaveTheFile;
    CheckNameFile('N',NonMemPath,NonExt);
    if canceled then goto SaveTheFile;
  end;
  Inc(ClubCount);
SaveTheFile:
  if Application.terminated then exit;
  SYear := '';
  CPref := '';
  if islhafile then begin
    if Lhaname[7] < '9' then SYear := '1'+Lhaname[7]
    else SYear := '0'+Lhaname[7];
    CPref := Copy(Lhaname,1,2);
    case Lhaname[8] of
      'A' : SMonth := '10';
      'B' : SMonth := '11';
      'C' : SMonth := '12';
      else SMonth := '0'+Lhaname[8];
    end;
  end
  else begin
    Lhaname := inname;
    Linfilename := infilename;
    FindErr := Pos('.',inname);
    if FindErr > 0 then begin
      if Lhaname[FindErr+1] < '9' then SYear := '1'+inname[FindErr+1]
      else SYear := '0'+inname[FindErr+1];
      SMonth := Copy(inname,FindErr+2,2);
      if FindErr > 6 then CPref := Copy(inname,FindErr-6,2);
    end;
  end;
  if (Length(SYear) = 2) and IsNumber(SYear) and IsNumber(CPref)
    and IsNumber(SMonth) then begin
    if Length(CPref) <> 2 then CPref := 'XX';
    CPref := CPref+'xxxx';
    SYear := '20'+SYear;
    CreateDirAttr(ClubSavePath+SYear,faReadOnly);
    CreateDirAttr(ClubSavePath+SYear+'\'+SMonth,faReadOnly);
    CreateDirAttr(ClubSavePath+SYear+'\'+SMonth+'\'+CPref,faReadOnly);
    if canceled then SaveClubReportFile(Linfilename,BadPath,Lhaname)
    else SaveClubReportFile(Linfilename,
      ClubSavePath+SYear+'\'+SMonth+'\'+CPref+'\',Lhaname);
  end;
  if inloc = 3 then DeleteFileAttr(Linfilename);
  canceled := false;
  goto path_again;
end;

procedure AppendClubMP;

function CheckProcess(const Fname,Rtype: string): boolean;
var
   F    : TSearchRec;
begin
  if FindFirst(Fname,faAnyFile,F) = 0 then begin
    FindClose(F);
    ErrBox('Last Club '+Fname+' ('+Rtype
      +') report file has not yet been processed by the A/S 400.'#13
      +'Either someone else has just submitted a club report to the 400,'#13
      +'or there is a problem with the above file on the 400.',MC,0);
    Result := false;
    exit;
  end;
  FindClose(F);
  Result := true;
end;

const
     MaxClubFiles       = 2000;
     {$ifdef repmponly}
     MaxMPclubs         = 200;
     {$endif}
type
    TClubFiles  = Array[1..MaxClubFiles] of String[11];
    PClubFiles  = ^TClubFiles;
var
   Year,
   Month,
   Day          : word;
   {$ifdef repmponly}
   NumMPonly    : word;
   {$endif}
   ClubFiles    : PClubFiles;
   NClubFiles   : word;
   NType1       : word;
   NRecs        : Longint;
   SearchFType  : String[3];
   SaveStr      : String;
   {$ifdef repmponly}
   MPonlyclubs  : Array[1..MaxMPclubs] of String;
   {$endif}
   j            : word;
   ProcessTypes : Array[1..ProcessMonths] of String[3];

procedure AppendClubFiles(const AppendFileName: String; Newtype: byte);
var
   j    : word;
   vers : word;
   {$ifdef repmponly}
   oldclub : String[6];
   {$endif}
begin
  AssignFile(outfile,AppendFileName);
  ReWrite(outfile);
  {$ifdef repmponly}
  oldclub := '      ';
  {$endif}
  for j := 1 to NClubFiles do begin
    AssignFile(infile,outpath+ClubFiles^[j]);
    {$I-} ReSet(infile); {$I+}
    CheckIOResult(outpath+ClubFiles^[j]);
    if canceled then exit;
    Label1.Caption := 'Processing '+ClubFiles^[j];
    while not eof(infile) do begin
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(outpath+ClubFiles^[j]);
      if canceled then exit;
      if Length(inrec) > 0 then begin
        WriteLn(outfile,inrec);
        Inc(NRecs);
        {$ifdef repmponly}
        if (Newtype < 1) and (oldclub <> Copy(inrec,8,6)) then begin
          oldclub := Copy(inrec,8,6);
          if NumMPonly < MaxMPclubs then begin
            Inc(NumMPonly);
            MPOnlyClubs[NumMPonly] := oldclub+' '+Copy(inrec,21,2)+'/20'
              +Copy(inrec,19,2)+' Older than 5.00 or on-line club.  No club name';
          end;
        end;
        {$endif}
        if inrec[1] = '1' then begin
          if NewType > 0 then Inc(NType1);
          {$ifdef repmponly}
          if (NewType = 1) and (inrec[83] <> 'W') then begin
            vers := Ival(inrec[83])*100+Ival(Copy(inrec,85,2));
            if (vers < 650) and (NumMPonly < MaxMPclubs) then begin
              Inc(NumMPonly);
              MPonlyClubs[NumMPonly] := Copy(inrec,22,6)+' '+Copy(inrec,12,2)
                +'/'+Copy(inrec,16,4)+' '+Copy(inrec,83,4)+' '+Copy(inrec,43,40);
            end;
          end;
          {$endif}
        end;
      end;
    end;
    if (NewType < 1) then Inc(NType1);
    CloseFile(infile);
  end;
end;

procedure FindClubFiles(const Cftype: byte; const AppendFileName: String);
var
   j            : word;
   FindErr      : integer;
   ClubMask     : String;
   f            : TextFile;
   Fname        : String;
   fileok       : boolean;
   ft           : String[3];
begin
  ClubMask := CFiletypes[Cftype]+ClubFileMask;
  escaped := false;
  NClubFiles := 0;
  if FileExists(FileListName) then begin
    AssignFile(F,FileListName);
    ReSet(F);
    while not Eof(F) do begin
      ReadLn(F,Fname);
      fileok := false;
      if Length(Fname) > 9 then begin
        if Cftype > 0 then fileok := UpperCase(Fname[1]) = CFiletypes[Cftype]
        else fileok := (Fname[1] in ['1','2','9']);
      end;
      if fileok then begin
        j := Pos('.',Fname);
        if j > 0 then begin
          ft := Copy(Fname,j+1,3);
          for j := 1 to ProcessMonths do if ft = ProcessTypes[j] then begin
            Inc(NClubFiles);
            ClubFiles^[NClubFiles] := FName;
            Break;
          end;
        end;
      end;
    end;
    CloseFile(F);
  end
  else for j := 1 to ProcessMonths do begin
    FindErr := FindFirst(OutPath+ClubMask+ProcessTypes[j],faArchive,Search_Rec);
    while (FindErr = 0) and (NClubFiles < MaxClubFiles) do
      with Search_rec do begin
      Inc(NClubFiles);
      ClubFiles^[NClubFiles] := Name;
      FindErr := FindNext(Search_Rec);
    end;
    FindClose(Search_Rec);
  end;
  NType1 := 0;
  NRecs := 0;
  AppendClubFiles(AppendFileName,Cftype);
end;

procedure CopyClubFiles(const AppendFileName,SharedFolderFileName,
          SaveFileName,OldNew: String);
begin
  if NType1 > 0 then begin
    Label1.Caption := 'Copying to '+SharedFolderFileName;
    CopyFile(PChar(AppendFileName),PChar(SharedFolderFileName),false);
    CopyFile(Pchar(AppendFileName),Pchar(SaveFileName),false);
    MsgBox('Processing '+OldNew+' format complete.  '+Long2StrL(NType1)
      +' clubs,  '+Long2StrL(NRecs)+' records.',MC,0);
    Anyfiles := true;
  end
  else MsgBox('No '+OldNew+' format club report files found.',MC,0);
end;

{$ifndef test}
procedure Send400FTPCommand;
begin
  FTPis400 := true;
  while Connected do Application.ProcessMessages;
  with NMFTP1 do begin
    Host := AS400Host;
    Timeout := 60000;
    UserId := AS400UserId;
    Password := AS400Password;
    Connect;
  end;
end;
{$endif}

{$ifdef image}
procedure SendImageFTPCommand;
begin
  FTPis400 := false;
  while Connected do Application.ProcessMessages;
  with NMFTP1 do begin
    Host := ImageHost;
    Timeout := 60000;
    UserId := ImageUserId;
    Password := ImagePassword;
    Connect;
  end;
end;
{$endif}

procedure DeleteOutFiles;
var
   F      : TextFile;
   Fname  : String;
begin
  if not FileExists(FileListName) then exit;
  AssignFile(F,FileListName);
  ReSet(F);
  while not Eof(F) do begin
    ReadLn(F,Fname);
    if FileExists(outpath+Fname) then DeleteFileAttr(outpath+Fname);
  end;
  CloseFile(F);
  DeleteFileAttr(FileListName);
end;

begin
  Form1.Caption := 'Append Club masterpoint files';
  MakeOutpathDirs;
  Connected := false;
  AnyFiles := false;
  if CheckDrive(Escaped,OutPath,false) <> 0 then exit;
  if CheckDrive(Escaped,SavePath,false) <> 0 then exit;
  if CheckDrive(Escaped,SharedFolder,false) <> 0 then exit;
  if not CheckProcess(OldSharedFolderFileName,'MP') then exit;
  if not CheckProcess(SharedFolderFileName,'MP') then exit;
  if not CheckProcess(FinSharedFolderFileName,'Financial') then exit;
  DecodeDate(Now,Year,Month,Day);
  Dec(Year,ProcessYears);
  for j := 1 to ProcessMonths do begin
    ProcessTypes[j] := Long2StrL(Year mod 10)+Numcvt(month,2,true);
    Inc(Month);
    if Month > 12 then begin
      Inc(Year);
      Month := 1;
    end;
  end;
  {$ifdef repmponly}
  NumMPonly := 0;
  {$endif}
  New(ClubFiles);
  FindClubFiles(1,AppendFileName);
  if canceled then exit;
  if NType1 > 0 then
    WriteLn(outfile,'0',numcvt(NType1,5,true),numcvt(NRecs,5,true));
  CloseFile(outfile);
  CopyClubFiles(AppendFileName,SharedFolderFileName,SaveFileName,'NEW');
  Erase(outfile);
  FindClubFiles(0,OldAppendFileName);
  if canceled then exit;
  CloseFile(outfile);
  CopyClubFiles(OldAppendFileName,OldSharedFolderFileName,OldSaveFileName,'OLD');
  Erase(outfile);
  FindClubFiles(2,FinAppendFileName);
  if canceled then exit;
  if NType1 > 0 then
    WriteLn(outfile,'0',numcvt(NType1,4,true),numcvt(NRecs,5,true));
  CloseFile(outfile);
  CopyClubFiles(FinAppendFileName,FinSharedFolderFileName,FinSaveFileName,
    'Finance');
  Erase(outfile);
  DeleteOutFiles;
  if FileExists(outpath+FinTextFileName) then begin
    DecodeDate(IncMonth(Now,-1),Year,Month,Day);
    SaveStr := Numcvt(Year,4,true);
    CreateDirAttr(ClubSavePath+SaveStr,faReadOnly);
    SaveStr := SaveStr+'\'+numcvt(Month,2,true);
    CreateDirAttr(ClubSavePath+SaveStr,faReadOnly);
    SaveStr := SaveStr+'\';
    j := 0;
    repeat
      Inc(j);
      SearchFType := numcvt(j,3,true);
    until (j > 999) or not FileExists(ClubSavePath+SaveStr+FinTextName+SearchFType);
    if j > 999 then SearchFType := '000';
    CopyFileAttr(outpath+FinTextFileName,
      PChar(ClubSavePath+SaveStr+FinTextName+SearchFType),faReadOnly);
    {$ifdef image}
    SendImageFtpCommand;
    {$else}
    CopyFile(PChar(outpath+FinTextFileName),PChar(SharedFolder+FinTextFileName),false);
    {$endif}
    DeleteFileAttr(outpath+FinTextFileName);
  end;
  {$ifndef test}
  if AnyFiles then Send400FtpCommand
  else MsgBox('No club reports found.  Nothing processed.',MC,0);
  {$endif}
  {$ifdef repmponly}
  if NumMPonly > 0 then begin
    ViewerInit;
    ViewerAdd('Please print this (File, Print).',true);
    ViewerAdd('',true);
    ViewerAdd('The following clubs do NOT have a financial report included.',true);
    ViewerAdd('The paper financial report submitted by the club needs to be processed.',true);
    ViewerAdd('',true);
    ViewerAdd('Club#  Month   Version, Club name',true);
    for j := 1 to NumMPonly do ViewerAdd(MPonlyClubs[j],true);
    ErrBox('Please print the next screen',MC,0);
    ViewNeedPrint := true;
    ViewerShow('Clubs with no Financial report','NoFinClubs.txt');
    ViewerDone;
  end;
  {$endif}
  exit;
end;

procedure PrintClubMP;

procedure PrintMasterpoints;

var
   PlayKey        : KeyStr;
   wseq           : word;
   lastseqdiff    : word;
   j              : word;
   Drec           : integer;
   Thismp         : word;
begin
  lastseqdiff := 0;
  Reset(infile);
  FillChar(TotMPS,SizeOf(TotMPs),0);
  FillChar(ChangeMPS,SizeOf(ChangeMPs),0);
  recs := 0;
  ClubMPRec := @inrec[1];
  {$I-} ReadLn(infile,inrec); {$I+}
  CheckIOResult(infilename);
  if canceled then exit;
  if intype = mpftnew then begin
    inrec := PadL(inrec,96);
    Move(ClubMPRec^,ClubMPHeader,SizeOf(TClubMPRec));
    InitClubMPPrint;
    Inc(recs);
    ERec := 0;
    while ERec < EventRecs do begin
      Inc(ERec);
      if Bad_Eof(infile) then exit;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
      Move(ClubMPRec^,ClubMPEvent,SizeOf(TClubMPRec));
      with ClubMPEvent do begin
        if not ValidField(@RecType,1,'2') then exit;
        wseq := ChrNum(@sequence,4);
        if wseq > ERec+lastseqdiff then begin
          Dec(EventRecs,wseq-ERec-lastseqdiff);
          lastseqdiff := wseq-ERec;
        end;
        if lastseqdiff > 0 then NumChr(@sequence,4,ERec);
        if not ValidField(@sequence,4,numcvt(ERec,4,true)) then exit;
        if not ValidField(@ClubNum,6,club) then exit;
        hrating := ChrNum(@Erating,2);
        InitEventMPPrint;
      end;
      Inc(recs);
      LastPlayer := CharStrL(' ',40);
      FillChar(LastMps,SizeOf(LastMPs),0);
      gtables := RRound(ChrReal(@ClubMPEvent.tables,4,1)+0.4);
      {bad49 := false;
      check49 := false;}
      for DRec := 1 to DetailRecs do begin
        if Bad_Eof(infile) then exit;
        {$I-} ReadLn(infile,inrec); {$I+}
        CheckIOResult(infilename);
        if canceled then exit;
        with ClubMPRec^ do begin
          if not (RecType in ['3','4']) then
            if not ValidField(@RecType,1,'3') then exit;
          if not ValidField(@sequence,4,numcvt(DRec,4,true)) then exit;
          if not ValidField(@ClubNum,6,club) then exit;
          Inc(recs);
          PrintMPDetailRec;
        end;
      end;
      FinishLastPlayer;
      ReportLine(lskip,pnone,1,CharStrL('-',40));
    end;
    if Bad_Eof(infile) then exit;
    {$I-} ReadLn(infile,inrec); {$I+}
    CheckIOResult(infilename);
    if canceled then exit;
    Inc(recs);
    with ClubMPRec^ do begin
      if not ValidField(@RecType,1,'9') then exit;
      if not ValidField(@sequence,4,numcvt(recs,4,true)) then exit;
    end;
    for j := 1 to 4 do Inc(TotMPS[5],TotMPS[j]);
    ReportLine(lskip,pnone,1,'Totals:        '
      +Real2StrL(TotMPs[5]/100,11,2)+' ');
    for j := 1 to MaxHighColor do ReportLine(0,pnone,1,
      Real2StrL(TotMPs[j]/100,7,2)+ColorLetter[j]);
  end
  else begin
    InitClubMPPrint;
    ReportLine(lskip,ppica,1,Copy(inrec,21,2)+'/20'+Copy(inrec,19,2));
    ReportLine(0,pnone,1,' '+StrPas(RatingText[16]));
    NewLine(1);
    while not Eof(infile) do begin
      if not Empty(inrec) then begin
        PlayKey := Player_key(Copy(inrec,1,7));
        FindKey(IdxKey[Filno,2]^,Recno[Filno],PlayKey);
        if ok then with Playern do begin
          GetARec(FilNo);
          ThisPlayer := PadL(Copy(TrimL(First_Name)+' '+Trim(Last_Name),1,20),20)
            +Player_no;
        end
        else ThisPlayer := PadL('Player not found.',20)+Copy(inrec,1,7);
        Thismp := Ival(Copy(inrec,14,5));
        Inc(TotMPs[1],Thismp);
        ReportLine(1,pelite,1,ThisPlayer+Real2StrL(ThisMP/100,7,2));
      end;
      {$I-} ReadLn(infile,inrec); {$I+}
      CheckIOResult(infilename);
      if canceled then exit;
    end;
    ReportLine(lskip,pnone,1,'Totals:                '+Real2StrL(TotMPs[1]/100,11,2));
  end;
  CloseMPFiles;
  CloseDBFile(Playern_no);
  if Destination = 1 then ViewerShow('',infilename);
end;

const
     MaxChoice = 7;
     choicetext : Array[1..MaxChoice] of Pchar =
                ('1 Club masterpoint report',
                 '2 Club financial report',
                 '3 Internet Club masterpoint report',
                 '4 This year''s Email log file',
                 '5 Last year''s Email log file',
                 '6 List of clubs allowed to email without credit card',
                 '7 List of clubs not allowed to report');
     Credittext : Array[1..3] of Pchar =
                ('1 Show clubs allowed to email without credit card',
                 '2 Add a club to the list',
                 '3 Remove a club from the list');
     BadClubtext : Array[1..3] of Pchar =
                ('1 Show clubs not allowed to report',
                 '2 Add a club to the list',
                 '3 Remove a club from the list');
   Choice         : byte = 1;
   LastPick       : String = '';

var
   typetext       : String;
   Mask           : String;
   cline          : string;
   FindErr        : integer;
   firstdone      : boolean;
   Day,Month,Year : word;
   CheckF         : TextFile;
   CheckT         : TextFile;
   ifunc          : byte;
   iclen          : byte;
   clubn          : string;

function isCheckClub(const club: string):boolean;
begin
  if FileExists(CheckFName) then begin
    AssignFile(CheckF,CheckFName);
    Reset(CheckF);
    cline := 'xx';
    repeat
      ReadLn(CheckF,cline);
    until Eof(CheckF) or (cline=club);
    Result := cline=club;
    CloseFile(CheckF);
  end
  else Result := false;
end;

procedure AddCheckClub(const club: string);
begin
  AssignFile(CheckF,CheckFName);
  if FileExists(CheckFName) then Append(CheckF)
  else ReWrite(CheckF);
  WriteLn(CheckF,club);
  CloseFile(CheckF);
end;

function isBadClub(const club: string):boolean;
begin
  if FileExists(BadClubFName) then begin
    AssignFile(CheckF,BadClubFName);
    Reset(CheckF);
    cline := 'xx';
    repeat
      ReadLn(CheckF,cline);
    until Eof(CheckF) or (cline=club);
    Result := cline=club;
    CloseFile(CheckF);
  end
  else Result := false;
end;

procedure AddBadClub(const club: string);
begin
  AssignFile(CheckF,BadClubFName);
  if FileExists(BadClubFName) then Append(CheckF)
  else ReWrite(CheckF);
  WriteLn(CheckF,club);
  CloseFile(CheckF);
end;

label
     try_again,
     credit_again,
     BadClub_again,
     path_again;
begin
  Form1.Caption := 'Display/print Club files';
  MakeOutpathDirs;
  escaped := false;
  InLocP[3] := ClubSavePath;
  InLocP[6] := ClubSavePath;
  firstdone := false;
  inloc := 3;
  DecodeDate(Now,Year,Month,Day);
path_again:
  DelTempDirFiles;
  ChDir(TempDir);
  Close_Destination;
  MaxHighColor := 0;
  if escaped then exit;
  Choice := PickChoice(MaxChoice,Choice,'type of club file (ESC to quit) ',
    @choicetext,MC,false,0);
  if Choice < 1 then exit;
  case Choice of
    1: begin
      typetext := 'masterpoint';
      Mask := CfileTypes[1]+ClubFileMask+'*';
    end;
    2: begin
      typetext := 'financial';
      Mask := Cfiletypes[2]+ClubFileMask+'*';
    end;
    3: begin
      typetext := 'masterpoint';
      Mask := CfileTypes[0]+ClubFileMask+'*';
    end;
    4,5: begin
      typetext := 'email log';
      Mask := 'CLUB'+numcvt(Year+4-Choice,4,true)+EmailLogExt;
      inpath := EmailPath;
    end;
    6: begin
      Choice := 1;
Credit_again:
      Choice := PickChoice(3,Choice,'option (ESC to quit) ',@Credittext,MC,false,0);
      if Choice < 1 then goto path_again;
      case Choice of
        1: begin
          if FileExists(CheckFname) then ViewFile('Club email log file',CheckFname,'')
          else ErrBox('No clubs are allowed to email without credit card',MC,0);
        end;
        2: begin
          repeat
            StrBox(escaped,ifunc,'Club number to add','Enter club number to add',
              clubn,6,iclen,2,4,0,MC,nil);
            if not escaped and not Empty(clubn) and not Valid_club(clubn,true) then
              ErrBox('Club '+clubn+' is invalid',MC,0);
          until Valid_club(clubn,true) or escaped or Empty(clubn);
          if Valid_club(clubn,true) then begin
            if isCheckClub(clubn) then ErrBox('Club '+clubn+' is already in the list',MC,0)
            else begin
              AddCheckClub(clubn);
              MsgBox('Club '+clubn+' has been added',MC,0);
            end;
          end;
        end;
        3: begin
          repeat
            StrBox(escaped,ifunc,'Club number to remove',
              'Enter club number to remove',clubn,6,iclen,2,4,0,MC,nil);
            if not escaped and not Empty(clubn) and not Valid_club(clubn,true) then
              ErrBox('Club '+clubn+' is invalid',MC,0);
          until Valid_club(clubn,true) or escaped or Empty(clubn);
          if Valid_club(clubn,true) then begin
            if not isCheckClub(clubn) then ErrBox('Club '+clubn+' is not in the list',MC,0)
            else begin
              AssignFile(CheckF,CheckFName);
              Reset(CheckF);
              AssignFile(CheckT,CheckTName);
              ReWrite(CheckT);
              cline := 'xx';
              repeat
                ReadLn(CheckF,cline);
                if cline <> clubn then WriteLn(CheckT,cline);
              until Eof(CheckF);
              CloseFile(CheckF);
              CloseFile(CheckT);
              DeleteFile(CheckFName);
              RenameFile(CheckTName,CheckFName);
              MsgBox('Club '+clubn+' has been removed',MC,0);
            end;
          end;
        end;
      end;
      goto Credit_again;
    end;
    7: begin
      Choice := 1;
BadClub_again:
      Choice := PickChoice(3,Choice,'option (ESC to quit) ',@BadClubtext,MC,false,0);
      if Choice < 1 then goto path_again;
      case Choice of
        1: begin
          if FileExists(BadClubFname) then ViewFile('Club email log file',BadClubFname,'')
          else ErrBox('No clubs are not allowed to report',MC,0);
        end;
        2: begin
          repeat
            StrBox(escaped,ifunc,'Club number to add','Enter club number to add',
              clubn,6,iclen,2,4,0,MC,nil);
            if not escaped and not Empty(clubn) and not Valid_club(clubn,true) then
              ErrBox('Club '+clubn+' is invalid',MC,0);
          until Valid_club(clubn,true) or escaped or Empty(clubn);
          if Valid_club(clubn,true) then begin
            if isBadClub(clubn) then ErrBox('Club '+clubn+' is already in the list',MC,0)
            else begin
              AddBadClub(clubn);
              MsgBox('Club '+clubn+' has been added',MC,0);
            end;
          end;
        end;
        3: begin
          repeat
            StrBox(escaped,ifunc,'Club number to remove',
              'Enter club number to remove',clubn,6,iclen,2,4,0,MC,nil);
            if not escaped and not Empty(clubn) and not Valid_club(clubn,true) then
              ErrBox('Club '+clubn+' is invalid',MC,0);
          until Valid_club(clubn,true) or escaped or Empty(clubn);
          if Valid_club(clubn,true) then begin
            if not isBadClub(clubn) then ErrBox('Club '+clubn+' is not in the list',MC,0)
            else begin
              AssignFile(CheckF,BadClubFName);
              Reset(CheckF);
              AssignFile(CheckT,BadClubTName);
              ReWrite(CheckT);
              cline := 'xx';
              repeat
                ReadLn(CheckF,cline);
                if cline <> clubn then WriteLn(CheckT,cline);
              until Eof(CheckF);
              CloseFile(CheckF);
              CloseFile(CheckT);
              DeleteFile(BadClubFName);
              RenameFile(BadClubTName,BadClubFName);
              MsgBox('Club '+clubn+' has been removed',MC,0);
            end;
          end;
        end;
      end;
      goto BadClub_again;
    end;
  end;
  quarintine := false;
  if Choice in [1,2,3] then begin
    if firstdone then inpath := inpathsave
    else begin
      InLoc := PickChoice(MaxLocPick,inloc,'club '+typetext
        +' file location (ESC to quit) ',@inLocT,MC,false,0);
      if InLoc < 1 then exit;
      if (Inloc = 4)
        and not GetFolder(inLocP[Inloc],'Select Club report location',true,
          'OtherViewLoc','C:\') then exit;
      inPath := inLocP[inLoc];
    end;
  end;
try_again:
  if CheckDrive(Escaped,inPath,false) <> 0 then goto path_again;
  if Choice in [4,5] then begin
    FindErr := FindFirst(inPath+Mask,faArchive,Search_Rec);
    FindClose(Search_Rec);
    if FindErr <> 0 then begin
      ErrBox('No club '+typetext+' file(s) found at '+inPath,MC,0);
      goto Path_again;
    end;
    infilename := inPath+Search_Rec.Name;
    ViewFile('Club email log file',infilename,'');
    goto Path_again;
  end;
  inFileName := LastPick;
  {$ifdef pickfolder}
  if (Inloc in [3,6]) and not GetFolder(Inpath,'Select Club report location',
    '','','') then goto path_again;
  {$endif}
  allowfolderchange := false;
  if Inloc = 6 then begin
    if not GetInFileName(Inpath,DupClubMask,inFileName,
      'Select duplicate club report to view','','','ViewClubLocs') then goto path_again;
  end
  else if Choice = 3 then begin
    if not GetInFileName(Inpath,OldClubMask+';'+Mask,inFileName,
      'Select club report to view','','','ViewIntLocs') then goto path_again;
  end
  else if not GetInFileName(Inpath,NewClubMask+';'+Mask,inFileName,
    'Select club report to view','','','ViewClubLocs') then goto path_again;
  LastPick := JustFileNameL(infilename);
  firstdone := true;
  inpathsave := fixpath(JustPathNameL(infilename));
  DelTempDirFiles;
  inPath := fixpath(TempDir);
  if UpperCase(JustExtensionL(infilename)) = 'LZH' then begin
    if not RunLHA(infilename,inPath) then begin
      ErrBox(infilename+' is corrupt',MC,0);
      goto Path_again;
    end;
    FindErr := FindFirst(inPath+Mask,faArchive,Search_Rec);
    FindClose(Search_Rec);
    if FindErr <> 0 then begin
      ErrBox('No club '+typetext+' file(s) found at '+inPath,MC,0);
      goto Path_again;
    end;
    infilename := inPath+Search_Rec.Name;
  end
  else begin
    CopyFile(PChar(infilename),PChar(inPath+LastPick),false);
    infilename := inPath+LastPick;
  end;
  inname := JustFileNameL(infilename);
  if Empty(inname) then goto path_again;
  if Choice in [1,3] then begin
    Cftype := 'MP';
    intype := ValidMPFile(infile,infileName,inrec,true);
    if intype < 1 then goto path_again;
    CloseMPfiles;
    {if intype <> 2 then begin
      ErrBox('Only new club MP format can be viewed.',MC,0);
      goto path_again;
    end;}
  end;
  OpenDestination(1,0,105);
  SetPageProc(Choice);
  case Choice of
    1,3: begin
      CFG.PLpath := LanPlPath;
      OpenDBFile(Playern_no);
      FilNo := Playern_no;
      PrintMasterpoints;
      CloseDBFile(Playern_no);
    end;
    2: begin
      PrintFinance;
            if quarintine and YesNoBox(escaped,false,'Copy '+LastPick
        +' for processing',0,MC,nil) then begin
        CopyFile(PChar(InLocP[Inloc]+LastPick),PChar(ClubrepPath+LastPick),false);
        if FileExists(ClubrepPath+LastPick) then begin
          DeleteFileAttr(InLocP[Inloc]+lastpick);
          MsgBox(LastPick
            +' will be included the next time club email reports are processed.',
            MC,0);
          if (ClubFinMast2.PaymentType <> '1') and
            not isCheckClub(club) and YesNoBox(escaped,false,'Allow club '+club
            +' email reports to be processed'
            +#13'without credit card information in the future',0,MC,nil) then
            AddCheckClub(club);

        end
        else ErrBox(LastPick+' could not be copied',MC,0);
      end;
    end;
  end;
  if canceled then exit;
  goto path_again;
end;

begin
  AppName := 'CmpCopy';
  FindCFG;
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  DBReadOnly := true;
  Flush_DF := true;
  Flush_IF := true;
  UseLookupDB := true;
  inloc := 1;
  if UpperCase(ParamStr(1)) = 'COPY' then CopyClubMP
  else if UpperCase(ParamStr(1)) = 'PRINT' then PrintClubMP
  else if UpperCase(ParamStr(1)) = 'APPEND' then AppendClubMP;
  Application.Terminate;
end;

procedure TForm1.TaskComplete(Sender: TObject);
begin
  isTaskComplete := true;
end;

procedure TForm1.TaskError(Sender: TObject; Error: Word);
begin
  isTaskError := true;
  TaskErrorNo := Error;
  isTaskComplete := true;
end;

procedure TForm1.TaskTimeOut(Sender: TObject);
begin
  isTaskTimeOut := true;
  isTaskComplete := true;
end;

procedure TForm1.FTPerror(var Handled: Boolean; Trans_Type: TCmdType);
var
   Errmsg : String;
begin
  Errmsg := '';
  Case Trans_Type of
    cmdChangeDir: Errmsg := 'ChangeDir failure';
    cmdMakeDir: Errmsg := 'MakeDir failure';
    cmdDelete: Errmsg := 'Delete failure';
    cmdRemoveDir: Errmsg := 'RemoveDir failure';
    cmdList: Errmsg := 'List failure';
    cmdRename: Errmsg := 'Rename failure';
    cmdUpRestore: Errmsg := 'UploadRestore failure';
    cmdDownRestore: Errmsg := 'DownloadRestore failure';
    cmdDownload: Errmsg := 'Download failure';
    cmdUpload: Errmsg := 'Upload failure';
    cmdAppend: Errmsg := 'UploadAppend failure';
    cmdReInit: Errmsg := 'ReInit failure';
    cmdAllocate: Errmsg := 'Allocate failure';
    cmdNList: Errmsg := 'NList failure';
    cmdDoCommand: Errmsg := 'DoCommand failure';
  end;
  if Length(errmsg) > 0 then begin
    ShowMessage(errmsg);
    Application.Terminate;
  end;
end;

procedure TForm1.FTPConnect(Sender: TObject);
begin
  Connected := true;
  if FTPis400 then begin
    {$ifndef test}
    Label1.Caption := 'Sending job to AS400...';
    Application.ProcessMessages;
    try
      NMFTP1.DoCommand(AS400Command);
      MsgBox('Job has been submitted to AS400',MC,0);
    except
      ErrBox('Unable to send job to AS400',MC,0);
    end;
    {$endif}
  end
  else begin
    {$ifdef image}
    Label1.Caption := 'Copying '+FinTextFileName+' to Image server';
    Application.ProcessMessages;
    try
      NMFTP1.ChangeDir(ImageDir);
      NMFTP1.UploadAppend(outpath+FinTextFileName,FinTextFileName);
    except
      ErrBox('Unable to copy '+FinTextFileName+' to Image server',MC,0);
    end;
    {$endif}
  end;
  NMFTP1.Disconnect;
end;

procedure TForm1.FTPDisconnect(Sender: TObject);
begin
  Connected := false;
end;

procedure TForm1.LHAfiledone(const originalFilePath, newFilePath: String;
  dateTime: Integer; mode: TCompLHAProcessMode);
var
   LFname : string;
   j      : integer;
begin
  if (mode in [cmLHAExpand]) then begin
    LFname := JustFileNameL(newFilePath);
    j := Pos('.',LFname);
    if j > 8 then begin
      LFname := Copy(LFname,j-7,20);
      CopyFile(PChar(newFilePath),PChar(fixpath(JustPathNameL(newFilePath))+LFname),false);
      DeleteFile(newFilePath);
    end;
  end;
end;

end.
