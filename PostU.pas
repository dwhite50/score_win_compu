unit PostU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Postit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses
    pvio,
    YesNoBoxU,
    dohttp;

const
     Posturl = 'www.acbl.org/asset/_code/_devel/acblscore-capture-headers.php';
     fname = 'postu.pas';
     headerdata = 'application/octet-stream';

procedure TForm1.Postit(Sender: TObject);
begin
//  if GetHTTP(Posturl,'headers.php',10) then MsgBox('get ok',MC,0);
  //exit;
  if not FileExists(fname) then begin
    ErrBox('File '+fname+' does not exist',MC,0);
  end
  else if PostHTTP(Posturl,headerdata,fname,10) then begin
    MsgBox('File '+fname+' has been posted to '+posturl,MC,0);
  end
  else ErrBox('Unable to post '+fname+' to '+posturl,MC,0);
end;

end.
