unit printeru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure doprinter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses
    csdef,
    selprinter;

{$R *.DFM}

procedure TForm1.doprinter(Sender: TObject);
begin
  IniName := GetCurrentDir+'\'+DefIniName;
  AppName := 'Printer';
  SelectPrinter;
  Application.Terminate;
end;

end.
