unit StrBoxU;
{$define help}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcEF, OvcSF, pvio, OvcEditF, OvcEdPop, OvcEdCal;

type
  TStringForm = class(TForm)
    StringField: TOvcSimpleField;
    OvcController1: TOvcController;
    OKbut: TButton;
    CanBut: TButton;
    HelpBut: TButton;
    DateField: TOvcDateEdit;
    Label1: TLabel;
    procedure AcceptIt(Sender: TObject);
    procedure CancelIt(Sender: TObject);
    procedure FieldChange(Sender: TObject);
    procedure UserCommand(Sender: TObject; Command: Word);
    procedure DoHelp(Sender: TObject);
    procedure FirstEntry(Sender: TObject);
    procedure KeyPress(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
   acase   : byte;
   last_ret : boolean;
   atype    : byte;
   check_changed : boolean;
   doarr         : boolean;
   dofunc        : boolean;
   SlashBack     : boolean;
   Canceled  : boolean;
   funccode  : byte;
   first     : boolean;
  public
    { Public declarations }
  end;

procedure StrBox(var esc: boolean; var ifunc: byte;
          const title,prompt: String; var field : String;
          const len: byte; var iclen: byte; const typea,icase: smallint;
          const topic: word; const location: byte; const Stproc: TStringProc);

procedure DateBox(var esc: boolean; const title,prompt: String; var field : String;
          const topic: word; const location: byte; const Stproc: TStringProc);
(*INPUT:
	title   Form title
        prompt  Prompt displayed at top of field
	field   String   Initial and returned field value
	LEN	byte	 Maximum field length
	ICLEN	byte	 Returned length
	TYPEA	smallint Editing type:
		1	Normal field editing with case conversion.
		2	Initialize to blanks, normal editing with case
			conversion.
		4	Normal editing with case conversion, "/" in first
			position clears field.
                5       Normal editing with case conversion, must not be
                        blank.
	    TYPE+10	Allow UP and DOWN cursor key return
	    TYPE+20	Allow Function key return
        -ve Set ASKESC variable if field changed.
	ICASE	smallint Case conversion type:
		0	No case conversion.
		1	Convert to UPPER-CASE.
		2	Convert to lower-case.
                3       Convert 1st letter in each word to Upper-case.
		4	Numeric only.
                5       1st letter in each word UC, all others lower-case.
                6       Player number - UC - convert first charcter from 0 to O.
                7       File name. -> UC, check for invalid chars
                CASE+10 Exit if last field position entered
        -ve  "/" key = backspace
        TOPIC   word    If > 0 then help topic number
        Stproc procedure address of procedure to call - usually nil.

OUTPUT:
	ESC	boolean	 true if ESC key pressed or Cancel pressed, otherwise false.
	IFUNC 	byte	non-zero if function key pressed.
*)

const
     PassField : boolean = false;
     sUseFixedFont : boolean = false;
     sInsoff       : boolean = false;

implementation

uses
    csdef,
    util1,
    util2,
    StStrL;

{$R *.DFM}

const
  StringForm: TStringForm = nil;

{var
   HelpTopic : word;}

procedure SetFormWidth(const title,prompt: String; const Iwidth: integer;
          const topic: word; const location: byte; const isdate: boolean);
var
   MaxTextWidth        : integer;
   MaxTextHeight       : integer;
   w     : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   yline : String;
   cw    : integer;
   j     : integer;
begin
  with StringForm do begin
    w := 0;
    yline := xline;
    repeat
      j := Pos(#13,yline);
      if j < 1 then j := Length(yline)+1;
      if j > 1 then Line := Copy(yline,1,j-1)
      else Line := '';
      yline := Copy(yline,j+1,32000);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
        cw := Canvas.TextHeight(Line);
        if cw > MaxTextHeight then MaxTextHeight := cw;
      end;
      Inc(w);
    until Length(yLine) < 1;
  end;
end;

begin
  with StringForm do begin
    {$ifdef help}
    HelpFile := DefHelpFile;
    HelpContext := topic;
    {$else}
    HelpContext := 0;
    {$endif}
    First := false;
    HelpBut.Visible := HelpContext > 0;
    MaxTextWidth := 0;
    MaxTextHeight := 10;
    GetTextWidth(title);
    GetTextWidth(prompt);
    if HelpContext > 0 then Width := 230
    else Width := 200;
    if HelpContext > 0 then Width := Width+HelpBut.Width+14;
    ClientHeight := w * MaxTextHeight + 85;
    if isdate then DateField.Top := Height-100
    else StringField.Top := Height-100;
    OkBut.Top := Height - 60;
    CanBut.Top := OKBut.Top;
    HelpBut.Top := OKBut.top;
    if isdate then with DateField do begin
      Height := MaxTextHeight;
      Width := Canvas.TextWidth(CharStrL('M',Iwidth+1));
      if Width > MaxTextWidth then MaxTextWidth := Width;
    end
    else with StringField do begin
      Height := MaxTextHeight;
      Width := Canvas.TextWidth(CharStrL('M',Iwidth+1));
      if Width > MaxTextWidth then MaxTextWidth := Width;
    end;
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    with Label1 do begin
      Height := MaxTextHeight * w;
      Caption := prompt;
      Top := 10;
      Left := 5;
      Width := StringForm.Width-20;
    end;
    Okbut.Left := 8;
    if HelpContext > 0 then begin
      HelpBut.Left := Width-HelpBut.Width-14;
      CanBut.Left := (Width-CanBut.Width) div 2;
    end
    else CanBut.Left := Width-CanBut.Width-14;
    Caption := title;
    Left := SetformLeft(Width,location);
    Top := SetFormtop(Height,location);
  end;
end;

procedure DateBox(var esc: boolean; const title,prompt: String; var field : String;
          const topic: word; const location: byte; const Stproc: TStringProc);
var
   Year,Month,Day : word;
begin
  FreeHintWin;
  if not Assigned(StringForm) then
    Application.CreateForm(TStringForm, StringForm);
  with StringForm do begin
    StringField.Visible := false;
    DateField.Visible := true;
    Label1.Visible := true;
    if sUseFixedFont then begin
//      UseGlobal := true;
      Font.Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
      sUseFixedFont := false;
    end;
    Font.Size := ViewFontSize;
    SetFormWidth(title,prompt,10,topic,location,true);
    Canceled := false;
    if Length(field) = 8 then begin
      Year := Ival(Copy(field,5,4));
      Month := Ival(Copy(field,1,2));
      Day := Ival(Copy(field,3,2));
      DateField.Date := EnCodeDate(Year,Month,Day);
    end;
    ShowModal;
    if not Canceled then field := DateField.DateString('mmddyyyy');
    esc := Canceled;
    Free;
  end;
  StringForm := nil;
  if not esc and Assigned(StProc) then StProc(prompt+' '+Slash(field));
end;

procedure StrBox(var esc: boolean; var ifunc: byte; const title,prompt: String;
          var field : String; const len: byte; var iclen: byte;
          const typea,icase: smallint; const topic: word; const location: byte;
          const StProc: TStringProc);

procedure CheckPLFirst;
begin
  if (Length(field) > 0) and (field[1] = '0') then field[1] := 'O';
end;

begin
  FreeHintWin;
  if not Assigned(StringForm) then
    Application.CreateForm(TStringForm, StringForm);
  with StringForm do begin
    DateField.Visible := false;
    StringField.Visible := true;
    Label1.Visible := true;
    if sUseFixedFont then begin
//      UseGlobal := true;
      Font.Name := GetAppProfile('ViewFontName','Courier New',GlobalApp);
      sUseFixedFont := false;
    end;
    Font.Size := ViewFontSize;
    SetFormWidth(title,prompt,len,topic,location,false);
    Canceled := false;
    atype := abs(typea);
    check_changed := ((ifunc and 2) = 2) or (typea < 0);
    doarr := (atype > 9);
    if doarr then Dec(atype,10);
    dofunc := (atype > 9);
    if dofunc then Dec(atype,10);
    acase := Abs(icase);
    SlashBack := icase < 0;
    last_ret := acase > 9;
    funccode := 0;
    if last_ret then Dec(acase,10);
    with StringField do begin
      Datatype := sftString;
      case acase of
        0: PictureMask := 'X';
        1: PictureMask := '!';
        2: PictureMask := 'L';
        3,5: PictureMask := 'x';
        4: PictureMask := '#';
        6: begin
          PictureMask := '!';
          CheckPLFirst;
        end;
        7: PictureMask := '!';
        else PictureMask := 'X';
      end;
      MaxLength := len;
      if atype = 2 then SetInitialValue
      else Text := field;
      if PassField then Options := [efoCaretToEnd,efoTrimBlanks,efoPasswordMode]
      else if sInsOff then Options := [efoForceOverType,efoTrimBlanks]
      else Options := [efoCaretToEnd,efoTrimBlanks];
    end;
    ShowModal;
    if not Canceled then begin
      field := StringField.EditString;
      if acase = 6 then CheckPLFirst;
    end;
    iclen := Length(field);
    esc := Canceled;
    ifunc := funccode;
    Free;
  end;
  StringForm := nil;
  PassField := false;
  sInsoff := false;
  if not esc and Assigned(StProc) then StProc(prompt+' '+field);
end;

procedure TStringForm.AcceptIt(Sender: TObject);
begin
  if (atype = 5) and (Length(Trim(StringField.EditString)) < 1) then
    MessageBeep(mb_iconasterisk)
  else Close;
end;

procedure TStringForm.CancelIt(Sender: TObject);
begin
  Close;
  Canceled := true;
end;

procedure TStringForm.FieldChange(Sender: TObject);
begin
  with StringField do
    if last_ret and (CurrentPos >= MaxLength) then Close;
end;

procedure TStringForm.UserCommand(Sender: TObject; Command: Word);
begin
  if not dofunc then exit;
  Close;
  Funccode := Command-255;
end;

procedure TStringForm.DoHelp(Sender: TObject);
begin
  if HelpContext > 0 then begin
    keybd_event(VK_F1,MapVirtualKey(VK_F1,0),0,0);
  end;
end;

procedure TStringForm.FirstEntry(Sender: TObject);
begin
  if First then exit;
  if DateField.Visible then DateField.PopupOpen;
  if sInsOff then StringField.DeSelect;
  First := true;
end;

procedure TStringForm.KeyPress(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if doarr then begin
    funccode := 0;
    case Key of
      VK_UP: Funccode := 72;
      VK_DOWN: Funccode := 80;
      VK_PRIOR: Funccode := 73;
      VK_NEXT: Funccode := 81;
    end;
    if funccode > 0 then begin
      Key := 0;
      close;
    end;
  end;
  if key = 121 then begin
    key := 0;
    UserCommand(Sender,264);
  end;
end;

end.
