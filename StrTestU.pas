unit StrTestU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure StrTest(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    StrBoxU,
    EditBoxU,
    StStrL,
    pvio;

{$R *.DFM}

procedure TForm1.StrTest(Sender: TObject);
var
   ES     : string;
   esc    : boolean;
   ifunc  : byte;
   len    : byte;
begin
  Es := '00000000';
  MsgBox('Before edit: '+Es,0);
  StrBox(esc,ifunc,'Box title','Edit prompt'#13'Prompt Line 2',es,40,len,5,3,0);
  MsgBox('After edit: '+Es+' Length='+Long2StrL(len),0);
  EditBox(esc,ifunc,'Box title','Edit prompt'#13'Prompt Line 2','##/##/####',
    es,10,0);
  MsgBox('After edit: '+Es,0);
  Application.Terminate;
end;

end.
