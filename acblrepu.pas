unit acblrepu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure runacblrep(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    acblrep,
    version,
    csdef;

{$R *.DFM}

procedure TForm1.runacblrep(Sender: TObject);
begin
  DoACBLrep;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
