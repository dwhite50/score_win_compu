unit dbimpu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TDBiform = class(TForm)
    procedure dodbimp(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DBiform: TDBiform;

implementation

uses
    version,
    dbimp;

{$R *.DFM}

const
     first : boolean = false;

procedure TDBiform.dodbimp(Sender: TObject);
begin
  if not first then RunDBImp;
  first := true;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
