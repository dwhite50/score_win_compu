unit Saveemail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label1: TLabel;
    procedure OpenTheFile(Sender: TObject);
    procedure SaveTheFile(Sender: TObject);
    procedure ProcessEmails(Sender: TObject);
    procedure DoCancel(Sender: TObject);
    procedure DoInit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Fin       : TextFile;
    Fout      : TextFile;
    FinName   : String;
    FoutName  : string;
    iscanceled : boolean;
    inopen     : boolean;
    outopen    : boolean;
    clubnum    : boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses
    StStrL,
    pvio,
    msort,
    FOpen,
    YesNoBoxU,
    util2,
    csdef;

{$R *.DFM}

procedure CancelIt;
begin
  with Form1 do begin
    iscanceled := true;
    if inopen then CloseFile(Fin);
    if outopen then CloseFile(Fout);
  end;
  Application.Terminate;
end;

procedure TForm1.OpenTheFile(Sender: TObject);
begin
  if GetFileName('','*.*',FinName,'Select file to scan for emails',
    'OpenDir','',false) then begin
    Button2.Enabled := true;
    Button1.Enabled := false;
    if Pos('CLUB',UpperCase(JustFileNameL(FinName))) > 0 then
      clubnum := YesNoBox(iscanceled,true,'Include club numbers',0,MC,nil);
    if iscanceled then CancelIt;
  end
  else CancelIt;
end;

procedure TForm1.SaveTheFile(Sender: TObject);
begin
  if GetFileName('','*.*',FoutName,'Select file save emails','SaveDir','',true)
    then begin
    Button3.Enabled := true;
    Button2.Enabled := false;
  end
  else CancelIt;
end;

procedure TForm1.DoCancel(Sender: TObject);
begin
  CancelIt;
end;

procedure TForm1.DoInit(Sender: TObject);
const
     ScoreDir = 'C:\ACBLSCOR';
begin
  iscanceled := false;
  inopen := false;
  outopen := false;
  clubnum := false;
  Button1.Enabled := true;
  Button2.Enabled := false;
  Button3.Enabled := false;
  Label1.Caption := 'This procedure will scan any file for email'
    +#13+'addresses and save them to a text file.';
  AppName := 'FindMail';
  CreateDir(ScoreDir);
  IniName := ScoreDir+'\ACBLUTIL.INI';
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Cancelit;
end;

procedure TForm1.ProcessEmails(Sender: TObject);
const
     delim = ' ,"<>()/\';
     sortlen = 60;
     sortkey = 'S(2,15,C,A)';
     sortname = 'SORT.TMP';
var
   Line   : String;
   email  : String;
   lemail : string;
   RepName: string;
   Line2  : String[80];
   words  : integer;
   j      : integer;
   num     : integer;
   longest : byte;
   ismail  : boolean;
   needtoread : boolean;
   sfile      : file;
begin
  Button3.Enabled := false;
  AssignFile(Fin,FinName);
  Reset(Fin);
  inopen := true;
  AssignFile(Fout,FoutName);
  if FileExists(FoutName) then Append(Fout)
  else ReWrite(Fout);
  outopen := true;
  longest := 0;
  num := 0;
  needtoread := true;
  while not Eof(Fin) do begin
    if needtoread then ReadLn(Fin,Line);
    needtoread := true;
    if (Length(Line) > 5) and not Empty(Line) then begin
      words := WordCountL(Line,delim);
      ismail := false;
      j := 0;
      while (j < words) and not ismail do begin
        Inc(j);
        email := LowerCase(ExtractWordL(j,Line,delim));
        if ValidEmailAddress(email,100) > 0 then begin
          ismail := clubnum and (Pos('acbl.org',email) < 1);
          if not ismail and not clubnum then WriteLn(Fout,email);
          if Length(email) > longest then begin
            Longest := Length(email);
            Lemail := email;
          end;
          Inc(num);
        end;
      end;
      if ismail then while needtoread do begin
        ReadLn(Fin,Line);
        RepName := LowerCase(ExtractWordL(3,Line,' '));
        if (Length(RepName) > 11) and Valid_Club(Copy(RepName,1,6),true) then
          WriteLn(Fout,Copy(RepName,1,6),' ',Copy(Line,9,2),'/',Copy(Line,1,5),
          ' ',email)
        else needtoread := false;
      end;
    end;
  end;
  MsgBox('Number of emails '+Long2StrL(num)+#13
    +'Longest email '+Long2StrL(longest)+#13+lemail,MC,0);
  if clubnum then begin
    CloseFile(Fin);
    CloseFile(Fout);
    inopen := false;
    outopen := false;
    Reset(Fout);
    AssignFile(sfile,sortname);
    ReWrite(sfile,1);
    while not Eof(Fout) do begin
      ReadLn(Fout,Line2);
      if Length(Line2) > 0 then BlockWrite(sfile,Line2,SortLen);
    end;
    CloseFile(Fout);
    CloseFile(sfile);
    MrgSort(sortname,sortname,sortkey,sortlen);
    ReSet(sfile,1);
    ReWrite(Fout);
    Line := '';
    num := 0;
    while not Eof(sfile) do begin
      BlockRead(sfile,Line2,sortlen,j);
      if j = sortlen then begin
        if (Length(Line) > 6) and (Copy(Line,1,6) <> Copy(Line2,1,6)) then begin
          WriteLn(Fout,Line);
          Inc(num);
        end;
        Line := Line2;
      end;
    end;
    WriteLn(Fout,Line);
    Inc(num);
    CloseFile(Fout);
    CloseFile(sfile);
    DeleteFile(sortname);
    MsgBox('Number of club emails: '+Long2StrL(num),MC,0);
  end;
  CancelIt;
end;

end.
