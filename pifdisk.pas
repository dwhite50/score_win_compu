unit pifdisk;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StBase, StShBase, StBrowsr, OvcBase, CompLHA;

type
  TForm1 = class(TForm)
    CompLHA1: TCompLHA;
    procedure MakePif(Sender: TObject);
    procedure LHAProg(var PercentageDone: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses
  history,
  util2,
  util1,
  pvio,
  version,
  Progress,
  vListBox,
  YesNoBoxU,
  FOpen,
  StStrL,
  StText,
  StUtils,
  csdef,
  mtables;

procedure TForm1.MakePif(Sender: TObject);
var
  Ch : Char;
  OutLoc  : byte;
  CompType : byte;
  OutPath : String;
  FreeSpace : Longint;
  done : boolean;
  key_char     : word;
  ifunc : byte;
  len   : byte;
  ftype : word;
  key   : word;
  Escaped : boolean;
  imp_file : textFile;
  ans : word;
  out_file : textFile;
  import_record : string;
  out_record : string;
  recs : LongInt;
  ImpTotRecs : longint;
  infilename,
  outfilename : String;
  Player_no : string[7];
  accept : boolean;
  loop : word;
  ok : boolean;
  ImpDrive : char;
  DosError     : integer;
  FTime        : TFtime;

const
     fsizesI : array[1..5] of LongInt =
              (361984,
               729600,
               1213440,
               1457152,
               0);

     RecSize   = 170;
     XrecSize  = RecSize+2;

     fsizesT : array[1..5] of PChar =
              ('360 K  (5.25" DD)',
               '720 K  (3.5" DD)',
               '1.2 M  (5.25" HD)',
               '1.44 M  (3.5" HD)',
               'Unlimited');

     OutLocT : Array[1..3] of PChar =
             ('A: Diskette drive',
              'B: Diskette drive',
              'Other');

     OutLocP : Array[1..3] of String = ('A:\','B:\','C:\temp');

     CompressT : Array[1..2] of PChar =
              ('No compression',
               'LZH compression');

     ImpPath  = '\\QACBL\ACBLIFS\';
     TempPath : String = 'C:\ACBLTEMP';
     ScoreDir = 'C:\ACBLSCOR\';

{$I acblname.dcl}

var
   Search_Rec  : TSearchRec;
   AnyFiles     : boolean;
   OutNames     : Array[1..200] of string[12];
   tempfilename : String;

const
     mask    = '*.*';

procedure Error_abort(const error_msg: string);
begin
  ErrBox(error_msg,MC,0);
  canceled := true;
  Application.Terminate;
end;

function get_import_field(import_record: string; offset : impfield) : string;
begin
  get_import_field := copy(import_record, offset[1], offset[2]);
end;

function ReadImport: boolean;
begin
  {$I-}ReadLn(imp_file, import_record);{$I+}
  CheckIOResult(infilename);
  ReadImport := false;
  if length(import_record) > 1 then begin
    inc(recs);
    if (Length(import_record) > 10) and (Pos('"',Copy(import_record,1,10)) > 0)
      then begin
      ReadImport := true;
      exit;
    end;
    if Length(import_record) <> RecSize then begin
      ErrBox(infilename+' is not a player information file',MC,0);
      CloseFile(imp_file);
      exit;
    end;
    player_no := get_import_field(import_record,a_number);
    if player_check(player_no,pnACBL) or valid_club(Copy(player_no,2,6),true) then
      ReadImport := true
    else begin
      ErrBox('Invalid player number: '+player_no+' at record '+long2strL(recs),MC,0);
      CloseFile(imp_file);
    end;
  end
end;

procedure ValidFolder(const Path: String);
begin
  if canceled then exit;
  if not IsDirectory(JustPathNameL(Path)) then
    Error_abort('Folder: '+Path+' not found.');
end;

label
     path_again,
     file_again,
     impdone;
begin
  Form1.Caption := 'Create Player information diskettes';
  CreateDir(ScoreDir);
  IniName := ScoreDir+'ACBLUTIL.INI';
  AppName := 'MakePIF';
  ValidFolder(ImpPath);
  if canceled then exit;
file_again:
  done := false;
  DosError := FindFirst(ImpPath+mask,faAnyFile,Search_Rec);
  FindClose(Search_Rec);
  Anyfiles := DosError = 0;
  infilename := '';
  if not GetFileName(ImpPath,'*.*',infilename,
    'Select file to process','','',false) then begin
    Application.Terminate;
    exit;
  end;
  assignFile(imp_file, infilename);
  {$I-} Reset(imp_file); {$I+}
  if IOResult <> 0 then Error_abort('File '+infilename+' not found');
  if canceled then exit;
  recs := 0;
  if not ReadImport then goto file_again;
  CloseFile(imp_file);
  if Pos('\',TempPath) > 0 then begin
    CreateDir(TempPath);
    TempPath := fixpath(TempPath);
  end;
  tempfilename := TempPath+JustFileNameL(infilename);
path_again:
  OutLoc := PickChoice(3,3,'output location ',@OutLocT,MC,false,0);
  if OutLoc < 1 then begin
    Application.Terminate;
    exit;
  end;
  if OutLoc = 3 then begin
    if not GetFolder(OutLocP[3],'Select output location','','SaveFileLoc',
      'C:\temp') then begin
      Application.Terminate;
      exit;
    end;
  end;
  OutPath := fixpath(OutLocP[OutLoc]);
  if CheckDrive(Escaped,OutPath,true) <> 0 then goto path_again;
  FreeSpace := CheckDiskSpace(OutPath);
  ans := 1;
  if FreeSpace > (fsizesI[4]+2000) then ans := 5
  else while (ans < 4) and (FreeSpace > (fsizesI[ans]+2000)) do Inc(ans);
  ans := PickChoice(5,ans,'maximum file size ',@fsizesT,MC,false,0);
  if ans < 1 then begin
    Application.Terminate;
    exit;
  end;
  {$I-} Reset(imp_file); {$I+}
  if IOResult <> 0 then Error_abort('File '+infilename+' not found');
  if canceled then exit;
  ImpTotRecs := TextFileSize(imp_file) div XrecSize;
  if ans < 5 then begin
    ftype := TextFileSize(imp_file) div fsizesI[ans] + 1;
    if ftype > 1 then MsgBox(Long2StrL(ftype)+' diskettes will be required.',MC,0);
  end;
  CloseFile(imp_file);
  if ans = 5 then begin
    CompType := PickChoice(2,2,'compression option ',@CompressT,MC,false,0);
    if CompType < 1 then goto path_again;
  end
  else CompType := 1;
  if (ftype < 2) and (ans < 5) then begin
    repeat
      FreeSpace := CheckDiskSpace(OutPath);
      done := true;
      if FreeSpace < fsizesI[ans] then begin
        done := false;
        if EscBox('Diskette is the wrong density or contains data',MC) then begin
          Application.Terminate;
          exit;
        end;
      end;
    until done;
    PCopyfile(infilename,OutPath+JustFileNameL(infilename),true,canceled,false);
    Application.Terminate;
    exit;
  end;
  PCopyFile(infilename,tempfilename,true,canceled,false);
  assignFile(imp_file, tempfilename);
  {$I-} Reset(imp_file); {$I+}
  if IOResult <> 0 then Error_abort('File '+tempfilename+' not found');
  if canceled then exit;
  recs := 0;
  ftype := 1;
  outfilename := TempPath+JustFileNameL(infilename);
  loop := Pos('.', outfilename);
  if loop > 0 then outfilename := Copy(outfilename, 1, loop-1);
  assignFile(out_file, outfilename+'.'+Long2StrL(ftype));
  ReWrite(out_file);
  OutNames[ftype] := JustFileNameL(outfilename+'.'+Long2StrL(ftype));
  ProgressBarInit(tempfilename+' --> '+outfilename+'.'+Long2StrL(ftype),
    'Verifying Player information file',false);
  recs := 0;
  while (not eof(imp_file)) do begin
    if not ReadImport then begin
      Application.Terminate;
      exit;
    end;
    if (ans < 5) and (TextFileSize(out_file) > fsizesI[ans]) then begin
      GetFileTime(TTextRec(imp_file).Handle,@FTime[1],@FTime[2],@FTime[3]);
      CloseFile(out_file);
      SetTheFileTime(outfilename+'.'+Long2StrL(ftype),FTime);
      Inc(ftype);
      assignFile(out_file, outfilename+'.'+Long2StrL(ftype));
      ReWrite(out_file);
      OutNames[ftype] := JustFileNameL(outfilename+'.'+Long2StrL(ftype));
    end;
    {$I-}WriteLn(out_file, import_record);{$I+}
    CheckIOResult(outfilename);
    Application.ProcessMessages;
    If (recs mod 100 = 0) and ProgressBar(recs, ImpTotRecs) then begin
      Application.Terminate;
      exit;
    end;
  end;
impdone:
  If (recs mod 100 = 0) and ProgressBar(recs, recs) then begin
    Application.Terminate;
    exit;
  end;
  ProgressBarDone;
  GetFileTime(TTextRec(imp_file).Handle,@FTime[1],@FTime[2],@FTime[3]);
  CloseFile(out_file);
  SetTheFileTime(outfilename+'.'+Long2StrL(ftype),FTime);
  closeFile(imp_file);
  {$I-} Erase(imp_file); {$I+}
  if IOResult = 0 then;
  done := true;
  for loop := 1 to ftype do begin
    repeat
      if (outloc < 3) and ((loop > 1) or not done) then begin
        if EscBox('Insert new diskette for file '+OutNames[loop],MC) then begin
          Application.Terminate;
          exit;
        end;
      end;
      done := CheckDrive(Escaped,OutPath,true) = 0;
      if done then begin
        FreeSpace := CheckDiskSpace(OutPath);
        if FreeSpace < fsizesI[ans] then begin
          done := false;
          if EscBox('Diskette is the wrong density or contains data',MC) then begin
            Application.Terminate;
            exit;
          end;
        end;
      end;
    until done;
    PCopyfile(TempPath+OutNames[loop],OutPath+OutNames[loop],true,canceled,false);
    assignFile(out_file,TempPath+OutNames[loop]);
    {$I-} Erase(out_file); {$I+}
    if IOResult = 0 then;
  end;
  if CompType > 1 then with CompLHA1 do begin
    outfilename := JustFileNameL(outfilename);
    len := Pos('.',outfilename);
    if len > 1 then outfilename := Copy(outfilename,1,len-1);
    FilesToProcess.Clear;
    FilesToProcess.Add(OutPath+OutNames[1]);
    ArchiveName := OutPath+outfilename+'.LZH';
    ProgressBarInit(OutPath+OutNames[1]+' --> '+OutPath+outfilename+'.LZH',
      'Compressing a file',false);
    Compress;
    ProgressBarDone;
  end;
  Application.Terminate;
end;

procedure TForm1.LHAProg(var PercentageDone: Integer);
begin
  canceled := ProgressBar(PercentageDone,100);
  if canceled then PerCentageDone := -1;
end;

end.
