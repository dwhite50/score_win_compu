{$I debugdir.inc}
{.$define test}
unit backutil;

interface


procedure get_drive;
function CheckTheFloppy(var Escaped: boolean; const BackupDir: String): boolean;
procedure backup_files(const CompressRatio: Real; const file_name: string;
          const BackCommand: char);

implementation

uses
    SysUtils,
    YesNoBoxU,
    vListBox,
    StStrL,
    FOpen,
    LHAUnit,
    backdata,
    pvio,
    csdef,
    version,
    history,
    util1,
    util2;

function CheckTheFloppy(var Escaped: boolean; const BackupDir: String): boolean;
const
     FloppyOptText      : Array[1..2] of PChar =
           ('1  Try another diskette',
            '2  Accept this diskette');
var
   drive_ok : boolean;
   FloppyOpt  : word;
   DosError   : integer;
label
     again;
begin
again:
  Escaped := false;
  drive_ok := true;
  DosError := FindFirst(BackupDir+'*.*',faAnyFile,Search_Rec);
  FindClose(Search_Rec);
  if DosError = 0 then begin
    RingClear(false);
    ErrBox('The diskette in '+BackupDir+' is not empty.',MC,0);
    FloppyOpt := PickChoice(2,1,'option (ESC to exit) ',@FloppyOptText,MC,false,topic);
    EraseBoxWindow;
    case FloppyOpt of
      0: begin
        drive_ok := false;
        Escaped := true;
      end;
      1: drive_ok := false;
      2: drive_ok := true;
    end;
  end;
  CheckTheFloppy := drive_ok;
end;

procedure get_drive;
begin
  Escaped := not GetFolder(BackupLoc,'Backup Location',false,
    'BackupLocation','A:\');
  if not Escaped then BackupLoc := fixpath(BackupLoc);
end;

procedure backup_files(const CompressRatio: Real; const file_name: string;
          const BackCommand: char);
var
   DosError : integer;
   dbfname  : string;
begin
  BackupFree := CheckDiskSpace(BackupLoc);
  BackupSize := 0;
  DosError := FindFirst(file_name,faAnyFile,Search_Rec);
  While DosError = 0 do with Search_Rec do begin
    Inc(BackupSize,Size);
    DosError := FindNext(Search_Rec);
  end;
  FindClose(Search_Rec);
  BackupSize := RRound(BackupSize*CompressRatio);
  BackupSize := (BackupSize+1000) div 1000 * 1000;
  if BackupSize > BackupFree then begin
    ErrBox('Insufficient space on the backup drive.'#13
      +'Approximately '+Long2StrL(BackupSize)+' bytes required.'#13+
      Long2StrL(BackupFree)+' bytes available.  Backup not performed.',MC,0);
    Escaped := true;
    exit;
  end;
  if RunLHA('A',arc_name+ArcSuffix,file_name,'','',true)
    and (BackCommand = 'm') then begin
    DosError := FindFirst(file_name,faAnyFile,Search_Rec);
    FindClose(Search_Rec);
    While DosError = 0 do with Search_Rec do begin
      DeleteFile(CFG.GFpath+Name);
      if MakeBMname(CFG.GFpath+Name,dbfname) then DeleteFile(dbfname);
      DosError := FindFirst(file_name,faAnyFile,Search_Rec);
    end;
    MsgBox('Backed up game files have been deleted'#13'from '+CFG.GFpath,MC,0);
  end
  else if BackCommand = 'm' then
    MsgBox('Backed up game files have NOT been deleted'#13+'from '+CFG.GFpath,MC,0);
  if not CFG.tournament then MsgBox('The backup file '+arc_name+ArcSuffix+#13
    +'is NOT part of any ACBL report and should'#13'NOT be sent to ACBL.',MC,0);
end;

end.
