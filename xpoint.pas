{$I defines.dcl}
unit point;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcEditF, OvcCmbx, OvcEF, OvcPB, OvcNF;

type
  TForm1 = class(TForm)
    EventNameEdit: TOvcEdit;
    PointController: TOvcController;
    EventNameLabel: TOvcAttachedLabel;
    SancEdit: TOvcEdit;
    SancEditLabel: TOvcAttachedLabel;
    RatingComboBox: TOvcComboBox;
    RatingLabel: TOvcAttachedLabel;
    GameTypeComboBox: TOvcComboBox;
    GameTypeLabel: TOvcAttachedLabel;
    EventTypeComboBox: TOvcComboBox;
    EventTypeLabel: TOvcAttachedLabel;
    RestrictComboBox: TOvcComboBox;
    RestrictLabel: TOvcAttachedLabel;
    OKButton: TButton;
    CanButton: TButton;
    ChampComboBox: TOvcComboBox;
    ChampLabel: TOvcAttachedLabel;
    SessComboBox: TOvcComboBox;
    SessLabel: TOvcAttachedLabel;
    BackBut: TButton;
    StratEdit: TOvcNumericField;
    StratEditLabel: TOvcAttachedLabel;
    procedure Initialize(Sender: TObject);
    procedure NameSancExit(Sender: TObject);
    procedure RatingExit(Sender: TObject);
    procedure GameTypeExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  StStrL,
  StBase,
  CSdef,
  pvio,
  version,
  mpdef,
  mtables,
  NatMps,
  txtview1,
  util2,
  ListBox,
  NumBoxU,
  util1;

{$R *.DFM}

const
     MaxFlights = 24;
     MaxSections = 9;
     FlightLetters : String = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
     {ERow = 1;
     ECol = 39;
     MatchCol = 42;
     MaxMPCol = 13;
     MfactCol = 50;
     GroupCol = 57;}
     defcopies         : word = 1;
     {$ifdef office}
     DefChoice : Word = 1;
     MaxPDest = 8;
     {$else}
     DefChoice : Word = 5;
     MaxPDest = 6;
     {$endif}
     PDest_text : array[1..MaxPDest] of Pchar =
                ('Display on Screen',
                 'Send to Printer',
                 'Another Masterpoint Chart',
                 'Quit',
                 'Print summary for DIC verification',
                 'Edit Masterpoint awards'
                 {$ifdef office},
                 'Print overall awards table',
                 'Print session awards table'
                 {$endif});

type
    TRatingChoice  = (rcNone,rcUnit,rcSect,rcReg,rcNat,rcNAP,rcCOP,rcCC,
                   rcRP,rcSectC{$ifdef office},rcPupil,rcIntro{$endif});
const
    rating_tab : array[1..rating_types] of byte =
    {rating indices}
               (4,                   {Unit championship}
                5,                   {Sectional}
                6,                   {Regional}
                13,                  {National}
                9,                   {NAP/GNT}
                10,                  {COPC,CNTC,CWTC}
                2,                   {Club Championship}
                1,                   {Club Masterpoint}
                {$ifdef office}
                5,                   {Sectional - use club M factors}
                47,                  {Pupil}
                48);                 {Introductory}
                {$else}
                5);                  {Sectional - use club M factors}
                {$endif}
    rating_title = 'event rating ';
    rating_text : array[1..rating_types] of String =
                ('Unit Championship',
                 'Sectional',
                 'Regional',
                 'National',
                 'GNT / NAP',
                 'CNTC / COPC / CWTC (Canada)',
                 'Club Championship',
                 'Club Masterpoint',
                 {$ifdef office}
                 'Sectional - Club M factors',
                 'Pupil Game',
                 'Introductory Game');
                 {$else}
                 'Sectional - Club M factors');
                 {$endif}

const
     {Constants for T factor index}
     T_individual = 1;
     T_pair       = 2;
     T_BAM        = 3;
     T_swiss      = 4;
     T_KO         = 0;

const
    type_tab : array[1..types] of byte =
               (T_individual,
                T_pair,
                T_Swiss,
                T_KO,
                T_BAM,
                T_swiss,
                T_pair);

type
    TGameTypeChoice = (gtcNone,gtcInd,gtcPair,gtcSwiss,gtcKO,gtcBAM,gtcZKO,
      gtcT2P);

type
    TEventType = (etNone,etOpen,etFlt,etStrat,etBRR,etFltBRR);

const
    event_type_title = 'event type ';
    event_types = 5;
    event_text : array[1..event_types] of PChar =
    {event types for award chart}
                 ('Open',
                  'Flighted/Stratiflighted',
                  'Stratified',
                  'Bracketed Round Robin',
                  'Flighted/Bracketed');

type
    TChampType = (ctNone,ctChamp,ctCons,ctSide,ctCPSide,ctCPCharity);

const
    event3_type_title = 'Championship rating ';
    event3_types = 5;
    event3_text : array[1..event3_types] of PChar =
    {championship types for award chart}
                 ('Championship',
                  'Consolation',
                  'Side game',
                  'Continuous pairs as Side game',
                  'Continuous pairs as Charity game');

type
    TKOEventType = (ketNone,ketOpen,ketFlt,ketBrk,ketCompact,ketFltBrk,
                 ketCons,ketBrkCons);

const
    ko_event_types = 7;
    ko_event_text : array[1..ko_event_types] of PChar =
    {types of KO events}
                  ('Open',
                   'Flighted',
                   'Bracketed',
                   'Compact',
                   'Flighted/Bracketed',
                   'Consolation',
                   'Bracketed Consolation');

const
    m_high_offset : array[1..m_facts] of byte =
                   (0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    m_facts,
                    m_facts,
                    m_facts,
                    m_facts,
                    m_facts,
                    m_facts,
                    m_facts,
                    m_facts);
const
    p_fact_split_text : array[1..p_facts-1] of PChar =
                ('0 Restrictions (80% of 100%)',
                 '1 Restriction (80% of 80%)',
                 '2 Restrictions (80% of 70%)');
const
    p_fact_title = 'number of restrictions ';
    p_fact_text : array[1..p_facts] of PChar =
                ('0 Restrictions (100%)',
                 '1 Restriction (80%)',
                 '2 Restrictions (70%)',
                 '3 Split Regional (Top 2 overalls only)');
const
    s_fact_title = 'number of sessions ';
    s_fact_text : array[1..s_facts] of Pchar =
                ('1 Session',
                 '2 Sessions',
                 '3 Sessions',
                 '4 Sessions',
                 '5 Sessions',
                 '6 Sessions');

type
    SecArray = array[1..MaxSections] of word;
    SecMPArray = array[1..MaxSections] of Real48;
var
  {Ch : Char;}
  RepLine : String;
  j       : word;
  more_to_do : boolean;
  {ERowx      : byte;}
  Escaped,
  restore_screen : boolean;
  KO_tie         : boolean;
  doback         : boolean;
  {section       : word;}
  count      : word;
  count2     : word;
  done       : boolean;
  dosplit    : boolean;
  {multicolor : boolean;}
  isaxevent     : boolean;
  isstrflt      : boolean;
  issecawards   : boolean;
  ExpertStrat  : boolean;
  {key_char    : word;}
  Rating      : TRating;
  GameType    : word;
  Rating_choice      : word;
  RateChoice         : TRatingChoice;
  GameType_choice    : TGameTypeChoice;
  LevelChoice        : word;
  Restriction : word;
  EventType   : TEventType;
  KOEventType : TKOEventType absolute EventType;
  ChampType   : TChampType;
  SessChoice  : word;
  FLetters    : String[MaxFlights];
  FLettersb   : Array[0..MaxFlights] of byte absolute FLetters;
  FLModified  : Array[1..MaxFlights] of boolean;
  Fletter     : String[1];
  BktText : String[4];
  atemp,
  Award       : Real48;
  TopName     : String[7];
  ColorChoice : array[1..MaxFlights] of word;
  MPChoice    : array[1..MaxFlights,1..2] of word;
  MPtables    : array[1..MaxFlights] of Real48;
  EVtables    : array[1..MaxFlights] of Real48;
  MatchSize   : array[1..MaxFlights] of word;
  KOEvents    : array[1..MaxFlights] of word;
  FlightSize  : array[1..MaxFlights] of word;
  TopStrat    : array[1..MaxFlights] of boolean;
  TieBreak    : array[1..MaxFlights] of boolean;
  Depth       : array[1..MaxFlights] of smallint;
  SecSizes    : array[1..Maxflights] of SecArray;
  SecGold     : array[1..Maxflights] of SecArray;
  SecHowell   : array[1..Maxflights] of SecArray;
  SecAwards   : array[1..MaxFlights] of SecMParray;
  OAawards    : array[1..MaxFlights] of Real48;
  MatchAwards : array[1..MaxFlights] of Real48;
  FixedOATab  : array[1..MaxFlights] of word;
  DispLine    : array[1..MaxFlights] of byte;
  MaxGoldDepth : array[1..MaxFlights] of byte;
  MostMPs     : array[1..MaxFlights] of longint;
  LeastMPs    : array[1..MaxFlights] of longint;
  EventName   : String;
  Sanction    : String;
  OtherTables : Real48;
  Allow2Tables : boolean;
  RateText    : String;
  Type1Text   : String;
  Type2Text   : String;
  Type3Text   : String;
  RestrictText: String;
  SessText    : String;
  MPColorText : array[1..MaxFlights] of String[17];
  KONames     : array[1..MaxFlights] of String;
  R,
  S,
  P,
  T,
  K,
  D           : Real48;
  M           : array[1..MaxFlights] of Real48;
  MS          : array[1..MaxFlights] of Real48;
  B           : array[1..MaxFlights] of Real48;
  L           : array[1..MaxFlights] of Real48;
  NumStrats     : word;
  Flights       : word;
  {Flight      : word;}
  year,
  month,
  day,
  dow         : word;
  DateStr     : String[8];
  comspec,
  loop        : word;
  ifunc       : byte;
  len         : byte;
  OKPushed    : boolean;

procedure TForm1.NameSancExit(Sender: TObject);
begin
  EventName := EventNameEdit.Text;
  Sanction := SancEdit.Text;
  if not Empty(EventName) and not Empty(Sanction) then begin
    NatFixed := false;
    with Defaults do Sanc := Sanction;
    WriteDefaults;
    ExpertStrat := false;
    isaxevent := false;
    isstrflt := false;
    issecawards := false;
    case Sanction[1] of
      'R': Rating_Choice := 3;
      'S': Rating_Choice := 2;
      'N': Rating_Choice := 3;
      else Rating_Choice := 1;
    end;
    RateChoice := TRatingChoice(Rating_Choice);
    RatingComboBox.ListIndex := Rating_Choice-1;
    RatingComboBox.Enabled := true;
    EventTypeComboBox.Enabled := true;
    RestrictComboBox.Enabled := true;
  end;
end;

procedure SetEventComboBox;
var
   j      : word;
  EvTypes     : word;
begin
  with Form1.EventTypeComboBox do if GameType in [T_KO] then begin
    EvTypes := ko_event_types;
    if (RateChoice in [rcCC,rcRP,rcSectC,rcNAP,rcCOP]) then EvTypes := 2;
    Items.Clear;
    for j := 1 to EvTypes do Items.Add(ko_event_text[j]);
    if EvTypes < 3 then ListIndex := 0
    else ListIndex := 2;
  end
  else begin
    EvTypes := event_types;
    if GameType <> T_swiss then Dec(EvTypes,2);
    if (RateChoice in [rcCC,rcRP,rcSectC,rcNAP,rcCOP
      {$ifdef office},rcPupil,rcIntro{$endif}]) then EvTypes := 3;
    Items.Clear;
    for j := 1 to EvTypes do Items.Add(event_text[j]);
    ListIndex := Byte(EventType)-1;
  end;
end;

procedure NewAwardChart;
begin
  with Form1 do begin
    FillChar(MPChoice,SizeOf(MPChoice),0);
    FillChar(MostMPs,SizeOf(MostMPs),0);
    FillChar(LeastMPs,SizeOf(LeastMPs),0);
    FillChar(MatchSize,SizeOf(MatchSize),0);
    FillChar(SecSizes,SizeOf(SecSizes),0);
    FillChar(KONames,SizeOf(KONames),0);
    Flights := 1;
    FLetters := FlightLetters;
    FillChar(FLModified,SizeOf(FLModified),false);
    Allow2Tables := false;
    EventNameEdit.Text := EventName;
    {EditAddText(TournEditRec,1,'',EventName,32,-5,3,nil,nil,0,18,0);}
    Sancedit.Text := Sanction;
    {EditAddText(TournEditRec,2,'',Sanction,12,-5,1,nil,PostEditTourn,0,18,0);}
    SetEventComboBox;
    RatingComboBox.Enabled := false;
    GameTypeComboBox.Enabled := false;
    EventTypeComboBox.Enabled := false;
    RestrictComboBox.Enabled := false;
    OKButton.Enabled := false;
    Escaped := false;
    OKPushed := false;
  end;
end;

procedure TForm1.RatingExit(Sender: TObject);
var
   j      : word;
begin
  Rating_Choice := RatingComboBox.ListIndex+1;
  RateChoice := TRatingChoice(Rating_Choice);
  Rating := TRating(rating_tab[Rating_Choice]);
  RateText := PickChoiceLine;
  LevelChoice := 0;
  FillChar(FixedOATab,SizeOf(FixedOATab),0);
  if Rating in [rNat] then SetNatRating(true); {process National events}
  if NatFixed then begin
    Gametype_Choice := TGameTypeChoice(NatTable.NGameType);
    GameTypeComboBox.Enabled := false;
  end
  else with GameTypeComboBox do begin
    Enabled := true;
    Items.Clear;
    if RateChoice in [rcNAP,rcCOP] then begin
      for j := 2 to 4 do Items.Add(type_text[j]);
      ListIndex := 0;
    end
    else begin
      for j := 1 to types do Items.Add(type_text[j]);
      ListIndex := Byte(GameType_Choice)-1;
    end;
  end;
end;

procedure TForm1.GameTypeExit(Sender: TObject);
begin
  with GameTypeComboBox do if RateChoice in [rcNAP,rcCOP] then begin
    GameType_Choice := TGameTypeChoice(ListIndex+2);
    Rating := rSect;
    LevelChoice := GetNAPLevel(RateChoice=rcNAP,GameType_Choice=gtcPair,3,1,0);
    if LevelChoice < 1 then begin
      Escaped := true;
      exit;
    end;
    case LevelChoice of
      3: if (RateChoice in [rcNAP]) then Rating := rReg;
      4: Rating := rNat;
    end;
    RateText := PadL(StrPas(RatingText[Byte(Rating)]),30);
    if Rating in [rNat] then SetNatRating(true); {process National events}
    if NatFixed then Gametype_Choice := TGameTypeChoice(NatTable.NGameType);
  end
  else GameType_Choice := TGameTypeChoice(ListIndex+1);
end;

procedure TForm1.Initialize(Sender: TObject);
var
   j      : word;
begin
  ReadDefaults;
  with Defaults do Sanction := Sanc;
  EventName := '';
  GetaDate(Year,Month,Day);
  DateStr := numcvt(Year,4,true)+numcvt(Month,2,true)+numcvt(Day,2,true);
  if DateStr > '20000308' then xMPrev := 2;
  {$ifndef office}
  if not CFG.tournament then DefChoice := 2;
  {$endif}
  Rating_choice := 1;
  RateChoice := TRatingChoice(Rating_Choice);
  with RatingComboBox do begin
    Items.Clear;
    for j := 1 to rating_types do Items.Add(rating_text[j]);
    ListIndex := Rating_Choice-1;
  end;
  GameType_Choice := gtcPair;
  GameType := type_tab[Byte(Gametype_Choice)];
  EventType := etOpen;
  ChampType := ctChamp;
  NewAwardChart;
end;

function MPLimitText(const Flight: word): String;
var
   str  : String;
begin
  if ExpertStrat and (Flight = 2) then str := Long2StrL(LeastMPs[2])+'+/-'
  else begin
    if LeastMPs[Flight] < 0 then str := 'LM'
    else if (LeastMPs[Flight] > 0) or ((MostMPs[Flight] <> 0)
      and (MostMPs[Flight] < 65000)) then str := Long2StrL(LeastMPs[Flight])
    else str := '';
    if (Length(str) > 0) and (MostMPs[Flight] <> 0)
      and (MostMPs[Flight] < 65000) then str := str+'-';
    if MostMPs[Flight] = 301 then str := str+'NLM'
    else if (MostMPs[Flight] > 0) and (MostMPs[Flight] < 65000) then
      str := str+Long2StrL(MostMPs[Flight])
    else if LeastMPs[Flight] = 0 then str := 'None'
    else str := str+'+';
  end;
  MPLimitText := str;
end;

procedure PrintMPHeader(const skip: byte; const Pitch: TPrintPitch;
          const printname: boolean; const Flight: word);
var
   Overp        : byte;
begin
  if skip > 0 then Overp := 2
  else Overp := 1;
  if printname then begin
    ReportLine(skip+1,Pitch,Overp,EventName);
    if (skip < 1) and (NumStrats > 1) then ReportLine(0,Pitch,1,',  ');
  end
  else NewLine(skip+1);
  if NumStrats > 1 then ReportLine(skip,Pitch,Overp,TopName+' '
    +FLetters[Flight]+' '+KONames[Flight]);
end;

procedure Showcolor(const ColorTab: TEventMP);
var
   j    : byte;
begin
  with ColorTab do for j := 1 to 2 do if MPPercent[j] > 0 then begin
    if j > 1 then ReportLine(0,pnone,1,', ');
    ReportLine(0,pnone,1,PerStr(MPPercent[j],false,1)
      +MP_Color_text[Byte(MPColors[j])]);
  end;
end;

function GetClubAward(const HighTab,MFact: Real48): Real48;
var
   Awards       : Real48;
begin
  case Rating of
    rRP,rCC: begin
      Awards := HighTab * MFact * 0.1;
      if MFact < 0.6 then begin
        if Awards > 1 then Awards := 1;
      end
      else if MFact < 1 then begin
        if Awards > 1.2 then Awards := 1.2;
      end
      else if Awards > 1.5 then Awards := 1.5;
    end;
    {$ifdef office}
    rPupil: begin
      Awards := HighTab * MFact * 0.02;
      if Awards > 0.5 then Awards := 0.5;
    end;
    rIntro: begin
      Awards := HighTab * MFact * 0.01;
      if Awards > 0.5 then Awards := 0.5;
    end;
    {$endif}
    else Awards := 0;
  end;
  GetClubAward := Awards;
end;

{$ifdef office}
procedure PrintMPTable(const dooa: boolean);

var
  LowTab,
  HighTab       : word;
   B    : Real48;
   Awards : Real48;
   Award    : Real48;
   tables   : word;
   Depth    : word;
   Position : word;
   MaxDepth   : word;
   Unittype   : String[7];
   pbody      : TPrintPitch;
   j          : word;
   MinTab     : word;
   DiffSecTop : boolean;

procedure ShowKOAward(const Low,High,tables: word);
var
   rankok       : boolean;
begin
  ReportLine(0,pnone,1,Real2Str(GetKOaward(Awards,Low,High,tables,0,
    RateChoice in [rcNAP],KOEventType in [ketCompact],rankok),7,2));
end;

begin
  if NumStrats > 1 then Flight := Numbox(Escaped,ifunc,1,
    TopName+' # to print',1,NumStrats,2,0,MC,0)
  else Flight := 1;
  if GameType = T_KO then MinTab := 5
  else if (GameType = T_Individual) or (not dooa and (NumStrats > 1)
    and (Flight = NumStrats)) then MinTab := 2
  else MinTab := 3;
  LowTab := Numbox(Escaped,ifunc,0,'Minimum number of tables',
    MinTab,999,3,0,MC,0);
  HighTab := Numbox(Escaped,ifunc,0,'Maximum number of tables',
    LowTab,999,3,0,MC,0);
  if dooa then begin
    if GameType = T_KO then begin
      Awards := KOaward(Bfactor(HighTab),K,L[Flight],M[Flight],P);
      MaxDepth := GetKODepth(HighTab,MatchSize[Flight]);
    end
    else begin
      if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
        Awards := GetClubAward(HighTab,M[Flight])
      else Awards := OAaward(Bfactor(HighTab),R,S,M[Flight],P,T);
      MaxDepth := GetOADepth(Awards,R,M[Flight],P,SessChoice,Hightab,
        HighTab,GameType_Choice in [gtcSwiss,gtcBAM,gtcKO,gtcT2P,gtcZKO],
        GameType = T_individual);
      if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
        MaxDepth := MinWord(MaxDepth,6);
    end;
    UnitType := 'Tables';
  end
  else begin
    MaxDepth := GetSecDepth(HighTab);
    case GameType of
      T_individual: UnitType := 'Players';
      T_pair:       UnitType := 'Pairs';
      else          Unittype := 'Tables';
    end;
  end;
  Set_Destination(60,3,2,nil,'',true,pcshort,topic);
  PleaseWait;
  if GameType = T_KO then pbody := ppica
  else begin
    if Destination < 3 then MaxDepth := MinWord(MaxDepth,18);
    case MaxDepth of
      0..10: pbody := ppica;
      11..12: pbody := pelite;
      else pbody := pcompress;
    end;
  end;
  PrintMPHeader(0,ppica,Flight=1);
  ReportLine(1,ppica,1,'MP Limits: '+MPLimitText(Flight));
  ReportLine(0,pnone,1,',  Rating: '+MPRateFact(M[Flight],P,T)+RateText);
  if dooa and (GameType <> T_KO) then ReportLine(0,pnone,1,',  '+SessText);
  DiffSecTop := false;
  if dooa then begin
    ReportLine(1,ppica,1,Type1Text+' Overall Awards');
    if ColorChoice[Flight] < color_types then begin
      ReportLine(0,pnone,1,' (');
      ShowColor(Event_MP_color_table[ColorChoice[Flight]]);
      ReportLine(0,pnone,1,')');
    end;
  end
  else begin
    ReportLine(1,ppica,1,Type1Text+' Session Awards');
    if ColorChoice[Flight] < color_types then begin
      DiffSecTop := CompStruct(SecTop_MP_color_table[ColorChoice[Flight]],
        Sess_MP_color_table[ColorChoice[Flight]],SizeOf(TEventMP)) <> Equal;
      ReportLine(0,pnone,1,' (');
      ShowColor(Sess_MP_color_table[ColorChoice[Flight]]);
      if DiffSecTop then begin
        ReportLine(0,pnone,1,';  Section Top: ');
        ShowColor(SecTop_MP_color_table[ColorChoice[Flight]]);
      end;
      ReportLine(0,pnone,1,')');
      if DiffSecTop then begin
        ReportLine(2,ppica,1,'* = ');
        ShowColor(SecTop_MP_color_table[ColorChoice[Flight]]);
        ReportLine(0,pnone,1,' Section Top');
      end;
    end;
  end;
  ReportLine(2,pbody,1,Pad(UnitType,7));
  if GameType = T_KO then begin
    for j := 1 to 2 do ReportLine(0,pnone,1,numcvt(j,5,false)+'  ');
    if MaxDepth > 2 then ReportLine(0,pnone,1,'   3/4 ');
    if MaxDepth > 4 then ReportLine(0,pnone,1,'   5/8 ');
    if MaxDepth > 8 then ReportLine(0,pnone,1,'   9/16');
    if MaxDepth > 16 then ReportLine(0,pnone,1,'  17/32');
  end
  else for j := 1 to MaxDepth do ReportLine(0,pnone,1,numcvt(j,5,false)+'  ');
  for tables := LowTab to HighTab do begin
    if dooa then begin
      if GameType = T_KO then begin
        Awards := KOaward(Bfactor(tables),K,L[Flight],M[Flight],P);
        Depth := GetKODepth(tables,MatchSize[Flight]);
      end
      else begin
        if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
          Awards := GetClubAward(tables,M[Flight])
        else Awards := OAaward(Bfactor(tables),R,S,M[Flight],P,T);
        Depth := GetOADepth(Awards,R,M[Flight],P,SessChoice,tables,
          tables,GameType_Choice in [gtcSwiss,gtcBAM,gtcKO,gtcT2P,gtcZKO],
          GameType = T_individual);
        if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
          Depth := MinWord(Depth,6);
      end;
    end
    else begin
      if Rating in [rRP,rCC{$ifdef office},rPupil,rIntro{$endif}] then
        Awards := GetClubAward(tables,MS[Flight])
      else Awards := SecAward(tables,R,MS[Flight],P,T,0,GameType = T_individual);
      Depth := GetSecDepth(tables);
    end;
    Depth := MinWord(Depth,MaxDepth);
    ReportLine(1,pbody,1,numcvt(tables,4,false)+'   ');
    if GameType = T_KO then begin
      ShowKOAward(1,1,tables);
      ShowKOAward(2,2,tables);
      if Depth > 2 then ShowKOAward(3,4,tables);
      if Depth > 4 then ShowKOAward(5,8,tables);
      if Depth > 8 then ShowKOAward(9,16,tables);
      if Depth > 16 then ShowKOAward(17,32,tables);
    end
    else for Position := 1 to Depth do begin
      if dooa then Award := GetOAaward(Awards,Position,SessChoice{,0})
      else Award := GetSecAward(Awards,Position);
      if DiffSecTop then begin
        if Position = 2 then ReportLine(0,pnone,1,Real2Str(Award,6,2))
        else begin
          ReportLine(0,pnone,1,Real2Str(Award,7,2));
          if Position = 1 then ReportLine(0,pnone,1,'*');
        end;
      end
      else ReportLine(0,pnone,1,Real2Str(Award,7,2));
    end;
  end;
  EraseBoxWindow;
  ViewCentered := true;
  if Destination = 1 then ViewerShow(1,2,80,25,Window_Attr,Frame_Attr,'');
  Close_Destination;
end;
{$endif}

function GetFlightSize(const count2: word; const title: string;
         const DefStrats,ThisFlight: byte; var Strats: byte): word;
var
   count  : word;
   MS     : word;
begin
  if ThisFlight = 1 then MS := 2
  else MS := 3;
  if (ThisFlight > 1) and (DefStrats < 2) then begin
    ErrBox(title+' has 1 Stratum.',0);
    Strats := 1;
  end
  else begin
    if MaxFlights-count2 > 3 then Strats := NumBox(Escaped,DefStrats,
      'Number of Strata','Number of Strata in '+title,1,MS,3,0)
    else Strats := MaxFlights-count2;
  end;
  for count := count2+1 to count2+Strats do begin
    FlightSize[count] := Strats + 1 + count2 - count;
    TopStrat[count] := count = count2+1;
  end;
  GetFlightSize := count2+Strats;
end;

{procedure ShowFlightLet(const j: byte);
begin
  if NumStrats > 1 then
    FastWriteWindow(BktText+FLetters[j],DispLine[j],1,window_attr);
end;

procedure MakeTableWindow;
var
   j : byte;
   CurLine : byte;
   doline  : boolean;
   WinTopLine : byte;
begin
  Flight := 0;
  CurLine := 1;
  done := false;
  FillChar(EVTables,SizeOf(EVTables),0);
  OtherTables := 0;
  WinTopLine := TopLine+9;
  if NumStrats > 13 then Dec(WinTopLine,NumStrats-13);
  if not makewindow(w2,1,WinTopLine,80,WinTopLine+NumStrats+Flights+1,
    true,true,true,window_attr,frame_attr,frame_attr,' Event Size ') then
    OutOfMemory;
  SetInactiveFrame(w2,#218#192#191#217#196#179,window_attr,window_attr);
  if not displaywindow(w2) then OutOfMemory;
  case GameType of
    T_individual,
    T_pair,
    T_BAM : if dosplit then FastWriteWindow(
        'Tables MP Limits  Color',1,6,window_attr)
      else FastWriteWindow(
        'Tables MP Limits  Color             -----------Section Sizes-----------',
        1,6,window_attr);
    T_Swiss: case EventType of
      etBRR,etFltBRR: FastWriteWindow(
        'Tables MP Limits  Color         Boards/Match  M factor',
        1,6,window_attr);
      else FastWriteWindow(
        'Tables MP Limits  Color         Boards/Match',1,6,window_attr);
    end;
    T_KO: case KOEventType of
      ketOpen,ketCons: FastWriteWindow(
        'Tables MP Limits  Color         Boards/Match',1,6,window_attr);
      ketFlt: FastWriteWindow(
        'Tables MP Limits  Color         Boards/Match    Num Groups',
        1,6,window_attr);
      ketBrk,ketFltBrk,ketBrkCons,ketCompact: FastWriteWindow(
        'Tables MP Limits  Color         Boards/Match  M factor',
        1,6,window_attr);
    end;
  end;
  for j := 1 to NumStrats do begin
    Inc(CurLine);
    doline := false;
    if (j > 1) and TopStrat[j] and (Flights > 1) then case GameType of
      T_KO: if (KOEventType = ketFltBrk) then begin
              if j = 2 then doline := true;
            end
            else doline := true;
      T_Swiss: if EventType = etFltBRR then begin
                 if j = 2 then doline := true;
               end
               else doline := true;
      else doline := true;
    end;
    if doline then begin
      FastWriteWindow(CharStr('-',76),CurLine,1,window_attr);
      Inc(CurLine);
    end;
    DispLine[j] := CurLine;
    ShowFlightLet(j);
  end;
end;}

procedure CalcL(const Flight: word);
begin
  L[Flight] := l_fact(MatchSize[Flight]);
  SecSizes[Flight][1] := 0;
end;

procedure CalcOA(Second: boolean; const Flight: word);
var
   Atab,Btab    : word;
begin
  B[Flight] := Bfactor(Round(MPTables[Flight]));
  if (RateChoice in [rcNAP]) and (LevelChoice = 3) then
    OAawards[Flight] := GetDistNAPAward(MostMPs[Flight],
    GameType=T_KO,GameType=T_pair,SessChoice)
  else begin
    if GameType = T_KO then OAawards[Flight] := KOaward(B[Flight],K,L[Flight],
      M[Flight], P)
    else if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
      OAawards[Flight] := GetClubAward(MPtables[Flight],M[Flight])
    else OAawards[Flight] := OAaward(B[Flight], R, S, M[Flight], P, T);
    if Second then begin
      if (GameType = T_KO) and (KOEventType in [ketFlt,ketFltBrk])
        and (NumStrats > 1) and (Flight = 1)
        and (OAawards[1] < OAawards[2] * 1.1) then
        OAawards[1] := Round2(OAawards[2] * 1.1);
      if (GameType <> T_KO) and (EventType in [etFlt]) and (Flight = 2)
        and (NumStrats > 2) and not TopStrat[2] and isaxevent then begin
        B[2] := Bfactor(Round(EVTables[2]));
        OAawards[2] := OAaward(B[2], R, S, M[2], P, T);
        Btab := Round(EVTables[2]);
        Atab := Round(EVTables[1]+EVTables[2])-Btab;
        OAawards[2] := StratXAward(Atab,Btab,OAawards[2],OAawards[3]);
      end;
    end;
  end;
  if NatFixed then with NatTable do if OAMP > 0 then OAawards[Flight] := OAMP;
end;

procedure CalcDepth(const Flight: word);
var
   tables        : Real48;
   count         : word;
begin
  if GameType_Choice in [gtcKO,gtcZKO] then begin
    Depth[Flight] := GetKODepth(Round(EVTables[Flight]),MatchSize[Flight]);
    if (GameType_Choice = gtcKO) and (MatchSize[Flight] >= 18)
      and (Round(EVTables[Flight]) in [8,16,32]) then begin
      Depth[Flight] := Depth[Flight] * 2;
      TieBreak[Flight] := true;
    end
    else TieBreak[Flight] := false;
  end
  else begin
    tables := 0;
    for count := Flight to Flight + FlightSize[Flight] - 1 do
      if count <= NumStrats then tables := tables+EVTables[count];
    if dosplit then begin
      if ColorChoice[Flight] <= ccGoldRed then Depth[Flight] := 2
      else Depth[Flight] := 0;
    end
    else begin
      Depth[Flight] := GetOADepth(OAawards[Flight],R,M[Flight],P,
        SessChoice,Round(tables),Round(MPTables[Flight]),
        GameType_Choice in [gtcSwiss,gtcBAM,gtcKO,gtcT2P,gtcZKO],
        GameType = T_individual);
      if Allow2Tables and (Depth[Flight] < 1) and (tables < 2.5) then
        Depth[Flight] := 1;
    end;
    if Rating in [rRP{$ifdef office},rPupil,rIntro{$endif}] then
      Depth[Flight] := MinWord(Depth[Flight],6);
  end;
  if NatFixed then with NatTable do if OADepth > 0 then begin
    Depth[Flight] := OADepth;
    TieBreak[Flight] := false;
  end;
end;

procedure CalcMatch(const Flight: word);
begin
  if MatchAwards[Flight] = 0 then Exit;
  with NatTable do if NatFixed and (NMatchAward > 0) then
    MatchAwards[Flight] := NMatchAward * MatchSize[Flight] / 64.0;
end;

{function PreEditAwards(const ERecordP : PEditRecord; const Direction: ShortInt;
         var ifunc: byte): Word; Far;
begin
  PreEditAwards := 0;
  with ERecordP^ do case EditSeq of
    4: FEditMax := MPTables[Flight];
  end;
end;

function PostEditAwards(const ERecordP : PEditRecord; const Direction : ShortInt;
         var Ifunc: byte) : Word; Far;
begin
  PostEditAwards := 0;
  with ERecordP^ do case EditSeq of
    2: begin
         CalcOA(true);
         EditProcess(EditRec[Flight], 3);
       end;
    3: begin
         CalcDepth;
         EditProcess(EditRec[Flight], 4);
       end;
    5: CalcMatch;
  end;
end;}

procedure CalcMPTables;

procedure CalcClubMatchAward(const Flight: byte);
begin
  Case Rating of
    rRP: MatchAwards[Flight] := l_fact(MatchSize[Flight])*0.24;
    rCC: MatchAwards[Flight] := l_fact(MatchSize[Flight])*0.36;
    {$ifdef office}
    rPupil: MatchAwards[Flight] := l_fact(MatchSize[Flight])*0.12;
    rIntro: MatchAwards[Flight] := l_fact(MatchSize[Flight])*0.06;
    {$endif}
  end;
  if M[Flight] < 1 then begin
    if M[Flight] > 0.5 then MatchAwards[Flight] := MatchAwards[Flight] * 0.8
    else MatchAwards[Flight] := MatchAwards[Flight] * 0.5;
  end;
  MatchAwards[Flight] := Round2(MatchAwards[Flight]+0.004);
end;

var
   flighta : word;
   flight_tables : Real48;
   section       : word;
   Flight        : word;
begin
  For Flight := 1 to NumStrats do begin
    Depth[Flight] := 0;
    MPTables[Flight] := EVtables[Flight];
    if Flight = 1 then flight_tables := OtherTables
    else flight_tables := 0;
    For flighta := Flight+1 to NumStrats do begin
      if GameType = T_KO then
        flight_tables := flight_tables + EVtables[flighta] * KOEvents[flighta]
      else if (M[1] >= 1.00)
        or (EventType in [etOpen,etStrat,etFlt,etBRR,etFltBRR]) then begin
        if (EventType in [etFlt]) and not TopStrat[Flight]
          and TopStrat[flighta] and isaxevent then Break;
        flight_tables := flight_tables + EVtables[flighta];
      end;
    end;
    if (Flight = 1) and (MostMPs[Flight] < 1) then MostMPs[Flight] := 65000;
    if (GameType = T_KO) or ((GameType = T_swiss) and ((EventType = etBRR)
      or ((EventType = etFltBRR) and (Flight > 1)))) then
      MPTables[Flight] := MPTables[Flight]+Round(flight_tables *
      KOLowerTableFact(MostMPs[Flight],Round(flight_tables),MostMPs[Flight],
      Flight,xMPRev > 0,GameType = T_swiss))
    else MPTables[Flight] := MPTables[Flight] + flight_tables;
    if (Flight = 1) and (OtherTables < MPTables[1]) and (GameType <> T_KO)
      and (OtherTables > (MPTables[1]-OtherTables)*2) then
      MPTables[1] := (MPTables[1]-OtherTables) * 3;
    if ChampType in [ctCons,ctSide,ctCPSide] then begin
      if M[Flight] > SideMFact then M[Flight] := SideMFact;
      if ChampType in [ctCons,ctSide] then if ((GameType <> T_Swiss)
        or (ChampType = ctSide)) and (MS[Flight] > SideMFact) then
        MS[Flight] := SideMFact;
    end;
    case GameType of
      T_KO : begin
        if KOEventType in [ketCons,ketBrkCons] then begin
          if M[Flight] > SideMFact then M[Flight] := SideMFact;
          if MS[Flight] > SideMFact then MS[Flight] := SideMFact;
        end;
        if Rating in [rCC,rRP{$ifdef office},rPupil,rIntro{$endif}] then
          CalcClubMatchAward(Flight)
        else MatchAwards[Flight] := MatchAward(D, L[Flight], MS[Flight], P);
      end;
      T_Swiss : begin
        if TopStrat[Flight] then begin
          if Rating in [rCC,rRP{$ifdef office},rPupil,rIntro{$endif}] then
            CalcClubMatchAward(Flight)
          else MatchAwards[Flight] := MatchAward(D, L[Flight], MS[Flight],P);
        end
        else MatchAwards[Flight] := MatchAwards[Flight-1];
      end;
      else begin
        MatchAwards[Flight] := 0;
        section := 1;
        while (section <= MaxSections) and (SecSizes[Flight][section] > 0)
          do begin
          if Rating in [rRP,rCC{$ifdef office},rPupil,rIntro{$endif}] then
            SecAwards[Flight][section] := GetClubAward(SecSizes[Flight][section],MS[Flight])
          else SecAwards[Flight][section] :=
            SecAward(SecSizes[Flight][section],R,MS[Flight],P,T,
            SecHowell[Flight][section],GameType = T_individual);
          Inc(section);
        end;
      end;
    end;
    CalcOA(false,Flight);
    CalcDepth(Flight);
    CalcMatch(Flight);
  end;
  for Flight := 1 to NumStrats do begin
    CalcOA(true,Flight);
    CalcDepth(Flight);
    CalcMatch(Flight);
  end;
end;

{procedure EditAwards;
var
   footer  : String;
   flinit  : array[1..MaxFlights] of boolean;
   FuncSet : EditFuncSet;
   title   : String;
begin
  Flight := 1;
  if NumStrats = 1 then footer := '';
  FillChar(flinit,SizeOf(flinit),0);
  FillChar(EditRec,SizeOf(EditRec),0);
  repeat
    if not flinit[Flight] then begin
      if NumStrats = 1 then title := 'Edit Awards'
      else title := 'Edit '+TopName+' '+Fletters[Flight]+' Awards';
      if NumStrats = 2 then footer := 'PgUp or PgDn or F7 or F8='+BktText
        +Fletters[3-Flight]
      else if NumStrats > 1 then begin
        if Flight = 1 then footer := 'PgUp or F7='+BktText+Fletters[NumStrats]
        else footer := 'PgUp or F7='+BktText+Fletters[Flight-1];
        if Flight = NumStrats then footer := footer+'; PgDn or F8='+BktText
          +Fletters[1]
        else footer := footer+'; PgDn or F8='+BktText+Fletters[Flight+1];
      end;
      if length(footer) > 0 then footer := ' '+footer+' ';
      if Flight > 9 then Dec(Erowx)
      else Inc(Erowx);
      EditInit(EditRec[Flight],ERowx,ECol-Flight,' '+title+' ',footer);
      EditAddLine(EditRec[Flight],8,'Press ESC when Done',true,revon,1,0);
      EditAddFloat(EditRec[Flight],2,MPTables[Flight],2,999,5,1,
        'Tables to base Overall Awards',PreEditAwards,PostEditAwards,2,2,0);
      EditAddFloat(EditRec[Flight],3,OAawards[Flight],0.0,500.0,8,2,
        'First Overall Masterpoints',PreEditAwards,PostEditAwards,2,2,0);
      EditAddInt(EditRec[Flight],4,Depth[Flight],1,Round(MPTables[Flight]),3,
        'Number of Places',PreEditAwards,PostEditAwards,2,2,0);
      if (MatchAwards[Flight] > 0) then
        EditAddFloat(EditRec[Flight],5,MatchAwards[Flight],0.0,50.0,7,2,
        'Match Award Masterpoints',PreEditAwards,PostEditAwards,2,2,0);
      EditAddLine(EditRec[Flight],6,'Masterpoint Limits '+MPLimitText(Flight),
        false,0,2,2);
      if NumStrats > 1 then EditAddButton(EditRec[Flight],7,EditOKButton,
        'Next '+TopName,1,12,0);
      EditAddLine(EditRec[Flight],8,'',false,0,0,2);
      flinit[Flight] := true;
    end;
    if NumStrats > 1 then FuncSet := [7,8,73,81]
    else FuncSet := [];
    EditShow(EditRec[Flight],Escaped,ifunc,FuncSet,13);
    case ifunc of
      7,73: Dec(Flight);
      8,81: Inc(Flight);
      13: begin
        if NumStrats = 1 then Escaped := true
        else Inc(Flight);
      end;
    end;
    if Flight > NumStrats then Flight := 1;
    if Flight < 1 then Flight := NumStrats;
  until Escaped;
  For Flight := NumStrats downto 1 do EditDone(EditRec[Flight]);
end;}

{procedure GetSectionSizes(const Flight: word);
var
   section : byte;
   done    : boolean;
   ginput  : boolean;
begin
  section := 0;
  done := false;
  MaxGoldDepth[Flight] := 0;
  repeat
    case GameType of
      T_individual,
      T_pair,
      T_BAM : begin
        Inc(section);
        repeat
          ifunc := 3;
          SecSizes[Flight][section] := gtfnum(Escaped,ifunc,
            SecSizes[Flight][section],'',DispLine[Flight],section * 4 + 38,
            true,0,999,3,0);
          if (ifunc = 72) and (Flight > 1) then begin
            Dec(Flight,2);
            doback := true;
            exit;
          end;
          if ifunc > 0 then RingClear(false);
        until ifunc < 1;
        done := (SecSizes[Flight][section] = 0);
        if not done then begin
          SecGold[Flight][section] := 0;
          SecHowell[Flight][section] := 0;
          with SecTop_MP_color_table[ColorChoice[Flight]] do
            if (MPColors[1] = cGold) and
            (CompStruct(SecTop_MP_color_table[ColorChoice[Flight]],
            Sess_MP_color_table[ColorChoice[Flight]],SizeOf(TEventMP))
            <> Equal) then begin
            if (EventType in [etStrat,etFlt]) and not TopStrat[Flight] then
              SecGold[Flight][section] := MaxGoldDepth[Flight-1]
            else If SecSizes[Flight][section] < 18 then
              SecGold[Flight][section] := 1
            else begin
              SecGold[Flight][section] := (SecSizes[Flight][section]+13)
                div 16;
              if SecGold[Flight][section] < 1 then
                SecGold[Flight][section] := 1;
            end;
            if (SecGold[Flight][section] > 1) or (SecSizes[Flight][section] > 17)
              then begin
              ifunc := 2;
              SecGold[Flight][section] := NumBox(Escaped, ifunc,
                SecGold[Flight][section],
                'Enter the number of Positions to receive'#13+
                'Gold points for Section size '
                +Long2Str(SecSizes[Flight][section]), 1, 10, 2, 0,MC,0);
            end;
            MaxGoldDepth[Flight] := MaxWord(MaxGoldDepth[Flight],SecGold[Flight][section]);
          end;
          if (GameType = T_individual)
            and (SecSizes[Flight][section] > 7) then
            if YesNoBox(Escaped, ifunc, false, 'Is Section Size '
            +Long2Str(SecSizes[Flight][section])+' a Howell'#13+
            '(One Winner) Movement', 0,MC,0) then
            SecHowell[Flight][section] := 1;
        end;
        if section > 1 then issecawards := true;
        done := done or (section >= MaxSections);
      end;
      T_Swiss: begin
        If (Flight > 1) then begin
          if not TopStrat[Flight] or (MatchSize[Flight] < 1) then
            MatchSize[Flight] := MatchSize[Flight-1];
        end
        else if MatchSize[Flight] < 1 then MatchSize[Flight] := 7;
        repeat
          ifunc := 3;
          If TopStrat[Flight] then MatchSize[Flight] := gtfnum(Escaped,
            ifunc,MatchSize[Flight],'',DispLine[Flight],MatchCol,true,4,99,2,0)
          else MatchSize[Flight] := gtfnum(Escaped,ifunc,
            MatchSize[Flight],'',DispLine[Flight],MatchCol,false,4,99,2,0);
          if (ifunc = 72) and (Flight > 1) then begin
            Dec(Flight,2);
            doback := true;
            exit;
          end;
          if ifunc > 0 then RingClear(false);
        until ifunc < 1;
        ifunc := 2;
        if EventType in [etBRR,etFltBRR] then begin
          if EventType = etBRR then ginput := Flight > 1
          else ginput := Flight > 2;
          if ginput then M[Flight] := BracketFact(Flight,NumStrats,
            MostMPs[Flight]);
          repeat
            ifunc := 3;
            M[Flight] := gtffnm(Escaped, ifunc, M[Flight], '',
              DispLine[Flight], MfactCol, false, 0.1, 1.5, 7, 4, 0);
            MS[Flight] := M[Flight];
            if (ifunc = 72) and (Flight > 1) then begin
              Dec(Flight,2);
              doback := true;
              exit;
            end;
            if ifunc > 0 then RingClear(false);
          until ifunc < 1;
        end;
        CalcL;
        done := true;
      end;
      T_KO: begin
        if MatchSize[Flight] < 12 then begin
          If Flight > 1 then MatchSize[Flight] := MatchSize[Flight-1]
          else if NatFixed and (NatTable.NMatchAward > 0) then
            MatchSize[Flight] := 64
          else if KOEventType in [ketCompact] then MatchSize[Flight] := 12
          else MatchSize[Flight] := 24;
        end;
        repeat
          ifunc := 3;
          MatchSize[Flight] := gtfnum(Escaped,ifunc,MatchSize[Flight],'',
            DispLine[Flight],MatchCol,true,4,99,2,0);
          if (ifunc = 72) and (Flight > 1) then begin
            Dec(Flight,2);
            doback := true;
            exit;
          end;
          if ifunc > 0 then RingClear(false);
        until ifunc < 1;
        ifunc := 2;
        case KOEventType of
          ketOpen,ketCons: KOEvents[Flight] := 1;
          ketFlt: begin
            KOEvents[Flight] := 1;
            ginput := YesnoBox(Escaped,ifunc,false,
              'More than 1 Group in Flight '+FLetters[Flight],0,MC,0);
            if ginput then Inc(KOEvents[Flight]);
            KOEvents[Flight] := gtfnum(Escaped, ifunc,
              KOEvents[Flight],'',DispLine[Flight],GroupCol,ginput,1,5,2,0);
          end;
          ketBrk,ketBrkCons,ketCompact: begin
            KOEvents[Flight] := 1;
            ginput := Flight > 1;
            if ginput then begin
              M[Flight] := BracketFact(Flight,NumStrats,MostMPs[Flight]);
            end;
            repeat
              ifunc := 3;
              M[Flight] := gtffnm(Escaped,ifunc,M[Flight],'',DispLine[Flight],
                MfactCol,false,0.1,1.5,7,4,0);
              MS[Flight] := M[Flight];
              if (ifunc = 72) and (Flight > 1) then begin
                Dec(Flight,2);
                doback := true;
                exit;
              end;
              if ifunc > 0 then RingClear(false);
            until ifunc < 1;
          end;
          ketFltBrk: begin
            KOEvents[Flight] := 1;
            ginput := Flight > 2;
            if ginput then M[Flight] := BracketFact(Flight-1,NumStrats-1,
              MostMPs[Flight]);
            repeat
              ifunc := 3;
              M[Flight] := gtffnm(Escaped,ifunc,M[Flight],'',DispLine[Flight],
                MfactCol,false,0.1,1.5,7,4,0);
              MS[Flight] := M[Flight];
              if (ifunc = 72) and (Flight > 1) then begin
                Dec(Flight,2);
                doback := true;
                exit;
              end;
              if ifunc > 0 then RingClear(false);
            until ifunc < 1;
          end;
        end;
        CalcL;
        done := true;
      end;
    end;
  until done;
end;}

procedure GetMPColors(const Flight: word);

begin
  if RateChoice in [rcNAP] then case LevelChoice of
    1: if MPChoice[Flight,2] < 8 then ColorChoice[Flight] := ccBlack
       else ColorChoice[Flight] := ccRed50;
    2: if SessChoice > 1 then begin
         if MPChoice[Flight,2] >= m_facts then ColorChoice[Flight] := ccGold20
         else if MPChoice[Flight,2] > 12 then ColorChoice[Flight] := ccGold10
         else ColorChoice[Flight] := ccGold5;
       end
       else ColorChoice[Flight] := ccRed;
    3: if MPChoice[Flight,2] < 9 then ColorChoice[Flight] := ccGold25
       else ColorChoice[Flight] := ccGoldRed;
    4: ColorChoice[Flight] := ccGold;
  end
  else if RateChoice in [rcCOP] then case LevelChoice of
    1,2,3: ColorChoice[Flight] := ccRed;
    4: if GameType_Choice in [gtcPair] then ColorChoice[Flight] := ccGoldRed
       else ColorChoice[Flight] := ccGold;
  end
  else Case Rating of
    rCC,rRP,rUnit{$ifdef office},rPupil,rIntro{$endif}:
      ColorChoice[Flight] := ccBlack;
    rSect: case RateChoice of
      rcSectC: ColorChoice[Flight] := ccBlack;
      else ColorChoice[Flight] := ccSilver;
    end;
    rReg: Case GameType of
      T_KO: Case KOEventType of
        ketOpen,ketFlt: if (SessChoice > 1) and (MPChoice[Flight,2] > 9)
          and (Flight < 3) then ColorChoice[Flight] := ccGoldRed
          else ColorChoice[Flight] := ccRed;
        ketBrk,ketCompact: ColorChoice[Flight] := GetBracketColor(Flight,
          MostMPs[Flight],MostMPs[1]);
        ketFltBrk: ColorChoice[Flight] := GetBracketColor(Flight,
          MostMPs[Flight],MostMPs[2]);
        ketCons,ketBrkCons: ColorChoice[Flight] := ccRed;
      end;
      else begin
        if SessChoice < 2 then begin
          if (ChampType in [ctCPSide,ctCPCharity]) and (Flight = 1) then
            ColorChoice[Flight] := ccRedGold
          else ColorChoice[Flight] := ccRed
        end
        else if (GameType = T_swiss) and ((EventType = etBRR)
          or ((EventType = etFltBRR) and (Flight > 1))) then
          ColorChoice[Flight] := GetBracketColor(Flight,MostMPs[Flight],
          MostMPs[1])
        else if (MPChoice[Flight,2] > 9) and ((Flight < 3) or TopStrat[Flight])
          and (ChampType in [ctChamp]) then begin
          if (Flight > 1) and (M[1] < 1.00) then ColorChoice[Flight] := ccRed
          else ColorChoice[Flight] := ccGoldRed;
        end
        else ColorChoice[Flight] := ccRed;
      end;
    end;
    rNat: ColorChoice[Flight] := ccGold;
  end;
  {FastWriteWindow(Pad(Copy(MPLimitText(Flight),1,10),10),DispLine[Flight],
    MaxMPCol,GetTColor(nattrt));}
  if NatFixed then ColorChoice[Flight] := NatTable.NColor
  else if RateChoice in [rcSectC] then
    ColorChoice[Flight] := PickMPColors(Byte(rUnit),ColorChoice[Flight],
    color_title,true)
  else if not (RateChoice in [rcNAP,rcCOP]) then
    ColorChoice[Flight] := PickMPColors(Byte(Rating),ColorChoice[Flight],
    color_title,true);
  MPColorText[Flight] := Copy(GetLbrPickChoiceLine(colorName,ColorChoice[Flight]),4,40);
  {FastWriteWindow(MPColorText[Flight],DispLine[Flight],24,GetTColor(nattrt));}
end;

procedure GetMFactor(const Flight: word);
{var
   oldaskesc : boolean;}
begin
  {oldaskesc := getabn;
  setabn(off);
  ERec := nil;
  EditInit(ERec,2,45,StrPas(m_low_text[m_facts+1]),'');
  EditAddLine(ERec,3,'F9 when done, F1=table',true,revon,1,0);}
  M[Flight] := 1;
  {EditAddFloat(ERec,1,M[Flight],0.1,3.0,7,4,'Event M Factor',nil,nil,2,2,240);}
  MS[Flight] := 1;
  {EditAddFloat(ERec,2,MS[Flight],0.1,3.0,7,4,'Session M Factor',nil,nil,2,2,240);
  SetBackColor(CFG.InformColor);
  EditShow(ERec,Escaped,ifunc,[9],13);
  EditDone(ERec);
  setabn(oldaskesc);}
  MostMPs[Flight] := 0;
  LeastMPs[Flight] := 0;
end;

procedure GetOpenLimits(const AskLower: boolean; const Flight: word);
var
   high_choices  : byte;
   columns       : byte;
begin
  MPChoice[Flight,1] := MaxWord(MPChoice[Flight,1],1);
  if NatFixed then MPChoice[Flight,1] := NatTable.NLowerMP
  else if not AskLower or (RateChoice in [rcCC,rcRP,rcSectC,rcNAP,rcCOP
    {$ifdef office},rcPupil,rcIntro{$endif}]) then
    MPChoice[Flight,1] := 1
  else MPChoice[Flight,1] := Pickchoice(m_facts+1,MPChoice[Flight,1],
    m_low_title,@m_low_text,true,0);
  {if MPChoice[Flight,1] > m_facts then GetMFactor
  else begin}
    High_choices := m_facts - m_high_offset[MPChoice[Flight,1]];
    if high_choices > 10 then columns := 2
    else columns := 1;
    if MPChoice[Flight,2] < 1 then MPChoice[Flight,2] := m_facts
    else MPChoice[Flight,2] := MaxWord(MPChoice[Flight,2],MPChoice[Flight,1]);
    if m_high_offset[MPChoice[Flight,1]] = m_facts then
      MPChoice[Flight,2] := m_facts
    else if NatFixed then MPChoice[Flight,2] := NatTable.NUpperMP
    else MPChoice[Flight,2] := Pickchoice(high_choices,
      MPChoice[Flight,2]-m_high_offset[MPChoice[Flight,1]],
      m_high_title,@m_high_text[m_high_offset[MPChoice[Flight,1]]+1],true,0)
      +m_high_offset[MPChoice[Flight,1]];
    if (RateChoice in [rcCC,rcRP,rcSectC
      {$ifdef office},rcPupil,rcIntro{$endif}]) then begin
      M[Flight] := c_fact(MPChoice[Flight,2]);
      MS[Flight] := M[Flight];
    end
    else begin
      M[Flight] := m_fact(MPChoice[Flight,1],MPChoice[Flight,2]);
      MS[Flight] := M[Flight];
    end;
    LeastMPs[Flight] := m_low_MPs[MPChoice[Flight,1]];
    MostMPs[Flight] :=  m_high_MPs[MPChoice[Flight,2]];
  {end;}
end;

function ValidTables(const tables: Real48; const dplaces: byte): boolean;
begin
  ValidTables := true;
  if dplaces < 1 then begin
    if Trunc(Tables) = Tables then exit;
    ValidTables := false;
    {RingClear(false);}
    ErrBox('Number of tables must be a whole number',0);
  end
  else begin
    if (Round(Frac(Tables) * 10) mod 5) = 0 then exit;
    ValidTables := false;
    {RingClear(false);}
    ErrBox('Number of tables must end in .0 or .5',0);
  end;
end;

procedure GetOtherTables(const Flight: word);
begin
  if (Flight > 1) or (M[Flight] < 1) or (Restriction > 1)
    or (KOeventtype in [ketCons,ketBrkCons])
    or not (ChampType in [ctChamp]) or (LevelChoice > 0)
    or not (Rating in [rSect,rReg,rStac,rUnit,rRP,rCC]) then exit;
  MsgBox('Enter the exact number of tables (to the nearest 1/2 table).'#13
    +'Do not round to full tables.',0);
  repeat
    OtherTables := FloatBox(Escaped,OtherTables,'',
      'Number of tables in other concurrent restricted events to use'#13
      +'toward masterpoint calculations for the top '+TopName+'.  Do not'#13+
      'include tables for this event that will be entered below.',
      0,500,5,1,0);
  until ValidTables(OtherTables,1);
  {EraseBoxWindow;}
end;

{procedure GetBRLimits(const Flight: word);
begin
  repeat
    ifunc := 3;
    MostMPs[Flight] := Round(gtffnm(Escaped,ifunc,MostMPs[Flight],'',
      DispLine[Flight],MaxMPCol,true,1,65000,5,0,0));
    if (ifunc = 72) and (Flight > 1) then begin
      Dec(Flight,2);
      doback := true;
      exit;
    end;
    if ifunc > 0 then RingClear(false);
  until ifunc < 1;
  if Flight > 1 then begin
    LeastMPs[Flight-1] := MostMPs[Flight];
    FastWriteWindow(Pad(Copy(MPLimitText(Flight-1),1,10),10),
      DispLine[Flight-1],MaxMPCol,GetTColor(nattrt));
  end;
end;}

procedure GetFlightLimits(const Flight: word);
var
  high_default : byte;
   k    : word;
   mpok : boolean;
begin
  If Flight = 1 then high_default := m_facts
  else if (EventType = etFlt) and (Flight = 2) and not TopStrat[2]
    and (MPChoice[1,2] = m_facts) then high_default := m_facts-2
  else begin
    mpok := false;
    if (EventType in [etFlt,etStrat]) and not isaxevent
      and (Flight in [2,3]) and ((MostMPs[1] > 60000) or (MostMPs[1] < 1))
      and (Defaults.DStrats[Flight] > 0) and ((Flight = 2)
      or (Defaults.DStrats[Flight] < MostMPs[Flight-1])) then begin
      for k := 1 to m_facts do if Defaults.DStrats[Flight] = m_high_mps[k]
        then begin
        high_default := k;
        mpok := true;
        Break;
      end;
    end;
    if not mpok then
      high_default := m_next_default[Rating_Choice,MPChoice[Flight-1,2]];
  end;
  MPChoice[Flight,1] := 1;
  if NatFixed then MPChoice[Flight,2] := NatTable.NUpperMP
  else begin
    if (Flight = 2) and (M[1] = 1) then k := m_facts+2
    else k := m_facts+1;
    if (MPChoice[Flight,2] > 0) and (MPChoice[Flight,2] <= k) then
      high_default := MPChoice[Flight,2];
    MPChoice[Flight,2] := Pickchoice(k,high_default,
      m_high_title+' - '+TopName+' '+FLetters[Flight]+' ',@m_high_text,
      true,0);
  end;
  if MPChoice[Flight,2] = m_facts+1 then GetMFactor(Flight)
  else begin
    if MPChoice[Flight,2] = m_facts+2 then begin
      MostMPs[Flight] := 0;
      M[Flight] := 0.8;
      ExpertStrat := true;
    end
    else begin
      MostMPs[Flight] := m_high_MPs[MPChoice[Flight,2]];
      if (EventType in [etFlt,etStrat]) and not isaxevent
        and (Flight in [2,3]) and ((MostMPs[1] > 60000) or (MostMPs[1] < 1))
        then Defaults.DStrats[Flight] := MostMPs[Flight];
      if (Flight > 1) and not TopStrat[Flight] then begin
        LeastMPs[Flight-1] := MostMPs[Flight];
        {FastWriteWindow(Pad(Copy(MPLimitText(Flight-1),1,10),10),
          DispLine[Flight-1],MaxMPCol,GetTColor(nattrt));}
        if ExpertStrat and (Flight = 3) then begin
          LeastMPs[1] := MostMPs[Flight];
          {FastWriteWindow(Pad(Copy(MPLimitText(1),1,10),10),
            DispLine[1],MaxMPCol,GetTColor(nattrt));}
        end;
      end;
      if (RateChoice in [rcCC,rcRP,rcSectC
        {$ifdef office},rcPupil,rcIntro{$endif}]) then
        M[Flight] := c_fact(MPChoice[Flight,2])
      else M[Flight] := m_fact(MPChoice[Flight,1],MPChoice[Flight,2]);
    end;
    MS[Flight] := M[Flight];
  end;
end;

procedure GetTables(var Flight: word);
var
   Prompt : String;
   Fltot  : Real48;
   Mintab : Real48;
   Mintabi: Real48;
   Evtab  : word;
   j      : word;
   dplaces :byte;
begin
  if GameType_Choice in [gtcSwiss,gtcBAM,gtcKO,gtcT2P,gtcZKO] then
    dplaces := 0
  else dplaces := 1;
  Inc(Flight);
  FLetter := Copy(Fletters,Flight,1);
  If (NumStrats > 1) then repeat
    {gtfstr(Escaped,ifunc,'',Fletter,1,len,DispLine[Flight],4,-15,1,0);}
    Fletters[Flight] := Fletter[1];
    FLModified[Flight] := true;
    if (Flight < NumStrats) and not FLModified[Flight+1] then begin
      if (GameType <> T_KO) and (Fletter[1] in ['X','2']) then begin
        if Flight > 1 then Flettersb[Flight+1] := Flettersb[Flight-1]+1;
      end
      else Flettersb[Flight+1] := Flettersb[Flight]+1;
      {ShowFlightLet(Flight+1);}
      for j := Flight+2 to NumStrats do begin
        Flettersb[j] := Flettersb[j-1]+1;
        {ShowFlightLet(j);}
      end;
    end;
    if (ifunc = 21) and (Flight > 1) then begin
      Dec(Flight,2);
      doback := true;
      exit;
    end;
    {if ifunc > 0 then RingClear(false);}
  until ifunc < 1;
  j := Flight;
  Fltot := 0;
  while (j > 1) and not TopStrat[j] do begin
    Dec(j);
    Fltot := Fltot+EvTables[j];
  end;
  j := Flight - j;
  if GameType = T_Individual then Mintabi := 2
  else Mintabi := 2.5;
  if TopStrat[Flight] then begin
    if (FlightSize[Flight] < 2) then Mintab := Mintabi
    else Mintab := 0.5;
  end
  else if FlightSize[Flight] > 1 then Mintab := 0.5
  else begin
    Mintab := Mintabi-Fltot;
    if Mintab < 1.5 then Mintab := 1.5;
  end;
  if dplaces > 0 then MsgBox(
    'Enter the exact number of tables (to the nearest 1/2 table).'#13
    +'Do not round to full tables.',0);
  {SetTopTiledWindow(w2);}
  repeat
    ifunc := 3;
    {if dplaces > 0 then
      EVTables[Flight] := gtffnm(Escaped,ifunc,EVTables[Flight],'',
      DispLine[Flight],6,true,Mintab,999,4+dplaces,dplaces,0)
    else EVTables[Flight] := gtfnum(Escaped,ifunc,Round(EVTables[Flight]),'',
      DispLine[Flight],6,true,Round(Mintab),999,3,0);}
    if (ifunc = 72) and (Flight > 1) then begin
      Dec(Flight,2);
      doback := true;
      {EraseBoxWindow;}
      exit;
    end;
    {if ifunc > 0 then RingClear(false);}
  until (ifunc < 1) and ValidTables(EvTables[Flight],dplaces);
  {EraseBoxWindow;}
  if (NumStrats > 0) then
    if ((Gametype = T_KO)
    and (KOEventType in [ketFlt,ketBrk,ketFltBrk,ketBrkCons,ketCompact]))
    or ((Gametype = T_Swiss) and (EventType in [etBRR,etFltBRR])) then begin
    Prompt := 'Event Name for '+TopName+' ';
    repeat
      StrBox(Escaped,Prompt+Fletter,KONames[Flight],30,len,-11,3,0);
      if (ifunc = 21) and (Flight > 1) then begin
        Dec(Flight,2);
        doback := true;
        exit;
      end;
      {if ifunc > 0 then RingClear(false);}
    until ifunc < 1;
  end;
end;

procedure PrintAwardChart(const expitch: TPrintPitch; const Flight: word);
var
   MultiColor   : boolean;

procedure ShowOAaward(const count,count2: word; const TestTie: boolean);
var
   Atemp : Real48;
   Award : Real48;
   loop    : word;
   Rankok  : boolean;
begin
  if (GameType_Choice in [gtcKO,gtcZKO]) then
    Award := GetKOAward(OAawards[Flight],count,count2,Round(EVTables[Flight]),
    FixedOATab[Flight],RateChoice in [rcNAP],KOEventType in [ketCompact],rankok);
  if count = count2 then begin
    if not (GameType_Choice in [gtcKO,gtcZKO]) then
      Award := GetOAaward(OAawards[Flight],count,SessChoice);
    if count = 1 then ReportLine(2,pexpand,2,LeftPadL(Long2StrL(count),3)+'  '
      +Real2StrL(Award,8,2))
    else ReportLine(1,pexpand,1,LeftPadL(Long2StrL(count),3)+'  '
      +Real2StrL(Award,8,2));
  end
  else begin
    if not (GameType_Choice in [gtcKO,gtcZKO]) then begin
      Award := 0;
      for loop := count to count2 do
        Award := Award + GetOAaward(OAawards[Flight],loop,SessChoice);
      Award := Award / (count2 - count + 1);
    end;
    if TestTie then
    ReportLine(1,pexpand,1,'*'+Long2StrL(count)+'/'
      +PadL(Long2StrL(count2),2)+Real2StrL(Award,8,2))
    else ReportLine(1,pexpand,1,LeftPadL(Long2StrL(count),2)+'/'
      +PadL(Long2StrL(count2),2)+Real2StrL(Award,8,2));
  end;
  if MultiColor then with Event_MP_color_table[ColorChoice[Flight]] do begin
    Atemp := Round2(Award * MPPercent[1] / 10000);
    if (MPLimit[1] > 0) and (Atemp > MPLimit[1] / 100) then
      Atemp := MPLimit[1] / 100;
    ReportLine(0,pnone,1,'   '+Real2StrL(Atemp,8,2)+Real2StrL(Award-Atemp,8,2));
  end;
end;

var
   count      : word;
   count2     : word;
begin
  MultiColor := false;
  if (ColorChoice[Flight] < color_types) and (Depth[Flight] > 0) then begin
    if Event_MP_color_table[ColorChoice[Flight]].MPPercent[1] > 9990 then
      ReportLine(0,pnone,1,' ('+MP_Color_text[Byte(Event_MP_color_table
      [ColorChoice[Flight]].MPColors[1])]+')')
    else begin
      ReportLine(0,pnone,1,LeftPadL(MP_Color_text[Byte(Event_MP_color_table
        [ColorChoice[Flight]].MPColors[1])],10)+LeftPadL(MP_Color_text
        [Byte(Event_MP_color_table[ColorChoice[Flight]].MPColors[2])],8));
      MultiColor := true;
    end;
  end;
  ko_tie := false;
  if (GameType_Choice in [gtcKO,gtcZKO]) then case Depth[Flight] of
    4,8,16,32,64,128 : if (GameType_Choice in [gtcZKO])
      or (MatchSize[Flight] >= 18) then ko_tie := true;
  end;
  if ko_tie then begin
    count := 1;
    while count <= Depth[Flight] do begin
      case count of
        1,2: count2 := count;
        else count2 := (count - 1) * 2;
      end;
      ShowOAaward(count,count2,TieBreak[Flight] and (count2 = Depth[Flight]));
      count := count2 + 1;
    end;
    if TieBreak[Flight] then ReportLine(1,expitch,1,'*For winning play off');
  end
  else for count := 1 to Depth[Flight] do ShowOAaward(count,count,false);
end;

procedure PrintMatchAward(const expitch: TPrintPitch; const Flight: word);
var
   MultiColor   : boolean;
begin
  ReportLine(3,expitch,1,'Boards per Match: '+Long2StrL(MatchSize[Flight]));
  ReportLine(1,expitch,1,'Match Award:    '+Real2StrL(MatchAwards[Flight],6,2));
  if ColorChoice[Flight] < color_types then begin
    MultiColor := Sess_MP_color_table[ColorChoice[Flight]].MPPercent[1] <= 9990;
    if MultiColor and (ColorChoice[Flight] >= ccRed) then
      ReportLine(1,expitch,1,'              ('+MPColorText[Flight]+')')
    else ReportLine(0,pnone,1,' ('+MP_Color_text[Byte(Sess_MP_color_table
      [ColorChoice[Flight]].MPColors[1])]+')');
  end;
end;

procedure PrintSectAwards(const expitch: TPrintPitch; const Flight: word);
var
   MultiColor   : boolean;
   section      : word;
   count        : word;
   loop         : word;
begin
  multicolor := (SecTop_MP_color_table[ColorChoice[Flight]].MPPercent[1]
    < 10000) or (CompStruct(SecTop_MP_color_table[ColorChoice[Flight]],
    Sess_MP_color_table[ColorChoice[Flight]],SizeOf(TEventMP))<> 0);
  ReportLine(3,expitch,-1,'Session Awards');
  if (ColorChoice[Flight] < color_types) and not MultiColor then
    ReportLine(0,pnone,1,' ('+MP_Color_text[Byte(Sess_MP_color_table
    [ColorChoice[Flight]].MPColors[1])]+')');
  section := 1;
  While (section <= MaxSections) and (SecSizes[Flight][section] > 0) do begin
    RepLine := LeftPadL(Long2StrL(SecSizes[Flight][section]),3);
    case GameType of
      T_individual: if SecHowell[Flight][section] > 0 then
        RepLine := RepLine+' Players '
        else RepLine := RepLine+' Tables ';
      T_pair: if Gametype_Choice = gtcT2P then RepLine := RepLine+' Tables '
        else RepLine := RepLine+' Pairs  ';
      T_BAM: RepLine := RepLine+' Tables ';
    end;
    ReportLine(2,expitch,2,RepLine);
    if MultiColor then ReportLine(0,pnone,1,LeftPadL(MP_Color_text[Byte(SecTop_MP_color_table
      [ColorChoice[Flight]].MPColors[1])],11)
      +LeftPadL(MP_Color_text[Byte(Sess_MP_color_table
      [ColorChoice[Flight]].MPColors[2])],8));
    count := GetSecDepth(SecSizes[Flight][section]);
    for loop := 1 to count do begin
      Award := GetSecAward(SecAwards[Flight][section],loop);
      if loop = 1 then ReportLine(2,expitch,1,LeftPadL(Long2StrL(loop),3)+Real2StrL(Award,8,2))
      else ReportLine(1,expitch,1,LeftPadL(Long2StrL(loop),3)+Real2StrL(Award,8,2));
      if MultiColor then begin
        if (SecGold[Flight][section] >= loop) then
          with SecTop_MP_color_table[ColorChoice[Flight]] do begin
          Atemp := Round2(Award * MPPercent[1] / 10000);
          if (MPLimit[1] > 0) and (Atemp > MPLimit[1] / 100) then
            Atemp := MPLimit[1] / 100;
        end
        else with Sess_MP_color_table[ColorChoice[Flight]] do begin
          Atemp := Round2(Award * MPPercent[1]/ 10000);
          if (MPLimit[1] > 0) and (Atemp > MPLimit[1] / 100) then
            Atemp := MPLimit[1] / 100;
        end;
        if (SecGold[Flight][section] >= loop)
          or (ColorChoice[Flight] >= ccRed) then begin
          ReportLine(0,pnone,1,'   '+Real2StrL(Atemp,8,2));
          if Atemp < Award then ReportLine(0,pnone,1,Real2StrL(Award-Atemp,8,2));
        end
        else ReportLine(0,pnone,1,CharStrL(' ',11)+Real2StrL(Award,8,2));
      end
      else if (ChampType in [ctCPSide,ctCPCharity]) and (loop = 1)
        and (ColorChoice[Flight] = ccRedGold) then ReportLine(0,pnone,1,'***');
    end;
    Inc(section);
  end;
end;

procedure PrintMPChart(const doSummary: boolean);
var
  copynum     : word;
  Copies      : word;
  expitch     : TPrintPitch;
  skip        : word;
   count      : word;
   Flight     : word;
begin
  if (Destination = 2) and not doSummary then begin
    expitch := pexpand;
    skip := 1;
    defcopies := NumBox(Escaped,defcopies,'','Number of Copies to print',
      1, 9, 2, 0);
    Copies := defcopies;
  end
  else begin
    copies := 1;
    expitch := pnone;
    skip := 0;
  end;
  SetLineSpace(6);
  For copynum := 1 to copies do begin
    ReportLine(0,expitch,1,'Master Point Awards');
    For Flight := 1 to NumStrats do begin
      if dosplit and (ColorChoice[Flight] > ccRedGold) then continue;
      if Destination = 2 then ReportLine(0,ppica,0,'');
      if not doSummary then begin
        PrintMPHeader(skip,expitch,Flight=1,Flight);
        if Flight = 1 then begin
          if GameType <> T_KO then
            ReportLine(1,expitch,1,'Number of Sessions:    '+SessText);
          ReportLine(1,expitch,1,'Sanction Number:       '+Sanction);
          if NumStrats > 1 then NewLine(skip+1);
        end;
        ReportLine(1,expitch,1,'Master Point Limits:   ');
        if (GameType = T_KO) and (KOEventType in [ketBrk,ketFltBrk,
          ketCompact,ketBrkCons]) then begin
          if MostMPs[Flight] >= 65000 then begin
            if (KOEventType in [ketFltBrk]) then ReportLine(0,pnone,1,'None')
            else ReportLine(0,pnone,1,Long2StrL(LeastMPs[Flight]*4)+'+');
          end
          else ReportLine(0,pnone,1,Long2StrL(LeastMPs[Flight]*4)+'-'
            +Long2StrL(MostMPs[Flight]*4));
        end
        else ReportLine(0,pnone,1,MPLimitText(Flight));
        ReportLine(1,expitch,1,'Rating:                '
          +MPRateFact(M[Flight],P,T)+RateText);
        ReportLine(1,expitch,1,'Tables Entered:        '
          +Real2StrL(EVTables[Flight],3,1));
        ReportLine(1,expitch,1,'Tables Award Based on: '
          +Long2StrL(Round(MPTables[Flight])));
        if Depth[Flight] < 1 then ReportLine(2,expitch,-1,'No Overall Awards')
        else ReportLine(3,expitch,-1,'Overall Awards');
        PrintAwardChart(expitch,Flight);
        case GameType of
          T_KO,
          T_Swiss: PrintMatchAward(expitch,Flight);
          else if SecSizes[Flight][1] > 0 then PrintSectAwards(expitch,Flight);
        end;
        if issecawards then NewPage
        else NewLine(3);
      end
      else begin
        if Flight < 2 then begin
          ReportLine(0,expitch,1,' - '+EventName);
          if GameType <> T_KO then ReportLine(0,expitch,1,'; '+SessText);
          NewLine(1);
        end;
        if (Flight > 1) and TopStrat[Flight] and not (GameType in [T_KO])
          then begin
          ReportLine(0,expitch,1,CharStrL('-',50));
          NewLine(1);
        end;
        NewLine(1);
        if NumStrats > 1 then ReportLine(0,expitch,1,TopName+' '
          +FLetters[Flight]+'; ');
        ReportLine(0,expitch,1,'Limits: '+MPLimitText(Flight)+'; '
          +MPRateFact(M[Flight],P,T)+RateText);
        if ColorChoice[Flight] < color_types then begin
          ReportLine(0,expitch,1,'; ');
          ShowColor(Event_MP_color_table[ColorChoice[Flight]]);
        end;
        ReportLine(2,expitch,1,'Tables Entered: '
          +Real2StrL(EVTables[Flight],3,1)+'; Based on: '
          +Long2StrL(Round(MPTables[Flight])));
        if Depth[Flight] < 1 then ReportLine(0,expitch,-1,'No Overall Awards')
        else begin
          if GameType <> T_KO then ReportLine(0,expitch,1,'; Depth: '
            +Long2StrL(Depth[Flight]));
          ReportLine(0,expitch,1,'; First: '
            +Real2StrL(OaAwards[Flight],5,2));
        end;
        NewLine(2);
      end;
    end;
    if not doSummary and not issecawards and (Destination = 2) then begin
      if CFG.SheetSize < 30 then NewLine(CFG.RecapLines);
      NewPage;
    end;
  end;
  if Destination = 1 then ViewerShow('Award Chart','');
  if Destination = 2 then begin
    if doSummary then DefChoice := 2
    else DefChoice := 3;
  end;
  Close_Destination;
end;

function GetNumberofBrackets(const Flt: boolean): word;
begin
  {RingClear(false);}
  if Flt then GetNumberofBrackets := NumBox(Escaped,0,'Number of Brackets',
    'The average MP holding of the highest team in 2nd and lower bracket in'#13
    +'Flight B should be available from the game file.  Use the SHOW command'#13+
    'after entering names.  Number of Brackets in Flight B (0 to quit)',
    0,MaxFlights-1,3,0)
  else GetNumberofBrackets := NumBox(Escaped,0,'Number of Brackets',
    'The average MP holding of the highest team in 2nd and lower bracket'#13
    +'should be available from the game file.  Use the SHOW command after'#13+
    'entering names.  Number of Brackets (0 to quit)',0,MaxFlights,3,0);
end;

Var
   Strats1,
   Strats3,
   Strats2       : byte;

label
     abort,
     abort1,
     Dest_again;

begin
  repeat
    if Escaped then goto Abort;
    D := d_fact(Byte(Rating));
    R := r_fact(Byte(Rating));
    with NatTable do if NatFixed and (NFixedTab > 0)
      and (NFixedTab <= FixedOASize) then FixedOATab[1] := NFixedTab;
    Type1Text := StrPas(type_text[Byte(Gametype_Choice)]);
    GameType := type_tab[Byte(Gametype_Choice)];
    EventType := etOpen;
    ChampType := ctChamp;
    Type3Text := StrPas(event3_text[Byte(ChampType)]);
    If GameType <> T_KO then begin {process all non-KO events}
      case GameType_Choice of
        gtcInd,gtcPair,gtcSwiss,gtcBAM : EventType := etStrat;
        else EventType := etOpen;
      end;
      if (GameType = T_Individual) and (Rating in [rRP,rCC,rCCh,rInt,rCCM])
        then T := t_fact(2)
      else T := t_fact(GameType);
      if GameType_Choice = gtcZKO then ChampType := ctSide;
      j := event_types;
      if GameType <> T_swiss then Dec(j,2);
      if (RateChoice in [rcCC,rcRP,rcSectC,rcNAP,rcCOP
        {$ifdef office},rcPupil,rcIntro{$endif}]) then j := 3;
      if not NatFixed then
        EventType := TEventType(Pickchoice(j,Byte(EventType),1,
        event_type_title,@event_text,TR,true,0,pcchar));
      Type2Text := StrPas(event_text[Byte(EventType)]);
      FastWriteWindow(Type2Text,topline+3{5},19,GetTColor(nattrt));
      if RateChoice in [rcUnit,rcSect,rcReg] then begin
        j := event3_types;
        if Copy(Sanction,1,2) = 'SD' then ChampType := ctSide;
        if GameType <> T_pair then Dec(j,2);
        ChampType := TChampType(Pickchoice(j,Byte(ChampType),1,
          event3_type_title,@event3_text,TR,true,0,pcchar));
      end;
      Type3Text := StrPas(event3_text[Byte(ChampType)]);
      FastWriteWindow(Type2Text+' '+Type3Text,topline+3{5},19,GetTColor(nattrt));
      dosplit := false;
      if NatFixed then Restriction := NatTable.NRestrictions + 1
      else if RateChoice in [rcNAP,rcCOP] then Restriction := 1
      else begin
        Restriction := 0;
        if (Rating in [rReg]) and (GameType in [T_Individual,T_Pair,T_BAM]) then
          Restriction := Pickchoice(p_facts,1,1,p_fact_title,@p_fact_text,TR,
          true,0,pcchar);
        dosplit := Restriction = p_facts;
        if dosplit then Restriction := Pickchoice(p_facts-1,1,1,
          p_fact_title+'(split site) ',@p_fact_split_text,TR,true,0,pcchar);
        if Restriction < 1 then Restriction := Pickchoice(p_facts-1,1,1,
          p_fact_title,@p_fact_text,TR,true,0,pcchar);
      end;
      RestrictText := StrPas(p_fact_text[Restriction]);
      FastWriteWindow(RestrictText,topline+4{6},19,GetTColor(nattrt));
      P := p_fact(Restriction);
      if dosplit then P := P * p_fact(p_facts);
      case Rating of
        rUnit,rSect,rCC,rRP{$ifdef office},rPupil,rIntro{$endif}: SessChoice := 1;
        rReg: if ChampType = ctChamp then SessChoice := 2
           else SessChoice := 1;
        rNat: SessChoice := 6;
      end;
      if (ChampType in [ctCPSide,ctCPCharity]) or (GameType_Choice = gtcZKO)
        then SessChoice := 1
      else if NatFixed then SessChoice := NatTable.Sessions
      else SessChoice := Pickchoice(s_facts,SessChoice,1,s_fact_title,
        @s_fact_text,TR,true,0,pcchar);
      SessText := StrPas(s_fact_text[SessChoice]);
      FastWriteWindow(SessText,topline+5{7},19,GetTColor(nattrt));
      S := s_fact(SessChoice);
      case EventType of
        etOpen: begin
          NumStrats := 1;
          FlightSize[1] := 1;
          TopStrat[1] := true;
          Flights := 1;
          TopName := 'Flight';
          BktText := 'Flt ';
        end;
        etStrat: begin
          BktText := 'Str ';
          TopName := 'Stratum';
          NumStrats := NumBox(Escaped,ifunc,3,'Number of Strata',
            'Number of Strata',0,3,3,0,MC,0);
          if NumStrats < 1 then goto Abort;
          Flights := 1;
          for count := 1 to NumStrats do begin
            FlightSize[count] := NumStrats + 1 - count;
            TopStrat[count] := count = 1;
          end;
        end;
        etFlt: begin
          NumStrats := NumBox(Escaped,ifunc,3,'Number of Flights',
            'Number of Flights',0,3,3,0,MC,0);
          if NumStrats < 2 then goto Abort;
          Flights := NumStrats;
          count2 := GetFlightSize(0,'Top Flight',1,1,Strats1);
          if (Count2 = 2) then begin
            if Flights > 2 then begin
              isaxevent := true;
              ErrBox('This event will be treated as a Stratiflighted A/X event.',
                MC,0);
            end
            else isaxevent := YesNoBox(Escaped,ifunc,true,
              'Is this event advertised as a Stratiflighted A/X event',
              0,MC,CFG.InformColor);
          end;
          if isaxevent then begin
            Fletters := 'AX'+Copy(FlightLetters,2,9);
            FlModified[2] := true;
            FlModified[3] := true;
          end;
          if Flights > 2 then j := 1
          else j := 3;
          count2 := GetFlightSize(count2,'Second Flight',j,2,Strats2);
          if Flights > 2 then
            count2 := GetFlightSize(count2,'Third Flight',3,3,Strats3)
          else Strats3 := 1;
          isstrflt := (Strats1 > 1) or (Strats2 > 1) or (Strats3 > 1);
          NumStrats := count2;
          if isstrflt then begin
            BktText := 'Str ';
            TopName := 'Stratum';
          end
          else begin
            BktText := 'Flt ';
            TopName := 'Flight';
          end;
        end;
        etBRR: begin
          BktText := 'Bkt ';
          TopName := 'Bracket';
          Flights := 1;
          ifunc := 2;
          NumStrats := GetNumberofBrackets(false);
          if NumStrats < 1 then goto Abort;
          for count := 1 to NumStrats do begin
            FlightSize[count] := 1;
            TopStrat[count] := true;
          end;
        end;
        etFltBRR: begin
          BktText := 'Bkt ';
          TopName := 'Bracket';
          Flights := 2;
          ifunc := 2;
          NumStrats := GetNumberofBrackets(true)+1;
          if NumStrats < 2 then goto Abort;
          for count := 1 to NumStrats do begin
            FlightSize[count] := 1;
            TopStrat[count] := true;
          end;
        end;
      end;
      MakeTableWindow;
      repeat
        doback := false;
        GetTables;
        if doback then continue;
        if not done then begin
          case EventType of
            etOpen: GetOpenLimits(true);
            etBRR: case Flight of
              1: GetOpenLimits(false);
              2..MaxFlights: GetBRLimits;
            end;
            etFltBRR: case Flight of
              1,2: GetFlightLimits;
              3..MaxFlights: GetBRLimits;
            end;
            Else GetFlightLimits;
          end;
          if doback then continue;
          GetOtherTables;
          GetMPColors;
          if not dosplit then GetSectionSizes;
        end;
      until (Flight >= NumStrats) or done;
      NumStrats := Flight;
      if (EvTables[NumStrats] < 2.5) and CFG.tournament then
        Allow2Tables := YesNoBox(Escaped,ifunc,false,
        'The lowest stratum has less than 2.5 tables.  Include'#13+
        'overall awards for this stratum',
        0,MC,CFG.InformColor);
    end
    else begin  {process KOs}
      SessChoice := 2;
      ChampType := ctChamp;
      T := 1;
      K := k_fact(Byte(Rating));
      KOTypes := ko_event_types;
      if (RateChoice in [rcCC,rcRP,rcSectC,rcNAP,rcCOP]) then KOTypes := 2;
      if not NatFixed then
        KOEventType := TKOEventType(Pickchoice(KOTypes,3,1,
        event_type_title,@ko_event_text, TR,true,0,pcchar));
      Type2Text := StrPas(ko_event_text[Byte(KOEventType)]);
      FastWriteWindow(Type2Text,topline+3{5},19,GetTColor(nattrt));
      if NatFixed then Restriction := NatTable.NRestrictions + 1
      else if RateChoice in [rcNAP,rcCOP] then Restriction := 1
      else Restriction := Pickchoice(p_facts,1,1,p_fact_title,@p_fact_text,
        TR,true,0,pcchar);
      RestrictText := StrPas(p_fact_text[Restriction]);
      FastWriteWindow(RestrictText,topline+4{6},19,GetTColor(nattrt));
      P := p_fact(Restriction);
      case KOEventType of
        ketOpen,ketCons: begin
          BktText := 'Flt ';
          TopName := 'Flight';
          NumStrats := 1;
          Flights := 1;
        end;
        ketFlt: begin
          BktText := 'Flt ';
          TopName := 'Flight';
          ifunc := 2;
          NumStrats := NumBox(Escaped,ifunc,3,'Number of Flights',
            'Number of Flights',0,3,2,0,MC,0);
          if NumStrats < 1 then goto Abort;
          Flights := NumStrats;
        end;
        ketBrk,ketCompact,ketBrkCons: begin
          BktText := 'Bkt ';
          TopName := 'Bracket';
          Flights := 1;
          ifunc := 2;
          NumStrats := GetNumberofBrackets(false);
          if NumStrats < 1 then goto Abort;
        end;
        ketFltBrk: begin
          BktText := 'Bkt ';
          TopName := 'Bracket';
          Flights := 2;
          ifunc := 2;
          NumStrats := GetNumberofBrackets(true)+1;
          if NumStrats < 2 then goto Abort;
        end;
      end;
      for count := 1 to NumStrats do begin
        FlightSize[count] := 1;
        TopStrat[count] := true;
      end;
      MakeTableWindow;
      repeat
        doback := false;
        GetTables;
        if doback then continue;
        if not done then begin
          case KOEventType of
          ketOpen,ketCons: GetOpenLimits(true);
          ketFlt: GetFlightLimits;
          ketBrk,ketCompact,ketBrkCons: case Flight of
            1: GetOpenLimits(false);
            2..MaxFlights: GetBRLimits;
          end;
          ketFltBrk: case Flight of
               1,2: GetFlightLimits;
               3..MaxFlights: GetBRLimits;
             end;
          end;
          if doback then continue;
          GetOtherTables;
          GetMPColors;
          GetSectionSizes;
        end;
      until (Flight >= NumStrats) or done;
      NumStrats := Flight;
    end;
    if NumStrats > 0 then begin
      CalcMPTables;
      while true do begin
Dest_again:
        JumpSet := false;
        more_to_do := true;
        j := MaxPDest;
        {$ifdef office}
        if GameType in [T_swiss,T_KO] then Dec(j);
        {$endif}
        Set_Destination(0,j,DefChoice,@PDest_text,'Action ',false,
          pcshort,topic);
        if Destination = 3 then goto Abort1;
        SetJump(ESCJump);
        if JumpSet then goto Dest_again;
        JumpSet := true;
        case Destination of
          1,2: PrintMPChart(false);
          0,4: MainHalt;
          5: begin
            OpenDestination(2,0);
            PrintMPChart(true);
          end;
          6: EditAwards;
          {$ifdef office}
          7,8: PrintMPTable(Destination = 7);
          {$endif}
        end;
      end;
    end;
Abort:
    JumpSet := false;
    setabn(off);
    EraseBoxWindow;
    more_to_do := YesNoBox(Escaped, ifunc, true,
      'Produce another Masterpoint Chart', 0,MC,CFG.InformColor);
Abort1:
    setabn(off);
    if Assigned(w2) then KillWindow(w2);
    w2 := nil;
    EditDone(TournEditRec);
    {$ifdef office}
    DefChoice := 1;
    {$else}
    if CFG.tournament then DefChoice := 5
    else DefChoice := 2;
    {$endif}
  until not more_to_do;
  WriteDefaults;
  MainHalt;
end;

end.
