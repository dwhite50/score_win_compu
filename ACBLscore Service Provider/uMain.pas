unit uMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP;

type
  TForm1 = class(TForm)
    IdHTTP1: TIdHTTP;
    Label1: TLabel;
    edtPersonName: TEdit;
    Label2: TLabel;
    edtMIMEType: TEdit;
    Label3: TLabel;
    edtDescription: TEdit;
    Label4: TLabel;
    edtFile: TEdit;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    Button2: TButton;
    Label5: TLabel;
    edtHost: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  MsMultiPartFormData;

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    edtFile.Text := OpenDialog1.FileName;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  ResponseStream: TMemoryStream;
  MultiPartFormDataStream: TMsMultiPartFormDataStream;
begin
  MultiPartFormDataStream := TMsMultiPartFormDataStream.Create;
  ResponseStream := TMemoryStream.Create;
  try
    IdHttp1.Request.ContentType := MultiPartFormDataStream.RequestContentType;
    MultiPartFormDataStream.AddFormField('PersonName', edtPersonName.Text);
    MultiPartFormDataStream.AddFormField('Description', edtDescription.Text);
    MultiPartFormDataStream.AddFile(edtFile.Name, edtFile.Text, edtMIMEType.Text);
    { You must make sure you call this method *before* sending the stream }
    MultiPartFormDataStream.PrepareStreamForDispatch;
    MultiPartFormDataStream.Position := 0;
    IdHTTP1.Post(edtHost.Text, MultiPartFormDataStream, ResponseStream);
  finally
    MultiPartFormDataStream.Free;
    ResponseStream.Free;
  end;
end;

end.
