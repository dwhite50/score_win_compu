unit EditBoxU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcEF, OvcSF, OvcPB, OvcPF;

type
  TEditForm = class(TForm)
    OvcController1: TOvcController;
    OKbut: TButton;
    CanBut: TButton;
    StringField: TOvcPictureField;
    StringLabel: TOvcAttachedLabel;
    procedure AcceptIt(Sender: TObject);
    procedure CancelIt(Sender: TObject);
    procedure UserCommand(Sender: TObject; Command: Word);
  private
    { Private declarations }
   Canceled  : boolean;
   funccode  : byte;
   AllowUser : boolean;
  public
    { Public declarations }
  end;

const
     EditBoxYLoc : word = 50;
     EditBoxXLoc : word = 50;

procedure EditBox(var esc: boolean; var ifunc: byte;
          const title,prompt,Mask: String; var field : String;
          const len: byte; const topic: word; const AllowFunc: boolean);

implementation

uses
    csdef,
    StStrL;

{$R *.DFM}

const
  EditForm: TEditForm = nil;


procedure SetFormWidth(const title,prompt: String; const len: byte);
var
   MaxTextWidth        : integer;
   w     : integer;
   lenw  : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   cw    : integer;
begin
  with EditForm do begin
    w := 1;
    repeat
      Line := ExtractWordL(w,xLine,#13);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
      end;
      if Length(Line) > 0 then Inc(w);
    until Length(Line) < 1;
    Dec(w);
  end;
end;

begin
  with EditForm do begin
    MaxTextWidth := 0;
    GetTextWidth(title);
    GetTextWidth(prompt);
    Width := 100;
    Height := 161;
    if w < 3 then StringLabel.Top := (3-w) * 15
    else begin
      StringLabel.Top := 0;
      Height := (w-3) * 20 + 124;
      OkBut.Top := Height - 60;
      CanBut.Top := OkBut.Top;
    end;
    lenw := Canvas.TextWidth(CharStrL('n',len));
    if lenw > MaxTextWidth then MaxTextWidth := lenw;
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    Top := (Screen.Height * EditBoxYLoc) div 100 - (Height div 2);
    if Top < 0 then Top := 0;
    Left := (Screen.Width * EditBoxXLoc) div 100 - (Width div 2);
    if Left < 0 then Left := 0;
    StringField.Width := lenw;
    StringField.Left := (ClientWidth div 2)-(StringField.Width div 2);
    StringLabel.Width := Width-20;
    StringLabel.Left := 10;
    Caption := title;
    StringLabel.Caption := prompt;
    OkBut.Left := (ClientWidth div 4)-(OkBut.Width div 2);
    OkBut.Top := ClientHeight-40;
    CanBut.Left := (ClientWidth div 4)*3-(CanBut.Width div 2);
    CanBut.Top :=OkBut.Top;
  end;
end;

procedure EditBox(var esc: boolean; var ifunc: byte;
          const title,prompt,Mask: String; var field : String;
          const len: byte; const topic: word; const AllowFunc: boolean);
begin
  if not Assigned(EditForm) then
    Application.CreateForm(TEditForm, EditForm);
  with EditForm do begin
    Font.Size := ViewFontSize;
    Canceled := false;
    SetFormWidth(title,prompt,len);
    with StringField do begin
      Datatype := pftString;
      PictureMask := Mask;
      AsString := field;
      Options := [efoCaretToEnd,efoTrimBlanks];
      MaxLength := len;
    end;
    funccode := 0;
    AllowUser := AllowFunc;
    ShowModal;
    if not Canceled then field := StringField.GetStrippedEditString;
    esc := Canceled;
    ifunc := funccode;
    Free;
  end;
  EditForm := nil;
end;

procedure TEditForm.AcceptIt(Sender: TObject);
begin
  Close;
end;

procedure TEditForm.CancelIt(Sender: TObject);
begin
  Close;
  Canceled := true;
end;

procedure TEditForm.UserCommand(Sender: TObject; Command: Word);
begin
  if not AllowUser then exit;
  Close;
  Funccode := Command-255;
end;

end.
