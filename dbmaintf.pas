unit dbmaintf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  Tmform = class(TForm)
    procedure rundbmain(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  mform: Tmform;

implementation

uses dbmaint, version;

{$R *.DFM}

procedure Tmform.rundbmain(Sender: TObject);
begin
  dodbmaint;
  DisposeMainWindow;
  Application.Terminate;
end;

end.
