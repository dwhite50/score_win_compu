{$I debugdir.inc}
unit backup;

interface

procedure CallBackup;


implementation
Uses
    SysUtils,
    vListBox,
     csdef,
     backdata,
     backutil,
     LhaUnit,
     backback,
     backrest,
     version,
     util1,
     util2,
     history,
     pvio;

procedure CallBackup;
const
  ClubItems = 9;
  club_backup_text : Array[1..ClubItems] of PChar =
              (' 1 Backup Data Base',
               ' 2 Backup Game files',
               ' 3 Restore Data Base',
               ' 4 Restore Game files',
               ' 5 Backup and Delete Game files',
               ' 6 Backup Game files - selective',
               ' 7 Backup Data Base Archive',
               ' 8 Restore Data Base Archive',
               ' 9 Import from a Data Base Backup');

  TournItems = 9;
  tourn_backup_text : Array[1..TournItems] of PChar =
              (' 1 Backup Data Base',
               ' 2 Backup Game files',
               ' 3 Restore Data Base',
               ' 4 Restore Game files',
               ' 5 Backup and Delete Game files',
               ' 6 Create ACBL report diskette',
               ' 7 Backup Financial data only',
               ' 8 Restore Financial data only',
               ' 9 Import from a Data Base Backup');
{$ifndef office}
     MsgFileName        = 'MSG.TXT';
{$endif}
var
   ic           : word;
   j            : word;
   {$ifndef office}
   MsgLine      : String[80];
   {$endif}
{$I officepa.dcl}
begin
  IniName := GetCurrentDir+'\'+DefIniName; //set ini file to ACBLSCORE.INI
  AppName := 'WBACKUP';  //set APP name in ini file for getprofile, setprofile
  MakeMainWindow('ACBLscore Backup/Restore');
  AllowMinimize := true;
  topic := 26;
(*  if (ParamCount in [2,3,4]) then begin
    {$ifdef showmem}
    ErrBox(ParamStr(1)+' '+ParamStr(2)+' '+ParamStr(3)+' '+ParamStr(4),MC,0);
    {$endif}
    case ParamCount of
      2: RunLHA('a /m /l2 /w+ ',ParamStr(1),'@'+ParamStr(2),true);
      3: begin
        RunLHA('e /c /m ',ParamStr(2),ParamStr(3),false);
        {$ifdef office}
        RunMac(#27'G'#2);
        {$else}
        with NextCmdRec do begin
          NextP1 := 'DBIMP.EXE';
          NextP2 := ParamStr(1)+' '+ParamStr(3);
          NextP3 := false;
          NextKey := 0;
          SetNextCmd(@NextCmdRec);
        end;
        {$endif}
      end;
      4: begin
        RunLHA('e /c /m ',ParamStr(2),ParamStr(3)+' '+ParamStr(4),false);
        RepairNoAsk := true;
        SetDBRepair(true);
      end;
    end;
    {$ifndef office}
    if ExistFile(MsgFileName) then begin
      Assign(backfile,MsgFileName);
      {$I-} ReSet(backfile); {$I+}
      if IOResult <> 0 then MainHalt;
      OpenDestination(1,0,0);
      while not Eof(backfile) do begin
        ReadLn(backfile,MsgLine);
        ReportLine(1,pnone,1,MsgLine);
      end;
      Close(backfile);
      {$I-} Erase(backfile); {$I+}
      if IOResult = 0 then;
      ViewCentered := true;
      ViewerShow(1,2,80,25,inton{Window_Attr},Frame_Attr,'');
      Close_Destination;
    end;
    {$endif}
  end*)
  while true do begin
    if not CFG.tournament then ic := PickChoice(ClubItems,1,
      'function (ESC to exit) ',@club_backup_text,MC,false,topic)
    else begin
      ic := PickChoice(TournItems,1,'function (ESC to exit) ',
        @tourn_backup_text,MC,false,topic);
      case ic of
        6: ic := 10;
        7: ic := 11;
        8: ic := 12;
      end;
    end;
    {$ifdef office}
    if ic in [4,5] then CheckReadOnlyCFG;
    {$endif}
    case ic of
      0 : MainHalt;
      1 : BackupDataBase(false);
      2 : if not CFG.tournament then BackupClubGames('a')
          else BackupTournFiles('a',true);
      3 : RestoreDataBase(false,false);
      4 : RestoreGameFiles;
      5 : if not CFG.tournament then BackupClubGames('m')
          else BackupTournFiles('m',true);
      6 : BackupTournFiles('a',true);
      7:  BackupArchive;
      8:  RestoreArchive;
      9:  RestoreDataBase(false,true);
      10: BackupForMemphis;
      11: BackupDataBase(true);
      12: RestoreDataBase(true,false);
    end;
  end;
end;

end.
