unit acblutil;

interface

implementation

{$define test}
uses
  {Extend,
  TPCrt,
  TPDos,
  Dos,
  TPdir,
  TPString,
  TPWindow,
  Strings,}
  SysUtils,
  StUtils,
  StStrL,
  db33,
  {db_edit,
  ExWin,}
  fixpl,
  CSdef,
  groups,
  version,
  pvio,
  util1,
  history,
  util2,
  fdbase,
  fxdbase,
  Dbase,
  xdbase;

const
     ArcSuffix = '.LZ?';
     HtmSuffix = '.HTM';
     PPSuffix = '.PP';
     Repsuffix = '.REP';
     Finsuffix = '.FIN';
     STACtestFile = 'CLUBDEF.DAT';
     STACtestMinSize = 300;
     GFMask    = '???????';
     DBMask    = 'D*';
     MinSpaceNeeded = 1024 * 2048;
     TempDir = 'C:\ACBLTEMP\';
     MaxDBFileTypes = 2;
     DBFileTypes    : Array[1..MaxDBFileTypes] of String[3] = ('DAT','K?');
     PathChoice   : word = 3;
     {$ifdef test}
     HtmDestLocation = 'C:\TEMP\HTM\';
     PPDestLocation = 'C:\TEMP\PP\';
     TournDBLoc = 'C:\TEMP\TOURNDB\';
     TournArchive = 'C:\TOURNS\';
     AS400Loc = TournArchive;
     {$else}
     BridgeSys = '\\BRIDGE\SYS\';
     Dept730 = BridgeSys+'DEPT\730\';
     HtmDestLocation = Dept730+'TournResults\';
     TournDBLoc = Dept730+'TOURNDB\';
     PPDestLocation = Dept730+'DVICKNAI\PP\';
     TournArchive = BridgeSys+'TOURNS\';
     AS400Loc = '\\QACBL\ACBLIFS\';
     {$endif}
     NABCSancPref = 'NABC';
     OtherLocation = TournArchive+'ACBL32A';
     SancLZH      = 'T';
     MaxLZHSize   = $1000000;
     TournRepLocText          : Array[1..4] of Pchar =
       ('A:',
        'B:',
        'E:\DEPT\730\TOURNREP',
        'Other');
     {EXLow  = 1;
     EYLow  = 3;
     EXHigh = 80;
     EYHigh = 24;}

var
   esc        : boolean;
   len,
   ifunc      : byte;
   Search_Rec  : TSearchRec;
   dolzh       : boolean;
   found      : boolean;
   DBfiles    : word;
   path    : String;
   DBfileList : Array[1..200] of String[12];
   Sanction   : String[10];
   RepDestLocation    : String;
   RepDestValid       : boolean;
   PPDestValid       : boolean;
   HtmDestValid      : boolean;

procedure ReadSancLocs(const TestDest: boolean);

begin
  if TestDest then begin
    RepDestLocation := AS400Loc;
    RepDestValid := IsDirectory(JustPathNameL(RepDestLocation));
    if not RepDestValid then begin
//      RingClear(false);
      if not YesNoBox(esc,false,
        'AS/400 Shared drive is not accessable.'#13
        +'Masterpoint (.REP) files will not be processed.'#13'Continue',0) then
        MainHalt;
    end;
    PPDestValid := IsDirectory(JustPathNameL(PPDestLocation));
    HtmDestValid := IsDirectory(JustPathNameL(HtmDestLocation));
  end
  else begin
    RepDestValid := false;
    RepDestLocation := '';
  end;
end;

procedure DelTempDirFiles;
var
   FndErr : integer;
begin
  CreateDir(JustPathNameL(TempDir));
  repeat
    FndErr := FindFirst(TempDir+'*.*',faArchive,Search_Rec);
    FindClose(Search_Rec);
    if FndErr = 0 then if not DeleteFile(TempDir+Search_Rec.Name) then
      FndErr := -1;
  until FndErr <> 0;
end;

function RunLHA(const CmdLine,ArcName,FileName,Title: String): word;
var
   ExitCode : Word;
begin
  if not makewindow(EWin,EXLow,EYLow,EXHigh,EYHigh,true,true,false,
    GetTColor(defattrt),GetTColor(defattrt),GetTColor(defattrt),title) then
    OutOfMemory;
  if not displaywindow(EWin) then OutOfMemory;
  Exec_Win(EXLow,EYLow,EXHigh,EYHigh,CFG.LoadPath+'LHA.EXE',
    CmdLine+' '+ArcName+' '+FileName,'',title);
  CheckDosError(CFG.LoadPath+'LHA.EXE');
  WriteLn;
  ExitCode := DosExitCode;
  RunLHA := ExitCode;
  if ExitCode > 0 then Anykey;
  KillWindow(EWin);
  EWin := nil;
end;

function NumSanc(const Sanction: String): boolean;
var
   j          : byte;
begin
  NumSanc := false;
  if Length(Sanction) < 4 then exit;
  for j := 1 to 4 do if Pos(Sanction[j],'0123456789') < 1 then exit;
  NumSanc := true;
end;

{$I acblint.dcl}

type
    THashTable = Array[1..500,1..2] of String[3];
    PHashTable = ^THashTable;
    TfSancTable = Array[1..200] of String[10];
    PfSancTable = ^TfSancTable;

const
     HashTable : PHashTable = nil;
     HashTableSize : word = 0;
     fSancTable : PfSancTable = nil;
     SancTableSize : word = 0;
     TotalRecs     : Longint = 0;

{$I mpadj.inc}

procedure UnpackTourn(const full: boolean);
const
     ReportPath : String[40] = 'A:';
var
   DistNo       : String[2];
   DotLoc : byte;
   CurrentDir    : PathStr;
   errcode : byte;
   GFname  : PathStr;
   DBDelName : PathStr;
   GameDelName : PathStr;
   ArcName : String[12];
   Askpath : boolean;
   j       : word;
   k       : word;
   SpaceNeeded       : longint;
   SpaceUnused       : longint;
   f                 : file;
   TempKey           : KeyStr;
   FndKey            : KeyStr;
   Sanctions         : word;
   CurSanction       : word;
   SancTable         : Array[1..50] of String[10];
   oktoprocess       : Array[1..50] of boolean;
   Line              : String;
   lenx              : word;
   anyok             : boolean;
   TournName         : Array[1..50] of String[75];
   SancString        : Array[1..2] of String[76];
   SancStringNo      : byte;
   dorep             : boolean;
   PlayerRecLength   : longint;
   AnyGameFiles      : boolean;
   RRead             : longint;
   Added             : longint;
   Modified     : longint;
   OktoMerge    : boolean;
   dofin             : boolean;

procedure MakeFinReport;

function FM(const num: longint; const width: byte):_Str20;
begin
  FM := numcvt(num,width,true);
end;

function FFM(const rnum: real; const mult: word; const width: byte):_Str20;
begin
  FFM := FM(Round(rnum*mult),width);
end;

function ZF(const Field: OpenString):_Str20;
var
   len  : byte;
   OutField : _Str20;
   j        : byte;
begin
  len := Length(Field);
  OutField := Field;
  for j := 1 to len do if OutField[j] = ' ' then OutField[j] := '0';
  ZF := OutField;
end;

var
   SancKey      : Keystr;
   FinFileName  : PathStr;
   F            : text;
   Seq          : word;
   workrecs     : word;
   tempkey      : keystr;
   invkey       : keystr;
   supp_sess    : real;
   sick_sess    : real;
   Other_sess   : real;
   Train_sess   : real;
   isdistno     : boolean;

function NextSeq: _Str4;
begin
  Inc(Seq);
  NextSeq := FM(Seq,4);
end;

label
     abort;
begin
  fXPrepend := Tempdir;
  fOpenDBFile(Ftinfo_no);
  FinFileName := tempdir+Sanction+FinSuffix;
  Assign(F,FinFileName);
  SancKey := SancTable[CurSanction];
  FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],SancKey);
  if not ok then goto abort;
  OpenDBFile(Tourn_no);
  FindKey(IdxKey[Tourn_no,1]^,Recno[Tourn_no],SancKey);
  isdistno := ok;
  if ok then GetARec(Tourn_no);
  CloseDBFile(Tourn_no);
  ok := true;
  fGetARec(Ftinfo_no);
  if isdistno then begin
    Ftinfo.District_no := Tourn.District_no;
    fPutARec(Ftinfo_no);
  end;
  ReWrite(F);
  Seq := 0;
  PleaseWait;
  with Ftinfo do begin
    WriteLn(F,'01',Sanction_no,NextSeq,Tourn_type,Tourn_city,tourn_dates,
      tourn_name,tourn_site,Td_no,Dic_Name,District_no,Unit_no,
      FFM(Valu(Dic_Supp),100,5),FFM(Valu(Sanction_fee),100,5),
      FFM(Valu(supply_rate),100,4),FFM(Valu(surcharge),100,5),
      FFM(Valu(Per_Diem_rate),100,5),FFM(Valu(Mileage_rate),1000,4),
      FFM(Valu(Non_Memb_acbl_rate),100,5),Use_EX_rate,
      FFM(Valu(Exchange_rate),100,5),FFM(Valu(Exchange_rate_td),100,5),
      FFM(Valu(Gst),100,4));
    workrecs := 0;
    tempkey := SancKey;
    fOpenDBFile(Fwksheet_no);
    SearchKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Fwksheet do begin
      fGetARec(Fwksheet_no);
      if Event_code <> 'ZZZZ' then Inc(workrecs);
      NextKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    end;
    WriteLn(F,'02',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Entry_total),100,9),FFM(Valu(Non_mem),100,8),
      FFM(Valu(Fill_in),100,8),FFM(Valu(Net),100,9),
      FFM(Valu(total_tables),10,7));
    tempkey := SancKey;
    SearchKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Fwksheet do begin
      fGetARec(Fwksheet_no);
      if Event_code <> 'ZZZZ' then
        WriteLn(F,'03',Sanction_no,NextSeq,IdxDate(Event_date),ZF(Time),
        Event_code,Event_Name,ZF(sessions),FFM(Valu(tables),10,5),ZF(Entries),
        FFM(Valu(Entry_fee),100,5),FFM(Valu(Entry_total),100,7),
        FFM(Valu(non_memb),100,5),FFM(Valu(Fill_in),100,5));
      NextKey(fIdxKey[Fwksheet_no,1]^,fRecno[Fwksheet_no],tempkey);
    end;
    workrecs := 0;
    fCloseDBFile(Fwksheet_no);
    fOpenDBFile(Finvoice_no);
    tempkey := SancKey;
    SearchKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do begin
      Inc(workrecs);
      NextKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    end;
    WriteLn(F,'04',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Dir_sessions),100,5),FFM(Valu(Director_fees),100,8),
      FFM(Valu(Trans_total_inv),100,7),ZF(Hotel_days_total),
      FFM(Valu(Hotel_total),100,7),FFM(Valu(Per_diem_total_days),10,5),
      FFM(Valu(Per_diem_total),100,8),FFM(Valu(Sanction_tables),10,7),
      FFM(Valu(Sanction_fee_total),100,8),FFM(Valu(Supply_tables),10,7),
      FFM(Valu(Supply_total),100,8),FFM(Valu(Hand_record_total),100,7),
      ZF(Non_members_acbl),FFM(Valu(non_mem_total_acbl),100,8),
      FFM(Valu(Expenses_total),100,9));
    tempkey := SancKey;
    fOpenDBFile(Finvitem_no);
    SearchKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with Finvoice do begin
      fGetARec(Finvoice_no);
      InvKey := Fhash;
      supp_sess := 0;
      sick_sess := 0;
      Other_sess := 0;
      Train_Sess := 0;
      SearchKey(fIdxKey[Finvitem_no,1]^,fRecno[Finvitem_no],invkey);
      while ok and CompareKey(invkey,Fhash) do with Finvitem do begin
        fGetARec(Finvitem_no);
        if Ival(item_type) = 1 then case Ival(sub_item_type) of
          3: supp_sess := supp_sess+Valu(items);
          4: sick_sess := sick_sess+Valu(items);
          5: Other_sess := Other_sess+Valu(items);
          7: Train_sess := Train_sess+Valu(items);
        end;
        NextKey(fIdxKey[Finvitem_no,1]^,fRecno[Finvitem_no],invkey);
      end;
      ok := true;
      if salaried = 'Y' then Empl_Stat := 'S      ';
      WriteLn(F,'05',Sanction_no,NextSeq,Td_no,Player_no,Name,Empl_stat[1],
        salaried,td_rank,dates,FFM(Valu(dic_sessions),100,4),
        FFM(Valu(Staff_sessions),100,4),FFM(supp_sess,100,4),
        FFM(sick_sess,100,4),FFM(other_sess,100,4),
        FFM(Valu(Staff_td_total),100,7),FFM(Valu(Trans_amt),100,7),
        FFM(Valu(Trans_amtd),100,7),ZF(Hotel_days),FFM(Valu(Hotel_amt),100,7),
        FFM(Valu(per_diem_days),10,3),FFM(Valu(Advance_amt),100,6),
        FFM(Valu(exchange_total),100,7),FFM(Train_sess,100,4));
      NextKey(fIdxKey[Finvoice_no,1]^,fRecno[Finvoice_no],tempkey);
    end;
    fCloseDBFile(Finvitem_no);
    fCloseDBFile(Finvoice_no);
    fOpenDBFile(FOfs_no);
    workrecs := 0;
    tempkey := SancKey;
    SearchKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fofs do begin
      fGetARec(Fofs_no);
      if Ival(item_type) in [1,3,4,5,6,7,8,11] then Inc(workrecs);
      NextKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    end;
    if Copy(Sanction,1,2) = '00' then begin
      Part_time_fees := Fstr(Valu(Part_time_fees)+Valu(Extended_fees),9,2);
      Extended_fees := Fstr(0,9,2);
    end;
    WriteLn(F,'06',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(Salaried_fees),100,8),FFM(Valu(Full_time_fees),100,8),
      FFM(Valu(Part_time_fees),100,8),FFM(Valu(Sanc_free_tables),10,6),
      FFM(Valu(sanction_tables),10,7),FFM(Valu(Sanction_fee_total),100,8),
      FFM(Valu(hand_record_total),100,7),FFM(Valu(supply_tables),10,7),
      FFM(Valu(supply_total),100,8),FFM(Valu(trans_prepaid),100,7),
      ZF(non_members_acbl),FFM(Valu(non_mem_total_acbl),100,8),
      FFM(Valu(memb_fees),100,6),FFM(Valu(acbl_other),100,7),
      FFM(Valu(acbl_collected),100,9),FFM(Valu(comp_rental),100,5),
      FFM(Valu(trans_dic),100,7),FFM(Valu(acbl_other_paid),100,7),
      FFM(Valu(acbl_paid_out),100,8),FFM(Valu(acbl_due),100,9),
      FFM(Valu(scrip_to_acbl),100,7),FFM(Valu(us_due_acbl),100,9),
      FFM(Valu(us_check),100,9),FFM(Valu(zcanadian_due_acbl),100,9),
      FFM(Valu(can_memb_fees),100,6),FFM(Valu(can_check),100,9),
      FFM(Valu(Extended_fees),100,8));
    tempkey := SancKey;
    SearchKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fofs do begin
      fGetARec(Fofs_no);
      if Ival(item_type) in [1,3,4,5,6,7,8,11] then
        WriteLn(F,'07',Sanction_no,NextSeq,ZF(item_type),description,
        FFM(Valu(items),10,5),FFM(Valu(item_rate),100,7),
        FFM(Valu(amount),100,8));
      NextKey(fIdxKey[FOfs_no,1]^,fRecno[FOfs_no],tempkey);
    end;
    fCloseDBFile(FOfs_no);
    fOpenDBFile(FBalance_no);
    workrecs := 0;
    tempkey := SancKey;
    SearchKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fbalance do begin
      fGetARec(Fbalance_no);
      if Ival(item_type) in [1,2,3,4,5] then Inc(workrecs);
      NextKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    end;
    WriteLn(F,'08',Sanction_no,NextSeq,FM(workrecs,4),
      FFM(Valu(additions_total),100,8),FFM(Valu(deductions_total),100,8),
      FFM(Valu(exchange_td_total),100,8),FFM(Valu(exchange_acbl_total),100,8),
      FFM(Valu(expenses_total),100,9),FFM(Valu(sponsor_check),100,9),
      FFM(Valu(sponsor_net),100,9),FFM(Valu(checks_total),100,8),
      FFM(Valu(cash_total),100,8),FFM(Valu(other_total),100,8),
      FFM(Valu(money_total),100,9));
    tempkey := SancKey;
    SearchKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    while ok and CompareKey(tempkey,SancKey) do with fbalance do begin
      fGetARec(Fbalance_no);
      if Ival(item_type) in [1,2,3,4,5] then
        WriteLn(F,'09',Sanction_no,NextSeq,ZF(item_type),description,
        FFM(Valu(items),10,5),FFM(Valu(item_rate),100,7),
        FFM(Valu(amount),100,8));
      NextKey(fIdxKey[Fbalance_no,1]^,fRecno[Fbalance_no],tempkey);
    end;
    fCloseDBFile(FBalance_no);
    WriteLn(F,'99',Sanction_no,NextSeq);
  end;
  Close(F);
  Copyfile(FinFileName,RepDestLocation+Sanction+FinSuffix,false);
  EraseBoxWindow;
abort:
  fCloseFiles;
end;

procedure CheckCat;
begin
  with Playern do begin
    if Pos(cat_a,cat_a_str) = 0 then cat_a_str := cat_a_str+cat_a;
    if Pos(cat_b,cat_b_str) = 0 then cat_b_str := cat_b_str+cat_b;
  end;
end;

procedure GetReportPath(const Askpath,doDB: boolean);
var
   NewChoice    : word;
label
     path_ok,
     path_again;
begin
  if not Askpath then goto path_ok;
path_again:
  NewChoice := PickChoice(4,PathChoice,1,'location of tournament report files',
    @TournRepLocText,MC,false,0,pcchar);
  esc := NewChoice < 1;
  if esc then exit;
  PathChoice := NewChoice;
  ifunc := 0;
  if PathChoice = 4 then
    StrBox(esc,ifunc,'Location of tournament report files'#13,
    ReportPath,40,len,5,7,0,MC,0)
  else ReportPath := StrPas(TournRepLocText[PathChoice]);
  if esc then Exit;
  ReportPath := fixpath(ReportPath);
path_ok:
  if CheckDrive(esc,ReportPath,false) <> 0 then goto path_again;
  if doDB then FindFirst(ReportPath+DBMask+ArcSuffix,Archive,Search_Rec)
  else FindFirst(ReportPath+GFMask+ArcSuffix,Archive,Search_Rec);
  if DosError <> 0 then begin
    RingClear(false);
    ErrBox('No tournament files found in '+ReportPath,MC,0);
    goto path_again;
  end;
end;

procedure BackFillDB;
var
   isplayer     : boolean;
   NameKey      : keystr;

procedure UpdatePl;
var
   OldKey       : keystr;
begin
  with playern do begin
    Inc(Modified);
    FastWriteWindow(long2str(Modified), 4,12, inton);
    Acbl_Date := xplayern.Acbl_date;
    Acbl_rank := xplayern.Acbl_rank;
    ACBL_Update_Date := xplayern.ACBL_Update_Date;
    Paid_Thru := xplayern.Paid_Thru;
    if Empty(Cat_A) then Cat_a := xplayern.Cat_a;
    if Empty(Cat_b) then Cat_b := xplayern.Cat_b;
    City := xplayern.City;
    State := xplayern.State;
    Street := xplayern.Street;
    zip := xplayern.zip;
    if not Empty(Street) then Mail_Code := 'M';
    Country := xplayern.Country;
    if Empty(Gender) then Gender := xplayern.Gender;
    Phone := xplayern.Phone;
    Unit_no := xplayern.Unit_no;
    District_no := xplayern.District_no;
    Tot_points := Num2Key(Key2Num(xplayern.Tot_points)-Key2Num(Y_T_D),4);
    if Player_check(xplayern.Player_no,pnPoundACBL)
      and not Player_check(Player_no,pnPoundACBL) then begin
      OldKey := GetKey(Playern_no,2);
      DeleteKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
      Player_no := xplayern.Player_no;
      OldKey := GetKey(Playern_no,2);
      AddKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
    end;
    if Empty(First_Name) and not Empty(xplayern.First_Name) then begin
      OldKey := GetKey(Playern_no,1);
      DeleteKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
      First_Name := xplayern.First_Name;
      OldKey := GetKey(Playern_no,1);
      AddKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
    end;
    PutARec(Playern_no);
  end;
end;

var
   Key_Char     : word;
label
     abort;
begin
  GroupInit;
  OpenDBFile(Playern_no);
  Filno := Playern_no;
  XPrepend := CFG.PLpath;
  with xplayern do begin
    Paid_Thru := CharStr(' ',6);
    ACBL_Update_Date := CharStr(' ',8);
    District_no := '  ';
    Country := '  ';
  end;
  RRead := 0;
  Modified := 0;
  BoxTitle := ' BackFilling the Data Base ';
  MsgBox('Reading #'#13'Imported                    '#13'Modified',defattrt,
    false,MC,0,0);
  Flush_DF := false;
  Flush_IF := false;
  xOpenDBFile(Playern_no);
  ClearKey(IdxKey[Playern_no,1]^);
  NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  while ok do begin
    while KeyPressed do begin
      key_char := KeyIn;
      if Lo(key_char) = 27 then goto abort;
    end;
    GetARec(Playern_no);
    Inc(RRead);
    FastWriteWindow(long2str(RRead), 2,12, inton);
    TempKey := Player_key(Playern.Player_no);
    isplayer := Player_check(Playern.Player_no,pnPoundACBL);
    if isplayer then
      FindKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],TempKey)
    else ok := false;
    if not ok then with Playern do begin
      Namekey := Upper(Last_name+Copy(First_name,1,8));
      TempKey := Namekey;
      SearchKey(xIdxKey[Playern_no,1]^,xRecno[Playern_no],Tempkey);
      while ok and CompareKey(Tempkey,NameKey) do begin
        xGetARec(Playern_no);
        ok := (Upper(City) = Upper(xPlayern.City))
          and (Upper(First_name) = Upper(xPlayern.First_name))
          and (Empty(State) or (State = xPlayern.State));
        if ok and isplayer then
          ok := not Player_check(xplayern.Player_no,pnPoundACBL);
        if ok then Break;
        ok := true;
        NextKey(xIdxKey[Playern_no,1]^,xRecno[Playern_no],Tempkey);
      end;
      ok := ok and CompareKey(Tempkey,Namekey);
    end;
    if ok then begin
      xGetARec(Playern_no);
      UpdatePl;
      CheckCat;
    end;
    NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],FndKey);
  end;
abort:
  GroupWrite;
  XCloseDBFile(Playern_no);
  CloseDBFile(Playern_no);
  EraseBoxWindow;
end;

Procedure Transfile(const Fno: word; const Source,Target,HashLoc,SancLoc: Pointer;
          const Len: word; const BuildHash: boolean);
var
   FndKey       : KeyStr;
   SaveKey      : Array[1..MaxKeyno] of KeyStr;
   Found        : boolean;
   j            : word;
   Sanc         : String[10];
begin
  OpenDBFile(Fno);
  xOpenDBFile(Fno);
  Filno := Fno;
  ClearKey(xIdxKey[Fno,1]^);
  repeat
    NextKey(xIdxKey[Fno,1]^,xRecno[Fno],FndKey);
    if ok then begin
      xGetARec(Fno);
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      if Assigned(SancLoc) then begin
        Found := false;
        Move(SancLoc^,Sanc,SizeOf(Sanc));
        for j := 1 to Sanctions do if CompareKey(Sanc,SancTable[j]) then begin
          Found := oktoprocess[j];
          Break;
        end;
        if not Found then continue;
      end;
      if BuildHash then begin
        Inc(HashTableSize);
        HashTable^[HashTableSize,1] := xGetKey(Fno,3);
      end;
      FindKey(IdxKey[Fno,1]^,Recno[Fno],FndKey);
      Found := ok;
      if ok then begin
        GetARec(Fno);
        if BuildHash then HashTable^[HashTableSize,2] := GetKey(Fno,3);
        for j := 1 to MaxKeyno do if KeyLen[Fno,j] > 0 then
          SaveKey[j] := GetKey(Fno,j);
      end;
      Move(Source^,Target^,Len);
      if not Found then begin
        ok := true;
        Add_Record;
        if BuildHash then HashTable^[HashTableSize,2] := GetKey(Fno,3);
        Inc(Added);
        FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
      end
      else begin
        if Assigned(HashLoc) then Move(HashTable^[HashTableSize,2],HashLoc^,4);
        for j := 1 to MaxKeyno do if KeyLen[Fno,j] > 0 then begin
          FndKey := GetKey(Fno,j);
          if SaveKey[j] <> FndKey then begin
            DeleteKey(IdxKey[Fno,j]^,Recno[Fno],SaveKey[j]);
            AddKey(IdxKey[Fno,j]^,Recno[Fno],FndKey);
          end;
        end;
        PutARec(Fno);
        Inc(Modified);
        FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
      end;
    end;
  until not ok;
  CloseDBFile(Fno);
  xCloseDBFile(Fno);
end;

procedure DeleteRecords(const Fno: word);
var
   j    : word;
   TempKey,
   FndKey  : KeyStr;
begin
  fFilno := Fno;
  for j := 1 to SancTableSize do begin
    FndKey := fSancTable^[j];
    TempKey := FndKey;
    SearchKey(fIdxKey[Fno,1]^,fRecno[Fno],TempKey);
    while ok and CompareKey(FndKey,TempKey) do begin
      fGetARec(Fno);
      fDelARec(Fno);
      TempKey := FndKey;
      SearchKey(fIdxKey[Fno,1]^,fRecno[Fno],TempKey);
    end;
    ok := true;
  end;
end;

Procedure fTransfile(const Fno: word; const Source,Target,HashLoc,SancLoc: Pointer;
          const Len: word; const BuildHash: byte);
          {0 no action, 1= build hash, 2=build sanction table}
var
   FndKey       : KeyStr;
   TempKey      : KeyStr;
   SaveKey      : Array[1..MaxKeyno] of KeyStr;
   Found        : boolean;
   j            : word;
   Sanc         : String[10];
begin
  fOpenDBFile(Fno);
  fxOpenDBFile(Fno);
  fFilno := Fno;
  if BuildHash <> 2 then DeleteRecords(Fno);
  ClearKey(fxIdxKey[Fno,1]^);
  repeat
    NextKey(fxIdxKey[Fno,1]^,fxRecno[Fno],FndKey);
    if ok then begin
      fxGetARec(Fno);
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      if Assigned(SancLoc) then begin
        Found := false;
        Move(SancLoc^,Sanc,SizeOf(Sanc));
        for j := 1 to Sanctions do if CompareKey(Sanc,SancTable[j]) then begin
          Found := oktoprocess[j];
          Break;
        end;
        if not Found then continue;
      end;
      if BuildHash <> 2 then begin
        Found := false;
        for j := 1 to SancTableSize do
          if CompareKey(fSancTable^[j],FndKey) then begin
          Found := true;
          Break;
        end;
        if not Found then continue;
      end;
      case BuildHash of
        1: begin
          Inc(HashTableSize);
          HashTable^[HashTableSize,1] := fxGetKey(Fno,2);
        end;
        2: begin
          Inc(SancTableSize);
          fSancTable^[SancTableSize] := fxGetKey(Fno,1);
        end;
      end;
      Found := false;
      if BuildHash = 2 then begin
        FindKey(fIdxKey[Fno,1]^,fRecno[Fno],FndKey);
        Found := ok;
        if ok then begin
          fGetARec(Fno);
          for j := 1 to fMaxKeyno do if fKeyLen[Fno,j] > 0 then
            SaveKey[j] := fGetKey(Fno,j);
        end;
      end;
      Move(Source^,Target^,Len);
      if not Found then begin
        ok := true;
        fAdd_Record;
        if BuildHash = 1 then HashTable^[HashTableSize,2] := fGetKey(Fno,2);
        Inc(Added);
        FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
      end
      else begin
        for j := 1 to fMaxKeyno do if fKeyLen[Fno,j] > 0 then begin
          FndKey := fGetKey(Fno,j);
          if SaveKey[j] <> FndKey then begin
            DeleteKey(fIdxKey[Fno,j]^,fRecno[Fno],SaveKey[j]);
            AddKey(fIdxKey[Fno,j]^,fRecno[Fno],FndKey);
          end;
        end;
        fPutARec(Fno);
        Inc(Modified);
        FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
      end;
    end;
  until not ok;
  fCloseDBFile(Fno);
  fxCloseDBFile(Fno);
end;

procedure MergeDistDB;

procedure UpdatePl;
var
   OldKey       : keystr;
begin
  with playern do begin
    Inc(Modified);
    FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
    if IdxDate(xplayern.Acbl_Date) > IdxDate(Acbl_Date) then
      Acbl_Date := xplayern.Acbl_date;
    if xplayern.Acbl_rank > Acbl_rank then Acbl_rank := xplayern.Acbl_rank;
    if IdxDate(xplayern.ACBL_Update_Date) > IdxDate(ACBL_Update_Date) then
      ACBL_Update_Date := xplayern.ACBL_Update_Date;
    if IdxDate(xplayern.Paid_Thru) > IdxDate(Paid_Thru) then
      Paid_Thru := xplayern.Paid_Thru;
    if Empty(Cat_A) then Cat_a := xplayern.Cat_a;
    if Empty(Cat_b) then Cat_b := xplayern.Cat_b;
    City := xplayern.City;
    State := xplayern.State;
    Street := xplayern.Street;
    zip := xplayern.zip;
    if not Empty(Street) then Mail_Code := 'M';
    if Empty(Country) then Country := xplayern.Country;
    if Empty(Gender) then Gender := xplayern.Gender;
    if IdxDate(xplayern.Local_date) > IdxDate(Local_date) then
      Local_Date := xplayern.Local_date;
    Phone := xplayern.Phone;
    Unit_no := xplayern.Unit_no;
    District_no := xplayern.District_no;
    if (Key2Num(Tot_points) < Key2Num(xplayern.Tot_points)) then
      Tot_points := Num2Key(Key2Num(xplayern.Tot_points)-Key2Num(Y_T_D),4);
    if Player_check(xplayern.Player_no,pnPoundACBL)
      and not Player_check(Player_no,pnPoundACBL) then begin
      OldKey := GetKey(Playern_no,2);
      DeleteKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
      Player_no := xplayern.Player_no;
      OldKey := GetKey(Playern_no,2);
      AddKey(IdxKey[Playern_no,2]^,Recno[Playern_no],OldKey);
    end;
    if Empty(First_Name) and not Empty(xplayern.First_Name) then begin
      OldKey := GetKey(Playern_no,1);
      DeleteKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
      First_Name := xplayern.First_Name;
      OldKey := GetKey(Playern_no,1);
      AddKey(IdxKey[Playern_no,1]^,Recno[Playern_no],OldKey);
    end;
    PutARec(Playern_no);
  end;
end;

var
   LockFile     : file;
   OldFileMode  : byte;
   ic           : word;
   j            : word;
   FndKey       : KeyStr;
   TempKey      : KeyStr;
   NameKey      : KeyStr;
   HashKey      : KeyStr;
   NewPlayer    : boolean;
   HashKey2     : KeyStr;
   Plchanged    : boolean;
   LastDeleted  : KeyStr;
   DelCount     : smallInt;
   isplayer     : boolean;
begin
  path := TournDBloc+DistNo+'\'+Copy(Sanction,1,2);
  FindFirst(path,Directory,Search_Rec);
  if DosError <> 0 then begin
    path := TournDBloc+DistNo;
    FindFirst(path,Directory,Search_Rec);
    if DosError <> 0 then begin
      RingBell(false);
      ErrBox('Data base for District '+DistNo+' not found.',MC,0);
      exit;
    end;
  end;
  path := fixpath(path);
  OldFileMode := FileMode;
  FileMode := $11; {write only, denyall}
  Assign(LockFile,path+DBLockFileName);
  {$I-} ReWrite(LockFile); {$I+}
  FileMode := OldFileMode;
  if IOResult <> 0 then begin
    RingBell(false);
    ErrBox('Data base for District '+DistNo+' is in use.  Merge not done.',MC,0);
    exit;
  end;
  GroupInit;
  RRead := 0;
  Added := 0;
  Modified := 0;
  CFG.DBpath := path;
  XPrepend := tempdir;
  fXPrepend := XPrepend;
  xOpenDBFile(Playern_no);
  TotalRecs := UsedRecs(xDatF[Playern_no]^);
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[PlayerGr_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[Attend_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[OArank_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[Tourn_no,0],dbUsedRecs));
  Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[TournEv_no,0],dbUsedRecs));
  if ExistFile(XPrepend+DBNames[ClubDef_no,0]) then
    Inc(TotalRecs,GetDBItemSize(XPrepend+DBNames[ClubDef_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[FTinfo_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Ftinfo_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[Fwksheet_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Fwksheet_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[Finvoice_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Finvoice_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[Finvitem_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[Finvitem_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[FBalance_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[FBalance_no,0],dbUsedRecs));
  if ExistFile(fXPrepend+fDBNames[FOfs_no,0]) then
    Inc(TotalRecs,GetDBItemSize(fXPrepend+fDBNames[FOfs_no,0],dbUsedRecs));
  BoxTitle := ' Importing from '+XPrepend+' to district '+DistNo+' ';
  ProgressBarInit('Imported'#13'Modified',defattrt,MC,0);
  if not Assigned(HashTable) then New(HashTable);
  HashTableSize := 0;
  Flush_DF := false;
  Flush_IF := false;
  if ExistFile(XPrepend+DBNames[ClubDef_no,0]) then
    TransFile(ClubDef_no,@xClubDef,@ClubDef,nil,nil,SizeOf(ClubDef),false);
  TransFile(Tourn_no,@xTourn,@Tourn,nil,@xTourn.Sanction_no,SizeOf(Tourn),false);
  TransFile(TournEv_no,@xTournEv,@TournEv,@TournEv.Hash,@xTournEv.Sanction_no,
    SizeOf(TournEv),true);
  OpenDBFile(Playern_no);
  xOpenDBFile(Playern_no);
  OpenDBFile(PlayerGr_no);
  xOpenDBFile(PlayerGr_no);
  OpenDBFile(Attend_no);
  xOpenDBFile(Attend_no);
  OpenDBFile(OARank_no);
  xOpenDBFile(OARank_no);
  ClearKey(xIdxKey[Playern_no,2]^);
  NextKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],FndKey);
  while ok do begin
    Inc(RRead);
    ProgressBar(RRead,TotalRecs);
    xGetARec(Playern_no);
    isplayer := Player_check(xplayern.Player_no,pnPoundACBL);
    if isplayer then FindKey(IdxKey[Playern_no,2]^,Recno[Playern_no],FndKey)
    else ok := false;
    if not ok then with xPlayern do begin
      Namekey := Upper(Last_name+Copy(First_name,1,8));
      TempKey := Namekey;
      SearchKey(IdxKey[Playern_no,1]^,Recno[Playern_no],Tempkey);
      while ok and CompareKey(Tempkey,NameKey) do begin
        GetARec(Playern_no);
        ok := (Upper(City) = Upper(Playern.City))
          and (Upper(First_name) = Upper(Playern.First_name))
          and (Empty(State) or (State = Playern.State));
        if ok and isplayer then
          ok := not Player_check(playern.Player_no,pnPoundACBL);
        if ok then Break;
        ok := true;
        NextKey(IdxKey[Playern_no,1]^,Recno[Playern_no],Tempkey);
      end;
      ok := ok and CompareKey(Tempkey,Namekey);
    end;
    if ok then begin
      NewPlayer := false;
      GetARec(Playern_no);
      UpdatePl;
    end
    else begin
      NewPlayer := true;
      Inc(Added);
      FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
      ok := true;
      playern := xplayern;
      Filno := Playern_no;
      Add_Record;
      Playern.tot_points := xplayern.tot_points;
      PutARec(Playern_no);
    end;
    CheckCat;
    FndKey := xPlayern.Record_no;
    TempKey := FndKey;
    SearchKey(xIdxKey[Playergr_no,1]^,xRecno[Playergr_no],TempKey);
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      xGetARec(PlayerGr_no);
      TempKey := Playern.Record_no+xPlayerGr.Group;
      FindKey(IdxKey[Playergr_no,1]^,Recno[Playergr_no],TempKey);
      if ok then with Playergr do begin
        Inc(Modified);
        FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
        GetARec(Playergr_no);
        if empty(Group_no) then Group_no := xplayergr.Group_no;
        PutARec(Playergr_no);
      end
      else begin
        Inc(Added);
        FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
        ok := true;
        playergr := xplayergr;
        Filno := Playergr_no;
        Add_Record;
      end;
      GroupAdd(Playergr.Group);
      NextKey(xIdxKey[Playergr_no,1]^,xRecno[Playergr_no],TempKey);
    end;
    plchanged := false;
    TempKey := FndKey;
    SearchKey(xIdxKey[Attend_no,1]^,xRecno[Attend_no],TempKey);
    Filno := Attend_no;
    LastDeleted := '   ';
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      DelCount := 0;
      xGetARec(Attend_no);
      HashKey := '';
      for j := 1 to HashTableSize do if xAttend.Hash = HashTable^[j,1] then
        begin
        HashKey := HashTable^[j,2];
        Break;
      end;
      if Length(HashKey) > 0 then begin
        if LastDeleted <> HashKey then begin
          HashKey2 := Playern.Record_no+HashKey;
          TempKey := HashKey2;
          SearchKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
          while ok and CompareKey(HashKey2,TempKey) do begin
            GetARec(Attend_no);
            MPAdjust(-Round(Valu(Attend.MPS)*100));
            DelARec(Attend_no);
            Inc(DelCount);
            Inc(Modified);
            FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
            plchanged := true;
            TempKey := HashKey2;
            SearchKey(IdxKey[Attend_no,1]^,Recno[Attend_no],TempKey);
          end;
          LastDeleted := HashKey;
        end;
        Attend := xAttend;
        Attend.Hash := HashKey;
        Dec(DelCount);
        if DelCount < 0 then begin
          Inc(Added);
          FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
        end;
        ok := true;
        Add_Record;
        MPAdjust(Round(Valu(Attend.MPS)*100));
        plchanged := true;
      end;
      NextKey(xIdxKey[Attend_no,1]^,xRecno[Attend_no],TempKey);
    end;
    TempKey := FndKey;
    SearchKey(xIdxKey[OARank_no,1]^,xRecno[OARank_no],TempKey);
    Filno := OARank_no;
    LastDeleted := '   ';
    while ok and CompareKey(FndKey,TempKey) do begin
      Inc(RRead);
      ProgressBar(RRead,TotalRecs);
      DelCount := 0;
      xGetARec(OARank_no);
      HashKey := '';
      for j := 1 to HashTableSize do if xOARank.Hash = HashTable^[j,1] then
        begin
        HashKey := HashTable^[j,2];
        Break;
      end;
      if Length(HashKey) > 0 then begin
        if LastDeleted <> HashKey then begin
          HashKey2 := Playern.Record_no+HashKey;
          TempKey := HashKey2;
          SearchKey(IdxKey[OARank_no,1]^,Recno[OARank_no],TempKey);
          while ok and CompareKey(HashKey2,TempKey) do begin
            GetARec(OARank_no);
            DelARec(OARank_no);
            Inc(DelCount);
            Inc(Modified);
            FastWriteWindow(long2str(Modified),BarLine2Loc,12,inton);
            TempKey := HashKey2;
            SearchKey(IdxKey[OArank_no,1]^,Recno[OARank_no],TempKey);
          end;
          LastDeleted := HashKey;
        end;
        OARank := xOARank;
        OArank.Hash := HashKey;
        Dec(DelCount);
        if DelCount < 0 then begin
          Inc(Added);
          FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
        end;
        ok := true;
        Add_Record;
      end;
      NextKey(xIdxKey[OARank_no,1]^,xRecno[OARank_no],TempKey);
    end;
    if plchanged then PutARec(Playern_no);
    NextKey(xIdxKey[Playern_no,2]^,xRecno[Playern_no],FndKey);
  end;
  CloseFiles;
  xCloseFiles;
  GroupWrite;
  if CFG.tournament and ExistFile(fXPrepend+fDBNames[FTinfo_no,0]) then begin
    if not Assigned(fSancTable) then New(fSancTable);
    HashTableSize := 0;
    SancTableSize := 0;
    fTransFile(FTinfo_no,@xFtinfo,@Ftinfo,nil,@xFTinfo.Sanction_no,
      SizeOf(Ftinfo),2);
    if ExistFile(fXPrepend+fDBNames[Fwksheet_no,0]) then
      fTransFile(Fwksheet_no,@xFwksheet,@Fwksheet,nil,@xFwksheet.Sanction_no,
      SizeOf(Fwksheet),0);
    if ExistFile(fXPrepend+fDBNames[Finvoice_no,0]) then begin
      fOpenDBFile(Finvitem_no);
      fTransFile(Finvoice_no,@xFinvoice,@Finvoice,@Finvoice.Fhash,
        @xFinvoice.Sanction_no,SizeOf(Finvoice),1);
      fCloseDBFile(Finvitem_no);
    end;
    if ExistFile(fXPrepend+fDBNames[FBalance_no,0]) then
      fTransFile(FBalance_no,@xFBalance,@FBalance,nil,@xFBalance.Sanction_no,
      SizeOf(FBalance),0);
    if ExistFile(fXPrepend+fDBNames[FOfs_no,0]) then
      fTransFile(FOfs_no,@xFOfs,@FOfs,nil,@xFOfs.Sanction_no,SizeOf(FOfs),0);
    if ExistFile(fXPrepend+fDBNames[Finvitem_no,0]) then begin
      fOpenDBFile(Finvitem_no);
      fxOpenDBFile(Finvitem_no);
      fFilno := Finvitem_no;
      fxFilno := fFilno;
      ClearKey(fxIdxKey[fxFilno,1]^);
      repeat
        NextKey(fxIdxKey[fxFilno,1]^,fxRecno[fxFilno],FndKey);
        if ok then begin
          Inc(RRead);
          ProgressBar(RRead,TotalRecs);
          DelCount := 0;
          fxGetARec(fxFilno);
          HashKey := '';
          for j := 1 to HashTableSize do
            if xFinvitem.FHash = HashTable^[j,1] then begin
            HashKey := HashTable^[j,2];
            Break;
          end;
          if Length(HashKey) > 0 then begin
            Inc(Added);
            Finvitem := xFinvitem;
            Finvitem.FHash := HashKey;
            FastWriteWindow(long2str(Added),BarLine1Loc,12,inton);
            ok := true;
            fAdd_Record;
          end;
        end;
      until not ok;
      fCloseDBFile(Finvitem_no);
      fxCloseDBFile(Finvitem_no);
    end;
  end;
  {$I-} Close(LockFile);
  if IOResult = 0 then;
  Erase(LockFile); {$I+}
  if IOResult = 0 then;
  CFG.DBpath := tempdir;
  if Assigned(HashTable) then Dispose(HashTable);
  HashTable := nil;
  anykey;
  EraseBoxWindow;
end;

procedure SetDistNumber;
{var
   UnitNo       : String[3];
   DistOk       : boolean;
   UnitOk       : boolean;}
begin
  DistNo := '00';
  {UnitNo := '000';}
  OktoMerge := false;
  OpenDBFile(Tourn_no);
  Filno := Tourn_no;
  ClearKey(IdxKey[Tourn_no,1]^);
  NextKey(IdxKey[Tourn_no,1]^,Recno[Tourn_no],FndKey);
  if ok and not CompareKey(FndKey,NABCSancPref) then begin
    GetARec(Tourn_no);
    DistNo := Tourn.District_no;
    OktoMerge := ValidDistrict(DistNo);
  end;
  {while ok do With Tourn do begin
    GetARec(Tourn_no);
    DistNo := District_no;
    UnitNo := Unit_no;
    repeat;
      ifunc := 0;
      StrBox(esc,ifunc,Trim(Sanction_no)+' '+Trim(Tourn_name)+#13+
        'Unit # for above tournament',UnitNo,3,len,1,4,0,MC,0);
      UnitOk := ValidUnit(UnitNo,DistNo);
      if not UnitOk then begin
        RingClear(false);
        ErrBox('Unit number '+UnitNo+' is not valid',MC,0);
      end
      else if Ival(UnitNo) < 1 then DistNo := District_no;
    until UnitOk;
    repeat;
      ifunc := 0;
      StrBox(esc,ifunc,Trim(Sanction_no)+' '+Trim(Tourn_name)+#13+
        'District # for above tournament',DistNo,2,len,5,4,0,MC,0);
      DistOk := ValidDistrict(DistNo);
      if not distok then begin
        RingClear(false);
        ErrBox('Invalid District number '+DistNo,MC,0);
      end;
    until DistOk;
    District_no := DistNo;
    Unit_no := UnitNo;
    PutARec(Tourn_no);
    NextKey(IdxKey[Tourn_no,1]^,Recno[Tourn_no],FndKey);
  end;}
  CloseDBFile(Tourn_no);
end;

const
     CopyText   : Array[1..4] of Pchar =
                ('1 Copy masterpoint and financial files to 400',
                 '2 Copy only masterpoint files to 400',
                 '3 Copy only financial files to 400',
                 '4 Do not copy any files to 400');

label
     gf_done,
     gf_again,
     game_again;
begin
  MakeMainWindow('Archive tournaments');
  ReadSancLocs(Full);
  if GetDriveType(DestDrive) = dtInvalid then begin
    RingClear(false);
    ErrBox('Drive '+DestDrive+' is invalid.',MC,0);
    MainHalt;
  end;
  CFG.tournament := true;
  CFG.DBpath := tempdir;
  repeat
    DBDelName := '';
    GameDelName := '';
    if Full then begin
      GetReportPath(true,true);
      if esc then MainHalt;
      errcode := GetFileName(ReportPath+DBMask+ArcSuffix,Archive+Directory,
        20,2,23,1,CFG.pickcolor,GFname,' Select tournament to process ');
      if Length(GFname) < 1 then continue;
      ArcName := JustFileName(GFname);
      DBDelName := GFname;
      DotLoc := Pos('.',ArcName);
      Sanction := copy(ArcName,2,DotLoc-1);
      DelTempDirFiles;
      RunLHA('e /c /m ',GFName,tempdir,' Extracting data base files ');
      CFG.DefDBVersion := 0;
      RepairDataBase(false,'');
      CheckSerial := false;
    end
    else with CFG do begin
      DelFile(DBpath+'LASTGAME');
      DelFile(DBpath+'ACBLOPEN');
      DelFile(DBpath+'CS.CFG');
    end;
    OpenDBFile(Tourn_no);
    Filno := Tourn_no;
    ClearKey(IdxKey[Filno,1]^);
    NextKey(IdxKey[Filno,1]^,Recno[Filno],TempKey);
    if not ok then begin
      ErrBox('No tournaments found in the data base.',MC,0);
      continue;
    end;
    Sanctions := 0;
    FillChar(SancString,SizeOf(SancString),0);
    SancStringNo := 1;
    while ok do with Tourn do begin
      Inc(Sanctions);
      GetARec(Filno);
      {SancTable[Sanctions] := SancDigits(Trim(TempKey));}
      SancTable[Sanctions] := Sanction_no;
      TournName[Sanctions] := Trim(Sanction_no)+' '+Trim(Tourn_City)+' '
        +Trim(Tourn_dates)+' '+Trim(Tourn_Name);
      if (Length(SancString[SancStringNo]) > 67) and (SancStringNo < 2) then
        Inc(SancStringNo);
      if Length(SancString[SancStringNo]) > 0 then
        SancString[SancStringNo] := SancString[SancStringNo]+' ';
      SancString[SancStringNo] := SancString[SancStringNo]
        +SancDigits(Trim(SancTable[Sanctions]));
      NextKey(IdxKey[Filno,1]^,Recno[Filno],TempKey);
    end;
    CloseDBFile(Tourn_no);
    if Full then BackFillDB;
    {if Full and RepDestValid
      and (CheckDrive(esc,RepDestLocation,false) <> 0) then Mainhalt;}
    MsgBox(Long2Str(Sanctions)+' sanction number(s) found:'#13+SancString[1]+#13+
      SancString[2],intont,false,TC,CFG.InformColor,0);
    CurSanction := 0;
    FillChar(oktoprocess,Sanctions,false);
    Anyok := false;
    While CurSanction < Sanctions do begin
      Inc(CurSanction);
      Sanction := SancDigits(Trim(SancTable[CurSanction]));
      oktoprocess[CurSanction] := YesNoBox(esc,ifunc,true,'Ok to process '
        +Sanction+#13+TournName[CurSanction],0,MC,CFG.InformColor);
      esc := false;
      if oktoprocess[CurSanction] then begin
        {x}
        Anyok := true;
      end;
    end;
    if not Anyok then begin
      EraseBoxWindow;
      continue;
    end;
    if Full then SetDistNumber;
    dorep := false;
    dofin := false;
    if not esc and Full and RepDestValid then begin
      fXPrepend := tempdir;
      dofin := ExistFile(fXPrepend+fDBNames[FTinfo_no,0]);
      dofin := dofin and fOpenDBFile(Ftinfo_no)
        and (UsedRecs(fDatF[Ftinfo_no]^) > 0);
      if dofin then begin
        j := PickChoice(4,1,1,'report copy option',@CopyText,MC,false,0,pcchar);
        esc := j < 1;
        dorep := j in [1,2];
        dofin := j in [1,3];
        {if dofin and fOpenDBFile(FInvitem_no)
          and (UsedRecs(fDatF[FInvitem_no]^) < 1) then begin
          RingClear(false);
          dofin := YesNoBox(esc,ifunc,false,TournName[CurSanction]
            +#13'does not have any voucher items.  Process financial'
            +#13'files anyway (Contact Butch or Jim before proceding)',
            0,BC,CFG.InformColor);
        end;}
        fCloseDBFile(FInvitem_no);
      end
      else dorep := YesNoBox(esc,ifunc,true,
        'Copy masterpoint report files to 400 for processing',
        0,BC,CFG.InformColor);
    end;
    EraseBoxWindow;
    if esc then continue;
    CurSanction := 0;
    while CurSanction < Sanctions do begin
      Inc(CurSanction);
      if not oktoprocess[CurSanction] then continue;
      Sanction := SancDigits(Trim(SancTable[CurSanction]));
      if Full and dorep then begin
        FindFirst(tempdir+Sanction+RepSuffix,Archive,Search_Rec);
        if DosError <> 0 then begin
          RingClear(false);
          MsgBox('Report file '+Sanction+RepSuffix+' not found.'#13
            +'Tournament '+Sanction+' not processed.',
            intont,true,MC,CFG.WarnColor,0);
          oktoprocess[CurSanction] := false;
        end;
      end;
      if Full and dofin and oktoprocess[CurSanction] then begin
        TempKey := SancTable[CurSanction];
        FindKey(fIdxKey[Ftinfo_no,1]^,fRecno[Ftinfo_no],TempKey);
        if not ok then begin
          RingClear(false);
          MsgBox('Financial information for '+Sanction+' not found.'#13
            +'Tournament '+Sanction+' not processed.',
            intont,true,MC,CFG.WarnColor,0);
          oktoprocess[CurSanction] := false;
        end;
      end;
    end;
    fCloseFiles;
    if Full then begin
      CurSanction := 0;
      while CurSanction < Sanctions do begin
        Inc(CurSanction);
        if not oktoprocess[CurSanction] then continue;
        Sanction := SancDigits(Trim(SancTable[CurSanction]));
        if dorep then Copyfile(tempdir+Sanction+RepSuffix,
          RepDestLocation+Sanction+RepSuffix,false);
        if dofin then MakeFinReport;
        if PPDestValid then begin
          FindFirst(tempdir+Sanction+PPSuffix,Archive,Search_Rec);
          if DosError = 0 then Copyfile(tempdir+Sanction+PPSuffix,
            PPDestLocation+Sanction+PPSuffix,false);
        end;
        if HtmDestValid then begin
          FindFirst(tempdir+Sanction+HtmSuffix,Archive,Search_Rec);
          if DosError = 0 then Copyfile(tempdir+Sanction+HtmSuffix,
            HtmDestLocation+Sanction+HtmSuffix,false);
        end;
      end;
    end;
    if not Full then begin
      AnyGameFiles := true;
      goto gf_done;
    end;
    CurSanction := 0;
    MsgBox('Ready to process game files'#13'Press any key to continue',
      intont,true,MC,CFG.InformColor,0);
    AnyGameFiles := false;
gf_again:
    AskPath := false;
    if CurSanction < Sanctions then Inc(CurSanction);
    Sanction := SancDigits(Trim(SancTable[CurSanction]));
    repeat
      GetReportPath(Askpath,false);
      if esc then goto gf_done;
      Askpath := true;
      GFname := Sanction+'.LZH';
      errcode := GetFileName(ReportPath+GFMask+ArcSuffix,Archive+Directory,
        20,2,23,1,CFG.pickcolor,GFname,
        ' Select archive to process for '+Sanction);
    until Length(GFname) > 0;
    GameDelName := GFname;
    RunLHA('e /c /m ',GFName,tempdir,' Extracting game files ');
    AnyGameFiles := true;
    if (PathChoice in [1,2]) and YesNoBox(esc,ifunc,
      GFName[Length(GFName)] <> 'H',
      'Any more game file archives to process for '+Sanction,0,MC,
      CFG.WarnColor) then begin
      if not EscBox('Insert the next diskette into drive.'#13
        +'Press any key to continue.',intont,MC,CFG.InformColor) then
        goto gf_again;
    end;
gf_done:
    if AnyGameFiles then esc := false;
    if esc then continue;
    CurSanction := 0;
    while CurSanction < Sanctions do begin
      Inc(CurSanction);
      if not oktoprocess[CurSanction] then continue;
      Sanction := SancDigits(Trim(SancTable[CurSanction]));
      DBFiles := 0;
      SpaceNeeded := 0;
      FindFirst(tempdir+'*.*',Archive,Search_Rec);
      while DosError = 0 do with Search_Rec do begin
        Inc(DBfiles);
        DBFileList[DBFiles] := Name;
        Inc(SpaceNeeded,Size);
        FindNext(Search_Rec);
      end;
      dolzh := SpaceNeeded < MaxLZHSize;
      if dolzh then begin
        RunLHA('a /l2 /w+',tempdir+SancLZH+Sanction,tempdir,
          ' Packing files for '+Sanction);
        Assign(F,tempdir+SancLZH+Sanction+'.LZH');
        {$I-} ReSet(F,1);
        SpaceNeeded := FileSize(F);
        Close(F); {$I+}
        if IOResult = 0 then;
      end;
      if CompareKey(Sanction,NABCSancPref) then path := TournArchive+NABCSancPref
      else if NumSanc(Sanction) then begin
        {$I-} MkDir(TournArchive+Copy(Sanction,1,2)); {$I+}
        if IOResult = 0 then;
        path := TournArchive+Copy(Sanction,1,2)+'\'+Copy(Sanction,3,2);
      end
      else path := OtherLocation;
      {$I-} MkDir(path); {$I+}
      if IOResult = 0 then;
      path := FixPath(path);
      if dolzh then begin
        FindFirst(path+SancLZH+Sanction+'.LZH',Archive,Search_Rec);
        if DosError = 0 then j := 5
        else j := 0;
      end
      else begin
        path := Fixpath(path)+Sanction;
        {$I-} MkDir(path); {$I+}
        j := IOResult;
      end;
      if Full then begin
        if j = 5 then begin
          if not YesNoBox(esc,ifunc,false,
            'Tournament sanction '+Sanction+' has already'#13+
            'been processed.  Process again',0,MC,CFG.WarnColor) then begin
            OktoMerge := false;
            continue;
          end;
        end
        else if j <> 0 then begin
          RingClear(false);
          ErrBox('Cannot access the Tournament location.',MC,0);
          MainHalt;
        end;
      end;
      path := fixpath(path);
      if dolzh then begin
        CopyFile(tempdir+SancLZH+Sanction+'.LZH',
          path+SancLZH+Sanction+'.LZH',true);
        {$I-} Erase(F); {$I+}
        if IOResult = 0 then;
      end
      else begin
        MsgBox('Copying files for sanction '+Sanction,
          intont,false,TC,CFG.InformColor,0);
        for k := 1 to DBfiles do
          CopyFile(tempdir+DBFileList[k],path+DBFileList[k],true);
        MsgBox('Tournament sanction '+Sanction+' processing complete.',
          intont,true,BC,CFG.InformColor,0);
        EraseBoxWindow;
      end;
    end;
    if Full and OktoMerge then MergeDistDB;
    CloseFiles;
    DelTempDirfiles;
    if Full then begin
      if not Empty(DBDelName) then DelFile(DBDelName);
      if not Empty(GameDelName) then DelFile(GameDelName);
    end;
  until not Full or not YesNoBox(esc,ifunc,true,
    'Process another tournament',0,MC,CFG.InformColor);
  MainHalt;
end;

{$I errexit.inc}

begin
  ProgName := 'ACBLUTIL';
  SetUpExit;
  ShowExtension := True;
  ShowSizeDateTime := True;
  FilesUpper := True;
  if Upper(ParamStr(1)) = 'UNPACK' then UnpackTourn(true);
  if Upper(ParamStr(1)) = 'REPACK' then UnpackTourn(false);
  MainHalt;
end.