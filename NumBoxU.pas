unit NumBoxU;
{$define help}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OvcBase, OvcEditF, OvcEdPop, OvcEdClc, OvcEF, OvcPB, OvcNF, pvio;

type
  TNumBoxForm = class(TForm)
    OvcController1: TOvcController;
    OKBut: TButton;
    CanButton: TButton;
    NumBoxEdit: TOvcNumericField;
    Helpbut: TButton;
    Label1: TLabel;
    procedure Acceptit(Sender: TObject);
    procedure CancelIt(Sender: TObject);
    procedure Dohelp(Sender: TObject);
    procedure AfterEnt(Sender: TObject);
    procedure usercommand(Sender: TObject; Command: Word);
    procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
   check_changed : boolean;
   doarr         : boolean;
   funccode  : byte;
   dofunc        : boolean;
  public
    { Public declarations }
  end;

const
     NumBoxYLoc : word = 50;
     NumBoxXLoc : word = 50;
     NumForceInsert : boolean = false;

function NumBox(var esc: boolean; var ifunc: byte; const ival: Integer;
         const title,prompt: String; const min,max: Integer;
         const Iwidth : byte; const topic: word; const location: byte;
         const StProc: TStringProc) : Integer;

function FloatBox(var esc: boolean; var ifunc: byte; const fval: Real48;
         const title,prompt: String; const fmin,fmax: Real48;
         const Iwidth,Places : byte; const topic: word; const location: byte;
         const StProc: TStringProc) : Real48;

implementation

uses
    YesNoBoxU,
    util1,
    csdef,
    StStrL;

{$R *.DFM}

const
  NumBoxForm: TNumBoxForm = nil;
  Canceled  : boolean = false;

{var
   HelpTopic : word;}

procedure SetFormWidth(const title,prompt: String; const Iwidth: integer;
          const topic: word; const location: byte);
var
   MaxTextWidth        : integer;
   MaxTextHeight       : integer;
   w     : integer;

procedure GetTextWidth(Const xLine: String);
var
   Line  : String;
   yline : String;
   cw    : integer;
   j     : integer;
begin
  with NumBoxForm do begin
    w := 0;
    yline := xline;
    repeat
      j := Pos(#13,yline);
      if j < 1 then j := Length(yline)+1;
      if j > 1 then Line := Copy(yline,1,j-1)
      else Line := '';
      yline := Copy(yline,j+1,32000);
      if Length(Line) > 0 then begin
        cw := Canvas.TextWidth(Line);
        if cw > MaxTextWidth then MaxTextWidth := cw;
        cw := Canvas.TextHeight(Line);
        if cw > MaxTextHeight then MaxTextHeight := cw;
      end;
      Inc(w);
    until Length(yLine) < 1;
  end;
end;

begin
  with NumBoxForm do begin
    Font.Size := ViewFontSize;
    MaxTextWidth := 0;
    MaxTextHeight := 10;
    {$ifdef help}
    HelpFile := DefHelpFile;
    HelpContext := topic;
    {$else}
    HelpContext := 0;
    {$endif}
    funccode := 0;
    HelpBut.Visible := HelpContext > 0;
    HelpContext := HelpContext;
    GetTextWidth(title);
    GetTextWidth(prompt);
    ClientHeight := w * MaxTextHeight + 85;
    NumBoxEdit.Top := Height-100;
    OkBut.Top := Height - 60;
    CanButton.Top := OKBut.Top;
    HelpBut.top := OKBut.top;
    if HelpContext > 0 then Width := 230
    else Width := 204;
    if MaxTextWidth > Width-30 then Width := MaxTextWidth+30;
    if location < 1 then begin
      Top := (Screen.Height * NumBoxYLoc) div 100 - (Height div 2);
      if Top < 0 then Top := 0;
      Left := (Screen.Width * NumBoxXLoc) div 100 - (Width div 2);
      if Left < 0 then Left := 0;
    end
    else begin
      Left := SetFormLeft(Width,location);
      Top := SetFormTop(Height,location);
    end;
    Label1.Width := Width-20;
    Okbut.Left := 8;
    Okbut.Left := 8;
    if HelpContext > 0 then begin
      HelpBut.Left := Width-HelpBut.Width-14;
      CanButton.Left := (Width-CanButton.Width) div 2;
    end
    else CanButton.Left := Width-CanButton.Width-14;
    Caption := title;
    Label1.Caption := prompt;
    Label1.Top := 10;
    Label1.Left := 5;
    NumBoxEdit.Width := Canvas.TextWidth(CharStrL('0',Iwidth+1));
    NumBoxEdit.Left := (Width-NumBoxEdit.Width) div 2;
    NumBoxEdit.LabelInfo.OffsetX := 8-NumBoxEdit.Left;
  end;
end;

function NumBox(var esc: boolean; var ifunc: byte; const ival: Integer;
         const title,prompt: String; const min,max: Integer;
         const Iwidth : byte; const topic: word; const location: byte;
         const StProc: TStringProc) : Integer;
var
   Value : integer;
begin
  Canceled := false;
  FreeHintWin;
  if not Assigned(NumBoxForm) then
    Application.CreateForm(TNumBoxForm, NumBoxForm);
  with NumBoxForm do begin
    SetFormWidth(title,prompt,Iwidth,topic,location);
    check_changed := (ifunc and 2) = 2;
    doarr := (ifunc and 1) = 1;
    dofunc := doarr;
    with NumBoxEdit do begin
      DataType := nftLongInt;
      if min < 0 then PictureMask := CharStrL('i',Iwidth)
      else PictureMask := CharStrL('9',Iwidth);
      Value := ival;
      SetValue(Value);
      if NumForceInsert then Options := [efoForceInsert];
    end;
    repeat
      ShowModal;
      esc := canceled;
      if not canceled then begin
        NumBoxEdit.GetValue(Value);
        if (Value < min) or (Value > max) then
          Errbox('Number must be in the range '+Long2StrL(min)+' to '
          +Long2StrL(max),location,0);
      end;
    until esc or ((Value >= min) and (Value <= max));
    Result := Value;
    ifunc := funccode;
    Free;
  end;
  NumForceInsert := false;
  NumBoxForm := nil;
  NumBoxYLoc := 50;
  NumBoxXLoc := 50;
  if not esc and Assigned(StProc) then StProc(prompt+' '+Long2StrL(Result));
end;

function FloatBox(var esc: boolean; var ifunc: byte; const fval: Real48;
         const title,prompt: String; const fmin,fmax: Real48;
         const Iwidth,Places : byte; const topic: word; const location: byte;
         const StProc: TStringProc) : Real48;
var
   Value :     Real48;
begin
  Canceled := false;
  FreeHintWin;
  if not Assigned(NumBoxForm) then
    Application.CreateForm(TNumBoxForm, NumBoxForm);
  with NumBoxForm do begin
    SetFormWidth(title,prompt,Iwidth+1,topic,location);
    check_changed := (ifunc and 2) = 2;
    doarr := (ifunc and 1) = 1;
    dofunc := doarr;
    with NumBoxEdit do begin
      DataType := nftReal;
      if (Places < 1) or (Places > Iwidth-2) then
        PictureMask := CharStrL('#',Iwidth)
      else PictureMask := CharStrL('#',Iwidth-Places-1)+'.'+CharStrL('#',Places);
      Value := fval;
      AsFloat := Value;
      if NumForceInsert then Options := [efoForceInsert];
    end;
    repeat
      ShowModal;
      esc := canceled;
      if not canceled then begin
        Value := NumBoxEdit.AsFloat;
        if (Value < fmin) or (Value > fmax) then
          Errbox('Number must be in the range '+Real2StrL(fmin,Iwidth,Places)
          +' to '+Real2StrL(fmax,Iwidth,Places),location,0);
      end;
    until esc or ((Value >= fmin) and (Value <= fmax));
    Result := Value;
    ifunc := funccode;
    Free;
  end;
  NumForceInsert := false;
  NumBoxForm := nil;
  NumBoxYLoc := 50;
  NumBoxXLoc := 50;
  if not esc and Assigned(StProc) then StProc(prompt+' '+Real2StrL(Result,Iwidth,Places));
end;

procedure TNumBoxForm.Acceptit(Sender: TObject);
begin
  Close;
end;

procedure TNumBoxForm.CancelIt(Sender: TObject);
begin
  Close;
  Canceled := true;
end;

procedure TNumBoxForm.Dohelp(Sender: TObject);
begin
  if HelpContext > 0 then begin
    keybd_event(VK_F1,MapVirtualKey(VK_F1,0),0,0);
  end;
end;

procedure TNumBoxForm.AfterEnt(Sender: TObject);
begin
  if NumForceInsert then NumBoxEdit.Deselect;
end;

procedure TNumBoxForm.usercommand(Sender: TObject; Command: Word);
begin
  if not dofunc then exit;
  Close;
  Funccode := Command-255;
end;

procedure TNumBoxForm.KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if doarr then begin
    funccode := 0;
    case Key of
      VK_LEFT: Funccode := 75;
      VK_UP: Funccode := 72;
      VK_RIGHT: Funccode := 77;
      VK_DOWN: Funccode := 80;
      VK_PRIOR: Funccode := 73;
      VK_NEXT: Funccode := 81;
    end;
    if funccode > 0 then begin
      Key := 0;
      close;
    end;
  end;
  if key = 121 then begin
    key := 0;
    UserCommand(Sender,264);
  end;
end;

procedure TNumBoxForm.KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = '-') and TOvcNumericField(Sender).Modified then
    keybd_event(VK_RETURN,MapVirtualKey(VK_RETURN,0),0,0);
end;

end.
