{.$define test}
unit dohttp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IpUtils, IpSock, IpHttp;

type
  TdohttpForm = class(TForm)
    IpHttpClient1: TIpHttpClient;
    procedure Connected(Sender: TObject; Socket: Cardinal);
    procedure Disconnected(Sender: TObject; Socket: Cardinal);
    procedure Errorexit(Sender: TObject; Socket: Cardinal;
      ErrCode: Integer; const ErrStr: String);
    procedure GotItem(Sender: TObject; Socket: Cardinal;
      const Item: String; Cached: Boolean);
    procedure HttpResult(Sender: TObject; Socket: Cardinal; Code,
      Message: String; HeaderDat: TStream);
    procedure IdleTimeout(Sender: TObject; Socket: Cardinal;
      var Reset: Boolean);
    procedure Itemsent(Sender: TObject; Socket, Handle,
      Remaining: Cardinal);
    procedure Itemstatus(Sender: TObject; Socket: Cardinal;
      Message: String);
    procedure Loaditem(Sender: TObject; Socket: Cardinal;
      const Item: String; Cached: Boolean);
    procedure Readline(Sender: TObject; Socket: Cardinal;
      const Line: String);
    procedure onstatus(Sender: TObject; Socket: Cardinal;
      Event: TIpStatusType; const Connection: TIpConnRec;
      const StatRec: TIpSockStatRec);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure MakeHttpForm;
procedure ClosehttpForm;
function GetHTTP(const xurl,OutName: String; const tm: integer): boolean;
function PostHTTP(const xurl,Header,FName: String; const tm: integer): boolean;

implementation

uses
    pvio,
    StStrL,
    YesNoBoxU;

{$R *.DFM}

const
  dohttpForm: TdohttpForm = nil;

const
   inhttp : boolean = false;
   htimer : integer = 0;
   istimeout : boolean = false;
   isactive : boolean = false;

{$ifdef test}
var
   Logfile : TextFile;
   isop    : boolean;
{$endif}

procedure ClosehttpForm;
begin
  isactive := false;
  if not Assigned(dohttpForm) then exit;
  dohttpForm.Close;
  dohttpForm.Free;
  dohttpForm := nil;
end;

function PostHTTP(const xurl,Header,FName: String; const tm: integer): boolean;
var
   tries : integer;
   ms : TMemoryStream;
   hms : TMemoryStream;
begin
  Result := false;
  if not FileExists(Fname) then exit;
  {$ifdef test}
  AssignFile(Logfile,'test.log');
  ReWrite(LogFile);
  isop := true;
  {$endif}
  ms := TMemoryStream.Create;
  ms.LoadFromFile(FName);
  hms := TMemoryStream.Create;
  hms.Write(Header[1],Length(Header));
//  MsgBox(Fname+' size: '+Long2StrL(ms.Size),mc,0);
  tries := 0;
  MakeHttpForm;
  with dohttpForm.IpHttpClient1 do repeat
    Inc(tries);
    inhttp := true;
    htimer := tm;
    istimeout := false;
    RequestFields.Clear;
    BoxMsg('Sending '+FName,'Sending '+FName+' to '+xurl
      +#13#13'Please wait',MC);
    try
      Post('http://'+xurl,hms);
      Result := PostWait('http://'+xurl,ms);
    except
      Result := false;
    end;
    FreeLink('http://'+xurl);
    EraseBoxWindow;
    htimer := 0;
    inhttp := false;
    DelTempIPFiles;
  until not istimeout or not Result or (tries > 0);
  ms.Destroy;
  {$ifdef test}
  isop := false;
  CloseFile(LogFile);
  {$endif}
end;

function GetHTTP(const xurl,OutName: String; const tm: integer): boolean;
var
   tries : integer;
begin
  tries := 0;
  DeleteFile(OutName);
  MakeHttpForm;
  {$ifdef test}
  AssignFile(Logfile,'test.log');
  ReWrite(LogFile);
  isop := true;
  {$endif}
  with dohttpForm.IpHttpClient1 do repeat
    Inc(tries);
    inhttp := true;
    htimer := tm;
    istimeout := false;
    RequestFields.Clear;
    BoxMsg('Getting '+OutName,'Getting '+OutName+' from '+xurl
      +#13#13'Please wait',MC);
    try
      Download('http://'+xurl,OutName);
      Result := true;
    except
      Result := false;
    end;
    FreeLink('http://'+xurl);
    EraseBoxWindow;
    htimer := 0;
    inhttp := false;
    DelTempIPFiles;
  until not istimeout or not Result or (tries > 0);
  {$ifdef test}
  isop := false;
  CloseFile(LogFile);
  {$endif}
end;

var
   TimerId       : LongWord;

function HttpTimer(Param: Pointer): integer;
begin
  Result := 0;
  while isactive do begin
    Application.ProcessMessages;
    sleep(1000);
    if htimer > 0 then begin
      while htimer > 0 do begin
        Application.ProcessMessages;
        sleep(1000);
        Dec(htimer);
      end;
      if inhttp then begin
        istimeout := true;
        dohttpForm.IpHttpClient1.Cancel;
      end;
    end;
  end;
end;

procedure MakeHttpForm;
begin
  if not Assigned(dohttpForm) then
    Application.CreateForm(TdohttpForm, dohttpForm);
  if not isactive then begin
    isactive := true;
    BeginThread(nil,0,HttpTimer,nil,0,TimerId);
  end;
end;

procedure TdohttpForm.Connected(Sender: TObject; Socket: Cardinal);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'Connected to '+Long2StrL(Socket));
  {$endif}
end;

procedure TdohttpForm.Disconnected(Sender: TObject; Socket: Cardinal);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'Disconnected from '+Long2StrL(Socket));
  {$endif}
end;

procedure TdohttpForm.Errorexit(Sender: TObject; Socket: Cardinal;
  ErrCode: Integer; const ErrStr: String);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'Error '+Long2StrL(ErrCode)+' '+ErrStr);
  {$endif}
end;

procedure TdohttpForm.GotItem(Sender: TObject; Socket: Cardinal;
  const Item: String; Cached: Boolean);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'Got item '+Item);
  {$endif}
end;

procedure TdohttpForm.HttpResult(Sender: TObject; Socket: Cardinal; Code,
  Message: String; HeaderDat: TStream);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'http result: '+code+'; '+Message);
  {$endif}
end;

procedure TdohttpForm.IdleTimeout(Sender: TObject; Socket: Cardinal;
  var Reset: Boolean);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'idle timeout');
  {$endif}
end;

procedure TdohttpForm.Itemsent(Sender: TObject; Socket, Handle,
  Remaining: Cardinal);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'item sent to '+Long2StrL(Socket));
  {$endif}
end;

procedure TdohttpForm.Itemstatus(Sender: TObject; Socket: Cardinal;
  Message: String);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'item status: '+message);
  {$endif}
end;

procedure TdohttpForm.Loaditem(Sender: TObject; Socket: Cardinal;
  const Item: String; Cached: Boolean);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'load item: '+item);
  {$endif}
end;

procedure TdohttpForm.Readline(Sender: TObject; Socket: Cardinal;
  const Line: String);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'read line: '+line);
  {$endif}
end;

procedure TdohttpForm.onstatus(Sender: TObject; Socket: Cardinal;
  Event: TIpStatusType; const Connection: TIpConnRec;
  const StatRec: TIpSockStatRec);
begin
  {$ifdef test}
  if isop then WriteLn(Logfile,'Status: '+Long2StrL(Socket));
  {$endif}
end;

end.
