unit intfixu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm1 = class(TForm)
    procedure Fixint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    YesNoBoxU,
    util2,
    pvio;

{$R *.DFM}

procedure TForm1.Fixint(Sender: TObject);
const
   drives : Array[1..5] of char = ('C','D','E','F','G');
   afolder = ':\ACBLSCOR\';
   nname = 'MISC.LBR';
var
   esc : boolean;
   j   : integer;
   fold : string;
   ts   : TSearchRec;
   found : integer;
begin
  if YesNoBox(esc,true,
    'This procedure will allow ACBLscore to set up the ACBL-wide International'
    +#13'Fund game on February 4, 2006.  Click YES to proceed.'
    ,0,MC,nil) then begin
    found := FindFirst(nname,faAnyFile,ts);
    if found <> 0 then begin
      ErrBox('Required file '+nname+' not found.  No action taken.',MC,0);
        Application.Terminate;
        exit;
    end;
    for j := 1 to 5 do begin
      if (j > 1) and not YesNoBox(esc,true,
        'Should I look for ACBLscore on drive '+drives[j]+':',0,MC,nil) then
        continue;
      fold := drives[j]+afolder+nname;
      found := FindFirst(fold,faAnyFile,ts);
      FindClose(ts);
      if found = 0 then begin
        found := PCopyFile(nname,fold,false,esc,false);
        if found = 0 then ErrBox(
          'ACBLscore has been update to allow the ACBL-wide international fund'
          +#13'game on February 4, 2006',MC,0)
        else Errbox('Could not update ACBLscore',MC,0);
        Application.Terminate;
        exit;
      end;
    end;
    ErrBox('ACBLscore could not be found.  No action taken.',MC,0);
  end
  else ErrBox('No action taken.',MC,0);
  Application.Terminate;
end;

end.
